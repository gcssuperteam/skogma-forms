﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsSkogma
{
    public class Artikel : IDisposable
    {
        Garp.Application GarpArtikel = null;
        Garp.IComponents GarpCompArtikel = null;

        Garp.Dataset dsAGA;
        Garp.IDatasetField DS_AGA_ArtikelNr;
        Garp.IComponent
            LabelVer
            , ArtikelNr
            ;

        Garp.ITable tblAGA, tblAGB;
        Garp.ITabField fld_AGA_ArtikelNr;

        Garp.IComponent iPIMbutton;

        bool blocked = false;
        bool m_disposed = false;

        public Artikel()
        {
            try
            {
                GarpArtikel = new Garp.Application();

                GarpCompArtikel = GarpArtikel.Components;
                ArtikelNr = GarpCompArtikel.Item("anrEdit");

                // Visa versionsnummer
                GarpCompArtikel.BaseComponent = "Panel2";
                LabelVer = GarpCompArtikel.AddLabel("Version");
                LabelVer.Top = 10;
                LabelVer.Left = 570;
                LabelVer.Width = 100;
                LabelVer.Text = "Forms version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build;

                // Hämta datakällan för Artikeltabell och skapa händelse för inläsning ny post
                dsAGA = GarpArtikel.Datasets.Item("agaMcDataSet");
                dsAGA.AfterScroll += AfterScroll_AGA;
                dsAGA.BeforePost += BeforePost_AGA;
                dsAGA.BeforeDelete += BeforeDelete_AGA;
                GarpArtikel.FieldExit += FieldExit;
                GarpArtikel.FieldEnter += FieldEnter;
                GarpArtikel.ButtonClick += ButtonClick;

                //Hämta tabeller
                tblAGA = GarpArtikel.Tables.Item("AGA");
                tblAGB = GarpArtikel.Tables.Item("AGB");

                fld_AGA_ArtikelNr = tblAGA.Fields.Item("ANR");

                //Data-fält
                DS_AGA_ArtikelNr = dsAGA.Fields.Item("ANR");

                CreateLabelsAndButtons();
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning artikel." + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #region Init

        private void CreateLabelsAndButtons()
        {
            try
            {
                GarpCompArtikel.BaseComponent = "TabSheet1";

                if (iPIMbutton == null)
                {
                    iPIMbutton = GarpCompArtikel.AddButton("iPIMbutton");
                    iPIMbutton.Top = GarpCompArtikel.Item("exP2Edit").Top + 26;
                    iPIMbutton.Left = GarpCompArtikel.Item("exP2Edit").Left;
                    iPIMbutton.Width = GarpCompArtikel.Item("exP2Edit").Width;
                    //iPIMbutton.Top = 205;
                    //iPIMbutton.Left = 355;
                    //iPIMbutton.Width = 70;
                    iPIMbutton.Height = 40;
                    iPIMbutton.Text = "Synka till" + Environment.NewLine + "iPIM";
                    iPIMbutton.TabStop = false;
                    iPIMbutton.Visible = true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i artikel - skapa knappar." + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #region Events

        private void ButtonClick()
        {
            try
            {
                if (GarpCompArtikel.CurrentField == "iPIMbutton")
                {
                    DialogResult answer = MessageBox.Show("Vill du synka artikeln till iPIM?", "iPIM", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (answer == DialogResult.Yes)
                    {
                        string response = CalliPIM();
                        int intRes = getIntFromStr(response);

                        if (intRes >= 200 && intRes <= 299)
                        {
                            MessageBox.Show("Synkning lyckad!", "iPIM", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Synkning misslyckad!", "iPIM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i artikel - ButtonClick" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        

        private void FieldEnter()
        {
            try
            {
                
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i artikel - FieldEnter" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void FieldExit()
        {
            try
            {

            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i artikel - FieldExit" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void BeforeDelete_AGA()
        {
            try
            {

            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i artikel - BeforeDelete_AGA" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void BeforePost_AGA()
        {
            try
            {

            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i artikel - BeforePost_AGA" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void AfterScroll_AGA()
        {
            try
            {

            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i artikel - AfterScroll_AGA" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #region Functions

        private string CalliPIM()
        {
            string result = "ERROR";

            try
            {
                //string rest = "http://localhost:55565/iPimSvc/REST/SyncSingleProduct/100150001001";
                string baseAddress = ConfigurationManager.AppSettings["iPimConnectorBaseAddress"] ?? "";
                string productNo = ArtikelNr.Text;
                string rest = baseAddress + "/REST/SyncSingleProduct/" + productNo;

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                result = responseStream.ReadToEnd();
                responseStream.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning artikel - CalliPIM" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return result;
        }

        #endregion

        #region Helper functions

        public int getIntFromStr(string value)
        {
            int result = 0;

            try
            {
                if (!int.TryParse(value, out result))
                {
                    result = 0;
                }
            }
            catch (Exception e)
            {
                
            }

            return result;
        }

        #endregion

        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName + "Forms artikel", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void Dispose(bool Disposing)
        {
            try
            {
                if (!m_disposed)
                {
                    if (Disposing)
                    {
                        m_disposed = true;
                        if (GarpArtikel != null)
                        {
                            GarpArtikel.ButtonClick -= ButtonClick;
                            GarpArtikel.FieldEnter -= FieldEnter;
                            GarpArtikel.FieldExit -= FieldExit;
                            dsAGA.BeforeDelete -= BeforeDelete_AGA;
                            dsAGA.BeforePost -= BeforePost_AGA;
                            dsAGA.AfterScroll -= AfterScroll_AGA;

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAGB);

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsAGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(GarpArtikel);
                        }
                    }
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName + "Forms artikel", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        ~Artikel()
        {
            Dispose(false);
        }
    }
}
