﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsSkogma
{
    public class Inkop : IDisposable
    {
        
        Garp.Application GarpPurchase = null;
        Garp.IComponents ComponentPurchase = null;

        Garp.Dataset dsIGA, dsIGR;

        Garp.IComponent LabelVer, CheckKontrollager;

        bool m_disposed = false;

        public Inkop()
        {
            try
            {
                GarpPurchase = new Garp.Application();

                ComponentPurchase = GarpPurchase.Components;

                dsIGA = GarpPurchase.Datasets.Item("igaMcDataSet");
                dsIGR = GarpPurchase.Datasets.Item("igrMcDataSet");

                dsIGA.AfterScroll += AfterScrollIGA;
                dsIGR.AfterScroll += AfterScrollIGR;
                dsIGA.BeforePost += BeforePostIGA;
                dsIGR.BeforePost += BeforePostIGR;
                dsIGA.BeforeDelete += BeforeDeleteIGA;
                dsIGR.BeforeDelete += BeforeDeleteIGR;

                GarpPurchase.ButtonClick += ButtonClik;
                GarpPurchase.FieldEnter += FieldEnter;
                GarpPurchase.FieldExit += FieldExit;

                CheckKontrollager = GarpPurchase.Components.Item("klaCheckBox");

                SetUpOH();
                SetUpOR();
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning inköp." + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , "Forms" + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void SetUpOH()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fel vid initiering orderhuvud " + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void SetUpOR()
        {
            try
            {
                ComponentPurchase.BaseComponent = "TabSheet3";

                // Visa versionsnummer
                if (LabelVer == null)
                {
                    LabelVer = ComponentPurchase.AddLabel("LblVer");
                    LabelVer.Top = 300;
                    LabelVer.Left = 8;
                    LabelVer.Width = 40;
                    LabelVer.Text = "Skogma, Forms version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fel vid initiering " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        public void SetCheckFlag()
        {
            try
            {
                CheckKontrollager.Checked = false;
                if (!string.IsNullOrEmpty(dsIGR.Fields.Item("LAG").Value))
                {
                    if (dsIGR.Fields.Item("LAG").Value == "1" && dsIGA.Fields.Item("OTY").Value != "4" && dsIGR.Fields.Item("KLA").Value != "1")
                    {
                        CheckKontrollager.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fel vid test av kontrollager-flagga " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void AfterScrollIGA()
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show("After scroll IGA " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void AfterScrollIGR()
        {
            try
            {
                if (string.IsNullOrEmpty(dsIGR.Fields.Item("LVF").Value) || (!string.IsNullOrEmpty(dsIGR.Fields.Item("LVF").Value) && int.Parse(dsIGR.Fields.Item("LVF").Value) < 5))
                {
                    SetCheckFlag();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("After scroll IGR " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void ButtonClik()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("Button klick " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void FieldEnter()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("Field Enter " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void FieldExit()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("Field Exit " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void BeforePostIGA()
        {
            try
            {



            }
            catch (Exception ex)
            {
                MessageBox.Show("BeforePost IGA " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void BeforePostIGR()
        {
            try
            {


            }
            catch (Exception ex)
            {
                MessageBox.Show("Before post IGR " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void BeforeDeleteIGA()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("Before delete IGA " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void BeforeDeleteIGR()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("Before delete IGR " + ex.Message, "Forms", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        // Stäng Garp-kopplingen
        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Kan ej stänga Garpkopplingen " + ex.Message, "Forms inköp", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void Dispose(bool Disposing)
        {
            try
            {
                if (!m_disposed)
                {
                    if (Disposing)
                    {
                        m_disposed = true;
                        if (GarpPurchase != null)
                        {
                            GarpPurchase.FieldExit -= FieldExit;
                            GarpPurchase.FieldEnter -= FieldEnter;
                            GarpPurchase.ButtonClick -= ButtonClik;
                            dsIGA.BeforeDelete -= BeforeDeleteIGR;
                            dsIGA.BeforeDelete -= BeforeDeleteIGA;
                            dsIGA.BeforePost -= BeforePostIGA;
                            dsIGR.BeforePost -= BeforePostIGR;
                            dsIGR.AfterScroll -= AfterScrollIGR;
                            dsIGA.AfterScroll -= AfterScrollIGA;

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsIGR);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsIGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(GarpPurchase);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Dispose " + ex.Message, "Forms inköp", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        ~Inkop()
        {
            Dispose(false);
        }
    }
}
