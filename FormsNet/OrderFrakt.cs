﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsSkogma
{
    public class OrderFrakt
    {
        // OM FUNTIONEN FÖR FRAKTBERÄKNING SKALL VARA AKTIVERAD!!!

        //--------------------------------------
        public bool BeraknaFrakt = false;
        //--------------------------------------


        // Fraktberäkning
        Garp.Application GarpOrderFrakt = new Garp.Application();
        Garp.ITable tbl_ArkivHuvud, tbl_ArkivRad, tbl_ArkivText;
        Garp.ITabField fld_ArkivHuvud_ArtikelNr, fld_ArkivHuvud_Huvudnr1, fld_ArkivHuvud_Huvudnr2, fld_ArkivHuvud_Huvudnr3, fld_ArkivHuvud_Textnr1, fld_ArkivHuvud_Textnr2;
        Garp.ITabField fld_ArkivRad_Huvudnr1, fld_ArkivRad_Huvudnr2, fld_ArkivRad_Huvudnr3, fld_ArkivRad_Textnr1, fld_ArkivRad_Textnr2, fld_ArkivRad_Text;
        Garp.ITabField fld_ArkivText_Huvudnr1, fld_ArkivText_Huvudnr2, fld_ArkivText_Huvudnr3, fld_ArkivText_Textnr1, fld_ArkivText_Textnr2, fld_ArkivText_Text;



        public string FraktRad { get; set; }
        public string FraktArtikel = "F";
        public string FraktArtikelAF = "F NF"; // Återförsäljare
        public string HuvudKey { get; set; }
        public string RadKey { get; set; }
        public string TextKey { get; set; }
        public double FraktPris { get; set; }
        public bool TestVikt { get; set; }
        public bool TestPris { get; set; }
        public bool FraktAF { get; set; }

        public string ButtonFraktPris = "Shippingprice";

        public List<Tuple<double, double>> ViktLista;
        public List<Tuple<double, double>> ViktListaAF;

        public OrderFrakt()
        {
            try
            {
                if (BeraknaFrakt)
                {
                    //Hämta tabeller
                    tbl_ArkivHuvud = GarpOrderFrakt.Tables.Item("I2H");
                    tbl_ArkivRad = GarpOrderFrakt.Tables.Item("I2R");
                    tbl_ArkivText = GarpOrderFrakt.Tables.Item("I2T");

                    fld_ArkivHuvud_ArtikelNr = tbl_ArkivHuvud.Fields.Item("NYC");
                    fld_ArkivHuvud_Huvudnr1 = tbl_ArkivHuvud.Fields.Item("HN1");
                    fld_ArkivHuvud_Huvudnr2 = tbl_ArkivHuvud.Fields.Item("HN2");
                    fld_ArkivHuvud_Huvudnr3 = tbl_ArkivHuvud.Fields.Item("HN3");
                    fld_ArkivHuvud_Textnr1 = tbl_ArkivHuvud.Fields.Item("TN1");
                    fld_ArkivHuvud_Textnr2 = tbl_ArkivHuvud.Fields.Item("TN2");

                    fld_ArkivRad_Huvudnr1 = tbl_ArkivRad.Fields.Item("HN1");
                    fld_ArkivRad_Huvudnr2 = tbl_ArkivRad.Fields.Item("HN2");
                    fld_ArkivRad_Huvudnr3 = tbl_ArkivRad.Fields.Item("HN3");
                    fld_ArkivRad_Textnr1 = tbl_ArkivRad.Fields.Item("TN1");
                    fld_ArkivRad_Textnr2 = tbl_ArkivRad.Fields.Item("TN2");
                    fld_ArkivRad_Text = tbl_ArkivRad.Fields.Item("TXT");

                    fld_ArkivText_Huvudnr1 = tbl_ArkivText.Fields.Item("HN1");
                    fld_ArkivText_Huvudnr2 = tbl_ArkivText.Fields.Item("HN2");
                    fld_ArkivText_Huvudnr3 = tbl_ArkivText.Fields.Item("HN3");
                    fld_ArkivText_Textnr1 = tbl_ArkivText.Fields.Item("TN1");
                    fld_ArkivText_Textnr2 = tbl_ArkivText.Fields.Item("TN2");
                    fld_ArkivText_Text = tbl_ArkivText.Fields.Item("BUF");

                    //ViktLista = new List<Tuple<double, double>>();

                    HuvudKey = "C30" + FraktArtikel;
                    ViktLista = FraktLista(HuvudKey);

                    HuvudKey = "C30" + FraktArtikelAF;
                    ViktListaAF = FraktLista(HuvudKey);
                }
                GarpOrderFrakt = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Hämta lista för fraktberäkning." + Environment.NewLine + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private List<Tuple<double, double>> FraktLista(string HuvudKey)
        {
            List<Tuple<double, double>> lista = new List<Tuple<double, double>>();

            double ListVikt = 0;
            double ListPris = 0;

            // Hämta ArkivHuvud för aktuell artikel
            tbl_ArkivHuvud.Find(HuvudKey);

            // Matcha ArkivHuvud med ArkivRad
            RadKey = fld_ArkivHuvud_Huvudnr1.Value + fld_ArkivHuvud_Huvudnr2.Value + fld_ArkivHuvud_Huvudnr3.Value;
            tbl_ArkivRad.Find(RadKey);
            tbl_ArkivRad.Next();

            // Loopa ArkivRader för aktuellt ArkivHuvud (artikel)
            while (tbl_ArkivRad.Eof != true && fld_ArkivRad_Huvudnr1.Value + fld_ArkivRad_Huvudnr2.Value + fld_ArkivRad_Huvudnr3.Value == RadKey)
            {
                // Matcha ArkivRad med ArkivText
                TextKey = fld_ArkivRad_Huvudnr1.Value + fld_ArkivRad_Huvudnr2.Value + fld_ArkivRad_Huvudnr3.Value;
                tbl_ArkivText.Find(TextKey);
                tbl_ArkivText.Next();

                // Loopa ArkivTexter hämta alla med rätt språkkod
                while (tbl_ArkivText.Eof != true && fld_ArkivText_Huvudnr1.Value + fld_ArkivText_Huvudnr2.Value + fld_ArkivText_Huvudnr3.Value == TextKey)
                {
                    if (fld_ArkivRad_Textnr2.Value == fld_ArkivText_Textnr2.Value && fld_ArkivRad_Text.Value == " S")
                    {
                        string[] cells = fld_ArkivText_Text.Value.Split(';');
                        if (cells[0].Length > 0)
                        {
                            TestVikt = Double.TryParse(cells[0], out ListVikt);
                            TestPris = Double.TryParse(cells[1], out ListPris);
                            if (TestVikt && TestPris)
                            {
                                lista.Add(Tuple.Create(ListVikt, ListPris));
                            }
                        }
                    }
                    tbl_ArkivText.Next();
                }
                tbl_ArkivRad.Next();
            }

            return lista;
        }

    }
}
