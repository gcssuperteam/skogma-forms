﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormsSkogma
{
    public class OrderPaketTB
    {
        // Struktur
        public string PaketArtikel { get; set; }
        public double PrisIn { get; set; }
        public double PrisUt { get; set; }
        public double SummaIn { get; set; }
        public double SummaUt { get; set; }
        public double TBProcent { get; set; }

        // fält för visning TB
        public string LblPaketTB_Header = "PaketTBHeader";
        public string LblPaketTB1 = "PaketTB1";
        public string LblPaketTB2 = "PaketTB2";

    }
}
