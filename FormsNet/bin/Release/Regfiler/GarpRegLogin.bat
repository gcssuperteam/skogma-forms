@echo off

echo -------------- S�tt s�kv�gar och filnam p� dll-fil --------------------
set PathServer=\\server1\c$\garp\anpassningar\Forms\Regfiler\
set PathLokal=C:\Garp\
set FileDll=FormsSkogma.dll
Rem -----------------------------------------------------------------------

if not exist %PathLokal% (mkdir %PathLokal%)

echo ---- Avregistrera dll-filer ----
c:\windows\microsoft.net\framework\v4.0.30319\regasm /u "%PathLokal%%FileDll%"

echo ---- H�mtar senaste dll-filen ----
echo cscript %PathServer%MoveFile.vbs "%PathServer%" "%PathLokal%" "%FileDll%"
cscript %PathServer%MoveFile.vbs "%PathServer%" "%PathLokal%" "%FileDll%"

echo ---- Registrera dll-filer "%PathLokal%%FileDll%" ----
c:\windows\microsoft.net\framework\v4.0.30319\regasm /codebase "%PathLokal%%FileDll%"

echo Done!

pause