﻿W100FB010 Bolagsnivå
W100FUxxxx Användarnivå

100 = Kundformuläret
200 = Orderformuläret
210 = Butik
400 = Inköp

FormsSkogma.Kund
FormsSkogma.Order
FormsSkogma.Butik
FormsSkogma.Inkop

Hos klienten kör som administratör: (På Garpservern)


-------------------------------
Manuell registrering:
Kopiera och registrera filen FormsSkogma.dll till en lokal mapp hos klienten
Registrera med Kommandotolken som körs som administratör.
C:\Windows\Microsoft.NET\Framework\v4.0.30319\regasm /codebase "C:\Temp\FormsSkogma.dll"
Avregistrera:
C:\Windows\Microsoft.NET\Framework\v4.0.30319\regasm /u "C:\Temp\FormsSkogma.dll"
------------------------------
Version 4.16.1
(2022-05-18)
Förstorat upp komponenterna lite och justerat positioner nedåt.

Version 4.16
(2022-05-13)
Fix för placering av komponenter på orderformuläret.

Version 4.15
Fix för problem där lagernummer inte följer med vid radsplitt vid otillräckligt lagersaldo.

Version 4.14
Fix för att antal och priser blir 0 vid ändring
Justering så att internordrar med ordertyp 9 får OB- och PL-flaggorna automatiskt ställda till 1 om de är 0 så att Klar-knappen inte ska behöva tryckas på

Version 4.13
Justering för saldoberäkning order, innan kunde en order få tre leveransdatum

Version 4.12
Leveranstidsberäkning sker inte på produktionsplanerade kundorder som inte är slutrapporterade
Ingående rader i paket får lagernummer 99 om paketet har LgrNr 99. Om man går ur fältet artikelnummer

Version 4.11
Byte av lagernummer enligt artikelns kod 3. Paketrader får samma lagernummer som paketartiken.
Lagt in extra test om värden är "null" eller "blankt" för order, kund och inköp

Version 4.10
 - Städat i koden, hantering av reservationskod. Mer frisläppning vid stängning av formuläret (order, inköp, kund och butik)

Version 4.9
- Stafflingsuppgifter, antal och pris, visas på artikelkortet om det finns att visa.

Version 4.8
- Fix för lagerplats, om en lagerplats ligger på artikeln.
- Fix för leveransdatum om reservationskoden är 2 på orderradsnivå men 1 på orderhuvudsnivå.

Version 4.7
Manuellt ändrad radrabatt följer med till splittad orderrad

Version 4.6
Orderrader på kunder utan stafflingspris eller kampanjprislista kunde få 0 i a-pris
Om orderrad splittas följer priset med från ursprungsraden
Priset för staffling beräknas bara om man gjort ändring i antalsfältet
Ny textrad på splittade orderrader

Version 4.5
Prissättningen på order hanterar stafflade priser från kampanjprislista

Version 4.4
Justering tester av leveranstid, raden efter paketslut blev felaktigt uppdaterad
Läsning textrader är anpassad för tomma textrader

Version 4.3
Extra nollställning av kalylerat pris med moms för förskott
Ändrat hantering av orderrader från matris för att säkerhetsställa att lagernumret byts från artikelns kodfält.
Ändrat beräkningen av förskottet. Procentsatsen är nu mer exakt plus att den justerar inte beloppet om man inte ändrar procentsatsen manuellt

Version 4.2
ER och Vår referens följer nu med från varuordern till förskottsordern
Ny parameter i configfilen
Tar inte med paketrader till förskottssorderns texter eller rader med 0 i pris och antal
Test att artikelnummer finns innnan saldotestet sker

Version 4.1
Forms inlagt för inköp
Till kontrollager uppdateras på inköpsraderna

Version 3.25
Förskott tar nu hänsyn till om ordern är momsfritt

Version 3.24
Lagernummer hämtas från artikelns kod och läggs på orderraden. Om Kod3 = "D" sätts lagernumret till 99
Upp till 4 olika lagernummer på raderna sparas till orderhuvudet för information. Används även till automatleveransen
Förskottshantering inlagt på order
Saldotest sker mot radens lagernummer. Om blankt lagernummer testas disponibelt på lager 1
Ny inställning i config-fil för om Förskottshantering skall vara aktiverad

Version 3.23
Visning av senaste kreditkontroll på order, hämtas från kunden och fältet Num3

Version 3.22
Test av paket använder inte artikeln SP längre

Version 3.21
Splittning av orderrad på paket lägger till nya raden efter paketet med ingående paketartiklar. Rapport körs med orderfil och uppdatering sker m.h.a. EDI Orderinläsning via fil
Städning i kod, optimering av order
Ny configfil att lägga i clientmappen, inställningar är flyttade från koden till filen

Version 3.20
Önskad leveranstid på internorder
Inforamtion visas på order om fakturan är förbetald via Klarna

Version 3.19
Information visas om plocksedel är utskriven
Information om automatleveransens status visas
Nytt formulär för utleverans enligt status från automatleverans
Ändrat villkor för leveranstid idag från N-kunder till Kund kod 5

Version 3.18
Ändrat hantering av orderunik adress på ny order
Ingen reservationskod på ny order
Splitt av orderrad sker automatiskt om reservationskoden är blank

Version 3.17
Justerat hämtning av kundens mobilnummer

Version 3.16
Test av leveranstid fungerar nu även för paket

Version 3.15
Tagit bort alla "F5" vid saldotest från ändring reservationskod

Version 3.14
Tagit bort en "F5"

Version 3.13
De rader som har skapats från en matris och kan dellevereras presenteras i en dialogruta. Om man svarar ja läggs nya rader på de som kan dellevereras.

Version 3.12
Lagt in villkor för rader utan reservationskod.
OM den framräknade leveranstiden är samma som den önskade flyttas inte datumet fram för den raden.
Rad som inte kan levereras enligt önskat datum får gemensam (med de som har reservationskod blank) sämsta leveranstid

Version 3.11
Kundformuläret. Fält för mallkund flyttat till höger om förnamn
Orderformuläret. Ändrat villkor för att dölja säsong på orderrad
Leveranstid på dellevererad rad kollar nu på kundnummer och leveranssätt om leverans kan ske idag

Version 3.10
Felmeddelanden när man stänger formuläret skall nu vara borta

Version 3.9
Ny order slår ihop för och efternamn
Säsong släcks på orderrad
Förnamnet visas nu rätt när man bläddrar mellan kunder
Fråga dyker upp om man vill splitta orderraden om delleverans är möjlig

Version 3.8
Kundfältet extra adress är flyttat till övre delen av kundformuläret och döpt till Förnamn

Version 3.7
Reservationskoden sattes ej om man skapade ny order via plus-knappen.
Reservationskoden kunde sättas på order med orderrader det görs ej nu.
Hämtningen av mobilnumret kunde ske till fel order.

Version 3.6
Ändrat vid ny order, funktion för hämtning av mobilnummer från kund och förvalt reservationskod 1.

Version 3.5
Rättat bugg, kontaktpersonen hämtades ej in till ordern

Version 3.4
Dialogbox borttagen som visade orderns alla artiklar
Ändring reservationskod på dellevererade order fungerade inte.
Lagt in mer tester innan saldotest om ordern är levererad.

Version 3.3
Knappen justera leveranstider gör nu saldotest på alla rader (tidigare gav den bara gemensam leveranstid)
Ändring reservationskod på orderhuvudet gör nu saldotest på alla rader.

Saldotesten för alla rader ger de med reservationskod 1 gemensam leveranstid (sämsta)
Övriga rader som kan levereras enligt orderhuvudets leveransdatum får det datumet, rader som ej kan levereras (ej reskod 1) får gemensam leveranstid (sämsta)

Leveranssätt 05 (Hämtas) ändrar orderhuvudets och önskad leveranstid till idag.

Fokus på antalsfältet borttagen, saldoberäkningen kördes inte på raden.

Version 3.2
På ny orderrad blir nu fokus på antal efter artikelnumret
Önskad leveranstid och knappen för justering leveranstider visas även på dellevererade order

Version 3.1
Saldotest på butiksorder

Version 2.1
När offert blir order uppdateras orderdatumet till nu och leveranstiden uppdateras till idag eller imorgon
Funktioner för fraktberäkning, EJ aktiverad!
Visar TB i procent för paket
Reservationskod blir förvalt 1 på nya order, dock ej Nordforest-kund
Mobilnumret hämtas från kund
Likvärdiga artiklar visas för sortimentsartiklar
Om datum finns på textfält 9 på artikeln gäller det som ledtid
Orderradens leveransdatum kan ej sättas före önskad leveranstid

Version 2.0
Forms för order
Nytt fält för önskad leveranstid, värdet sparas i tabellen OGC
Förvalt leveransdatum idag eller imorgon
Saldotest på orderrad när man angett antal eller datum, nytt datum anges om saldobrist finns inom artikelns ledtid
Vid registrering paket görs saldotest på paketartikeln och paketrader och sätter gemensam leveranstid på paketet
Order skapad från offert, leveranstid orderhuvud eller önskad leveranstid ändrad -> saldotest görs på alla rader, rader med reservationskod 1 får gemensam leveranstid (sämsta)
Leveranstid på orderhuvud kan ej anges före önskad leveranstid
Ny knapp för test av leveranstider på alla orderrader

Version 1.0
Spärr att redigera och radera kund enligt tabell 9SPKUND
Kundlista skapas med 9SPKUND001, 9SPKUND002 osv.
Kundnumret anges i Textfält 1

Redigering tillåten för användare SYS