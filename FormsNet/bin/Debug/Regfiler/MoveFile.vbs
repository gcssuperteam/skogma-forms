' -------- Hämta dll-filer från servern --------

PathServer = WScript.Arguments.Item("0")
PathLokal = WScript.Arguments.Item("1")
FileDll = WScript.Arguments.Item("2")

Set fso = CreateObject("Scripting.FileSystemObject")
If (fso.FileExists(PathLokal & FileDll)) Then
	fso.DeleteFile PathLokal & FileDll 
End If 
fso.CopyFile PathServer & FileDll, PathLokal
