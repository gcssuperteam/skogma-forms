﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormsSkogma
{
    public class Matrisrader
    {
        public string RadNr { get; set; }
        public string ArtNr { get; set; }
        public string Datum { get; set; }
        public string NyttDatum { get; set; }
        public double Antal { get; set; }
        public double NyRadAntal { get; set; }
        public string Text { get; set; }
        public string LagerNr { get; set; }
        public bool Matrisrad { get; set; }
    }
}
