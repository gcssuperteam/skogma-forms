﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsSkogma
{
    public partial class AdvancePayment : Form
    {
        public string InputValue { get; set; }
        public string ValueExMoms { get; set; }
        public string ValueMoms { get; set; }
        public double OutputValueExMoms { get; set; }
        public string TmpPercent { get; set; }

        public string inputBelopp
        {
            get { return Belopp.Text; }
        }

        public AdvancePayment()
        {
            InitializeComponent();
        }

        private void AdvancePayment_Load(object sender, EventArgs e)
        {
            Belopp.Text = InputValue;
            Procent.Text = "100";
            RestBeloppExMoms.Text = "0";
            RestBeloppInkMoms.Text = "0";
        }

        private void Belopp_Leave(object sender, EventArgs e)
        {
            Belopp.Text = Belopp.Text.Replace(".", ",");
            if (double.TryParse(Belopp.Text, out double value) && double.Parse(Belopp.Text) != 0 && double.Parse(Belopp.Text) <= double.Parse(InputValue.Replace(".", ",")))
            {
                double belopp = double.Parse(Belopp.Text);                       // värdet från beloppsfältet (inmatat), inkl moms
                double orderBelopp = double.Parse(InputValue.Replace(".", ",")); // ordens totala belopp inkl moms
                double procent = (belopp / orderBelopp) * 100;                   // räkna ut procentsatsen från valt belopp
                double RestInkmoms = orderBelopp - belopp;                       // restbeloppet inkl moms
                double momsBelopp = double.Parse(ValueMoms) * procent / 100;     // momsbeloppet, orderns totala momsbelopp x procentsatsen
                double RestMoms = double.Parse(ValueMoms) - momsBelopp;          // rest moms, orderns totala momsbelop - momsbeloppet
                double RestExmoms = RestInkmoms - RestMoms;                      // restbelopp exkl moms
                OutputValueExMoms = belopp - momsBelopp;                         // förskottsbelopp att debitera

                // Visa nya värden i formuläret
                Procent.Text = Math.Round(procent, 2).ToString();
                RestBeloppExMoms.Text = Math.Round(RestExmoms, 2).ToString();
                RestBeloppInkMoms.Text = Math.Round(RestInkmoms, 2).ToString();
            }
            else
            {
                Belopp.Focus();
            }
        }

        private void Procent_Enter(object sender, EventArgs e)
        {
            TmpPercent = Procent.Text;
        }

        private void Procent_Leave(object sender, EventArgs e)
        {
            // Ändra inga belopp om inte procentsatsen är orörd
            if (TmpPercent != Procent.Text)
            {
                Procent.Text = Procent.Text.Replace(".", ",");
                if (double.TryParse(Procent.Text, out double value) && double.Parse(Procent.Text) != 0 && double.Parse(Procent.Text) >= 0 && double.Parse(Procent.Text) <= 100)
                {
                    double procent = double.Parse(Procent.Text);
                    double orderBelopp = double.Parse(InputValue.Replace(".", ","));
                    double belopp = Math.Round((procent / 100) * orderBelopp, 2);
                    double RestInkmoms = orderBelopp - belopp;
                    double momsBelopp = double.Parse(ValueMoms) * procent / 100;
                    double RestMoms = double.Parse(ValueMoms) - momsBelopp;
                    double RestExkmoms = RestInkmoms - RestMoms;
                    OutputValueExMoms = belopp - momsBelopp;

                    // Visa nya värden i formuläret
                    Belopp.Text = belopp.ToString();
                    RestBeloppExMoms.Text = Math.Round(RestExkmoms, 2).ToString();
                    RestBeloppInkMoms.Text = Math.Round(RestInkmoms, 2).ToString();
                }
                else
                {
                    Procent.Focus();
                }
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                    SendKeys.Send("{TAB}");
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            return;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Belopp.Text = "";
            Close();
        }

        private void AdvPay_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            if (DialogResult == DialogResult.Cancel)
            {
                Belopp.Text = "";
            }
            else
            {
                return;
            }
        }
    }
}
