﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormsSkogma
{
    public class AdvanceOrderRow
    {
        //Artikelnummer, benämningar och antal
        public string Item { get; set; }
        public string Name { get; set; }
        public string Qty { get; set; }
        public string Unit { get; set; }
        public string VATcode { get; set; }
    }
}
