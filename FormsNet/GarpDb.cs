﻿using Garp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormsSkogma;

namespace GarpDb
{
    public class Customer
    {
        public ITable Table { get; }
        private ITabField KNR, KD5;
        /// <summary>
        /// KNR
        /// </summary>
        public string OrderNo { get { return KNR.Value; } set { KNR.Value = value; } }
        /// <summary>
        /// KD5
        /// </summary>
        public string Code5 { get { return KD5.Value; } set { KD5.Value = value; } }
        public Customer(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    return;
                }

                Table = garp.Tables.Item("KA");
                KNR = Table.Fields.Item("KNR");
                KD5 = Table.Fields.Item("KD5");
            }
            catch (Exception)
            {
            }
        }

        ~Customer()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }
    public class CustomerOrderFreight
    {
        public ITable Table { get; }
        private ITabField ONR, CH6;
        /// <summary>
        /// ONR
        /// </summary>
        public string OrderNo { get { return ONR.Value; } set { ONR.Value = value; } }
        /// <summary>
        /// CH6
        /// </summary>
        public string Freight06 { get { return CH6.Value; } set { CH6.Value = value; } }
        public CustomerOrderFreight(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    return;
                }

                Table = garp.Tables.Item("OGC");
                ONR = Table.Fields.Item("ONR");
                CH6 = Table.Fields.Item("CH6");
            }
            catch (Exception)
            {
            }
        }

        ~CustomerOrderFreight()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }
    public class MetaOrder
    {
        public CustomerOrder OrderHead { get; set; }
        public List<CustomerOrderRow> OrderRows { get; set; }
        public MetaOrder(Garp.Application garp)
        {
            OrderHead = new CustomerOrder(garp);
        }
    }

    public class CustomerOrder
    {
        public ITable Table { get; }
        private ITabField ONR, RES, OTY, PLF, X1F, OBF, ODT, LDT, FRY, WHO, KNR, LSE, OSE, OKL, LEF;
        /// <summary>
        /// ONR
        /// </summary>
        public string OrderNo { get { return ONR.Value; } set { ONR.Value = value; } }
        /// <summary>
        /// RES
        /// </summary>
        public string ReservationCode { get { return RES.Value; } set { RES.Value = value; } }
        /// <summary>
        /// OTY
        /// </summary>
        public string OrderType { get { return OTY.Value; } set { OTY.Value = value; } }
        /// <summary>
        /// PLF
        /// </summary>
        public string PickState { get { return PLF.Value; } set { PLF.Value = value; } }
        /// <summary>
        /// X1F
        /// </summary>
        public string X1State { get { return X1F.Value; } set { X1F.Value = value; } }
        /// <summary>
        /// OBF
        /// </summary>
        public string OrderConfirmationState { get { return OBF.Value; } set { OBF.Value = value; } }
        /// <summary>
        /// FRY
        /// </summary>
        public string FreezeCode { get { return FRY.Value; } set { FRY.Value = value; } }
        /// <summary>
        /// WHO
        /// </summary>
        public string WaitingAt { get { return WHO.Value; } set { WHO.Value = value; } }
        /// <summary>
        /// ODT
        /// </summary>
        public DateTime OrderDateTime { get { DateTime.TryParseExact(ODT.Value + OKL.Value, "yyMMddHHmm", null, DateTimeStyles.None, out DateTime value); return value; } set { ODT.Value = value.ToString("yyMMdd"); OKL.Value = value.ToString("HHmm"); } }
        /// <summary>
        /// LDT
        /// </summary>
        public DateTime MostRecentDeliveryDate { get { DateTime.TryParseExact(LDT.Value, "yyMMdd", null, DateTimeStyles.None, out DateTime value); return value; } set { LDT.Value = value.ToString("yyMMdd"); } }
        /// <summary>
        /// KNR
        /// </summary>
        public string CustomerNo { get { return KNR.Value; } set { KNR.Value = value; } }
        /// <summary>
        /// LSE
        /// </summary>
        public string ModeOfDelivery { get { return LSE.Value; } set { LSE.Value = value; } }
        /// <summary>
        /// OSE (K = Kundorder, I = Inköp)
        /// </summary>
        public string OrderSeries { get { return OSE.Value; } set { OSE.Value = value; } }
        /// <summary>
        /// LEF
        /// </summary>
        public string DeliveryState { get { return LEF.Value; } set { LEF.Value = value; } }
        public CustomerOrder(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    return;
                }

                Table = garp.Tables.Item("OGA");
                ONR = Table.Fields.Item("ONR");
                RES = Table.Fields.Item("RES");
                OTY = Table.Fields.Item("OTY");
                PLF = Table.Fields.Item("PLF");
                X1F = Table.Fields.Item("X1F");
                OBF = Table.Fields.Item("OBF");
                ODT = Table.Fields.Item("ODT");
                LDT = Table.Fields.Item("LDT");
                FRY = Table.Fields.Item("FRY");
                WHO = Table.Fields.Item("WHO");
                KNR = Table.Fields.Item("KNR");
                LSE = Table.Fields.Item("LSE");
                OSE = Table.Fields.Item("OSE");
                OKL = Table.Fields.Item("OKL");
                LEF = Table.Fields.Item("LEF");
            }
            catch (Exception)
            {
            }
        }

        ~CustomerOrder()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    public class CustomerOrderRow
    {
        public ITable Table { get; }
        private ITabField ONR, RDC, RES, PLF, X1F, OBF, LVF, LAG, RAT, BRA, ORA, ANR, LDT, BLF, TLA;
        /// <summary>
        /// ONR
        /// </summary>
        public string OrderNo { get { return ONR.Value; } set { ONR.Value = value; } }
        /// <summary>
        /// RDC
        /// </summary>
        public int RowNo { get { int.TryParse(RDC.Value, out int value); return value; } set { RDC.Value = value.ToString(); } }
        /// <summary>
        /// RES
        /// </summary>
        public string ReservationCode { get { return RES.Value; } set { RES.Value = value; } }
        /// <summary>
        /// PLF
        /// </summary>
        public string PickState { get { return PLF.Value; } set { PLF.Value = value; } }
        /// <summary>
        /// X1F
        /// </summary>
        public string X1State { get { return X1F.Value; } set { X1F.Value = value; } }
        /// <summary>
        /// OBF
        /// </summary>
        public string OrderConfirmationState { get { return OBF.Value; } set { OBF.Value = value; } }
        /// <summary>
        /// LVF
        /// </summary>
        public string DeliveryState { get { return LVF.Value; } set { LVF.Value = value; } }
        /// <summary>
        /// LAG
        /// </summary>
        public string WarehouseNo { get { return LAG.Value; } set { LAG.Value = value; } }
        /// <summary>
        /// RAT
        /// </summary>
        public string RowType { get { return RAT.Value; } set { RAT.Value = value; } }
        /// <summary>
        /// BRA
        /// </summary>
        public string AmountRowState { get { return BRA.Value; } set { BRA.Value = value; } }
        /// <summary>
        /// ORA
        /// </summary>
        public decimal OriginalQuantity { get { return Verktyg.GetDecimalFromStr(ORA.Value); } set { ORA.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// ANR
        /// </summary>
        public string ItemNo { get { return ANR.Value; } set { ANR.Value = value; } }
        /// <summary>
        /// LDT
        /// </summary>
        public DateTime DeliveryDate { get { DateTime.TryParseExact(LDT.Value, "yyMMdd", null, DateTimeStyles.None, out DateTime value); return value; } set { LDT.Value = value.ToString("yyMMdd"); } }
        /// <summary>
        /// BLF
        /// </summary>
        public string BState { get { return BLF.Value; } set { BLF.Value = value; } }
        /// <summary>
        /// TLA
        /// </summary>
        public decimal DeliveredQuantity { get { return Verktyg.GetDecimalFromStr(TLA.Value); } set { TLA.Value = value.ToString().Replace(",", "."); } }
        public CustomerOrderRow(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    return;
                }

                Table = garp.Tables.Item("OGR");
                ONR = Table.Fields.Item("ONR");
                RDC = Table.Fields.Item("RDC");
                RES = Table.Fields.Item("RES");
                PLF = Table.Fields.Item("PLF");
                X1F = Table.Fields.Item("X1F");
                OBF = Table.Fields.Item("OBF");
                LVF = Table.Fields.Item("LVF");
                LAG = Table.Fields.Item("LAG");
                RAT = Table.Fields.Item("RAT");
                BRA = Table.Fields.Item("BRA");
                ORA = Table.Fields.Item("ORA");
                ANR = Table.Fields.Item("ANR");
                LDT = Table.Fields.Item("LDT");
                BLF = Table.Fields.Item("BLF");
                TLA = Table.Fields.Item("TLA");
            }
            catch (Exception)
            {
            }
        }

        ~CustomerOrderRow()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    public class BaseTable
    {
        /// <summary>
        /// TA
        /// </summary>
        public ITable Table { get; }
        private readonly ITabField KEY, TX1, TX2;
        /// <summary>
        /// KEY
        /// </summary>
        public string Key { get { return KEY.Value; } set { KEY.Value = value; } }
        /// <summary>
        /// TX1
        /// </summary>
        public string Text1 { get { return TX1.Value; } set { TX1.Value = value; } }
        /// <summary>
        /// TX2
        /// </summary>
        public string Text2 { get { return TX2.Value; } set { TX2.Value = value; } }

        public BaseTable(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("BaseTable cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("TA");
                KEY = Table.Fields.Item("KEY");
                TX1 = Table.Fields.Item("TX1");
                TX2 = Table.Fields.Item("TX2");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating BaseTable: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ~BaseTable()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    public class PurchaseOrderHead
    {
        /// <summary>
        /// IGA
        /// </summary>
        public ITable Table { get; }
        private ITabField ONR, LNR, VRF, OTY;
        /// <summary>
        /// ONR
        /// </summary>
        public string PurchaseOrderNo { get { return ONR.Value; } set { ONR.Value = value; } }
        /// <summary>
        /// LNR
        /// </summary>
        public string SupplierNo { get { return LNR.Value; } set { LNR.Value = value; } }
        /// <summary>
        /// VRF
        /// </summary>
        public string OurReference { get { return VRF.Value; } set { VRF.Value = value; } }
        /// <summary>
        /// OTY
        /// </summary>
        public string OrderType { get { return OTY.Value; } set { OTY.Value = value; } }
        public PurchaseOrderHead(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("PurchaseOrder cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("IGA");
                ONR = Table.Fields.Item("ONR");
                LNR = Table.Fields.Item("LNR");
                VRF = Table.Fields.Item("VRF");
                OTY = Table.Fields.Item("OTY");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating PurchaseOrder: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ~PurchaseOrderHead()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    /// <summary>
    /// IGR
    /// </summary>
    public class PurchaseOrderRow
    {
        /// <summary>
        /// IGR
        /// </summary>
        public ITable Table { get; }
        private ITabField ONR, RDC, LDT, PRI, PRU, LVP, ANR, LVF, LAG, ORA, TLA;
        /// <summary>
        /// ONR
        /// </summary>
        public string PurchaseOrderNo { get { return ONR.Value; } set { ONR.Value = value; } }
        /// <summary>
        /// ANR
        /// </summary>
        public string ItemNo { get { return ANR.Value; } set { ANR.Value = value; } }
        /// <summary>
        /// RDC
        /// </summary>
        public int RowNo { get { int.TryParse(RDC.Value, out int value); return value; } set { RDC.Value = value.ToString(); } }
        /// <summary>
        /// LDT
        /// </summary>
        public DateTime DateStamp { get { DateTime.TryParseExact(LDT.Value, "yyMMdd", null, DateTimeStyles.None, out DateTime value); return value; } set { LDT.Value = value.ToString("yyMMdd"); } }
        /// <param name="garp"></param>
        /// <summary>
        /// PRI
        /// </summary>
        public decimal Price { get { return Verktyg.GetDecimalFromStr(PRI.Value); } set { PRI.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// PRU
        /// </summary>
        public decimal GrossPrice { get { return Verktyg.GetDecimalFromStr(PRU.Value); } set { PRU.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// LVP
        /// </summary>
        public decimal ItemCost { get { return Verktyg.GetDecimalFromStr(LVP.Value); } set { LVP.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// ORA
        /// </summary>
        public decimal InitialQuantity { get { return Verktyg.GetDecimalFromStr(ORA.Value); } set { ORA.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// TLA
        /// </summary>
        public decimal DeliveredQuantity { get { return Verktyg.GetDecimalFromStr(TLA.Value); } set { TLA.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// LVF
        /// </summary>
        public string DeliveryState { get { return LVF.Value; } set { LVF.Value = value; } }
        /// <summary>
        /// LAG
        /// </summary>
        public string WarehouseNo { get { return LAG.Value; } set { LAG.Value = value; } }
        /// <summary>
        /// IGR
        /// </summary>
        public PurchaseOrderRow(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("PurchaseOrderRow cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("IGR");
                ONR = Table.Fields.Item("ONR");
                RDC = Table.Fields.Item("RDC");
                LDT = Table.Fields.Item("LDT");
                PRI = Table.Fields.Item("PRI");
                PRU = Table.Fields.Item("PRU");
                LVP = Table.Fields.Item("LVP");
                ANR = Table.Fields.Item("ANR");
                LVF = Table.Fields.Item("LVF");
                LAG = Table.Fields.Item("LAG");
                ORA = Table.Fields.Item("ORA");
                TLA = Table.Fields.Item("TLA");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating PurchaseOrderRow: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        ~PurchaseOrderRow()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    /// <summary>
    /// IGR2
    /// </summary>
    public class PurchaseOrderRow2
    {
        /// <summary>
        /// IGR2
        /// </summary>
        public ITable Table { get; }
        private ITabField ONR, RDC, NU5, NU8, NU6, NU7;
        /// <summary>
        /// ONR
        /// </summary>
        public string PurchaseOrderNo { get { return ONR.Value; } set { ONR.Value = value; } }
        /// <summary>
        /// RDC
        /// </summary>
        public int RowNo { get { int.TryParse(RDC.Value, out int value); return value; } set { RDC.Value = value.ToString(); } }
        /// <summary>
        /// NU5: Total freight on order
        /// </summary>
        public decimal Number5 { get { return Verktyg.GetDecimalFromStr(NU5.Value); } set { NU5.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// NU8: Freight per item on row
        /// </summary>
        public decimal Number8 { get { return Verktyg.GetDecimalFromStr(NU8.Value); } set { NU8.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// NU6: Customs per item on row
        /// </summary>
        public decimal Number6 { get { return Verktyg.GetDecimalFromStr(NU6.Value); } set { NU6.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// NU7
        /// </summary>
        public decimal Number7 { get { return Verktyg.GetDecimalFromStr(NU7.Value); } set { NU7.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// IGR2
        /// </summary>
        /// <param name="garp"></param>
        public PurchaseOrderRow2(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("ItemHistory cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("IGR2");
                ONR = Table.Fields.Item("ONR");
                RDC = Table.Fields.Item("RDC");
                NU5 = Table.Fields.Item("NU5");
                NU6 = Table.Fields.Item("NU6");
                NU7 = Table.Fields.Item("NU7");
                NU8 = Table.Fields.Item("NU8");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating ItemHistory: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        ~PurchaseOrderRow2()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    /// <summary>
    /// HGA
    /// </summary>
    public class ItemHistory
    {
        /// <summary>
        /// HGA
        /// </summary>
        public ITable Table { get; }
        private ITabField ANR, TIP, PRI, BD1, LAG, ID1, RCT, LEA, LVB, HNR, RDC;
        /// <summary>
        /// ANR
        /// </summary>
        public string ItemNo { get { return ANR.Value; } set { ANR.Value = value; } }
        /// <summary>
        /// ID1
        /// </summary>
        public string TransactionId { get { return ID1.Value; } set { ID1.Value = value; } }
        /// <summary>
        /// RCT
        /// </summary>
        public string RecordType { get { return RCT.Value; } set { RCT.Value = value; } }
        /// <summary>
        /// HNR
        /// </summary>
        public string ReceiptNo { get { return HNR.Value; } set { HNR.Value = value; } }
        /// <summary>
        /// RDC
        /// </summary>
        public int RowNo { get { int.TryParse(RDC.Value, out int value); return value; } set { RDC.Value = value.ToString(); } }
        /// <summary>
        /// PRI
        /// </summary>
        public decimal SalesPrice { get { return Verktyg.GetDecimalFromStr(PRI.Value); } set { PRI.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// LEA
        /// </summary>
        public decimal Quantity { get { return Verktyg.GetDecimalFromStr(LEA.Value); } set { LEA.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// LVB
        /// </summary>
        public decimal WarehouseValue { get { return Verktyg.GetDecimalFromStr(LVB.Value); } set { LVB.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// BD1 + TIP
        /// </summary>
        public DateTime DateTimeStamp { get { DateTime.TryParseExact(BD1.Value + TIP.Value, "yyMMddHHmmss", null, DateTimeStyles.None, out DateTime value); return value; } set { BD1.Value = value.ToString("yyMMdd"); TIP.Value = value.ToString("HHmmss"); } }
        /// <summary>
        /// LAG
        /// </summary>
        public string BatchNo { get { return LAG.Value; } set { LAG.Value = value; } }
        /// <summary>
        /// HGA
        /// </summary>
        /// <param name="garp"></param>
        public ItemHistory(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("ItemHistory cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("HGA");
                ANR = Table.Fields.Item("ANR");
                TIP = Table.Fields.Item("TIP");
                PRI = Table.Fields.Item("PRI");
                BD1 = Table.Fields.Item("BD1");
                LAG = Table.Fields.Item("LAG");
                ID1 = Table.Fields.Item("ID1");
                RCT = Table.Fields.Item("RCT");
                LEA = Table.Fields.Item("LEA");
                LVB = Table.Fields.Item("LVB");
                HNR = Table.Fields.Item("HNR");
                RDC = Table.Fields.Item("RDC");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating ItemHistory: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Find(string itemNo, string date, string receiptNo, int indexNo = 2)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(itemNo) || string.IsNullOrWhiteSpace(date) || string.IsNullOrWhiteSpace(receiptNo))
                {
                    return false;
                }

                string index = itemNo.PadRight(13) + date;
                Table.IndexNo = indexNo;
                if (Table.Find(index))
                {
                    return true;
                }
                else
                {
                    Table.Next();
                    while (!Table.Eof && ItemNo == itemNo && DateTimeStamp.ToString("yyMMdd") == date)
                    {
                        if (ReceiptNo.PadRight(6) == receiptNo.PadRight(6))
                        {
                            if (ItemNo.PadRight(13) == index.Substring(0, 13))
                            {
                                return true;
                            }
                        }
                        Table.Next();
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in ItemHistory.Find: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        ~ItemHistory()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    /// <summary>
    /// HIC
    /// </summary>
    public class ReceiptFreight
    {
        /// <summary>
        /// HIC
        /// </summary>
        public ITable Table { get; set; }
        private ITabField HNR, INN, NU1, NU2;
        /// <summary>
        /// HNR
        /// </summary>
        public string ReceiptNo { get { return HNR.Value; } set { HNR.Value = value; } }
        /// <summary>
        /// INN
        /// </summary>
        public string InternalNo { get { return INN.Value; } set { INN.Value = value; } }
        /// <summary>
        /// NU1
        /// </summary>
        public decimal Number1 { get { return Verktyg.GetDecimalFromStr(NU1.Value); } set { NU1.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// NU2
        /// </summary>
        public decimal Number2 { get { return Verktyg.GetDecimalFromStr(NU2.Value); } set { NU2.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// HIC
        /// </summary>
        /// <param name="garp"></param>
        public ReceiptFreight(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("ReceiptFreight cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("HIC");
                HNR = Table.Fields.Item("HNR");
                INN = Table.Fields.Item("INN");
                NU1 = Table.Fields.Item("NU1");
                NU2 = Table.Fields.Item("NU2");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating ReceiptFreigt: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        ~ReceiptFreight()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    /// <summary>
    /// HIR
    /// </summary>
    public class ReceiptRow
    {
        /// <summary>
        /// HIR
        /// </summary>
        public ITable Table { get; set; }
        private ITabField HNR, RDC, LEA, PRI, PRU, LVP, ANR, LDT, ONR;
        /// <summary>
        /// HNR
        /// </summary>
        public string ReceiptNo { get { return HNR.Value; } set { HNR.Value = value; } }
        /// <summary>
        /// RDC
        /// </summary>
        public int RowNo { get { int.TryParse(RDC.Value, out int value); return value; } set { RDC.Value = value.ToString(); } }
        /// <summary>
        /// LEA
        /// </summary>
        public decimal ReceivedQuantity { get { return Verktyg.GetDecimalFromStr(LEA.Value); } set { LEA.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// PRI
        /// </summary>
        public decimal Price { get { return Verktyg.GetDecimalFromStr(PRI.Value); } set { PRI.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// PRU
        /// </summary>
        public decimal GrossPrice { get { return Verktyg.GetDecimalFromStr(PRU.Value); } set { PRU.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// LVP
        /// </summary>
        public decimal ItemCost { get { return Verktyg.GetDecimalFromStr(LVP.Value); } set { LVP.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// ANR
        /// </summary>
        public string ItemNo { get { return ANR.Value; } set { ANR.Value = value; } }
        /// <summary>
        /// LDT
        /// </summary>
        public DateTime DateStamp { get { DateTime.TryParseExact(LDT.Value, "yyMMdd", null, DateTimeStyles.None, out DateTime value); return value; } set { LDT.Value = value.ToString("yyMMdd"); } }
        /// <summary>
        /// ONR
        /// </summary>
        public string PurchaseOrderNo { get { return ONR.Value; } set { ONR.Value = value; } }
        /// <summary>
        /// HIR
        /// </summary>
        /// <param name="garp"></param>
        public ReceiptRow(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("ReceiptRow cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("HIR");
                HNR = Table.Fields.Item("HNR");
                RDC = Table.Fields.Item("RDC");
                LEA = Table.Fields.Item("LEA");
                PRI = Table.Fields.Item("PRI");
                PRU = Table.Fields.Item("PRU");
                LVP = Table.Fields.Item("LVP");
                ANR = Table.Fields.Item("ANR");
                LDT = Table.Fields.Item("LDT");
                ONR = Table.Fields.Item("ONR");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating ReceiptRow: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Find(string index, int rowNo = 0, int internalNo = 0, int indexNo = 1)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(index))
                {
                    return false;
                }

                index = index.PadRight(6);

                if (rowNo != 0 && internalNo != 0)
                {
                    if (Table.Find(index + internalNo.ToString().PadLeft(3) + rowNo.ToString().PadLeft(3)))
                    {
                        return true;
                    }
                    else
                    {
                        Table.Next();
                        if (ReceiptNo.PadRight(6) == index.PadRight(6).Substring(0, 6))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    Table.Find(index);
                    Table.Next();
                    if (ReceiptNo.PadRight(6) == index.PadRight(6).Substring(0, 6))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in ReceiptRow.Find: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        ~ReceiptRow()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    /// <summary>
    /// HIA
    /// </summary>
    public class Receipt
    {
        /// <summary>
        /// HIA
        /// </summary>
        public ITable Table { get; set; }
        private ITabField HNR, LDT, ODT, ONR, FSF;
        /// <summary>
        /// HNR
        /// </summary>
        public string ReceiptNo { get { return HNR.Value; } set { HNR.Value = value; } }
        /// <summary>
        /// ONR
        /// </summary>
        public string PurchaseOrderNo { get { return ONR.Value; } set { ONR.Value = value; } }
        /// <summary>
        /// LDT
        /// </summary>
        public DateTime ReceiptDate { get { DateTime.TryParseExact(LDT.Value, "yyMMdd", null, DateTimeStyles.None, out DateTime value); return value; } set { LDT.Value = value.ToString("yyMMdd"); } }
        /// <summary>
        /// ODT
        /// </summary>
        public DateTime ReceiptOrderDate { get { DateTime.TryParseExact(ODT.Value, "yyMMdd", null, DateTimeStyles.None, out DateTime value); return value; } set { ODT.Value = value.ToString("yyMMdd"); } }
        /// <summary>
        /// FSF
        /// </summary>
        public string DeliveryStateFlag { get { return FSF.Value; } set { FSF.Value = value; } }
        /// <summary>
        /// HIR
        /// </summary>
        /// <param name="garp"></param>
        public Receipt(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("Receipt cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("HIA");
                HNR = Table.Fields.Item("HNR");
                LDT = Table.Fields.Item("LDT");
                ODT = Table.Fields.Item("ODT");
                ONR = Table.Fields.Item("ONR");
                FSF = Table.Fields.Item("FSF");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating Receipt: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Find(string index, int indexNo = 1)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(index))
                {
                    return false;
                }

                Table.IndexNo = indexNo;

                if (index.PadRight(6).Length == 6)
                {
                    if (Table.IndexNo == 1)
                    {
                        index = index.PadRight(6) + "  1"; // Receipt no and inomnummer
                    }
                    else
                    {
                        index = index.PadRight(6);
                    }
                }

                if (Table.Find(index))
                {
                    return true;
                }
                else
                {
                    Table.Next();
                    if (ReceiptNo.PadRight(6) == index.PadRight(6))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in Receipt.Find: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        ~Receipt()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }

    /// <summary>
    /// AGA
    /// </summary>
    public class Item
    {
        /// <summary>
        /// AGA
        /// </summary>
        public ITable Table { get; set; }
        private ITabField ANR, SIP, VIP, MLG, SUS, TYP;
        /// <summary>
        /// ANR
        /// </summary>
        public string ItemNo { get { return ANR.Value; } set { ANR.Value = value; } }
        /// <summary>
        /// SIP
        /// </summary>
        public decimal LatestInPrice { get { return Verktyg.GetDecimalFromStr(SIP.Value); } set { SIP.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// VIP
        /// </summary>
        public decimal WeightedInPrice { get { return Verktyg.GetDecimalFromStr(VIP.Value); } set { VIP.Value = value.ToString().Replace(",", "."); } }
        /// <summary>
        /// MLG
        /// </summary>
        public string WarehousingType { get { return MLG.Value; } set { MLG.Value = value; } }
        /// <summary>
        /// SUS
        /// </summary>
        public string WarehousingImpact { get { return SUS.Value; } set { SUS.Value = value; } }
        /// <summary>
        /// TYP
        /// </summary>
        public string ItemType { get { return TYP.Value; } set { TYP.Value = value; } }

        /// <summary>
        /// AGA
        /// </summary>
        /// <param name="garp"></param>
        public Item(Garp.Application garp)
        {
            try
            {
                if (garp == null)
                {
                    _ = MessageBox.Show("Item cannot be initiated because Garp is NULL.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                Table = garp.Tables.Item("AGA");
                ANR = Table.Fields.Item("ANR");
                SIP = Table.Fields.Item("SIP");
                VIP = Table.Fields.Item("VIP");
                MLG = Table.Fields.Item("MLG");
                SUS = Table.Fields.Item("SUS");
                TYP = Table.Fields.Item("TYP");
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error inintating Item: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Find(string index)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(index))
                {
                    return false;
                }

                index = index.PadRight(13);

                if (Table.Find(index))
                {
                    return true;
                }
                else
                {
                    Table.Next();
                    if (ItemNo.PadRight(13) == index.PadRight(13))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in Item.Find: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        ~Item()
        {
            try
            {
                Marshal.ReleaseComObject(Table);
            }
            catch (Exception)
            {

            }
        }
    }
}
