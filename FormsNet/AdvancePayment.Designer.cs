﻿using System.Windows.Forms;

namespace FormsSkogma
{
    partial class AdvancePayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvancePayment));
            this.TextBelopp = new System.Windows.Forms.Label();
            this.Belopp = new System.Windows.Forms.TextBox();
            this.TextProcent = new System.Windows.Forms.Label();
            this.Procent = new System.Windows.Forms.TextBox();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.BerBelopp = new System.Windows.Forms.Label();
            this.BerProcent = new System.Windows.Forms.Label();
            this.TextBeloppExMoms = new System.Windows.Forms.Label();
            this.RestBeloppExMoms = new System.Windows.Forms.Label();
            this.TextBeloppInkMoms = new System.Windows.Forms.Label();
            this.RestBeloppInkMoms = new System.Windows.Forms.Label();
            this.Enhet = new System.Windows.Forms.Label();
            this.TextInkMoms = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextBelopp
            // 
            this.TextBelopp.AutoSize = true;
            this.TextBelopp.Location = new System.Drawing.Point(13, 13);
            this.TextBelopp.Name = "TextBelopp";
            this.TextBelopp.Size = new System.Drawing.Size(99, 13);
            this.TextBelopp.TabIndex = 0;
            this.TextBelopp.Text = "Belopp att debitera:";
            // 
            // Belopp
            // 
            this.Belopp.Location = new System.Drawing.Point(138, 13);
            this.Belopp.Name = "Belopp";
            this.Belopp.Size = new System.Drawing.Size(81, 20);
            this.Belopp.TabIndex = 1;
            this.Belopp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.Belopp.Leave += new System.EventHandler(this.Belopp_Leave);
            // 
            // TextProcent
            // 
            this.TextProcent.AutoSize = true;
            this.TextProcent.Location = new System.Drawing.Point(12, 48);
            this.TextProcent.Name = "TextProcent";
            this.TextProcent.Size = new System.Drawing.Size(100, 13);
            this.TextProcent.TabIndex = 2;
            this.TextProcent.Text = "Procent att debitera";
            // 
            // Procent
            // 
            this.Procent.Location = new System.Drawing.Point(138, 48);
            this.Procent.Name = "Procent";
            this.Procent.Size = new System.Drawing.Size(81, 20);
            this.Procent.TabIndex = 3;
            this.Procent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.Procent.Leave += new System.EventHandler(this.Procent_Leave);
            this.Procent.Enter += new System.EventHandler(this.Procent_Enter);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(91, 141);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 4;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(187, 141);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 14;
            this.CancelButton.TabStop = false;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // BerBelopp
            // 
            this.BerBelopp.AutoSize = true;
            this.BerBelopp.Location = new System.Drawing.Point(238, 19);
            this.BerBelopp.Name = "BerBelopp";
            this.BerBelopp.Size = new System.Drawing.Size(0, 13);
            this.BerBelopp.TabIndex = 6;
            // 
            // BerProcent
            // 
            this.BerProcent.AutoSize = true;
            this.BerProcent.Location = new System.Drawing.Point(241, 54);
            this.BerProcent.Name = "BerProcent";
            this.BerProcent.Size = new System.Drawing.Size(0, 13);
            this.BerProcent.TabIndex = 7;
            // 
            // TextBeloppExMoms
            // 
            this.TextBeloppExMoms.AutoSize = true;
            this.TextBeloppExMoms.Location = new System.Drawing.Point(13, 87);
            this.TextBeloppExMoms.Name = "TextBeloppExMoms";
            this.TextBeloppExMoms.Size = new System.Drawing.Size(109, 13);
            this.TextBeloppExMoms.TabIndex = 8;
            this.TextBeloppExMoms.Text = "Restbelopp ex. Moms";
            // 
            // RestBeloppExMoms
            // 
            this.RestBeloppExMoms.AutoSize = true;
            this.RestBeloppExMoms.Location = new System.Drawing.Point(147, 87);
            this.RestBeloppExMoms.Name = "RestBeloppExMoms";
            this.RestBeloppExMoms.Size = new System.Drawing.Size(0, 13);
            this.RestBeloppExMoms.TabIndex = 9;
            // 
            // TextBeloppInkMoms
            // 
            this.TextBeloppInkMoms.AutoSize = true;
            this.TextBeloppInkMoms.Location = new System.Drawing.Point(13, 116);
            this.TextBeloppInkMoms.Name = "TextBeloppInkMoms";
            this.TextBeloppInkMoms.Size = new System.Drawing.Size(112, 13);
            this.TextBeloppInkMoms.TabIndex = 10;
            this.TextBeloppInkMoms.Text = "Restbelopp ink. Moms";
            // 
            // RestBeloppInkMoms
            // 
            this.RestBeloppInkMoms.AutoSize = true;
            this.RestBeloppInkMoms.Location = new System.Drawing.Point(147, 116);
            this.RestBeloppInkMoms.Name = "RestBeloppInkMoms";
            this.RestBeloppInkMoms.Size = new System.Drawing.Size(0, 13);
            this.RestBeloppInkMoms.TabIndex = 11;
            // 
            // Enhet
            // 
            this.Enhet.AutoSize = true;
            this.Enhet.Location = new System.Drawing.Point(241, 54);
            this.Enhet.Name = "Enhet";
            this.Enhet.Size = new System.Drawing.Size(15, 13);
            this.Enhet.TabIndex = 13;
            this.Enhet.Text = "%";
            // 
            // TextInkMoms
            // 
            this.TextInkMoms.AutoSize = true;
            this.TextInkMoms.Location = new System.Drawing.Point(225, 13);
            this.TextInkMoms.Name = "TextInkMoms";
            this.TextInkMoms.Size = new System.Drawing.Size(52, 13);
            this.TextInkMoms.TabIndex = 15;
            this.TextInkMoms.Text = "Ink moms";
            // 
            // AdvancePayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 171);
            this.Controls.Add(this.TextInkMoms);
            this.Controls.Add(this.Enhet);
            this.Controls.Add(this.RestBeloppInkMoms);
            this.Controls.Add(this.TextBeloppInkMoms);
            this.Controls.Add(this.RestBeloppExMoms);
            this.Controls.Add(this.TextBeloppExMoms);
            this.Controls.Add(this.BerProcent);
            this.Controls.Add(this.BerBelopp);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.Procent);
            this.Controls.Add(this.TextProcent);
            this.Controls.Add(this.Belopp);
            this.Controls.Add(this.TextBelopp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdvancePayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdvancePayment";
            this.Load += new System.EventHandler(this.AdvancePayment_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TextBelopp;
        private System.Windows.Forms.TextBox Belopp;
        private System.Windows.Forms.Label TextProcent;
        private System.Windows.Forms.TextBox Procent;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Label BerBelopp;
        private System.Windows.Forms.Label BerProcent;
        private System.Windows.Forms.Label TextBeloppExMoms;
        private System.Windows.Forms.Label RestBeloppExMoms;
        private System.Windows.Forms.Label TextBeloppInkMoms;
        private System.Windows.Forms.Label RestBeloppInkMoms;
        private System.Windows.Forms.Label Enhet;
        private System.Windows.Forms.Label TextInkMoms;
    }
}