﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Configuration;
using Garp;
using GarpDb;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;
using System.Data;
using PrioOrder;

namespace FormsSkogma
{
    public class Order : IDisposable
    {
        Garp.Application GarpOrder = null;
        IComponents ComponentsOrder = null;

        Dataset dsOGA, dsOGR, dsOGL, dsOGF;
        ICounters NrSerie;

        IDatasetField
              DS_OrderRad_Levtid
            , DS_OrderRad_Antal
            , DS_OrderRad_Reservationskod
            ;

        #region Garp tabeller och fält

        ITable
              tblOGA       // Orderhuvud
            , tblOGF       // OrderHuvudText
            , tblOGC       // OrderHuvudFrakt
            , tblOGL       // OrderHuvudLevAdress
            , tblOGR       // Orderrad
            , tblOGK       // OrderradText
            , tblAGA       // Artikel
            , tblAGT       // ArtikelText
            , tblAGL       // ArtikelFlerlager
            , tblAGS       // ArtikelStruktur
                           //, tblIGR       // Inköpsorderrad
            , tblKA        // Kund
            , tblKB        // Kund-B
            , tblTA        // Tabellregistret
            , tblAPD       // Prislista
            , tblKP        // L-kundpriser
            ;

        ITabField
              fld_OGA_LevTid
            , fld_OGA_OrderNr
            , fld_OGA_Reservationskod
            , fld_OGA_LeveransFlagga
            , fld_OGA_OBflagga
            , fld_OGA_PLflagga
            , fld_OGA_KundNr
            , fld_OGA_Betvillk
            , fld_OGA_OurRef
            , fld_OGA_ErRef
            , fld_OGA_LevVilk
            , fld_OGA_LevSatt
            , fld_OGA_Saljare
            , fld_OGA_Valuta
            , fld_OGA_MomsKod
            , fld_OGA_Prislista

            , fld_OGL_OrderNr
            , fld_OGL_Namn
            , fld_OGL_Adress1
            , fld_OGL_Adress2
            , fld_OGL_Adress3
            , fld_OGL_Ort
            , fld_OGL_Land

            , fld_OGR_OrderNr
            , fld_OGR_RadNr
            , fld_OGR_ArtNr
            , fld_OGR_Benamning
            , fld_OGR_BenamningExtra
            , fld_OGR_UrsprungligtAntal
            , fld_OGR_LagerNr
            , fld_OGR_LevereratAntal
            , fld_OGR_LeveransFlagga
            , fld_OGR_LevTid
            , fld_OGR_Reservationskod
            , fld_OGR_Pris
            , fld_OGR_Fakturaflagga // paketflagga
            , fld_OGR_MomsKod
            , fld_OGR_Enhet
            , fld_OGR_OHrab
            , fld_OGR_RadRab
            , fld_OGR_Belaggning

            , fld_OGF_OrderNr
            , fld_OGF_Text1
            , fld_OGF_Text2
            , fld_OGF_ValfriInfo
            , fld_OGF_FakturKontakt

            , fld_OGC_OrderNr
            , fld_OGC_CH6 // Kundens önskade leveranstid
            , fld_OGC_CH3 // Används som flagga om mobilnumret är hämtat till ordern
            , fld_OGC_CH4 // Lagra lagernumren på ordern, max 4 st

            , fld_OrderRadText_OrderNr
            , fld_OrderRadText_RadNr
            , fld_OrderRadText_Sekvens
            , fld_OrderRadText_FAfl
            , fld_OrderRadText_FSfl
            , fld_OrderRadText_OBfl
            , fld_OrderRadText_PLfl
            , fld_OrderRadText_Text

            , fld_AGA_ArtikelNr
            , fld_AGA_Benamning
            , fld_AGA_TillvKopKod
            , fld_AGA_Saldohanteras
            , fld_AGA_Ledtid
            , fld_AGA_Saldo
            , fld_AGA_Vikt
            , fld_AGA_VIP
            , fld_AGA_Pris
            , fld_AGA_Decimaler
            , fld_AGA_Variantkod
            , fld_AGA_ModellLangd
            , fld_AGA_Flerlager
            , fld_AGA_Kod3
            , fld_AGA_Kategori

            , fld_AGL_Saldo

            , fld_AGS_StrukturArtikel
            , fld_AGS_UnderArtikel

            , fld_KA_namn
            , fld_KA_Adress1
            , fld_KA_Adress2
            , fld_KA_Ort
            , fld_KA_Land
            , fld_KA_GodsPostnr
            , fld_KA_TX5
            , fld_KA_VIPkund
            , fld_KA_Num3
            , fld_KA_Kod5

            , fld_KB_AdressExtra // Förnamn

            , fld_TA_Key
            , fld_TA_Text

            , fld_APD_Prislisteid
            , fld_APD_KampanjLagstaPris

            , fld_KP_Pris
            , fld_KP_Anmarkning
            ;

        IComponent
              LabelVersion
            , LabelDesiredDeliveryText
            , LabelDesiredDelivery
            , LabelPaketTBHeader
            , LabelPaketTB1
            , LabelPaketTB2
            , LabelPLStatus
            , LabelPLStatus2
            , LabelX1Status
            , LabelPaymentConditions
            , LabelDateCreditControlText
            , LabelDateCreditControl
            , LabelWarehouses

            , BtnNyOrder
            , BtnNyOrder2
            , BtnOrderKlar
            //, BtnSaldoTest
            //, BtnOrderRadSaldoTest
            , BtnFraktKostnad
            , BtnLevSatt
            , BtnLevEnlPlocklista
            , BtnAdvancePay
            , BtnPrioOrderCheckAll

            , Form_OrderHuvud_OrderTyp
            , Form_OrderHuvud_OrderNr
            , Form_OrderHuvud_KundNr
            , Form_OrderHuvud_OrderDatum
            , Form_OrderHuvud_OrderTid
            , Form_OrderHuvud_LevTid
            , Form_OrderHuvud_Reservationskod
            , Form_OrderHuvud_Text2
            , Form_OrderHuvud_LevSatt
            , Form_OrderHuvud_Namn

            , Form_OrderRad_RadNr
            , Form_OrderRad_LevTid
            , Form_OrderRad_Antal
            , Form_OrderRad_ArtNr
            , Form_OrderRad_Reservationskod
            , Form_OrderRad_Pris
            , Form_OrderRad_LagerNr
            , Form_OrderRad_Textsesong
            , Form_OrderRad_Sesong
            , Form_OrderRad_BtnSesong
            , Form_OrderRad_Benamning
            , Form_OrderRad_ExtraBen
            , Form_OrderRad_RabattKod
            , Form_OrderRad_Rabatt
            , Form_OrderRad_Revision
            , Form_OrderRad_Enh
            , Form_OrderRad_Fakturaflagga // paketradsflagga
        #endregion tabeller och fält

        #region stafflingsdata     

            // Nya komponenter för att visa stafflingdata rakt orderformuläret
            , LblAntal
            , LblPris
            ;

        Dictionary<string, List<IComponent>> BracketQuantities { get; set; }
        Dictionary<string, List<IComponent>> BracketsPrices { get; set; }

        private string StrAntal { get; } = "Antal";
        private string StrPris { get; } = "Pris";
        private int Top { get; } = 120;
        private int Left { get; } = 535;
        private int RowSpacing { get; } = 20;
        private int ColumnSpacing { get; } = 5;
        private int Width { get; } = 35;

        #endregion stafflingsdata

        public OrderFrakt frakt = new OrderFrakt();
        public OrderPaketTB paketTB = new OrderPaketTB();
        public OrderLikvArtikel likvArtikel = new OrderLikvArtikel();
        public Matrisrader matris = new Matrisrader();
        private readonly SettingsOrder order = new SettingsOrder();

        List<string> OrderRadResKod1;// = new List<string>();
        List<string> OrderRadReskodBlank;
        List<AdvanceOrderRow> OrderInfo;

        private CustomerOrder OrderHead { get; set; }
        private CustomerOrderRow OrderRow { get; set; }
        private CustomerOrderFreight OrderFreight { get; set; }
        private Item Item { get; set; }
        private GarpDb.Customer Customer { get; set; }
        private PurchaseOrderRow PurchaseOrderRow { get; set; }
        private PurchaseOrderHead PurchaseOrderHead { get; set; }
        private BaseTable BaseTable { get; }
        bool m_disposed = false;

        public Order()
        {
            GarpOrder = new Garp.Application();
            order.Bolag = GarpOrder.Bolag;

            ComponentsOrder = GarpOrder.Components; // Skapa en pekare för componets för att undvika krock med annat formulärs forms

            // Hämta datakällan för OrderHuvud och OrderRad
            dsOGA = GarpOrder.Datasets.Item("ogaMcDataSet");
            dsOGL = GarpOrder.Datasets.Item("oglMcDataSet");
            dsOGF = GarpOrder.Datasets.Item("ogfMcDataSet");
            dsOGR = GarpOrder.Datasets.Item("ogrMcDataSet");

            // Skapa bevakning på händelser
            dsOGA.AfterScroll += AfterScrollOGA;
            dsOGR.AfterScroll += AfterScrollOGR;
            dsOGA.BeforePost += BeforePostOH;
            dsOGR.BeforePost += BeforePostOR;
            //dsOGA.BeforeDelete += BeforeDeleteOGA;
            //dsOGR.BeforeDelete += BeforeDeleteOGR;

            GarpOrder.ButtonClick += ButtonClik;
            GarpOrder.FieldEnter += FieldEnter;
            GarpOrder.FieldExit += FieldExit;

            #region Skapa tabeller och fält
            // Hämta tabeller
            tblOGA = GarpOrder.Tables.Item("OGA");
            tblOGR = GarpOrder.Tables.Item("OGR");
            tblOGK = GarpOrder.Tables.Item("OGK");
            tblAGA = GarpOrder.Tables.Item("AGA");
            tblAGT = GarpOrder.Tables.Item("AGT");
            tblAGL = GarpOrder.Tables.Item("AGL");
            tblAGS = GarpOrder.Tables.Item("AGS");
            tblOGC = GarpOrder.Tables.Item("OGC");
            tblOGF = GarpOrder.Tables.Item("OGF");
            tblOGL = GarpOrder.Tables.Item("OGL");
            tblKA = GarpOrder.Tables.Item("KA");
            tblKB = GarpOrder.Tables.Item("KB");
            tblTA = GarpOrder.Tables.Item("TA");
            tblAPD = GarpOrder.Tables.Item("APD");
            tblKP = GarpOrder.Tables.Item("KP");

            // Tabellfält
            fld_OGA_LevTid = tblOGA.Fields.Item("BLT");
            fld_OGA_OrderNr = tblOGA.Fields.Item("ONR");
            fld_OGA_Reservationskod = tblOGA.Fields.Item("RES");
            fld_OGA_LeveransFlagga = tblOGA.Fields.Item("LEF");
            fld_OGA_OBflagga = tblOGA.Fields.Item("OBF");
            fld_OGA_PLflagga = tblOGA.Fields.Item("PLF");
            fld_OGA_KundNr = tblOGA.Fields.Item("KNR");
            fld_OGA_Betvillk = tblOGA.Fields.Item("BVK");
            fld_OGA_OurRef = tblOGA.Fields.Item("VRF");
            fld_OGA_ErRef = tblOGA.Fields.Item("ERF");
            fld_OGA_LevVilk = tblOGA.Fields.Item("LVK");
            fld_OGA_LevSatt = tblOGA.Fields.Item("LSE");
            fld_OGA_Saljare = tblOGA.Fields.Item("SLJ");
            fld_OGA_Valuta = tblOGA.Fields.Item("VAL");
            fld_OGA_MomsKod = tblOGA.Fields.Item("MOM");
            fld_OGA_Prislista = tblOGA.Fields.Item("PRL");

            fld_OGR_OrderNr = tblOGR.Fields.Item("ONR");
            fld_OGR_RadNr = tblOGR.Fields.Item("RDC");
            fld_OGR_ArtNr = tblOGR.Fields.Item("ANR");
            fld_OGR_Benamning = tblOGR.Fields.Item("BEN");
            fld_OGR_BenamningExtra = tblOGR.Fields.Item("BNX");
            fld_OGR_UrsprungligtAntal = tblOGR.Fields.Item("ORA");
            fld_OGR_LagerNr = tblOGR.Fields.Item("LAG");
            fld_OGR_LevereratAntal = tblOGR.Fields.Item("TLA");
            fld_OGR_LeveransFlagga = tblOGR.Fields.Item("LVF");
            fld_OGR_LevTid = tblOGR.Fields.Item("LDT");
            fld_OGR_Reservationskod = tblOGR.Fields.Item("RES");
            fld_OGR_Pris = tblOGR.Fields.Item("PRI");
            fld_OGR_Fakturaflagga = tblOGR.Fields.Item("FAF");
            fld_OGR_MomsKod = tblOGR.Fields.Item("MOM");
            fld_OGR_Enhet = tblOGR.Fields.Item("ENH");
            fld_OGR_OHrab = tblOGR.Fields.Item("HRF");
            fld_OGR_RadRab = tblOGR.Fields.Item("RAB");
            fld_OGR_Belaggning = tblOGR.Fields.Item("BLF");

            fld_OrderRadText_OrderNr = tblOGK.Fields.Item("ONR");
            fld_OrderRadText_RadNr = tblOGK.Fields.Item("RDC");
            fld_OrderRadText_Sekvens = tblOGK.Fields.Item("SQC");
            fld_OrderRadText_Text = tblOGK.Fields.Item("TX1");
            fld_OrderRadText_OBfl = tblOGK.Fields.Item("OBF");
            fld_OrderRadText_PLfl = tblOGK.Fields.Item("PLF");
            fld_OrderRadText_FSfl = tblOGK.Fields.Item("FSF");
            fld_OrderRadText_FAfl = tblOGK.Fields.Item("FAF");

            fld_OGC_OrderNr = tblOGC.Fields.Item("ONR");
            fld_OGC_CH6 = tblOGC.Fields.Item("CH6");
            fld_OGC_CH3 = tblOGC.Fields.Item("CH3");
            fld_OGC_CH4 = tblOGC.Fields.Item("CH4");

            fld_OGF_OrderNr = tblOGF.Fields.Item("ONR");
            fld_OGF_Text1 = tblOGF.Fields.Item("TX1");
            fld_OGF_Text2 = tblOGF.Fields.Item("TX2");
            fld_OGF_ValfriInfo = tblOGF.Fields.Item("TX3");
            fld_OGF_FakturKontakt = tblOGF.Fields.Item("ERF");

            fld_OGL_OrderNr = tblOGL.Fields.Item("ONR");
            fld_OGL_Namn = tblOGL.Fields.Item("NAM");
            fld_OGL_Adress1 = tblOGL.Fields.Item("AD1");
            fld_OGL_Adress2 = tblOGL.Fields.Item("AD2");
            fld_OGL_Adress3 = tblOGL.Fields.Item("AD3");
            fld_OGL_Ort = tblOGL.Fields.Item("ORT");
            fld_OGL_Land = tblOGL.Fields.Item("LND");

            fld_AGA_ArtikelNr = tblAGA.Fields.Item("ANR");
            fld_AGA_Benamning = tblAGA.Fields.Item("BEN");
            fld_AGA_TillvKopKod = tblAGA.Fields.Item("TVK");
            fld_AGA_Saldohanteras = tblAGA.Fields.Item("SUS");
            fld_AGA_Ledtid = tblAGA.Fields.Item("NLT");
            fld_AGA_Saldo = tblAGA.Fields.Item("LGA");
            fld_AGA_Vikt = tblAGA.Fields.Item("VPE");
            fld_AGA_VIP = tblAGA.Fields.Item("VIP");
            fld_AGA_Pris = tblAGA.Fields.Item("PRI");
            fld_AGA_Decimaler = tblAGA.Fields.Item("ADE");
            fld_AGA_Variantkod = tblAGA.Fields.Item("VKD");
            fld_AGA_ModellLangd = tblAGA.Fields.Item("HVL");
            fld_AGA_Flerlager = tblAGA.Fields.Item("MLG");
            fld_AGA_Kod3 = tblAGA.Fields.Item("KD3");
            fld_AGA_Kategori = tblAGA.Fields.Item("KAT");

            fld_AGL_Saldo = tblAGL.Fields.Item("LGA");

            fld_AGS_StrukturArtikel = tblAGS.Fields.Item("OAR");
            fld_AGS_UnderArtikel = tblAGS.Fields.Item("UAR");

            fld_KA_namn = tblKA.Fields.Item("NAM");
            fld_KA_Adress1 = tblKA.Fields.Item("AD1");
            fld_KA_Adress2 = tblKA.Fields.Item("AD2");
            fld_KA_Ort = tblKA.Fields.Item("ORT");
            fld_KA_Land = tblKA.Fields.Item("LND");
            fld_KA_GodsPostnr = tblKA.Fields.Item("GPN");
            fld_KA_TX5 = tblKA.Fields.Item("TX5");
            fld_KA_VIPkund = tblKA.Fields.Item(order.KundFaltVIP);
            fld_KA_Num3 = tblKA.Fields.Item("NM3");
            fld_KA_Kod5 = tblKA.Fields.Item("KD5");

            fld_KB_AdressExtra = tblKB.Fields.Item("AD3");

            fld_TA_Key = tblTA.Fields.Item("KEY");
            fld_TA_Text = tblTA.Fields.Item("TX1");

            fld_APD_Prislisteid = tblAPD.Fields.Item("PRL");
            fld_APD_KampanjLagstaPris = tblAPD.Fields.Item("KLP");

            fld_KP_Pris = tblKP.Fields.Item("NU2");
            fld_KP_Anmarkning = tblKP.Fields.Item("TX1");

            // Bevakning av knappar och fält i formuläret
            BtnNyOrder = ComponentsOrder.Item("actInsert");
            BtnNyOrder2 = ComponentsOrder.Item("plusBitBtn");
            BtnOrderKlar = ComponentsOrder.Item("OrderKlarBitBtn");
            BtnLevSatt = ComponentsOrder.Item("levsattButton");

            Form_OrderHuvud_OrderTyp = ComponentsOrder.Item("ogaOtyMcEdit");
            Form_OrderHuvud_OrderNr = ComponentsOrder.Item("onrEdit");
            Form_OrderHuvud_OrderDatum = ComponentsOrder.Item("ogaOdtMcEdit");
            Form_OrderHuvud_OrderTid = ComponentsOrder.Item("ogaOklMcEdit");
            Form_OrderHuvud_LevTid = ComponentsOrder.Item("ogaBltMcEdit");
            Form_OrderHuvud_Reservationskod = ComponentsOrder.Item("ogaResMcEdit");
            Form_OrderHuvud_Text2 = ComponentsOrder.Item("ogfTx2McEdit");
            Form_OrderHuvud_LevSatt = ComponentsOrder.Item("ogaLseMcEdit");
            Form_OrderHuvud_Namn = ComponentsOrder.Item("levnamnEdit");

            Form_OrderRad_RadNr = ComponentsOrder.Item("oradEdit");
            Form_OrderRad_LevTid = ComponentsOrder.Item("ogrLdtMcEdit");
            Form_OrderRad_Antal = ComponentsOrder.Item("ogrOraMcEdit");
            Form_OrderRad_ArtNr = ComponentsOrder.Item("ogrAnrMcEdit");
            Form_OrderRad_Reservationskod = ComponentsOrder.Item("ogrResMcEdit");
            Form_OrderRad_Pris = ComponentsOrder.Item("ogrPriMcEdit");
            Form_OrderRad_LagerNr = ComponentsOrder.Item("ogrLagMcEdit");
            Form_OrderRad_Textsesong = ComponentsOrder.Item("lblogr2mcEdit");
            Form_OrderRad_Sesong = ComponentsOrder.Item("ogr2SesMcEdit");
            Form_OrderRad_BtnSesong = ComponentsOrder.Item("ogr2SesLookupBtn");
            Form_OrderRad_Benamning = ComponentsOrder.Item("ogrBenMcEdit");
            Form_OrderRad_ExtraBen = ComponentsOrder.Item("ogrBnxMcEdit");
            Form_OrderRad_RabattKod = ComponentsOrder.Item("ogrRbkMcEdit");
            Form_OrderRad_Rabatt = ComponentsOrder.Item("ogrRabMcEdit");
            Form_OrderRad_Revision = ComponentsOrder.Item("mceOgrREV");
            Form_OrderRad_Enh = ComponentsOrder.Item("ogrEnhMcEdit");
            Form_OrderRad_Fakturaflagga = ComponentsOrder.Item("ogrFaf2McEdit");

            DS_OrderRad_Levtid = dsOGR.Fields.Item("LDT");
            DS_OrderRad_Antal = dsOGR.Fields.Item("ORA");
            DS_OrderRad_Reservationskod = dsOGR.Fields.Item("RES");

            Form_OrderHuvud_KundNr = ComponentsOrder.Item("knrEdit");
            #endregion tabeller och fält

            GarpOrder.Components.Item("ogrLagMcEdit").TabStop = false;

            // DTO's
            OrderHead = new CustomerOrder(GarpOrder);
            OrderRow = new CustomerOrderRow(GarpOrder);
            OrderFreight = new CustomerOrderFreight(GarpOrder);
            Item = new Item(GarpOrder);
            Customer = new GarpDb.Customer(GarpOrder);
            PurchaseOrderRow = new PurchaseOrderRow(GarpOrder);
            PurchaseOrderHead = new PurchaseOrderHead(GarpOrder);
            BaseTable = new BaseTable(GarpOrder);

            SetUpOH();
            SetUpOR();
        }

        private void SetUpOH()
        {
            try
            {
                // Fliken Orderhuvud
                ComponentsOrder.BaseComponent = "TabSheet1";

                int top = ComponentsOrder.Item("ogfTx2McEdit").Top + 10;

                // Skapa upp fält för info om plocksedelsstatus
                LabelPLStatus = ComponentsOrder.AddEdit("LblPlstat");
                LabelPLStatus.Top = top + 35;//326;
                LabelPLStatus.Left = 530;
                LabelPLStatus.Width = 110; //120;
                LabelPLStatus.Height = 21; //25;
                LabelPLStatus.Color = 65535;
                LabelPLStatus.ReadOnly = true;
                LabelPLStatus.TabStop = false;
                LabelPLStatus.Text = "Plocklista utskriven";
                LabelPLStatus.Visible = false;

                // Skapa upp fält för info om plocksedelsstatus, rad 2
                LabelPLStatus2 = ComponentsOrder.AddEdit("LblPlstat2");
                LabelPLStatus2.Top = top + 59;
                LabelPLStatus2.Left = 530;
                LabelPLStatus2.Width = 110; //120;
                LabelPLStatus2.Height = 21; //25;
                LabelPLStatus2.Color = 65535;
                LabelPLStatus2.ReadOnly = true;
                LabelPLStatus2.TabStop = false;
                LabelPLStatus2.Text = "Väntar på förskott";
                LabelPLStatus2.Visible = false;

                // Skapa fält för info om automatleverans
                LabelX1Status = ComponentsOrder.AddLabel("LblX1stat");
                LabelX1Status.Top = top + 35;
                LabelX1Status.Left = 388;
                LabelX1Status.Text = "Ny order";

                // Skapa fält för info om betalningsvillkor
                LabelPaymentConditions = ComponentsOrder.AddEdit("PaymentInfo");
                LabelPaymentConditions.Top = top + 59;
                LabelPaymentConditions.Left = 388;
                LabelPaymentConditions.Width = 112; //120;
                LabelPaymentConditions.Height = 21; //25;
                LabelPaymentConditions.Color = 65535;
                LabelPaymentConditions.ReadOnly = true;
                LabelPaymentConditions.TabStop = false;
                LabelPaymentConditions.Text = ""; // "Förbetald via " + order.PrePaymentConditionsName;
                LabelPaymentConditions.Visible = false;

                // Information om flera lagernummer
                LabelWarehouses = ComponentsOrder.AddLabel("Warehouses");
                LabelWarehouses.Top = top + 85;
                LabelWarehouses.Left = 388;
                LabelWarehouses.Visible = false;

                // Lägg till fälttext för önskad leveranstid
                if (ComponentsOrder.Item(order.LblDesiredDeliveryText) == null)
                {
                    LabelDesiredDeliveryText = ComponentsOrder.AddLabel(order.LblDesiredDeliveryText);
                    LabelDesiredDeliveryText.Top = top + 29;
                    LabelDesiredDeliveryText.Left = 13;
                    LabelDesiredDeliveryText.Width = 40; //45;
                    LabelDesiredDeliveryText.Text = "Önskad leveranstid:";
                }

                // Lägg till fält för önskad leveranstid. Tabell OrderHuvudFrakt, fältet Frakt06
                if (ComponentsOrder.Item(order.LblDesiredDelivery) == null)
                {
                    LabelDesiredDelivery = ComponentsOrder.AddEdit(order.LblDesiredDelivery);
                    LabelDesiredDelivery.Top = top + 25;
                    LabelDesiredDelivery.Left = 120;
                    LabelDesiredDelivery.Width = 60; //65;
                    LabelDesiredDelivery.Height = 21;
                }

                // Fält för info om senaste krditkontrollen på kund
                if (ComponentsOrder.Item(order.LblDateCreditControlTxt) == null)
                {
                    LabelDateCreditControlText = ComponentsOrder.AddLabel(order.LblDateCreditControlTxt);
                    LabelDateCreditControlText.Top = top + 56;
                    LabelDateCreditControlText.Left = 12;
                    LabelDateCreditControlText.Text = "Senaste kreditkontroll:";
                }

                if (ComponentsOrder.Item(order.LblDateCreditControl) == null)
                {
                    LabelDateCreditControl = ComponentsOrder.AddLabel(order.LblDateCreditControl);
                    LabelDateCreditControl.Top = top + 56;
                    LabelDateCreditControl.Left = 135;
                    LabelDateCreditControl.Text = "------";
                }

                // knapp för justering leveranstid alla rader
                //if (ComponentsOrder.Item(order.ButtonSaldoTest) == null)
                //{
                //    BtnSaldoTest = ComponentsOrder.AddButton(order.ButtonSaldoTest);
                //    BtnSaldoTest.Top = top + 25;
                //    BtnSaldoTest.Left = 188; //195
                //    BtnSaldoTest.Width = 100; //110;
                //    BtnSaldoTest.Height = 40; //45;
                //    BtnSaldoTest.Text = "Justera leveranstid" + Environment.NewLine + "alla orderrader";
                //    BtnSaldoTest.TabStop = false;
                //    BtnSaldoTest.Visible = false;
                //}

                // knapp för justering leveranstid alla rader enligt PrioOrder

                BtnPrioOrderCheckAll = ComponentsOrder.AddButton("BtnPrioOrderCheckAll");
                BtnPrioOrderCheckAll.Top = top + 25;
                BtnPrioOrderCheckAll.Left = 188;
                BtnPrioOrderCheckAll.Width = 110;
                BtnPrioOrderCheckAll.Height = 40;
                BtnPrioOrderCheckAll.Text = "Justera alla ordrar" + Environment.NewLine + "enligt PrioOrder";
                BtnPrioOrderCheckAll.TabStop = false;
                BtnPrioOrderCheckAll.Visible = false;

                // knapp för förskott
                if (ComponentsOrder.Item("ButtonAdvancePay") == null)
                {
                    BtnAdvancePay = ComponentsOrder.AddButton("ButtonAdvancePay");
                    BtnAdvancePay.Top = top + 25;
                    BtnAdvancePay.Left = 300; // 310;
                    BtnAdvancePay.Width = 60;
                    BtnAdvancePay.Height = 40; // 35;
                    BtnAdvancePay.Text = "Förskott";
                    BtnAdvancePay.TabStop = false;
                    BtnAdvancePay.Visible = false;
                }

                // Fliken Leverans
                ComponentsOrder.BaseComponent = "Tabsheet5";

                //Skapa upp knapp "Leverera enl. plocklista"
                BtnLevEnlPlocklista = ComponentsOrder.AddButton("BtnLevEnlPlocklista");
                BtnLevEnlPlocklista.Text = "Leverera enl. plocklista";
                BtnLevEnlPlocklista.Width = 130; //140; //oComp.Item("BitBtn3").Width - 2;
                BtnLevEnlPlocklista.Height = 25; //ComponentsOrder.Item("levallaBitBtn").Height - 2;
                BtnLevEnlPlocklista.Top = 130; //ComponentsOrder.Item("levallaBitBtn").Top;
                BtnLevEnlPlocklista.Left = 402; //ComponentsOrder.Item("levallaBitBtn").Left;
                BtnLevEnlPlocklista.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fel vid initiering orderhuvud " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private string GetPaymentConditionsText(string paymentCondition)
        {
            try
            {
                if (!BaseTable.Table.Find("01" + paymentCondition))
                {
                    _ = MessageBox.Show("Payment condition '" + paymentCondition + "' not found in Garp");
                    return string.Empty;
                }

                return "Förbetald via: " + BaseTable.Text1;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error in GetPaymentConditionsText: " + e.Message);
            }
            return string.Empty;
        }

        public void SetUpOR()
        {
            try
            {
                #region släck fält

                // Dölj säsong på orderrad
                if (Form_OrderRad_Textsesong != null && Form_OrderRad_Textsesong.Visible == true) { Form_OrderRad_Textsesong.Visible = false; }
                if (Form_OrderRad_Sesong != null && Form_OrderRad_Sesong.Visible == true) { Form_OrderRad_Sesong.Visible = false; }
                if (Form_OrderRad_BtnSesong != null && Form_OrderRad_BtnSesong.Visible == true) { Form_OrderRad_BtnSesong.Visible = false; }

                #endregion släck fält

                // Välj flik för fält och knappar
                ComponentsOrder.BaseComponent = "TabSheet3";

                // Hoppa över fält vid inmatning
                GarpOrder.Components.EnterAsTab = true;

                Form_OrderRad_Benamning.TabStop = false;
                Form_OrderRad_ExtraBen.TabStop = false;
                Form_OrderRad_Enh.TabStop = false;
                Form_OrderRad_Pris.TabStop = false;
                Form_OrderRad_RabattKod.TabStop = false;
                Form_OrderRad_Rabatt.TabStop = false;
                Form_OrderRad_LevTid.TabStop = false;
                Form_OrderRad_Reservationskod.TabStop = false;
                Form_OrderRad_Revision.TabStop = false;

                // Visa versionsnummer
                if (ComponentsOrder.Item(order.LblVersion) == null)
                {
                    LabelVersion = ComponentsOrder.AddLabel(order.LblVersion);
                    LabelVersion.Top = 320;//400;
                    LabelVersion.Left = 8;
                    LabelVersion.Width = 55;
                    LabelVersion.Text = "Skogma forms version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build;
                }

                // Knapp för justering leveranstider alla rader om orderen har fler än en rad, ej leveread
                if (ComponentsOrder.Item(order.ButtonORSaldoTest) == null)
                {
                    //BtnOrderRadSaldoTest = ComponentsOrder.AddButton(order.ButtonORSaldoTest);
                    //BtnOrderRadSaldoTest.Top = 320;//400;
                    //BtnOrderRadSaldoTest.Left = 200;
                    //BtnOrderRadSaldoTest.Width = 100; // 160
                    //BtnOrderRadSaldoTest.Height = 40;
                    //BtnOrderRadSaldoTest.Text = "Justera leveranstid" + Environment.NewLine + "alla orderrader";
                    //BtnOrderRadSaldoTest.TabStop = false;
                    //BtnOrderRadSaldoTest.Visible = false;
                }

                if (ComponentsOrder.Item(frakt.ButtonFraktPris) == null)
                {
                    BtnFraktKostnad = ComponentsOrder.AddButton(frakt.ButtonFraktPris);
                    BtnFraktKostnad.Top = 320; //400;
                    BtnFraktKostnad.Left = 320;
                    BtnFraktKostnad.Width = 100; //150;
                    BtnFraktKostnad.Height = 40;
                    BtnFraktKostnad.Text = "Beräkna" + Environment.NewLine + "fraktkostnad";
                    BtnFraktKostnad.TabStop = false;
                    BtnFraktKostnad.Visible = false;
                }

                //int top = 210;
                //int left = 535;
                //int rowSpacing = 20;
                //int columnSpacing = 5;
                //int width = 50;
                //int color = ComponentsOrder.Item("artikelinfoEdit").Color;

                LblAntal = ComponentsOrder.AddLabel(StrAntal);
                LblAntal.Text = "Antal:";
                LblAntal.Top = Top;
                LblAntal.Left = Left;
                LblAntal.Visible = false;

                LblPris = ComponentsOrder.AddLabel(StrPris);
                LblPris.Text = "Pris:";
                LblPris.Top = Top;
                LblPris.Left = Left + Width + ColumnSpacing;
                LblPris.Visible = false;

                BracketQuantities = new Dictionary<string, List<IComponent>>();
                BracketsPrices = new Dictionary<string, List<IComponent>>();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Fel vid initiering orderrad " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void AfterScrollOGA()
        {
            try
            {
                LabelDateCreditControl.Text = "------";
                BtnAdvancePay.Visible = false;
                //BtnSaldoTest.Visible = false;
                LabelDesiredDelivery.Visible = false;
                LabelDesiredDeliveryText.Visible = false;
                LabelPLStatus.Visible = false;
                LabelPLStatus2.Visible = false;
                LabelX1Status.Visible = false;
                LabelPaymentConditions.Visible = false;
                LabelWarehouses.Visible = false;
                order.InternOrder = false;
                order.Levererad = true;

                if (!string.IsNullOrEmpty(dsOGA.Fields.Item("ONR").Value))
                {
                    order.OrderNr = dsOGA.Fields.Item("ONR").Value;

                    // Ej levererad
                    if (int.Parse(dsOGA.Fields.Item("LEF").Value) < 5)
                    {
                        order.Levererad = false;
                    }

                    // Test om internorder
                    if (dsOGA.Fields.Item("OTY").Value == "9")
                    {
                        order.InternOrder = true;
                    }

                    // EJ internorder
                    if (order.InternOrder == false)
                    {
                        // Visa datum för senaste kreditlimit
                        ShowCreditControlDate();

                        // Visa information om förbetald order
                        if (order.PrePaymentConditions?.Exists(x => x == dsOGA.Fields.Item("BVK").Value) ?? false)//dsOGA.Fields.Item("BVK").Value == order.PrePaymentConditions)
                        {
                            LabelPaymentConditions.Text = GetPaymentConditionsText(dsOGA.Fields.Item("BVK").Value);
                            if (!string.IsNullOrEmpty(LabelPaymentConditions.Text))
                            {
                                LabelPaymentConditions.Visible = true;
                            }
                            else
                            {
                                LabelPaymentConditions.Visible = false;
                            }
                        }
                    }

                    // Ej levererad
                    if (order.Levererad == false)
                    {

                        // Lägg till info för önskad leveranstid
                        if (AddOGC())
                        {
                            LabelDesiredDelivery.Visible = true;
                            LabelDesiredDeliveryText.Visible = true;
                        }

                        // Spara info om lagernummer från rader till huvudet
                        WarehouseNrToOrderHead();

                        // Visar lagernumren från raderna om fler än ett
                        if (!string.IsNullOrEmpty(fld_OGC_CH4.Value) && fld_OGC_CH4.Value.Length > 1)
                        {
                            LabelWarehouses.Text = "Flera lagernummer: " + String.Join<char>(", ", fld_OGC_CH4.Value);
                            LabelWarehouses.Visible = true;
                        }

                        // EJ internorder
                        if (order.InternOrder == false)
                        {
                            // Visa knapp för förskott
                            ShowButtonAdvancePay();

                            // Hämta mobilnumret från kund och lägg på order
                            if (tblKA.Find(dsOGA.Fields.Item("KNR").Value) && !string.IsNullOrEmpty(fld_KA_TX5.Value))
                            {
                                GetMobileNumber();

                            }

                            // Visa knapp för justering leveranstider alla rader om orderen har fler än en rad och ej levererad
                            tblOGR.Find(order.OrderNr);
                            tblOGR.Next();
                            tblOGR.Next();
                            if (fld_OGR_OrderNr.Value == order.OrderNr)
                            {
                                //BtnSaldoTest.Visible = true;
                                BtnPrioOrderCheckAll.Visible = true;
                            }
                        }

                        // Visa status för plocksedel
                        switch (dsOGA.Fields.Item("PLF").Value.ToUpper())
                        {
                            case "5":
                                LabelPLStatus.Visible = true;
                                LabelPLStatus.Text = "Plocklista utskriven";
                                break;

                            case "V":
                                LabelPLStatus.Visible = true;
                                LabelPLStatus.Text = "Status väntläge";
                                tblOGK.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + "  0");
                                tblOGK.Next();

                                // Sök information om ordern väntar på förskott
                                while (tblOGK.Eof != true && fld_OrderRadText_OrderNr.Value == order.OrderNr && fld_OrderRadText_RadNr.Value == "  0")
                                {
                                    if (fld_OrderRadText_Text.Value?.Length > 28 && Verktyg.Left(fld_OrderRadText_Text.Value, 29) == "Förskott debiteras på order: ")
                                    {
                                        LabelPLStatus2.Visible = true;
                                        break;
                                    }
                                    tblOGK.Next();
                                }
                                break;

                            default:
                                LabelPLStatus.Visible = false;
                                break;
                        }

                        // Visa status för automatleverans
                        LabelX1Status.Visible = true;
                        switch (dsOGA.Fields.Item("X1F").Value)
                        {
                            case "1":
                                LabelX1Status.Text = "Ny, ej kontrollerad";
                                break;
                            case "2":
                                LabelX1Status.Text = "Leverans av hel order";
                                break;
                            case "4":
                                LabelX1Status.Text = "Delleverans";
                                break;
                            case "9":
                                LabelX1Status.Text = "Ej leverans idag";
                                break;
                            default:
                                LabelX1Status.Text = "Ny order";
                                break;
                        }
                    }
                }

                // Om det är en internorder ska OB- och PL-flaggorna bli 1
                InternalOrder();
            }
            catch (Exception ex)
            {
                MessageBox.Show("After scroll OGA " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void InternalOrder()
        {
            try
            {
                // Sätt OB och PL flaggor till 1 på ny internorder
                string ordertyp = dsOGA.Fields.Item("OTY").Value;
                string orderbekr = dsOGA.Fields.Item("OBF").Value;
                if (ordertyp == "9" && orderbekr == "0")
                {
                    _ = tblOGR.Find(dsOGA.Fields.Item("ONR").Value); // Ny tillverkningsorder
                    tblOGR.Next();
                    if (fld_OGR_OrderNr.Value != dsOGA.Fields.Item("ONR").Value)
                    {
                        dsOGA.Fields.Item("OBF").Value = "1";
                        dsOGA.Fields.Item("PLF").Value = "1";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i InternalOrder-funktionen: " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void AfterScrollOGR()
        {
            try
            {
                if (!string.IsNullOrEmpty(Form_OrderRad_RadNr.Text))
                {
                    order.ArtNr = Form_OrderRad_ArtNr.Text;
                    order.RadNr = Form_OrderRad_RadNr.Text;
                    order.Order_Rad = Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(order.RadNr, 3);

                    // Test om ordern är levererad
                    tblOGA.Find(order.OrderNr);
                    if (string.Compare(fld_OGA_LeveransFlagga.Value, "5") < 0)
                    {
                        order.Levererad = false;
                    }
                    else
                    {
                        order.Levererad = true;
                    }

                    // Visa knapp för Leverera enligt plocklista
                    if (order.Levererad == true)
                    {
                        BtnLevEnlPlocklista.Visible = false;
                    }
                    else
                    {
                        BtnLevEnlPlocklista.Visible = true; ;
                    }

                    // Knapp för justering leveranstider alla rader om orderen har fler än en rad, ej leveread
                    tblOGR.Find(order.OrderNr);
                    tblOGR.Next();
                    tblOGR.Next();
                    if (
                        fld_OGR_OrderNr.Value == order.OrderNr
                        && order.InternOrder == false
                        && order.Levererad == false
                        )
                    {
                        //BtnOrderRadSaldoTest.Visible = true;
                    }
                    else
                    {
                        // Släck knapp
                        //BtnOrderRadSaldoTest.Visible = false;
                    }

                    // Knapp för beräkning fraktkostnad
                    tblOGR.Find(order.OrderNr);
                    tblOGR.Next();
                    if (
                        frakt.BeraknaFrakt
                        && fld_OGR_OrderNr.Value == order.OrderNr
                        && order.InternOrder == false
                        && order.Levererad == false
                        && fld_OGA_OBflagga.Value != order.OrderEjKlarFlagga
                        )
                    {
                        BtnFraktKostnad.Visible = true;
                    }
                    else
                    {
                        // Släck knapp
                        BtnFraktKostnad.Visible = false;
                    }

                }

                if (!string.IsNullOrEmpty(dsOGA.Fields.Item("PRL").Value))
                {

                    if (tblAGA.Find(dsOGR.Fields.Item("ANR").Value))
                    {
                        if (!int.TryParse(fld_AGA_ModellLangd.Value, out int modellLangd))
                        {
                            modellLangd = 13;
                        }

                        tblAPD.Find("K" + dsOGA.Fields.Item("PRL").Value);
                        if (!string.IsNullOrEmpty(fld_APD_KampanjLagstaPris.Value))
                        {
                            List<CampaignBracketPricing> priceBrackets = GetCampaignBracketPricing(dsOGR.Fields.Item("ANR").Value, fld_APD_KampanjLagstaPris.Value, modellLangd);//ModellLangd); // Skickar med modellängden för extra läsning mot modellen
                            DisplayPriceBrackets(dsOGR.Fields.Item("ANR").Value, priceBrackets);
                        }
                        else
                        {
                            LblAntal.Visible = false;
                            LblPris.Visible = false;
                        }
                    }
                }
                else
                {
                    _ = ResetPriceBracketControlls(dsOGR.Fields.Item("ANR").Value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("After scroll OGR " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void ButtonClik()
        {
            try
            {
                // Knapp för ny order
                if (ComponentsOrder.CurrentField == BtnNyOrder.Name || ComponentsOrder.CurrentField == BtnNyOrder2.Name)
                {
                    order.BtnNewOrder = true;
                    order.tmpResKodSatt = false;
                }
                else
                {
                    order.BtnNewOrder = false;
                }

                #region Leverera enl. plocklista

                if (ComponentsOrder.CurrentField.Equals("BtnLevEnlPlocklista"))
                {
                    LevEnlPlock();
                }

                #endregion

                #region Saldotest alla rader och sätt sämsta leveranstid på alla rader med reservationskod 1

                // Knapp för uppdatering leveranstid alla orderrader
                //if (
                //(ComponentsOrder.Item(order.ButtonSaldoTest) != null && ComponentsOrder.CurrentField == BtnSaldoTest.Name)
                //|| (ComponentsOrder.Item(order.ButtonORSaldoTest) != null && ComponentsOrder.CurrentField == BtnOrderRadSaldoTest.Name)
                //)
                //{
                //    try
                //    {
                //        TestAllLines(false, ""); // !!!
                //    }
                //    catch (Exception ex)
                //    {
                //        MessageBox.Show("Saldotest alla rader " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //    }
                //}
                #endregion Sätt sämsta leveranstid på alla rader med reservationskod 1

                #region OrderKlar
                // Knapp OrderKlar eller beräkna frakt
                if (
                    frakt.BeraknaFrakt
                    && (ComponentsOrder.CurrentField == BtnOrderKlar.Name || ComponentsOrder.CurrentField == frakt.ButtonFraktPris)
                    )
                {
                    try
                    {
                        SendKeys.SendWait("{F5}");
                        order.OrderVikt = 0;
                        frakt.FraktRad = "0";
                        frakt.FraktPris = 0;
                        frakt.FraktAF = false;

                        // Loopa alla orderrader och summera artikelvikter
                        tblOGR.Find(order.OrderNr);
                        tblOGR.Next();
                        while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == order.OrderNr)
                        {
                            if (tblAGA.Find(fld_OGR_ArtNr.Value))
                            {
                                order.OrderVikt = order.OrderVikt + (Convert.ToDouble(fld_AGA_Vikt.Value.Replace(".", ",")) * Convert.ToDouble(fld_OGR_UrsprungligtAntal.Value.Replace(".", ",")));

                                // Spara orderraden för fraktartikeln
                                if (fld_OGR_ArtNr.Value == frakt.FraktArtikel)
                                {
                                    frakt.FraktRad = fld_OGR_RadNr.Value;
                                    frakt.FraktAF = false;
                                }
                                // Spara orderraden för fraktartikeln ÅF
                                if (fld_OGR_ArtNr.Value == frakt.FraktArtikelAF)
                                {
                                    frakt.FraktRad = fld_OGR_RadNr.Value;
                                    frakt.FraktAF = true;
                                }
                            }
                            tblOGR.Next();
                        }

                        // Loopa fraktpristabellen, hämta priset för aktuell vikt
                        if (frakt.FraktAF == false)
                        {
                            for (int i = 0; i < frakt.ViktLista.Count; i++)
                            {
                                if (order.OrderVikt < frakt.ViktLista[i].Item1 || order.OrderVikt == frakt.ViktLista[i].Item1)
                                {
                                    frakt.FraktPris = frakt.ViktLista[i].Item2;
                                    break;
                                }

                                // Om ordervikten är högre än sista posten i listan spara högsta priset
                                if (i + 1 == frakt.ViktLista.Count)
                                {
                                    frakt.FraktPris = frakt.ViktLista[i].Item2;
                                }
                            }
                        }
                        else
                        // Loopa fraktpristabellen för ÅF, hämta priset för aktuell vikt
                        {
                            for (int i = 0; i < frakt.ViktListaAF.Count; i++)
                            {
                                if (order.OrderVikt < frakt.ViktListaAF[i].Item1 || order.OrderVikt == frakt.ViktListaAF[i].Item1)
                                {
                                    frakt.FraktPris = frakt.ViktListaAF[i].Item2;
                                    break;
                                }

                                // Om ordervikten är högre än sista posten i listan spara högsta priset
                                if (i + 1 == frakt.ViktListaAF.Count)
                                {
                                    frakt.FraktPris = frakt.ViktListaAF[i].Item2;
                                }
                            }
                        }

                        // Om ordern har vikt och Rad för frakt finns
                        if (order.OrderVikt != 0 && frakt.FraktRad != "0")
                        {
                            // Ange fraktkostnaden på orderraden för frakt
                            tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(frakt.FraktRad, 3));
                            fld_OGR_Pris.Value = frakt.FraktPris.ToString();
                            tblOGR.Post();
                            SendKeys.SendWait("{F5}");
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("OrderKlar." + Environment.NewLine + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                #endregion OrderKlar

                if (ComponentsOrder.CurrentField == BtnLevSatt.Name)
                {
                    order.tmpLevSattBtn = Form_OrderHuvud_LevSatt.Text;
                    order.BtnLevSattKlick = true;
                }

                // Förskott
                if (ComponentsOrder.CurrentField == BtnAdvancePay.Name)
                {
                    SendKeys.SendWait("{F5}");
                    AdvancePay();
                }

                if (ComponentsOrder.CurrentField == BtnPrioOrderCheckAll.Name)
                {
                    string anr = dsOGR.Fields.Item("ANR").Value;

                    List<string> strings = new List<string>()
                    {
                        // "allaOrder"
                    };

                    PrioOrder.PrioOrder prioorder = new PrioOrder.PrioOrder(strings.ToArray(), GarpOrder);
                    SendKeys.SendWait("{F5}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Button klick " + ex.Message + Environment.NewLine + "Source: " + ex.Source + "Stack trace:" + Environment.NewLine + ex.StackTrace, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void FieldEnter()
        {
            try
            {
                #region Ny order via plusknappen

                //Ny order skapad via plus-knappen, slå ihop för och efternamn och sätt leveranstiden
                if (order.BtnNewOrder && order.TestNewOrderBtn)
                {

                    if (order.TestNewOrderBtn)
                    {
                        SetName();
                    }

                    tblOGR.Find(order.OrderNr);
                    tblOGR.Next();
                    if (
                        Form_OrderHuvud_LevTid.Text != order.OnskadLeveranstid &&
                        (fld_OGR_OrderNr.Value != order.OrderNr || (fld_OGR_OrderNr.Value == order.OrderNr && Int32.Parse(fld_OGR_RadNr.Value) > 250))
                        )
                    {
                        dsOGA.Fields.Item("BLT").Value = DeliveryDate();
                    }

                    order.BtnNewOrder = false;
                    SendKeys.SendWait("{F5}");
                }
                #endregion Ny order via plusknappen

                #region Test om ordertyp är ändrad
                //Aktuellt fält = Ordertyp
                if (ComponentsOrder.CurrentField == Form_OrderHuvud_OrderTyp.Name)
                {
                    order.tmpOrderTyp = Form_OrderHuvud_OrderTyp.Text;
                }
                #endregion Test om ordertyp är ändrad

                #region Nytt paket har registrerats

                // OrderRad.FAF (Fakturaflagga)
                // p (lilla P) = paket
                // i = paketrad
                // 1 = normal rad

                // HÄR LANDAR MAN EFTER PAKETARTIKEL HAR LAGT TILL ORDERRADER

                // Backa en rad om man redan hunnit skapa nästa rad
                tblOGR.Find(order.Order_Rad);
                tblOGR.Prior();

                if (fld_OGR_OrderNr.Value == order.OrderNr
                    && (fld_OGR_Fakturaflagga.Value == "i" || fld_OGR_Fakturaflagga.Value == "p") // ingående i paket eller paketartikel
                    && order.Paketstart == true
                    && Form_OrderHuvud_OrderTyp.Text != "0"
                    && order.InternOrder == false)
                {
                    tblOGR.Find(order.Order_Rad); // starta på paketslut
                    order.NyttPaket = true;
                    if (!string.IsNullOrEmpty(fld_OGR_ArtNr.Value))
                    {
                        order.tmpPaketSlutOrderRad = fld_OGR_RadNr.Value;
                    }
                    else
                    {
                        order.tmpPaketSlutOrderRad = order.RadNr; // Man har klickat på ny rad
                    }

                    order.TextNewDelTime = string.Empty;
                    order.TextPurcase = string.Empty;

                    order.IndexNrPaket = tblOGR.Bookmark; // Spara bokmärke för paketslut

                    // Gå till paketstart och loopa paketraderna för test om lagernummer skall bytas
                    tblOGR.Find(order.OrderNr.PadRight(6) + order.tmpPaketOrderRad.PadLeft(3));
                    DefaultWarehouse(fld_OGR_ArtNr.Value);
                    tblOGR.GotoBookmark(order.IndexNrPaket);

                    while (tblOGR.Bof != true && fld_OGR_OrderNr.Value == order.OrderNr && fld_OGR_RadNr.Value.PadLeft(3) != order.tmpPaketOrderRad.PadLeft(3)) // Loopa tillbaka till paketartikeln
                    {
                        if (!string.IsNullOrEmpty(fld_OGR_ArtNr.Value))
                        {
                            // Här loppas alla ingående i paket, gör saldotest på de som saldohanteras
                            order.TestArtNr = fld_OGR_ArtNr.Value;
                            order.LevTidOR = fld_OGR_LevTid.Value;
                            order.NyttLevDatum = order.LevTidOR;
                            order.LagerNr = "1";
                            if (!string.IsNullOrEmpty(fld_OGR_LagerNr.Value))
                            {
                                order.LagerNr = fld_OGR_LagerNr.Value;
                            }

                            Saldotest();

                            // Om nytt datum är tidigare än paketartikeln sätt samma datum som paketartikeln
                            if (string.Compare(order.NyttLevDatum, order.tmpPaketLevTid) < 0 || order.NyttLevDatum == order.tmpPaketLevTid)
                            {
                                fld_OGR_LevTid.Value = order.tmpPaketLevTid;
                                tblOGR.Post();
                            }

                            // Uppdatera paketraden med nya datumet
                            if (string.Compare(order.NyttLevDatum, order.tmpPaketLevTid) > 0)
                            {
                                fld_OGR_LevTid.Value = order.NyttLevDatum;
                                tblOGR.Post();

                                // flytta fram datum för att spara säsmsta datumet för paketet
                                order.tmpPaketLevTid = order.NyttLevDatum;

                                // Börja om i loopen för ny saldotest
                                tblOGR.GotoBookmark(order.IndexNrPaket);
                            }
                        }
                        tblOGR.Prior();
                    }

                    // Uppdatera datum på paketartikeln
                    tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(order.tmpPaketOrderRad, 3));
                    fld_OGR_LevTid.Value = order.tmpPaketLevTid;
                    tblOGR.Post();

                    // Uppdatera datum paketslut
                    tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(order.tmpPaketSlutOrderRad, 3));
                    fld_OGR_LevTid.Value = order.tmpPaketLevTid;
                    tblOGR.Post();

                    if (order.TextNewDelTime != string.Empty)
                    {
                        MessageBox.Show("Paketet " + order.tmpPaketArtikel + " skickas: " + order.tmpPaketLevTid, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    order.Paketstart = false;
                    order.NyttPaket = false;
                    Form_OrderHuvud_OrderNr.SetFocus();
                }
                #endregion Nytt paket har registrerats

                #region Bevaka fält, spara värdet
                //Aktuellt fält = Önskad leveranstid, spara värdet
                if (ComponentsOrder.Item(order.LblDesiredDelivery) != null && ComponentsOrder.CurrentField == ComponentsOrder.Item(order.LblDesiredDelivery).Name)
                {
                    order.DesiredDeliveryTmp = ComponentsOrder.Item(order.LblDesiredDelivery).Text;
                }

                // OrderHuvud leveranstid
                if (ComponentsOrder.CurrentField == Form_OrderHuvud_LevTid.Name)
                {
                    order.tmpOrderHuvudLeveranstid = Form_OrderHuvud_LevTid.Text;
                }

                // OrderHuvud reservationskod
                if (ComponentsOrder.CurrentField == Form_OrderHuvud_Reservationskod.Name)
                {
                    order.tmpOrderHuvudReservationskod = Form_OrderHuvud_Reservationskod.Text;
                }

                // Leveranssätt
                if (ComponentsOrder.CurrentField == Form_OrderHuvud_LevSatt.Name && order.BtnLevSattKlick == false)
                {
                    order.tmpLevSatt = Form_OrderHuvud_LevSatt.Text;
                }

                //Aktuellt fält = Orderrad leveranstid, spara värdet
                if (ComponentsOrder.CurrentField == Form_OrderRad_LevTid.Name)
                {
                    order.tmpORLevtid = Form_OrderRad_LevTid.Text;
                }

                //Aktuellt fält = Orderrad antal, spara värdet
                if (ComponentsOrder.CurrentField == Form_OrderRad_Antal.Name)
                {
                    order.tmpRadAntal = Form_OrderRad_Antal.Text;
                }

                #endregion Bevaka fält, spara värdet

            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldEnter " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void FieldExit()
        {
            #region Ny order, sätt leveranstid och slå ihop för och efternamn
            try
            {
                //Aktuellt fält = Ordernr
                if (ComponentsOrder.CurrentField == Form_OrderHuvud_OrderNr.Name && !string.IsNullOrEmpty(dsOGA.Fields.Item("ONR").Value))
                {
                    tblOGR.Find(dsOGA.Fields.Item("ONR").Value);
                    tblOGR.Next();
                    if (fld_OGR_OrderNr.Value != order.OrderNr)
                    {
                        // Sätt ihop för och efternamn
                        SetName();

                        if (Form_OrderHuvud_LevTid.Text != order.OnskadLeveranstid)
                        {
                            // Ge förslag på leveranstid
                            Form_OrderHuvud_LevTid.Text = DeliveryDate();
                        }

                        SendKeys.SendWait("{F5}");
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldExit, spara levtid orderhuvud " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            #endregion

            // Fältet artikelnummer
            if (ComponentsOrder.CurrentField == Form_OrderRad_ArtNr.Name)
            {
                // Sätt förvalt lagernummer, hämtas från artikel om antal = 0 (borde vara ny rad)
                if (double.Parse(dsOGR.Fields.Item("ORA").Value.Replace(".", ",")) == 0)
                {
                    tblOGR.Find(order.Order_Rad);
                    DefaultWarehouse(dsOGR.Fields.Item("ANR").Value);
                }

                //Form_OrderRad_Antal.SetFocus();
            }

            #region Ordertyp bytt från offert till order
            // Uppdatera orderdatum till nu om offert byts till order
            try
            {
                if (
                    ComponentsOrder.CurrentField == Form_OrderHuvud_OrderTyp.Name
                    && Form_OrderHuvud_OrderTyp.Text != "0"
                    && order.tmpOrderTyp == "0"
                    && Form_OrderHuvud_OrderTyp.Text != order.tmpOrderTyp
                    )
                {
                    // Sätt orderdatum och klockslag till nu
                    Form_OrderHuvud_OrderDatum.Text = DateTime.Now.ToString("yyMMdd");
                    Form_OrderHuvud_OrderTid.Text = DateTime.Now.ToString("HHmm");

                    // Uppdatera levernstid på orderhuvud och Önskat leveransdatum
                    string Levtid = DeliveryDate();

                    // Skapa tabellpost för OGC
                    if (!tblOGC.Find(order.OrderNr))
                    {
                        tblOGC.Insert();
                        fld_OGC_OrderNr.Value = order.OrderNr;
                    }
                    else
                    {
                        tblOGC.Find(order.OrderNr);
                    }

                    fld_OGC_CH6.Value = Levtid;
                    tblOGC.Post();

                    Form_OrderHuvud_LevTid.Text = Levtid;

                    // Saldotest på alla orderrader
                    TestAllLines(true, "Ny order!" + Environment.NewLine + "Skall saldotest göras på alla rader?");
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Offert bytt till order " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            #endregion Ordertyp bytt från offert till order

            #region OrderHuvud leveranstid ändrad
            try
            {
                // OrderHuvud leveranstid ändrat
                if (ComponentsOrder.CurrentField == Form_OrderHuvud_LevTid.Name && Form_OrderHuvud_LevTid.Text != order.tmpOrderHuvudLeveranstid)
                {
                    tblOGA.Find(order.OrderNr);
                    if (fld_OGA_LeveransFlagga.Value != "5")
                    {
                        SendKeys.SendWait("{F5}");
                        TestAllLines(false, "");
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("OrderHuvud leveranstid ändrad " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            #endregion OrderHuvud leveranstid ändrad

            #region Önskad leveranstid ändrad
            try
            {
                //Aktuellt fält = Önskad leveranstid och nytt värde har angetts
                if (
                    ComponentsOrder.Item(order.LblDesiredDelivery) != null
                    && ComponentsOrder.CurrentField == ComponentsOrder.Item(order.LblDesiredDelivery).Name
                    && ComponentsOrder.Item(order.LblDesiredDelivery).Text != order.DesiredDeliveryTmp // Ändring har gjorts på önskad leveranstid
                    )
                {
                    if (string.Compare(ComponentsOrder.Item(order.LblDesiredDelivery).Text, DateTime.Now.ToString("yyMMdd")) < 0)
                    {
                        MessageBox.Show("Kan ej ange dåtid!", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        // Spara nya värdet till OrderHuvudFrakt, Frakt06
                        if (tblOGC.Find(order.OrderNr))
                        {
                            fld_OGC_CH6.Value = LabelDesiredDelivery.Text;
                            tblOGC.Post();

                            Form_OrderHuvud_LevTid.Text = LabelDesiredDelivery.Text;

                            TestAllLines(false, "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldExit, ny önskad leveranstid " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            #endregion Önskad leveranstid ändrad

            #region OrderHuvud reservationskod ändrad
            //try
            {
                // TODO: Troligen behöver test göras om reskod är ändrat via uppslagsknappen

                // GARP LÄGGER INGET VÄRDE I Form_OrderHuvud_Reservationskod.Text FÖRSTA GÅNGEN!!!

                // OrderHuvud reservationskod ändrat
                if (ComponentsOrder.CurrentField == Form_OrderHuvud_Reservationskod.Name && Form_OrderHuvud_Reservationskod.Text != order.tmpOrderHuvudReservationskod)
                {
                    if (fld_OGA_LeveransFlagga.Value != "5")
                    {
                        TestAllLines(false, "");
                    }
                }
            }
            //catch (Exception ex)
            {
                // MessageBox.Show("FieldExit, OrderHuvud reservationskod ändrad " + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            #endregion OrderHuvud reservationskod ändrad

            #region Leveranstid orderrad ändrad
            try
            {
                if (
                    ComponentsOrder.CurrentField == Form_OrderRad_LevTid.Name
                    && Form_OrderRad_LevTid.Text != order.tmpORLevtid
                    && string.Compare(Form_OrderRad_LevTid.Text, order.OnskadLeveranstid) < 0
                    )
                {
                    tblOGA.Find(order.OrderNr);
                    MessageBox.Show("Leveranstiden kan ej sättas före önskad leveranstid: " + order.OnskadLeveranstid + Environment.NewLine, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Form_OrderRad_LevTid.Text = fld_OGR_LevTid.Value;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Leveranstid ändra på orderrad " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            #endregion Leveranstid orderrad ändrad

            #region Testa stafflingspris på matrisrader

            bool matrisrad = false;

            // Om funktionen är aktiverad
            if (ConfigurationManager.AppSettings["TestStafflingPriser"] == "False"
                || (ConfigurationManager.AppSettings["TestStafflingPriser"] == "True" && GarpOrder.User == ConfigurationManager.AppSettings["UserTestStaffling"])
                )
            {

                // Test efter rader via matris om priserna skall bytas enligt lista med stafflade priser

                if (ComponentsOrder.CurrentField == Form_OrderRad_ArtNr.Name && order.InternOrder == false)
                {
                    // Endast order med SEK
                    if (!string.IsNullOrEmpty(dsOGA.Fields.Item("PRL").Value) && (string.IsNullOrEmpty(dsOGA.Fields.Item("VAL").Value) || dsOGA.Fields.Item("VAL")?.Value == "SEK"))
                    {
                        string tmpArtNr = Form_OrderRad_ArtNr.Text;
                        //string tmpRadNr = Form_OrderRad_RadNr.Text;

                        //string matrisRadNr = string.Empty;

                        //if (!double.TryParse(dsOGR.Fields.Item("ORA").Value, out double tmpAntal))
                        //{
                        //    _ = MessageBox.Show("Kunde inte läsa in antal från rad.");
                        //    tmpAntal = 0; // ???
                        //}

                        if (!string.IsNullOrEmpty(tmpArtNr))
                        {
                            tblAGA.Find(tmpArtNr);
                            if (!string.IsNullOrEmpty(fld_AGA_Variantkod.Value) && !string.IsNullOrEmpty(fld_AGA_ModellLangd.Value))
                            {
                                Int16 ModellLangd = Int16.Parse(fld_AGA_ModellLangd.Value);
                                string Modell = Verktyg.Left(fld_AGA_ArtikelNr.Value, ModellLangd);
                                tblOGR.Find(order.Order_Rad);

                                // Int16 tmpCountRow = 0;

                                while (
                                tblOGR.Bof != true
                                && fld_OGR_OrderNr.Value == order.OrderNr
                                && !string.IsNullOrEmpty(fld_OGR_ArtNr.Value)
                                && fld_OGR_ArtNr.Value.Length >= ModellLangd
                                && Verktyg.Left(fld_OGR_ArtNr.Value, ModellLangd) == Modell)
                                {
                                    if (double.TryParse(fld_OGR_UrsprungligtAntal.Value.Replace(".", ","), out double antal) && decimal.TryParse(fld_OGR_Pris.Value.Replace(".", ","), out decimal pris))
                                    {
                                        // Hoppa över raden om antal = 0
                                        if (antal == 0)
                                        {
                                            tblOGR.Prior();
                                            continue;
                                        }

                                        matrisrad = true;

                                        if (!string.IsNullOrEmpty(dsOGA.Fields.Item("PRL").Value))
                                        {

                                            tblAPD.Find("K" + dsOGA.Fields.Item("PRL").Value);
                                            if (!string.IsNullOrEmpty(fld_APD_KampanjLagstaPris.Value))
                                            {
                                                List<CampaignBracketPricing> priceBrackets = GetCampaignBracketPricing(fld_OGR_ArtNr.Value, fld_APD_KampanjLagstaPris.Value, ModellLangd); // Skickar med modellängden för extra läsning mot modellen
                                                DisplayPriceBrackets(fld_OGR_ArtNr.Value, priceBrackets);
                                                string tmpPris = GetBestPrice(priceBrackets, antal, pris);

                                                if (fld_OGR_Pris.Value != tmpPris)
                                                {
                                                    fld_OGR_Pris.Value = tmpPris;
                                                    tblOGR.Post();
                                                }
                                            }
                                        }
                                    }

                                    tblOGR.Prior();
                                }
                                //SendKeys.SendWait("{F5}");
                            }
                        }
                    }
                }
            }
            #endregion

            #region saldotest på matrisrader
            // Om rader skapats via matris gör saldotest på alla matris-rader
            matris.Matrisrad = false;
            if (ComponentsOrder.CurrentField == Form_OrderRad_ArtNr.Name && Form_OrderHuvud_OrderTyp.Text != "0" && order.InternOrder == false)
            {
                tblOGR.Find(order.Order_Rad);
                //string tmpArtNr = fld_OGR_ArtNr.Value;
                string tmpRadNr = Form_OrderRad_RadNr.Text;

                // TODO: GÖR TEST ATT FLERA RADER ÄR FRÅN SAMMA MODELL INNAN MATRISTESTEN KÖRS
                // Byt verision


                // Testa om Matrisrader
                string matrisRadNr = string.Empty;
                double tmpAntal = double.Parse(dsOGR.Fields.Item("ORA").Value);

                if (!string.IsNullOrEmpty(fld_OGR_ArtNr.Value))
                {

                    if (fld_OGR_Fakturaflagga.Value == "i")
                    {
                        while (tblOGR.Bof != true && fld_OGR_OrderNr.Value == order.OrderNr && fld_OGR_Fakturaflagga.Value == "i")
                        {
                            tblOGR.Prior();
                        }
                    }

                    tblAGA.Find(fld_OGR_ArtNr.Value);
                    if (!string.IsNullOrEmpty(fld_AGA_Variantkod.Value) && !string.IsNullOrEmpty(fld_AGA_ModellLangd.Value))                        // ADDED: && fld_AGA_ModellLangd.Value != null) //Calle
                    {
                        Int16 ModellLangd = Int16.Parse(fld_AGA_ModellLangd.Value);                         // Int16 ModellLangd = Int16.Parse(fld_AGA_ModellLangd.Value); krash        CALLE
                        string Modell = Verktyg.Left(fld_AGA_ArtikelNr.Value, ModellLangd);

                        Int16 tmpCountRow = 0;

                        while (
                            tblOGR.Bof != true
                            && fld_OGR_OrderNr.Value == order.OrderNr
                            && !string.IsNullOrEmpty(fld_OGR_ArtNr.Value)
                            && fld_OGR_ArtNr.Value.Length >= ModellLangd
                            && Verktyg.Left(fld_OGR_ArtNr.Value, ModellLangd) == Modell
                            )
                        {
                            matrisRadNr = fld_OGR_RadNr.Value; // !
                            tmpCountRow += 1; // !

                            // Byt ev. lagernummer, enligt artikelns Kod 3
                            if (double.Parse(fld_OGR_UrsprungligtAntal.Value) != 0) // !
                            {
                                DefaultWarehouse(fld_OGR_ArtNr.Value);
                            }

                            tblOGR.Prior();
                        }
                        if (tmpCountRow > 1 && tmpAntal != 0)
                        {
                            matris.Matrisrad = true;

                            List<Matrisrader> rader = new List<Matrisrader>();
                            matris.Text = string.Empty;

                            // Saldotest på rader från matrisen
                            tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(matrisRadNr, 3));
                            while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == order.OrderNr && (string.Compare(fld_OGR_RadNr.Value, tmpRadNr) < 0 || fld_OGR_RadNr.Value == tmpRadNr))
                            {
                                order.TestArtNr = fld_OGR_ArtNr.Value;
                                order.LevTidOR = fld_OGR_LevTid.Value;
                                order.NyttLevDatum = order.LevTidOR;
                                order.TextNewDelTime = string.Empty;
                                order.LagerNr = "1";
                                if (!string.IsNullOrEmpty(fld_OGR_LagerNr.Value))
                                {
                                    order.LagerNr = fld_OGR_LagerNr.Value;
                                }

                                Saldotest();

                                if (string.Compare(order.NyttLevDatum, order.LevTidOR) > 0)
                                {
                                    if (order.TextNewDelTime != string.Empty && order.DispDelleverans > 0)
                                    {
                                        Matrisrader rad = new Matrisrader();

                                        rad.RadNr = fld_OGR_RadNr.Value;
                                        rad.ArtNr = fld_OGR_ArtNr.Value;
                                        rad.Datum = order.DatumDelleverans;
                                        rad.NyttDatum = order.NyttLevDatum;
                                        rad.Antal = order.DispDelleverans;
                                        rad.NyRadAntal = Math.Round(order.RadAntal - order.DispDelleverans, 5);
                                        string warehouseNo = GetWarehouseNoFromRow(order.OrderNr, fld_OGR_RadNr.Value);
                                        rad.LagerNr = string.IsNullOrEmpty(warehouseNo) ? order.LagerNr : warehouseNo;
                                        rader.Add(rad);

                                        matris.Text += Verktyg.fillBlankLeft(rad.RadNr, 4) + "     " + rad.Datum + "       " + Verktyg.fillBlankLeft(rad.Antal.ToString(), 7) + Environment.NewLine;

                                    }
                                }

                                // summera rader som eventuellt ändrades gällande levtid

                                tblOGR.Next();
                            }

                            if (rader.Count > 0)
                            {
                                matris.Text = "Dessa rader från matrisen kan dellevereras." + Environment.NewLine
                                    + "Rad  Datum        Antal" + Environment.NewLine
                                    + matris.Text + Environment.NewLine
                                    + "Vill du splitta orderrad(er)? OBS! Reservationskoden tas bort!";

                                DialogResult result = MessageBox.Show(matris.Text, System.Windows.Forms.Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                                if (result == DialogResult.Yes)
                                {
                                    // Hitta nästa lediga rad.
                                    string antal = order.DispDelleverans.ToString().Replace(",", ".");
                                    string NyRadNr = string.Empty;
                                    int tmpMatrisRadNr = 0;

                                    // Hämta nästa lediga radnummer
                                    tblOGR.Find(order.OrderNr);
                                    tblOGR.Next();
                                    while (!tblOGR.Eof && fld_OGR_OrderNr.Value == order.OrderNr && string.Compare(fld_OGR_RadNr.Value, "250") < 0)
                                    {
                                        NyRadNr = fld_OGR_RadNr.Value;
                                        tblOGR.Next();
                                    }
                                    tmpMatrisRadNr = Convert.ToInt16(NyRadNr);
                                    tmpMatrisRadNr += 1;

                                    // Loopa de matrisrader som kan dellevereras
                                    if (tmpMatrisRadNr < 250 - rader.Count)
                                    {
                                        bool NyaRaderFinns = false;
                                        foreach (var rad in rader)
                                        {
                                            if (rad.NyRadAntal > 0)
                                            {
                                                tblOGR.Insert();
                                                fld_OGR_OrderNr.Value = order.OrderNr;
                                                fld_OGR_RadNr.Value = tmpMatrisRadNr.ToString();
                                                fld_OGR_ArtNr.Value = rad.ArtNr;
                                                fld_OGR_UrsprungligtAntal.Value = rad.NyRadAntal.ToString();
                                                fld_OGR_LevTid.Value = rad.NyttDatum;
                                                fld_OGR_LagerNr.Value = rad.LagerNr;
                                                tblOGR.Post();

                                                // Warehouse no must be set after the post for some reason
                                                SetWarehouseNoOnRow(order.OrderNr, NyRadNr, string.IsNullOrEmpty(rad.LagerNr) ? order.LagerNr : rad.LagerNr);

                                                tmpMatrisRadNr += 1;
                                                NyaRaderFinns = true;
                                            }
                                        }

                                        if (NyaRaderFinns)
                                        {
                                            for (int i = 0; i < rader.Count; i++)
                                            {
                                                tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(rader[i].RadNr, 3));

                                                fld_OGR_LevTid.Value = rader[i].Datum;
                                                fld_OGR_UrsprungligtAntal.Value = rader[i].Antal.ToString();
                                                fld_OGR_Reservationskod.Value = null;

                                            }
                                            SendKeys.SendWait("{F5}");
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Ordern har ej tillräckligt med lediga rader!" + Environment.NewLine + "Rader delas ej.", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                    }
                                }
                                matris.Text = string.Empty;
                            }
                            ComponentsOrder.Item("plusBitBtn").SetFocus();
                        }
                    }
                }

            }
            #endregion

            #region Testa stafflingspris på manuell rad
            // Manuell rad !!!
            try
            {
                // Om antal är ändrat
                if (order.tmpRadAntal != Form_OrderRad_Antal.Text)
                {
                    string testStafflingPriser = ConfigurationManager.AppSettings["TestStafflingPriser"];
                    string userTestStaffling = ConfigurationManager.AppSettings["UserTestStaffling"];
                    // Om funktionen är aktiverad
                    if (ConfigurationManager.AppSettings["TestStafflingPriser"] == "False"
                    || (ConfigurationManager.AppSettings["TestStafflingPriser"] == "True" && GarpOrder.User == ConfigurationManager.AppSettings["UserTestStaffling"])
                    )

                    {
                        if (matris.Matrisrad != true
                            && order.InternOrder == false
                            && Form_OrderRad_RadNr.Text != null
                            && ComponentsOrder.CurrentField == Form_OrderRad_Antal.Name
                            && !matrisrad)
                        {
                            tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(Form_OrderRad_RadNr.Text, 3));
                            if (double.TryParse(Form_OrderRad_Antal.Text.Replace(".", ","), out double antal) && decimal.TryParse(fld_OGR_Pris.Value.Replace(".", ","), out decimal pris))
                            {
                                if (antal != 0)
                                {
                                    // Hämta aktuellt pris för kunden och artikeln
                                    IPrislista price = GarpOrder.Prislista;
                                    price.Kundnr = dsOGA.Fields.Item("FKN").Value;
                                    price.Artikelnr = fld_OGR_ArtNr.Value;
                                    price.OrderDatum = dsOGA.Fields.Item("ODT").Value;
                                    price.LeveransDatum = fld_OGR_LevTid.Value;
                                    price.Kvantitet = fld_OGR_UrsprungligtAntal.Value;
                                    price.Prislista = dsOGA.Fields.Item("PRL").Value;
                                    price.Calculate();
                                    //GarpConQuest.Logger($"Prislista: { price.Prislista }, Apris: { price.Apris } för artnr: {price.Artikelnr } med antal: {price.Kvantitet }");
                                    _ = decimal.TryParse(price.Apris.Replace(".", ","), out decimal tmpPriceOnRow);
                                    price = null;

                                    if (!string.IsNullOrEmpty(dsOGA.Fields.Item("PRL").Value))
                                    {
                                        tblAPD.Find("K" + dsOGA.Fields.Item("PRL").Value);
                                        if (!string.IsNullOrEmpty(fld_APD_KampanjLagstaPris.Value))
                                        {
                                            Int16 ModellLangd = 13;

                                            // Hitta ev. modellen
                                            tblAGA.Find(fld_OGR_ArtNr.Value);
                                            if (fld_AGA_Variantkod.Value != null && fld_AGA_ModellLangd.Value != null)
                                            {
                                                ModellLangd = Int16.Parse(fld_AGA_ModellLangd.Value);
                                            }

                                            List<CampaignBracketPricing> priceBrackets = GetCampaignBracketPricing(fld_OGR_ArtNr.Value, fld_APD_KampanjLagstaPris.Value, ModellLangd);
                                            DisplayPriceBrackets(fld_OGR_ArtNr.Value, priceBrackets);
                                            string tmpPris = GetBestPrice(priceBrackets, antal, tmpPriceOnRow);

                                            if (fld_OGR_Pris.Value != tmpPris)
                                            {
                                                //GarpConQuest.Logger($"Kodrad: 1786 - pris före: { fld_OGR_Pris.Value }");
                                                fld_OGR_Pris.Value = tmpPris;
                                                //GarpConQuest.Logger($"Kodrad: 1788 - pris före: { fld_OGR_Pris.Value }");
                                                tblOGR.Post();
                                            }
                                        }
                                    }
                                }
                                //SendKeys.SendWait("{F5}");
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show(e.Message, "Fel vid Manuell Rad", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            #endregion

            #region Saldtest på orderrad
            try
            {
                // TODO: Hantera internorder??? Nu görs ingen test på ordertyp 9

                order.RadAntalEllerDatumAndrad = false;

                //Aktuellt fält = Antal eller Rad leveranstid och antal > 0, ej internorder
                if (
                    matris.Matrisrad != true
                    && order.InternOrder == false
                    && Form_OrderRad_RadNr.Text != null
                    && (ComponentsOrder.CurrentField == Form_OrderRad_Antal.Name || ComponentsOrder.CurrentField == Form_OrderRad_LevTid.Name) // ! Sista villkoret ska inte med
                    )
                {
                    // Radantal eller Rad leveranstid ändrad
                    if (
                        (ComponentsOrder.CurrentField == Form_OrderRad_Antal.Name && Form_OrderRad_Antal.Text != order.tmpRadAntal) // ! Detta villkor ska stå på ovan if-sats
                        || (ComponentsOrder.CurrentField == Form_OrderRad_LevTid.Name && Form_OrderRad_LevTid.Text != order.tmpORLevtid)
                        )
                    {
                        order.RadAntalEllerDatumAndrad = true; // Här ska stafflingstestet göras med artikelnummer
                    }

                    // Antal är ändrat från 0
                    if (ComponentsOrder.CurrentField == Form_OrderRad_Antal.Name && order.tmpRadAntal == "0" && Form_OrderRad_Antal.Text != order.tmpRadAntal)
                    {
                        order.NyOrderrad = true;
                    }

                    order.LevTidOR = Form_OrderRad_LevTid.Text;
                    order.NyttLevDatum = order.LevTidOR;
                    order.TextNewDelTime = string.Empty;
                    order.TextPurcase = string.Empty;
                    order.TestArtNr = order.ArtNr;
                    tblOGR.Find(order.Order_Rad);
                    order.SplittaOrderrad = false;
                    order.LagerNr = "1";
                    if (!string.IsNullOrEmpty(fld_OGR_LagerNr.Value))
                    {
                        order.LagerNr = fld_OGR_LagerNr.Value;
                    }

                    // Testa om leveranstiden håller på raden
                    Saldotest();

                    // Information om att raden kan dellevereras
                    if (order.TextNewDelTime != string.Empty && order.DispDelleverans > 0)
                    {
                        order.NyRadAntal = Math.Round(order.RadAntal - order.DispDelleverans, 5);
                        if (order.NyRadAntal > 0)
                        {
                            order.TextNewDelTime += Environment.NewLine + "Vill du splitta orderraden? OBS! Reservationskoden tas bort!";
                            DialogResult result = MessageBox.Show(order.TextNewDelTime, System.Windows.Forms.Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                            if (result == DialogResult.Yes)
                            {
                                PartDelivery();
                            }
                            //if (string.IsNullOrEmpty(dsOGR.Fields.Item("RES").Value))
                            //{
                            //    PartDelivery();
                            //}
                            //else
                            //{
                            //    order.TextNewDelTime += Environment.NewLine + "Vill du splitta orderraden? OBS! Reservationskoden tas bort!";
                            //    DialogResult result = MessageBox.Show(order.TextNewDelTime, System.Windows.Forms.Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                            //    if (result == DialogResult.Yes)
                            //    {
                            //        PartDelivery();
                            //    }
                            //}
                        }
                        order.TextNewDelTime = string.Empty;
                    }

                    // Om förslag på nytt datum ligger längre fram än aktuellt datum
                    if (string.Compare(order.NyttLevDatum, order.LevTidOR) > 0 && !order.SplittaOrderrad)
                    {
                        Form_OrderRad_LevTid.Text = order.NyttLevDatum;
                    }


                    #region Paketartiklar

                    // Sök artikeln på orderraden, kolla om artikeln är en paketartikel, spara värden
                    if (tblAGA.Find(order.ArtNr) && fld_AGA_TillvKopKod.Value == order.Paketkod)
                    {
                        order.Paketstart = true;
                        order.tmpPaketArtikel = order.ArtNr;
                        order.tmpPaketOrderRad = order.RadNr;
                        order.tmpPaketLevTid = Form_OrderRad_LevTid.Text;
                        tblOGR.Find(order.Order_Rad);
                        tblOGR.Next();
                        if (fld_OGR_OrderNr.Value != order.OrderNr || int.Parse(fld_OGR_RadNr.Value) > 250)
                        {
                            order.NyttPaket = true;
                        }
                    }
                    else
                    {
                        order.Paketstart = false;
                    }

                    // TODO: Om man sätter leveranstid imorgon på paket som ej har saldohantering blir hela paketet ok även om brist finns på paketrad

                    // Om leveranstid på orderrad har ändrats på en paketartikel, ändra datum på ingående
                    if (
                        order.Paketstart == true
                        && order.NyttPaket == false
                        && order.RadAntalEllerDatumAndrad == true
                        )
                    {

                        // Loppa paketet
                        tblOGR.Find(order.Order_Rad);

                        order.IndexNrPaket = tblOGR.Bookmark;
                        tblOGR.Next();

                        while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == order.OrderNr && fld_OGR_Fakturaflagga.Value == "i")
                        {
                            order.TestArtNr = fld_OGR_ArtNr.Value;
                            order.LevTidOR = fld_OGR_LevTid.Value;
                            order.NyttLevDatum = order.LevTidOR;
                            order.UtanforLedtid = false;
                            order.LagerNr = "1";
                            if (!string.IsNullOrEmpty(fld_OGR_LagerNr.Value))
                            {
                                order.LagerNr = fld_OGR_LagerNr.Value;
                            }

                            Saldotest();

                            // Om nytt datum är tidigare än paketartikeln sätt samma datum som paketartikeln
                            if (
                                string.Compare(order.NyttLevDatum, order.tmpPaketLevTid) < 0
                                || order.NyttLevDatum == order.tmpPaketLevTid
                                || string.Compare(fld_AGA_Saldohanteras.Value, "5") > 0
                                || order.UtanforLedtid == true
                                )
                            {
                                fld_OGR_LevTid.Value = order.tmpPaketLevTid;
                                tblOGR.Post();
                            }
                            // Uppdatera paketraden med nya datumet
                            if (string.Compare(order.NyttLevDatum, order.tmpPaketLevTid) > 0 && string.Compare(fld_AGA_Saldohanteras.Value, "5") < 0 && order.UtanforLedtid == false)
                            {
                                fld_OGR_LevTid.Value = order.NyttLevDatum;
                                tblOGR.Post();

                                order.tmpPaketLevTid = order.NyttLevDatum;

                                // Börja om i loopen för ny saldotest
                                tblOGR.GotoBookmark(order.IndexNrPaket);
                            }
                            tblOGR.Next();
                        }
                        string a = fld_OGR_Fakturaflagga.Value;
                        string b = fld_OGR_RadNr.Value;


                        // Spara orderraden för paketslut, backa pekaren tillbaka till sista paketraden
                        tblOGR.Prior();
                        if (fld_OGR_OrderNr.Value == order.OrderNr && fld_OGR_Fakturaflagga.Value == "i")
                        {
                            order.tmpPaketSlutOrderRad = fld_OGR_RadNr.Value;
                        }
                        else // Om man kommer in här är något fel på orderraderna
                        {
                            tblOGR.Next();
                            order.tmpPaketSlutOrderRad = fld_OGR_RadNr.Value;
                        }

                        // Spara info att paketet får ny leveranstid
                        if (Form_OrderRad_LevTid.Text != order.tmpPaketLevTid || Form_OrderRad_LevTid.Text != order.tmpORLevtid)
                        {
                            order.TextNewDelTime = "Paketets leveranstid ändrad " + order.TextNewDelTime;
                        }

                        // uppdatera leveransdatum på paketartikeln
                        string c = Form_OrderRad_ArtNr.Text; // För att kolla vilkenb rad formuläret har fokus på, bör vara paketartikeln
                        Form_OrderRad_LevTid.Text = order.tmpPaketLevTid;

                        // Uppdatera datum paketslut
                        tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(order.tmpPaketSlutOrderRad, 3));
                        fld_OGR_LevTid.Value = order.tmpPaketLevTid;
                        tblOGR.Post();

                        if (order.RadAntalEllerDatumAndrad == true && order.TextNewDelTime != string.Empty)
                        {
                            MessageBox.Show("Paketet " + order.tmpPaketArtikel + " skickas: " + order.tmpPaketLevTid, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //SendKeys.SendWait("{F5}");
                            order.TextNewDelTime = string.Empty;
                        }
                    }

                    #endregion Paketartiklar

                    // Information om nytt leveransdatum
                    if (order.TextNewDelTime != string.Empty)
                    {
                        MessageBox.Show(order.TextNewDelTime, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        order.TextNewDelTime = string.Empty;
                    }
                    ComponentsOrder.Item("plusBitBtn").SetFocus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldExit, test saldo " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            #endregion saldotest på orderrad

            #region Likvärdig artikel och TB på paket

            // Fälten antal, levtid eller radnr, sätt tillbaka Likvärdig artikel för ny test
            if (ComponentsOrder.CurrentField == Form_OrderRad_Antal.Name
                || ComponentsOrder.CurrentField == Form_OrderRad_LevTid.Name
                || ComponentsOrder.CurrentField == Form_OrderRad_RadNr.Name
                )
            {
                likvArtikel.LikvArtikelHasBeenRun = false;
            }

            // Lämnat fältet artikelnummer
            if (
                ComponentsOrder.CurrentField == Form_OrderRad_ArtNr.Name
                && order.InternOrder == false
                && Form_OrderRad_ArtNr.Text != null
                )
            {

                #region Visa likvärdiga artiklar
                try
                {
                    likvArtikel.StrukturArtikel = "s" + Form_OrderRad_ArtNr.Text;
                    if (
                        likvArtikel.LikvArtikelHasBeenRun == false
                        && tblAGA.Find(likvArtikel.StrukturArtikel)
                        && fld_AGA_Benamning.Value != null
                        && Verktyg.Left(fld_AGA_Benamning.Value, Form_OrderRad_ArtNr.Text.Length) == Form_OrderRad_ArtNr.Text
                        )
                    {
                        Form_OrderRad_ArtNr.SetFocus();
                        Form_OrderRad_ArtNr.Text = "/" + Form_OrderRad_ArtNr.Text;

                        //SendKeys.SendWait("{TAB}");
                        likvArtikel.LikvArtikelHasBeenRun = true;
                        //Form_OrderRad_Antal.SetFocus();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Visa likvärdiga artiklar " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                #endregion Visa likvärdiga artiklar

                #region Räkna ut TB på paketartikel
                try
                {
                    paketTB.PaketArtikel = Form_OrderRad_ArtNr.Text;

                    // Test om artikeln är en paketartikel
                    if (tblAGA.Find(paketTB.PaketArtikel) && fld_AGA_TillvKopKod.Value == order.Paketkod)
                    {
                        // Loopa alla ingående artiklar i paketet spara försäljningspris och vägt inpris för att beräkna TB i procent
                        tblAGS.Find(paketTB.PaketArtikel);
                        tblAGS.Next();
                        while (tblAGS.Eof != true && fld_AGS_StrukturArtikel.Value == paketTB.PaketArtikel)
                        {
                            tblAGA.Find(fld_AGS_UnderArtikel.Value);

                            paketTB.PrisIn = Convert.ToDouble(fld_AGA_VIP.Value.Replace(".", ","));
                            paketTB.SummaIn = paketTB.SummaIn + paketTB.PrisIn;

                            paketTB.PrisUt = Convert.ToDouble(fld_AGA_Pris.Value.Replace(".", ","));
                            paketTB.SummaUt = paketTB.SummaUt + paketTB.PrisUt;

                            tblAGS.Next();
                        }
                        paketTB.TBProcent = Math.Round(((paketTB.SummaUt - paketTB.SummaIn) / paketTB.SummaUt) * 100, 2);

                        // Lägg till fält och visa värden
                        if (ComponentsOrder.Item(paketTB.LblPaketTB_Header) == null)
                        {
                            LabelPaketTBHeader = ComponentsOrder.AddLabel(paketTB.LblPaketTB_Header);
                            LabelPaketTBHeader.Top = 123;
                            LabelPaketTBHeader.Left = 370;
                            LabelPaketTBHeader.Width = 60;
                            LabelPaketTBHeader.Text = "Varukostnad underliggande artiklar:";

                            LabelPaketTB1 = ComponentsOrder.AddLabel(paketTB.LblPaketTB1);
                            LabelPaketTB1.Top = 140;
                            LabelPaketTB1.Left = 420;
                            LabelPaketTB1.Width = 30;
                            LabelPaketTB1.Text = paketTB.SummaIn.ToString();

                            LabelPaketTB2 = ComponentsOrder.AddLabel(paketTB.LblPaketTB2);
                            LabelPaketTB2.Top = 140;
                            LabelPaketTB2.Left = 470;
                            LabelPaketTB2.Width = 60;
                            LabelPaketTB2.Text = paketTB.TBProcent.ToString() + " % TB";
                        }
                    }
                    else
                    {
                        // Släck fält
                        if (ComponentsOrder.Item(paketTB.LblPaketTB_Header) != null)
                        {
                            ComponentsOrder.Delete(paketTB.LblPaketTB_Header);
                        }
                        if (ComponentsOrder.Item(paketTB.LblPaketTB1) != null)
                        {
                            ComponentsOrder.Delete(paketTB.LblPaketTB1);
                        }
                        if (ComponentsOrder.Item(paketTB.LblPaketTB2) != null)
                        {
                            ComponentsOrder.Delete(paketTB.LblPaketTB2);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Räkna ut TB på paketartikel " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                #endregion Räkna ut TB på paketartikel


                //Form_OrderRad_Antal.SetFocus();
            }

            #endregion Likvärdig artikel och TB på paket

            #region Leveranssätt ändrat

            // Leveranssätt ändrat
            if (ComponentsOrder.CurrentField == Form_OrderHuvud_LevSatt.Name)
            {
                // Ändrat via fältet
                string tmpLevsatt = string.Empty;
                bool LevsattAndrat = false;
                if (order.tmpLevSatt != Form_OrderHuvud_LevSatt.Text && order.BtnLevSattKlick == false)
                {
                    LevsattAndrat = true;
                    tmpLevsatt = order.tmpLevSatt;
                }

                // Ändrat via uppslagsknappen
                if (order.tmpLevSattBtn != Form_OrderHuvud_LevSatt.Text && order.BtnLevSattKlick == true)
                {
                    LevsattAndrat = true;
                    tmpLevsatt = order.tmpLevSattBtn;
                    order.BtnLevSattKlick = false;
                }

                if (LevsattAndrat == true)
                {
                    Form_OrderHuvud_LevTid.Text = DeliveryDate();

                    // Uppdatera Önskad leveranstid
                    if (tblOGC.Find(order.OrderNr))
                    {
                        fld_OGC_CH6.Value = Form_OrderHuvud_LevTid.Text;
                        tblOGC.Post();
                    }
                }
            }
            #endregion Leveranssätt ändrat

        }

        public void BeforePostOH()
        {
            try
            {
                #region Leveranstiden är satt till före önskat datum
                // Aktuellt fält = OH Leveranstid och ändring gjorts till tidigare datum än önskat datum
                if (
                    ComponentsOrder.CurrentField == Form_OrderHuvud_LevTid.Name
                    && string.Compare(Form_OrderHuvud_LevTid.Text, order.OnskadLeveranstid) < 0
                    )
                {
                    dsOGA.Abort();
                    tblOGA.Find(order.OrderNr);
                    MessageBox.Show("Leveranstiden kan ej sättas före önskad leveranstid: " + order.OnskadLeveranstid + Environment.NewLine, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Form_OrderHuvud_LevTid.Text = fld_OGA_LevTid.Value;

                }

                #endregion Leveranstiden är satt till före önskat datum

                #region test om ny order
                try
                {
                    // Test om ny order är vald
                    if (Form_OrderHuvud_OrderNr.Text != order.NewOrderNr)
                    {
                        order.TestNewOrder = true;
                        order.TestNewOrderBtn = true;
                        order.BtnLevSattKlick = true;
                    }
                    else { order.TestNewOrder = false; }

                    // Spara värden
                    order.NewOrderNr = Form_OrderHuvud_OrderNr.Text;

                }
                catch (Exception e)
                {
                    MessageBox.Show("Test om ny order är skapad " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                #endregion test om ny order

                #region Sätt reservationskod orderhuvud
                try
                {

                    // Ny order, sätt förvalda värden
                    if (
                        (ComponentsOrder.CurrentField == Form_OrderHuvud_OrderNr.Name || order.tmpResKodSatt == false)
                        && order.TestNewOrder == true
                        && Form_OrderHuvud_KundNr.Text != null
                        )
                    {
                        /* ----- Inaktiverat denna del 211202 /Per N  ----------
                         
                        // Ej Nordforest, Reservationskod = blank, sätt förvald reservationskod
                        if (Verktyg.Left(Form_OrderHuvud_KundNr.Text, 1) != order.KundNrNordforest)
                        {
                            Form_OrderHuvud_Reservationskod.Text = order.ForvadReservationskodOH; 
                            order.tmpResKodSatt = true;
                        }
                        */
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sätt förvalt reservationskod på ny order via plus och enter " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                #endregion Sätt reservationskod och mobilnummer på orderhuvud

            }
            catch (Exception e)
            {
                MessageBox.Show("BeforePostOH " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void BeforePostOR()
        {
            try
            {

            }
            catch (Exception e)
            {
                MessageBox.Show("BeforePostOR " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void WarehouseNrToOrderHead()
        {
            try
            {
                // Lagernummer från orderraderna lagras OrderHuvudFrakt, max 4 nummer
                List<string> wh = new List<string>();
                string tmpLgrNr, tmpLgrNr1;
                tblOGR.Find(order.OrderNr);
                tblOGR.Next();
                while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == order.OrderNr)
                {
                    if (fld_OGR_LagerNr.Value != "" && fld_OGR_LagerNr.Value != null)
                    {
                        tmpLgrNr = fld_OGR_LagerNr.Value;       // Lagernummer på raden

                        // Lägg till D om lagernumret är 99
                        if (tmpLgrNr == "99")
                        {
                            tmpLgrNr1 = "D";
                        }
                        else
                        {
                            tmpLgrNr1 = Verktyg.Left(tmpLgrNr, 1);  // 1:a tecknet från lagernumret
                        }

                        if (!wh.Any(x => x == tmpLgrNr1))
                        {
                            wh.Add(tmpLgrNr1);
                        }
                    }
                    tblOGR.Next();
                }

                // Slå ihop listan med lagernummer till en sträng
                var warehouses = string.Join(string.Empty, wh);

                // Spara upp till 4 st lagernummer från raderna till OrderHuvudFrakt, CH4
                switch (wh.Count)
                {
                    case 0:
                        fld_OGC_CH4.Value = null;
                        break;

                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        fld_OGC_CH4.Value = warehouses;
                        break;

                    default:
                        fld_OGC_CH4.Value = Verktyg.Left(warehouses, 4);
                        break;
                }

                tblOGC.Post();
            }
            catch (Exception e)
            {
                MessageBox.Show("Spara lagernummer från rader till huvudet " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void DefaultWarehouse(string artikel)
        
        {
            try
            {
                if (tblAGA.Find(artikel) && fld_AGA_Flerlager.Value != null)
                {
                    string Lgrnr = "1";
                    // Artikelns Kod 3 har värde och skilt från 1
                    if (!string.IsNullOrEmpty(fld_AGA_Kod3.Value) && fld_AGA_Kod3.Value != "1" && fld_OGR_LagerNr.Value != "99")
                    {
                        if (fld_AGA_Kod3.Value == "D")
                        {
                            Lgrnr = "99";                                                       // Lagernummer 99 för direktleveans
                        }
                        else
                        {
                            Lgrnr = fld_AGA_Kod3.Value;                                        // Sätt lagernummer till samma som artikelns kod 3
                        }
                        fld_OGR_LagerNr.Value = Lgrnr;
                        tblOGR.Post();
                    }

                    // Sätt eventuella paketrader till samma som paketartikeln
                    if (fld_AGA_TillvKopKod.Value == order.Paketkod)
                    {
                        Lgrnr = fld_OGR_LagerNr.Value;
                        int tmpTblOgrBookmark = tblOGR.Bookmark;
                        tblOGR.Next();
                        while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == order.OrderNr && fld_OGR_Fakturaflagga.Value == "i")
                        {
                            tblAGA.Find(fld_OGR_ArtNr.Value);
                            if (string.IsNullOrEmpty(fld_AGA_Kod3.Value)) // Koll att paketradens artikel inte har eget lagernummer
                            {
                                fld_OGR_LagerNr.Value = Lgrnr;
                            }
                            else
                            {
                                fld_OGR_LagerNr.Value = fld_AGA_Kod3.Value;
                                if (fld_AGA_Kod3.Value == "D")
                                {
                                    fld_OGR_LagerNr.Value = "99";
                                }
                            }

                            tblOGR.Post();
                            tblOGR.Next();
                        }
                        tblOGR.GotoBookmark(tmpTblOgrBookmark);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel när förvalt lagernummer skall sättas " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void ShowCreditControlDate()
        {
            // Hämtar info från kund om och när kredittest är gjord
            try
            {
                if (dsOGA.Fields.Item("KNR").Value != null && tblKA.Find(dsOGA.Fields.Item("KNR").Value) && fld_KA_Num3.Value != "0")
                {
                    LabelDateCreditControl.Text = fld_KA_Num3.Value;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel vid visning senaste kreditkontroll " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void ShowButtonAdvancePay()
        {
            try
            {
                if (
                    order.AdvPayActive?.ToLower() == "true"
                    && dsOGA.Fields.Item("OTY").Value != "0"
                    && dsOGA.Fields.Item("OBF").Value != "0"
                    && dsOGA.Fields.Item("PLF").Value != "0"
                    && dsOGA.Fields.Item("BVK").Value != null
                    && (!order.PrePaymentConditions?.Exists(x => x == dsOGA.Fields.Item("BVK").Value) ?? false) //dsOGA.Fields.Item("BVK").Value != order.PrePaymentConditions
                    )
                {
                    BtnAdvancePay.Visible = true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel vid visning senaste kreditkontroll " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void AdvancePay()
        {
            try
            {
                // 1. Dialogruta
                //   a) Visa totalsumman på order exl moms och inkl moms
                //   b) Val att ange %-sats eller summa
                //   c) Respektive fält räknas ut
                // 
                // 2. Artikel 91 (från settingfil) läggs till på ordern. -1 i antal. Priset från dialogrutan, exkl moms
                //   a) Textrad 0 skapas (eller uppdateras) med: Förskott debiteras på order: 123456
                //   b) Byt betalningsvillkor
                //   c) PL-flagg sätts till "V"
                //   d) Text visas: "Väntar på betalning"
                //
                // 3. Ny order skapas
                //   a) Artikel 91 läggs till på nya ordern. 1 i antal, summan från ursprungsordern
                //   b) Textrader skapas, en för varje ursprunglig orderad
                //   c) Artikelnummer, benämningar och antal
                //   d) Textrad 0 skapas med: Förskott kopierat från order: 654321
                //   e) Byt betalningsvillkor
                //   f) Leverans av förskottsordern

                // Förvalt värde = olevererat värde
                string input = string.Empty;
                double RowValue = 0;
                double RowMoms = 0;
                bool FindOGL = false;
                bool FindOGF = false;

                // Räkna ut orderns värde
                IOrderRowCalc calc = GarpOrder.OrderRowCalc;
                calc.Ordernr = dsOGA.Fields.Item("ONR").Value;
                calc.Calculate();
                MessageBox.Show("Order " + calc.Ordernr + Environment.NewLine + Environment.NewLine
                    + "Inkl moms: " + calc.InklMoms + Environment.NewLine
                    + "Exkl moms: " + calc.ExklMoms, "Skapa förskott", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Värde inkl moms
                input = calc.InklMoms.Replace(",", ".");

                // Nettopriset på ordern plus summerad moms
                tblOGR.Find(dsOGA.Fields.Item("ONR").Value);
                tblOGR.Next();
                while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == dsOGA.Fields.Item("ONR").Value)
                {
                    calc.RadnrFrom = fld_OGR_RadNr.Value;
                    calc.RadnrTo = fld_OGR_RadNr.Value;
                    calc.Calculate();
                    RowValue += double.Parse(calc.ExklMoms.Replace(".", ",")); // Räkna ut nettopriset (huvud och radrabatt)
                    RowMoms += double.Parse(calc.Moms.Replace(".", ","));
                    tblOGR.Next();
                }
                calc = null;

                // Öppna formulär för inmatning förskottsbelopp inkl moms
                AdvancePayment advpay = new AdvancePayment
                {
                    InputValue = input,
                    ValueExMoms = RowValue.ToString(),
                    ValueMoms = RowMoms.ToString()
                };
                advpay.ShowDialog();
                input = "";
                if (advpay.inputBelopp != "")
                {
                    // Efter OK, säkerhetsfråga med belopp till förskott
                    if (MessageBox.Show("Förskott: " + advpay.inputBelopp + " OK?", "Skapa förskott", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        input = advpay.OutputValueExMoms.ToString().Replace(",", ".");

                        if (tblOGL.Find(dsOGA.Fields.Item("ONR").Value))
                        {
                            FindOGL = true;
                        }
                        if (tblOGC.Find(dsOGA.Fields.Item("ONR").Value))
                        {
                            FindOGF = true;
                        }

                        // Spara artikelraderna för information till förskottsordern
                        tblOGR.Find(dsOGA.Fields.Item("ONR").Value);
                        tblOGR.Next();

                        OrderInfo = new List<AdvanceOrderRow>();

                        while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == dsOGA.Fields.Item("ONR").Value)
                        {
                            if (tblAGA.Find(fld_OGR_ArtNr.Value))
                            {
                                // Ta inte med rader med fakturaflagga = i förutom artkategori ZP som skall med ändå. Tar ej med om både antal och pris är 0
                                if (
                                    (fld_OGR_Fakturaflagga.Value != "i" || fld_AGA_Kategori.Value == order.ArtKatFrakter)
                                    && (double.Parse(fld_OGR_UrsprungligtAntal.Value.Replace(".", ",")) != 0 || double.Parse(fld_OGR_Pris.Value.Replace(".", ",")) != 0)
                                    )
                                {
                                    AdvanceOrderRow RowInfo = new AdvanceOrderRow
                                    {
                                        Item = fld_OGR_ArtNr.Value,
                                        Name = fld_OGR_Benamning.Value + " " + fld_OGR_BenamningExtra.Value,
                                        Qty = fld_OGR_UrsprungligtAntal.Value,
                                        Unit = fld_OGR_Enhet.Value,
                                        VATcode = fld_OGR_MomsKod.Value
                                    };
                                    OrderInfo.Add(RowInfo);
                                }
                            }
                            tblOGR.Next();
                        }

                        // Lägg till ny artikel (Förskott) på ordern med kredit, uppdatera textrad 0, skapa ny order
                        tblOGR.Find(dsOGA.Fields.Item("ONR").Value);
                        tblOGR.Next();
                        int row = 1;
                        string lagernr = "1";
                        // Hämtar nästa lediga radnummer, sparar lagernumret
                        while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == dsOGA.Fields.Item("ONR").Value && int.Parse(fld_OGR_RadNr.Value) < 251)
                        {
                            lagernr = fld_OGR_LagerNr.Value;
                            row = int.Parse(fld_OGR_RadNr.Value);
                            tblOGR.Next();
                        }
                        row++;
                        if (row < 250)
                        {
                            // Skapar ny orderrad med förskottet som kredit
                            tblOGR.Insert();
                            fld_OGR_OrderNr.Value = dsOGA.Fields.Item("ONR").Value;
                            fld_OGR_RadNr.Value = row.ToString();
                            fld_OGR_ArtNr.Value = order.AdvancePayItem;
                            fld_OGR_LagerNr.Value = lagernr;
                            fld_OGR_UrsprungligtAntal.Value = "-1";
                            fld_OGR_Pris.Value = input;
                            tblOGR.Post();

                            // Hämta nästa ordernummer för förskottsordern
                            NrSerie = GarpOrder.Counters;
                            NrSerie.Series = "A";
                            string NewOrderNr = NrSerie.GetCustomerOrderNo(true);

                            // Skapa textrad 0
                            InsertRowText(dsOGA.Fields.Item("ONR").Value, "0", "Förskott debiteras på order: " + NewOrderNr);

                            // Uppdaterar PL-flaggan med "V" och betalningsvillkoret på varuordern
                            dsOGA.Fields.Item("PLF").Value = "V";
                            dsOGA.Fields.Item("BVK").Value = order.AdvancePaymentTermGoodsOrder;

                            // Skapar ny order med förskottet att debitera
                            tblOGA.Insert();
                            fld_OGA_OrderNr.Value = NewOrderNr;
                            fld_OGA_KundNr.Value = dsOGA.Fields.Item("KNR").Value;
                            fld_OGA_LevTid.Value = DateTime.Now.ToString("yyMMdd");
                            fld_OGA_Betvillk.Value = order.AdvancePaymentTermAdvance;
                            fld_OGA_OurRef.Value = dsOGA.Fields.Item("VRF").Value;
                            fld_OGA_ErRef.Value = dsOGA.Fields.Item("ERF").Value;
                            fld_OGA_LevVilk.Value = dsOGA.Fields.Item("LVK").Value;
                            fld_OGA_LevSatt.Value = dsOGA.Fields.Item("LSE").Value;
                            fld_OGA_Saljare.Value = dsOGA.Fields.Item("SLJ").Value;
                            fld_OGA_Valuta.Value = dsOGA.Fields.Item("VAL").Value;
                            fld_OGA_MomsKod.Value = dsOGA.Fields.Item("MOM").Value;
                            tblOGA.Post();

                            tblOGC.Insert();
                            fld_OGC_OrderNr.Value = NewOrderNr;
                            fld_OGC_CH6.Value = DateTime.Now.ToString("yyMMdd"); // Kundens önskade leveranstid
                            tblOGC.Post();

                            if (FindOGL)
                            {
                                if (!tblOGL.Find(NewOrderNr))
                                {
                                    tblOGL.Insert();
                                    fld_OGL_OrderNr.Value = NewOrderNr;
                                }
                                fld_OGL_Namn.Value = dsOGL.Fields.Item("NAM").Value;
                                fld_OGL_Adress1.Value = dsOGL.Fields.Item("AD1").Value;
                                fld_OGL_Adress2.Value = dsOGL.Fields.Item("AD2").Value;
                                fld_OGL_Adress3.Value = dsOGL.Fields.Item("AD3").Value;
                                fld_OGL_Ort.Value = dsOGL.Fields.Item("ORT").Value;
                                fld_OGL_Land.Value = dsOGL.Fields.Item("LND").Value;
                                tblOGL.Post();
                            }

                            if (FindOGF)
                            {
                                if (!tblOGF.Find(NewOrderNr))
                                {
                                    tblOGF.Insert();
                                    fld_OGF_OrderNr.Value = NewOrderNr;
                                }
                                fld_OGF_Text1.Value = dsOGF.Fields.Item("TX1").Value;
                                fld_OGF_Text2.Value = dsOGF.Fields.Item("TX2").Value;
                                fld_OGF_ValfriInfo.Value = dsOGF.Fields.Item("TX3").Value;
                                fld_OGF_FakturKontakt.Value = dsOGF.Fields.Item("ERF").Value;
                                tblOGF.Post();
                            }

                            // Skapar ny orderrad med förskottet
                            tblOGR.Insert();
                            fld_OGR_OrderNr.Value = NewOrderNr;
                            fld_OGR_RadNr.Value = "1";
                            fld_OGR_ArtNr.Value = order.AdvancePayItem;
                            fld_OGR_LagerNr.Value = lagernr;
                            fld_OGR_UrsprungligtAntal.Value = "1";
                            fld_OGR_Pris.Value = input;
                            tblOGR.Post();

                            // Skapa textrad 0
                            InsertRowText(NewOrderNr, "0", "Förskott att debitera från order: " + dsOGA.Fields.Item("ONR").Value);

                            if (OrderInfo.Count > 0)
                            {
                                // Skapa textrader med artiklarna
                                string tmpText = "Antal   Artikel";
                                InsertRowText(NewOrderNr, "1", tmpText, "1", "1", "1", "1");

                                foreach (AdvanceOrderRow RowInfo in OrderInfo)
                                {
                                    if (double.Parse(RowInfo.Qty.Replace(".", ",")) != 0)
                                    {
                                        tmpText = RowInfo.Qty + " " + RowInfo.Unit + " " + RowInfo.Item + "-" + RowInfo.Name;
                                    }
                                    else
                                    {
                                        tmpText = "   " + RowInfo.Name;
                                    }
                                    InsertRowText(NewOrderNr, "1", tmpText, "1", "1", "1", "1");
                                }
                            }

                            // Leverera ordern direkt
                            IOrderRowDeliver LevereraOrder = GarpOrder.OrderRowDeliver; ;
                            LevereraOrder.Ordernr = NewOrderNr;
                            LevereraOrder.Radnr = "1";
                            LevereraOrder.Antal = "1";
                            LevereraOrder.Deliver();

                            LevereraOrder = null;

                            MessageBox.Show("Ny order " + NewOrderNr + " med förskott skapad och levererad", "Förskott", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SendKeys.SendWait("{F5}");
                        }
                        else
                        {
                            MessageBox.Show("Slut på rader för denna order", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }

                    }
                }
                calc = null;
            }

            catch (Exception e)
            {

                MessageBox.Show("Fel vid förskottshantering " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public bool AddOGC()
        {
            try
            {
                // Skapa tabellpost för OGC
                if (!tblOGC.Find(dsOGA.Fields.Item("ONR").Value)) // order.OrderNr))
                {
                    tblOGR.Find(order.OrderNr);
                    tblOGR.Next();

                    // Ordern saknar orderrader, CH6 = imorgon
                    if (
                        fld_OGR_OrderNr.Value != order.OrderNr
                        || (fld_OGR_OrderNr.Value == order.OrderNr && Int32.Parse(fld_OGR_RadNr.Value) > 250)
                        )
                    {
                        // Önskad leveranstid = idag
                        order.OnskadLeveranstid = DeliveryDate();
                    }

                    // Order med befintliga rader, CH6 = OH, leveranstid
                    else
                    {
                        order.OnskadLeveranstid = Form_OrderHuvud_LevTid.Text;
                    }

                    tblOGC.Insert();
                    fld_OGC_OrderNr.Value = order.OrderNr;
                    fld_OGC_CH6.Value = order.OnskadLeveranstid;
                    tblOGC.Post();

                }

                // Uppdatera fältet önskad leveranstid med värdet från tabellen
                else
                {
                    //tblOGC.Find(order.OrderNr);
                    order.OnskadLeveranstid = fld_OGC_CH6.Value;
                }

                // Visa värdet i fältet från tabellen, varna med rött om önksat datum skiljer mot OrderHuvud Levtid
                LabelDesiredDelivery.Text = order.OnskadLeveranstid;
                if (LabelDesiredDelivery.Text != Form_OrderHuvud_LevTid.Text)
                {
                    LabelDesiredDelivery.Color = 10066431;
                }
                else
                {
                    LabelDesiredDelivery.Color = 16777215;
                }
                return true;
            }

            catch (Exception e)
            {
                return false;
                //MessageBox.Show("Fel vid upplägg av fält för önskad leveranstid " + e.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void GetMobileNumber()
        {
            try
            {
                tblOGR.Find(order.OrderNr);
                tblOGR.Next();

                // Test om det är ny order
                if (fld_OGR_OrderNr.Value != order.OrderNr || string.Compare(fld_OGR_RadNr.Value, "250") > 0)
                {
                    // Skapa tabell OGC (OrderHuvudFrakt) om den inte finns
                    if (!tblOGC.Find(order.OrderNr))
                    {
                        tblOGC.Insert();
                        fld_OGC_OrderNr.Value = order.OrderNr;
                    }

                    // Skapa tabell OGF (OrderHuvudText) om den inte finns
                    if (!tblOGF.Find(order.OrderNr))
                    {
                        tblOGF.Insert();
                        fld_OGF_OrderNr.Value = order.OrderNr;
                    }

                    // Mobilnumret ej hämtat sedan tidigare
                    if (fld_OGC_CH3.Value == null && fld_OGF_Text2.Value == null)
                    {
                        // Sätt OrderHuvudFrakt till 1 = text 5 är hämtad från kund
                        fld_OGC_CH3.Value = "1";
                        tblOGC.Post();

                        // Lägg till mobilnumret på ordern
                        fld_OGF_Text2.Value = fld_KA_TX5.Value;
                        tblOGF.Post();
                    }
                }
            }

            catch (Exception e)
            {
                MessageBox.Show("Fel vid hämtning mobilnummer från kund " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void PartDelivery()
        {
            try
            {
                string antal = order.DispDelleverans.ToString().Replace(",", ".");
                string NyRadNr = string.Empty;
                int tmpRadNr = 0;
                int countPaketrader = 0;
                bool PaketRad = false;
                string paketArtikel = string.Empty;
                string paketAntal = string.Empty;
                string paketRadNr = string.Empty;
                string RadPris = fld_OGR_Pris.Value;
                string OHRabatt = fld_OGR_OHrab.Value;
                string RadRabatt = fld_OGR_RadRab.Value;

                // Raden är paketrad, flytta fram nya radnumret efter paketraderna
                if (fld_AGA_TillvKopKod.Value == order.Paketkod)
                {
                    paketArtikel = order.ArtNr;
                    paketAntal = order.NyRadAntal.ToString();
                    paketRadNr = dsOGR.Fields.Item("RDC").Value;
                    PaketRad = true;

                    tblAGS.Find(fld_AGA_ArtikelNr.Value);
                    tblAGS.Next();

                    // Räkna antal artiklar i strukturen
                    while (tblAGS.Eof != true && fld_AGS_StrukturArtikel.Value == fld_AGA_ArtikelNr.Value)
                    {
                        countPaketrader++;
                        tmpRadNr++;
                        tblAGS.Next();
                    }
                }

                // Hämta nästa lediga radnummer
                tblOGR.Find(order.OrderNr);
                tblOGR.Next();
                while (!tblOGR.Eof && fld_OGR_OrderNr.Value == order.OrderNr && string.Compare(fld_OGR_RadNr.Value, "250") < 0)
                {
                    NyRadNr = fld_OGR_RadNr.Value;
                    tblOGR.Next();
                }
                tmpRadNr += Convert.ToInt16(NyRadNr);

                // Lägg in ny rad med antalet som skall dellevereras
                tmpRadNr += 1;
                if (tmpRadNr + countPaketrader < 250)
                {
                    NyRadNr = tmpRadNr.ToString();

                    // Aktuell rad får datumet för delleverans och antal, reservationskoden tas bort
                    DS_OrderRad_Levtid.Value = order.DatumDelleverans;
                    DS_OrderRad_Antal.Value = antal;
                    DS_OrderRad_Reservationskod.Value = null;

                    // Lägg till nytt paket via orderimport
                    if (PaketRad)
                    {
                        string generator = ConfigurationManager.AppSettings["EDIGenerator"];
                        string rapport = ConfigurationManager.AppSettings["EDIRapport"];
                        string filePath = ConfigurationManager.AppSettings[@"FilePathUdaterOrder"];

                        if (filePath == "")
                        {
                            filePath = null;
                        }


                        SendKeys.SendWait("{F5}");
                        IReport rap = GarpOrder.ReportGenerators.Item(generator).Reports.Item(rapport);
                        rap.Medium = "D";
                        rap.Form = "OFIL";
                        if (filePath != null)
                        {
                            rap.FilePath = filePath;
                        }
                        rap.RangeFrom = order.OrderNr;
                        rap.RangeTo = order.OrderNr;
                        rap.SetDialogResponse("Radnr", paketRadNr);
                        rap.SetDialogResponse("Antal", paketAntal);
                        rap.SetDialogResponse("Levtid", order.NyttLevDatum);
                        rap.Run();
                        rap.Wait();

                        IEDI edi = GarpOrder.EDI;
                        edi.FilePath = filePath;
                        edi.CreateOrders();

                        // TODO: OM PAKETRADER INGÅR I STAFFLING MÅSTE LOOP FÖR PAKETET OCH UPPDATERA PRISER I EFTERHAND!!!!

                    }
                    // Lägg till ny rad
                    else
                    {
                        tblOGR.Insert();
                        fld_OGR_OrderNr.Value = order.OrderNr;
                        fld_OGR_RadNr.Value = NyRadNr;
                        fld_OGR_ArtNr.Value = order.ArtNr;
                        fld_OGR_UrsprungligtAntal.Value = order.NyRadAntal.ToString();
                        fld_OGR_LevTid.Value = order.NyttLevDatum;
                        fld_OGR_Pris.Value = RadPris; // Nya raden får samma pris som ursprungsraden
                        fld_OGR_OHrab.Value = OHRabatt;
                        fld_OGR_RadRab.Value = RadRabatt;
                        
                        tblOGR.Post();

                        // Warehouse no must be set after the post for some reason
                        string warehouseNo = GetWarehouseNoFromRow(order.OrderNr, dsOGR.Fields.Item("RDC").Value);
                        SetWarehouseNoOnRow(order.OrderNr, NyRadNr, string.IsNullOrEmpty(warehouseNo) ? order.LagerNr : warehouseNo);

                        InsertRowText(order.OrderNr, dsOGR.Fields.Item("RDC").Value, "Delleverans sker på rad " + NyRadNr);
                        InsertRowText(order.OrderNr, NyRadNr, "Delleverans från rad " + dsOGR.Fields.Item("RDC").Value.Trim());
                    }

                    order.SplittaOrderrad = true;
                    SendKeys.SendWait("{F5}");
                }
                else
                {
                    MessageBox.Show("Ordern har ej tillräckligt med lediga rader!" + Environment.NewLine + "Rader delas ej.", System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }

            catch (Exception e)
            {
                MessageBox.Show("Information om delleverans " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void SetName()
        {
            try
            {
                string Fornamn = string.Empty;
                string Efternam = string.Empty;
                string Helanamnet = string.Empty;
                string FraktOrt = string.Empty;
                tblKA.Find(dsOGA.Fields.Item("KNR").Value);
                Efternam = fld_KA_namn.Value;

                // Om förnamn finns på kunden, slå ihop för och efternamn till namnfältet
                if (tblKB.Find(dsOGA.Fields.Item("KNR").Value) && !string.IsNullOrEmpty(fld_KB_AdressExtra.Value))
                {
                    Fornamn = fld_KB_AdressExtra.Value;
                    Helanamnet = Fornamn + " " + Efternam;

                    if (!tblOGL.Find(dsOGA.Fields.Item("ONR").Value))
                    {
                        tblOGL.Insert();
                        fld_OGL_OrderNr.Value = dsOGA.Fields.Item("ONR").Value;
                    }
                    fld_OGL_Namn.Value = Helanamnet;
                    if (!string.IsNullOrEmpty(fld_KA_Adress1.Value))
                    {
                        fld_OGL_Adress1.Value = fld_KA_Adress1.Value;
                    }
                    if (!string.IsNullOrEmpty(fld_KA_Adress2.Value))
                    {
                        fld_OGL_Adress2.Value = fld_KA_Adress2.Value;
                    }


                    // Test om godspostnummer används, leta efter orten i tabellregistret
                    if (!string.IsNullOrEmpty(fld_KA_GodsPostnr.Value))
                    {
                        int TknPostNr = fld_KA_GodsPostnr.Value.Length;

                        for (int i = TknPostNr; i >= 0; i--)
                        {
                            if (tblTA.Find("16" + Verktyg.Left(fld_KA_GodsPostnr.Value, i)))
                            {
                                FraktOrt = fld_TA_Text.Value;
                                fld_OGL_Ort.Value = fld_KA_GodsPostnr.Value + " " + FraktOrt;
                                i = 0;
                            }
                        }
                    }
                    else
                    {
                        fld_OGL_Ort.Value = fld_KA_Ort.Value;
                    }
                    if (!string.IsNullOrEmpty(fld_KA_Land.Value))
                    {
                        fld_OGL_Land.Value = fld_KA_Land.Value;
                    }

                    tblOGL.Post();
                }
                order.TestNewOrderBtn = false;
            }

            catch (Exception e)
            {
                MessageBox.Show("Sätt ihop för och efternamn " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public string DeliveryDate()
        {
            try
            {
                string CurrentTime = DateTime.Now.ToString("HH:mm:ss", DateTimeFormatInfo.InvariantInfo);
                string KundNr = Form_OrderHuvud_KundNr.Text;
                string Levtid = DateTime.Now.ToString("yyMMdd");

                // Om klockan är efter 12 eller EJ VIP -kund och leveranssätt <> hämtas, sätt leveranstid imorgon
                if (
                    tblKA.Find(dsOGA.Fields.Item("KNR").Value) &&
                    Form_OrderHuvud_LevSatt.Text != order.LevSattIdag
                    && (string.Compare(CurrentTime, order.OrderStoppLevIdag) > 0 || string.IsNullOrEmpty(fld_KA_VIPkund.Value))
                    //Verktyg.Left(KundNr, 1) != order.KundNrLevIdag)
                    )
                {
                    // OH Levtid = imorgon
                    Levtid = GarpOrder.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), 1, -1);
                }
                return Levtid;
            }

            catch (Exception e)
            {
                MessageBox.Show("Sätt leveranstid till imorgon " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return DateTime.Now.ToString("yyMMdd");
            }
        }

        /// <summary>
        /// Saldotest på alla orderrader, bool om dialog skall visas, och string dialogtext
        /// </summary>
        /// <param name="dialog">Ange om dialog skall visas</param>
        /// <param name="dialogtext">Dialogtext som visas i dialogruta</param>
        public void TestAllLines(bool dialog, string dialogtext)
        {
            try
            {
                if (Form_OrderHuvud_OrderTyp.Text != "0" && order.InternOrder == false)
                {
                    // Test att orderrader finns
                    tblOGR.Find(order.OrderNr);
                    tblOGR.Next();
                    if (fld_OGR_OrderNr.Value == order.OrderNr)
                    {
                        bool yes = true;
                        bool paketstart = false;

                        if (dialog)
                        {
                            DialogResult result = MessageBox.Show(dialogtext, "Garp", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                            if (result != DialogResult.Yes)
                            {
                                yes = false;
                            }
                        }

                        if (yes)
                        {
                            tblOGA.Find(order.OrderNr);

                            // Sätt alla rader till önskad leveranstid men tidigast idag.
                            // EJ produktionsplanerade rader /220106 Per N
                            while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == order.OrderNr)
                            {
                                if (!string.IsNullOrEmpty(fld_OGR_ArtNr.Value) && fld_OGR_LeveransFlagga.Value == "0"
                                    && (fld_OGR_Belaggning.Value == "0" || fld_OGR_Belaggning.Value == "5")
                                    )
                                {
                                    // Datumet ska testas så att vi inte har datum innan idag -> imorgon beroende av villkor
                                    fld_OGR_LevTid.Value = GetAdjustedRequestedOrderDate(order.OnskadLeveranstid); //Form_OrderHuvud_LevTid.Text;

                                    tblOGR.Post();
                                }
                                tblOGR.Next();
                            }

                            // Lista med rader som har reskod = blankt och leverans = Ok enligt önskad levtid
                            OrderRadReskodBlank = new List<string>();
                            string lastLevDateResBlank = "";
                            string firstLevDateResBlank = "999999";

                            // Lista med rader som har reskod = 1
                            OrderRadResKod1 = new List<string>();
                            order.tmpORLevtid = "";

                            string Text = "";

                            // Börja om på sista raden för saldotest
                            tblOGR.Find(order.OrderNr);
                            tblOGR.Next();
                            string tmpOrderrad = Verktyg.fillBlankRight(fld_OGR_OrderNr.Value, 6) + Verktyg.fillBlankLeft(fld_OGR_RadNr.Value, 3);
                            while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == order.OrderNr)
                            {
                                tmpOrderrad = Verktyg.fillBlankRight(fld_OGR_OrderNr.Value, 6) + Verktyg.fillBlankLeft(fld_OGR_RadNr.Value, 3);
                                tblOGR.Next();
                            }
                            tblOGR.Find(tmpOrderrad);

                            while (tblOGR.Bof != true && fld_OGR_OrderNr.Value == order.OrderNr)
                            {
                                tmpOrderrad = Verktyg.fillBlankRight(fld_OGR_OrderNr.Value, 6) + Verktyg.fillBlankLeft(fld_OGR_RadNr.Value, 3);

                                if (!string.IsNullOrEmpty(fld_OGR_ArtNr.Value) && fld_OGR_LeveransFlagga.Value == "0")
                                {
                                    order.TestArtNr = fld_OGR_ArtNr.Value;
                                    order.LevTidOR = fld_OGR_LevTid.Value;
                                    order.NyttLevDatum = order.LevTidOR;
                                    order.LagerNr = "1";
                                    if (!string.IsNullOrEmpty(fld_OGR_LagerNr.Value))
                                    {
                                        order.LagerNr = fld_OGR_LagerNr.Value;
                                    }

                                    Saldotest(); // !!!

                                    Text += "Saldotest " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.NyttLevDatum + Environment.NewLine;

                                    // TODO: SÖKNINGEN I PAKET MÅSTE VÄNDAS!!!

                                    // OrderRad.FAF
                                    // p (lilla P) = paketstart
                                    // i = paketrad
                                    // 1 = Normal rad

                                    #region Paketartiklar

                                    // Om aktuell rad är paketstart
                                    if (tblAGA.Find(order.TestArtNr) && fld_AGA_TillvKopKod.Value == order.Paketkod && fld_OGR_Reservationskod.Value != "1")
                                    {
                                        paketstart = true;
                                        // Om förslag på nytt datum ligger längre fram än aktuellt datum för paketstart
                                        if (string.Compare(order.NyttLevDatum, order.LevTidOR) > 0)
                                        {
                                            Text = Text + "Paketstart " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.NyttLevDatum + Environment.NewLine;

                                            // Uppdatera orderradens leveranstid
                                            fld_OGR_LevTid.Value = order.NyttLevDatum;
                                            tblOGR.Post();
                                        }


                                        //
                                        // TODO: VARFÖR GÖRS JÄMFÖRELSE MED ORDERHUVUDETS LEVERANSDATUM???? /Per N 211208
                                        //
                                        //
                                        // Blank reskod och leveransdatum skiljer mellan rad och huvud, spara sämsta leveranstid.
                                        if (string.IsNullOrEmpty(fld_OGR_Reservationskod.Value) && fld_OGR_LevTid.Value != Form_OrderHuvud_LevTid.Text && fld_OGR_LeveransFlagga.Value == "0")
                                        {
                                            OrderRadReskodBlank.Add(fld_OGR_RadNr.Value);

                                            // Spara sämsta och fösta leveranstid
                                            if (string.Compare(fld_OGR_LevTid.Value, lastLevDateResBlank) > 0)
                                            {
                                                lastLevDateResBlank = fld_OGR_LevTid.Value;
                                            }
                                            if (string.Compare(fld_OGR_LevTid.Value, firstLevDateResBlank) < 0)
                                            {
                                                firstLevDateResBlank = fld_OGR_LevTid.Value;
                                            }
                                        }

                                        // Reskod 1, spara sämsta leveranstid. Lägg in den tiden på alla rader som har reskod 1
                                        if (fld_OGR_Reservationskod.Value == "1" && fld_OGR_LeveransFlagga.Value == "0")
                                        {

                                            OrderRadResKod1.Add(fld_OGR_RadNr.Value);

                                            // Testa om aktuell leveranstid är senare än senast sparade
                                            if (string.Compare(fld_OGR_LevTid.Value, order.tmpORLevtid) > 0)
                                            {
                                                order.tmpORLevtid = fld_OGR_LevTid.Value;
                                            }
                                        }

                                        order.tmpPaketOrderRad = fld_OGR_RadNr.Value;
                                        order.tmpPaketLevTid = fld_OGR_LevTid.Value;
                                        order.tmpPaketStartLevTid = order.tmpPaketLevTid;
                                        order.IndexNrPaket = tblOGR.Bookmark;
                                        tblOGR.Next();

                                        // Loopa paketet
                                        while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == order.OrderNr && fld_OGR_Fakturaflagga.Value == "i")
                                        {
                                            order.TestArtNr = fld_OGR_ArtNr.Value;
                                            order.LevTidOR = fld_OGR_LevTid.Value;
                                            order.NyttLevDatum = order.LevTidOR;
                                            order.UtanforLedtid = false;
                                            order.LagerNr = "1";
                                            if (!string.IsNullOrEmpty(fld_OGR_LagerNr.Value))
                                            {
                                                order.LagerNr = fld_OGR_LagerNr.Value;
                                            }

                                            Saldotest();

                                            Text = Text + "Saldotest paketrad " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.NyttLevDatum + Environment.NewLine;

                                            // Om nytt datum är tidigare än paketartikeln sätt samma datum som paketartikeln
                                            if (
                                                string.Compare(order.NyttLevDatum, order.tmpPaketLevTid) < 0
                                                || order.NyttLevDatum == order.tmpPaketLevTid
                                                || string.Compare(fld_AGA_Saldohanteras.Value, "5") > 0
                                                || order.UtanforLedtid == true
                                                )
                                            {
                                                Text = Text + "Paketrad " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.tmpPaketLevTid + Environment.NewLine;

                                                fld_OGR_LevTid.Value = order.tmpPaketLevTid;
                                                tblOGR.Post();
                                            }

                                            // Uppdatera paketraden med nya datumet
                                            if (string.Compare(order.NyttLevDatum, order.tmpPaketLevTid) > 0 && order.UtanforLedtid == false)
                                            {
                                                Text = Text + "Paketrad " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.NyttLevDatum + Environment.NewLine;

                                                fld_OGR_LevTid.Value = order.NyttLevDatum;
                                                tblOGR.Post();

                                                order.tmpPaketLevTid = order.NyttLevDatum;

                                                // Börja om i loopen för ny saldotest
                                                tblOGR.GotoBookmark(order.IndexNrPaket);
                                            }


                                            //
                                            // TODO: VARFÖR GÖRS JÄMFÖRELSE MED ORDERHUVUDETS LEVERANSDATUM???? /Per N 211208
                                            //
                                            //
                                            // Blank reskod och leveransdatum skiljer mellan rad och huvud, spara sämsta leveranstid.
                                            if (string.IsNullOrEmpty(fld_OGR_Reservationskod.Value) && fld_OGR_LevTid.Value != Form_OrderHuvud_LevTid.Text && fld_OGR_LeveransFlagga.Value == "0")
                                            {
                                                OrderRadReskodBlank.Add(fld_OGR_RadNr.Value);

                                                // Spara sämsta och fösta leveranstid
                                                if (string.Compare(fld_OGR_LevTid.Value, lastLevDateResBlank) > 0)
                                                {
                                                    lastLevDateResBlank = fld_OGR_LevTid.Value;
                                                }
                                                if (string.Compare(fld_OGR_LevTid.Value, firstLevDateResBlank) < 0)
                                                {
                                                    firstLevDateResBlank = fld_OGR_LevTid.Value;
                                                }
                                            }

                                            // Reskod 1, spara sämsta leveranstid. Lägg in den tiden på alla rader som har reskod 1
                                            if (fld_OGR_Reservationskod.Value == "1" && fld_OGR_LeveransFlagga.Value == "0")
                                            {

                                                OrderRadResKod1.Add(fld_OGR_RadNr.Value);

                                                // Testa om aktuell leveranstid är senare än senast sparade
                                                if (string.Compare(fld_OGR_LevTid.Value, order.tmpORLevtid) > 0)
                                                {
                                                    order.tmpORLevtid = fld_OGR_LevTid.Value;
                                                }
                                            }

                                            tblOGR.Next();
                                        }

                                        // Spara orderrad för paketslut, backa pekaren tillbaka till sista paketraden
                                        tblOGR.Prior();
                                        if (fld_OGR_OrderNr.Value == order.OrderNr && fld_OGR_Fakturaflagga.Value == "i")
                                        {
                                            order.tmpPaketSlutOrderRad = fld_OGR_RadNr.Value;
                                        }
                                        else // OBS! Om man kommer in här är något fel på ordferraderna
                                        {
                                            tblOGR.Next();
                                            order.tmpPaketSlutOrderRad = fld_OGR_RadNr.Value;
                                        }


                                        // Uppdatera datum paketstart
                                        tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(order.tmpPaketOrderRad, 3));

                                        Text = Text + "Paketstart " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.tmpPaketLevTid + Environment.NewLine;

                                        fld_OGR_LevTid.Value = order.tmpPaketLevTid;
                                        tblOGR.Post();

                                        tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(order.tmpPaketSlutOrderRad, 3));

                                        Text = Text + "Paketslut " + fld_OGR_RadNr.Value + Environment.NewLine;

                                        // Uppdatera datum paketslut
                                        Text = Text + "Paketslut " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.tmpPaketLevTid + Environment.NewLine;

                                        fld_OGR_LevTid.Value = order.tmpPaketLevTid;
                                        tblOGR.Post();
                                    }
                                    else
                                    {
                                        paketstart = false;
                                    }
                                    #endregion Paketartiklar

                                    // Om förslag på nytt datum ligger längre fram än aktuellt datum
                                    if (string.Compare(order.NyttLevDatum, order.LevTidOR) > 0 && fld_OGR_RadNr.Value != order.tmpPaketSlutOrderRad && fld_OGR_LeveransFlagga.Value == "0")
                                    {
                                        Text += "Orderrad " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.NyttLevDatum + Environment.NewLine;

                                        // Uppdatera orderradens leveranstid
                                        fld_OGR_LevTid.Value = order.NyttLevDatum;
                                        tblOGR.Post();
                                    }
                                    else
                                    {
                                        Text += "Orderrad " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Ingen ändring" + Environment.NewLine;
                                    }

                                    // Blank reskod och leveransdatum skiljer mellan rad och huvud, spara sämsta leveranstid.
                                    if (string.IsNullOrEmpty(fld_OGR_Reservationskod.Value) && fld_OGR_LeveransFlagga.Value == "0")  // && string.Compare(order.NyttLevDatum, order.OnskadLeveranstid) > 0) // Ändrat 220117, Per N
                                    {
                                        OrderRadReskodBlank.Add(fld_OGR_RadNr.Value);

                                        // Spara sämsta och fösta leveranstid
                                        if (string.Compare(fld_OGR_LevTid.Value, lastLevDateResBlank) > 0)
                                        {
                                            lastLevDateResBlank = fld_OGR_LevTid.Value;
                                        }
                                        if (string.Compare(fld_OGR_LevTid.Value, firstLevDateResBlank) < 0)
                                        {
                                            firstLevDateResBlank = fld_OGR_LevTid.Value;
                                        }
                                    }

                                    // Reskod 1, spara sämsta leveranstid. Lägg in den tiden på alla rader som har reskod 1
                                    if (fld_OGR_Reservationskod.Value == "1" && fld_OGR_LeveransFlagga.Value == "0")
                                    {
                                        OrderRadResKod1.Add(fld_OGR_RadNr.Value);

                                        // Testa om aktuell leveranstid är senare än senast sparade
                                        if (string.Compare(fld_OGR_LevTid.Value, order.tmpORLevtid) > 0)
                                        {
                                            order.tmpORLevtid = fld_OGR_LevTid.Value;
                                        }
                                    }
                                }
                                if (paketstart)
                                {
                                    // Gå tillbaka till paketstart
                                    tblOGR.GotoBookmark(order.IndexNrPaket);
                                }
                                tblOGR.Prior();
                            }

                            if (OrderRadReskodBlank.Count > 0)
                            {
                                // Loopa alla rader med blank reservationskod och sätt gemensam leveranstid (sämsta)
                                foreach (var rad in OrderRadReskodBlank)
                                {
                                    tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(rad, 3));
                                    string tmpLevdate = fld_OGR_LevTid.Value;
                                    if (string.Compare(fld_OGR_LevTid.Value, firstLevDateResBlank) < 0)
                                    {
                                        fld_OGR_LevTid.Value = firstLevDateResBlank;
                                    }
                                    else if (string.Compare(fld_OGR_LevTid.Value, firstLevDateResBlank) > 0)
                                    {
                                        fld_OGR_LevTid.Value = lastLevDateResBlank;
                                    }
                                    tblOGR.Post();
                                    Text = Text + "Reskod Blank, Orderrad " + fld_OGR_RadNr.Value + " Levtid: " + tmpLevdate + " Nytt datum " + fld_OGR_LevTid.Value + Environment.NewLine;
                                }

                                SendKeys.SendWait("{F5}"); // ÄNDRAT 180621 FÖR TESTNING
                            }

                            if (OrderRadResKod1.Count > 0)
                            {
                                // Loopa alla rader med reservationskod 1 och sätt gemensam leveranstid (sämsta)
                                foreach (var rad in OrderRadResKod1)
                                {
                                    tblOGR.Find(Verktyg.fillBlankRight(order.OrderNr, 6) + Verktyg.fillBlankLeft(rad, 3));
                                    Text = Text + "Reskod 1, Orderrad " + fld_OGR_RadNr.Value + " Levtid: " + fld_OGR_LevTid.Value + " Nytt datum " + order.tmpORLevtid + Environment.NewLine;
                                    fld_OGR_LevTid.Value = order.tmpORLevtid;
                                    tblOGR.Post();
                                }

                                SendKeys.SendWait("{F5}"); // ÄNDRAT 180621 FÖR TESTNING

                                // Uppdatera OrderHuvud Levtid om alla rader har reskod 1
                                if (OrderRadReskodBlank.Count == 0)
                                {
                                    Form_OrderHuvud_LevTid.Text = order.tmpORLevtid;
                                }
                            }

                            //MessageBox.Show(Text);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Testa alla orderrader " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public string Saldotest()
        {
            try
            {
                // Ingen saldoberäkning på produktionsplanerade orderrader /220106 Per N
                if (fld_OGR_Belaggning.Value != "0" && fld_OGR_Belaggning.Value != "5")
                {
                    return order.NyttLevDatum;
                }

                //Hämta ledtid och saldo från artikeln
                tblAGA.Find(order.TestArtNr);

                bool flerlager = false;
                if (!string.IsNullOrEmpty(fld_AGA_Flerlager.Value))
                {
                    flerlager = true;
                    if (!string.IsNullOrEmpty(fld_OGR_LagerNr.Value))
                    {
                        tblAGL.Find(Verktyg.fillBlankRight(order.TestArtNr, 13) + fld_OGR_LagerNr.Value);
                    }
                }


                // Spara leveranstid orderrad, konvertera radantalet till double
                string ursprantal = fld_OGR_UrsprungligtAntal.Value.Replace(".", ",");
                string levantal = fld_OGR_LevereratAntal.Value.Replace(".", ",");
                order.RadAntal = Convert.ToDouble(ursprantal) - Convert.ToDouble(levantal);

                double tmpDisp = 0;
                double tmpDisp2 = 0;
                string tmpDate = string.Empty;
                bool DatumDelleveransNu = false;
                order.DispDelleverans = 0;
                order.DatumDelleverans = string.Empty;
                string tmpText = string.Empty;

                if (string.Compare(fld_AGA_Saldohanteras.Value, "5") < 0 && order.RadAntal > 0 && fld_OGR_LeveransFlagga.Value != "5")
                {
                    int Ledtid = int.Parse(fld_AGA_Ledtid.Value);
                    order.DateLedtidArtikel = DateTime.Now.AddDays(Ledtid).ToString("yyMMdd");

                    //Testa disponibelt om leveranstiden är inom ledtiden och antal > 0
                    if ((string.Compare(order.LevTidOR, order.DateLedtidArtikel) < 0 || order.LevTidOR == order.DateLedtidArtikel) && order.RadAntal > 0)
                    {
                        //order.DatePurcase = string.Empty;
                        if (flerlager)
                        {
                            order.Saldo = fld_AGL_Saldo.Value?.Replace(".", ",");
                        }
                        else
                        {
                            order.Saldo = fld_AGA_Saldo.Value?.Replace(".", ",");
                        }
                        order.SaldoNu = Convert.ToDouble(order.Saldo);

                        //Kalkylera åtgångar inom ledtiden !!!
                        ICalcAvailable saldo = GarpOrder.CalcAvailable;
                        saldo.Artnr = order.TestArtNr;
                        saldo.DateFrom = "010101";
                        saldo.DateTo = order.DateLedtidArtikel;
                        if (flerlager)
                        {
                            saldo.Lagernr = order.LagerNr;
                        }

                        saldo.Func = "R"; // Endast åtgångar
                        saldo.Calculate();

                        //Alla åtgångar summerade inom ledtid + saldo nu
                        order.Available = saldo.TabGetAvailable(saldo.IntervalCount).Replace(".", ",");
                        order.Disponibelt = Convert.ToDouble(order.Available);
                        order.Disponibelt = order.Disponibelt + order.SaldoNu;

                        //Om Saldo nu minus kundorder inom ledtid är minst 0 håller leveranstiden. Annars beräkna när leverans kan ske:
                        if (order.Disponibelt < 0)
                        {
                            // ************* Beräkna om saldo finns under hela ledtiden *************
                            saldo.Func = ""; // Åtgångar och tillgångar
                            saldo.Calculate();

                            order.Available = saldo.TabGetAvailable(saldo.IntervalCount).Replace(".", ",");
                            order.Disponibelt = Convert.ToDouble(order.Available);

                            // Test om raden kan dellevereras
                            for (int i = 1; i <= saldo.IntervalCount; i++)
                            {
                                tmpDate = saldo.TabGetDate(i);
                                if (string.Compare(order.LevTidOR, tmpDate) > 0)
                                {
                                    tmpDisp2 = Convert.ToDouble(saldo.TabGetAvailable(i).Replace(".", ","));
                                }
                                else
                                {
                                    tmpDisp2 = Convert.ToDouble(saldo.TabGetAvailable(i).Replace(".", ",")) + order.RadAntal;
                                }

                                tmpText += Environment.NewLine + tmpDisp2.ToString() + " " + saldo.TabGetDate(i);
                            }

                            for (int i = saldo.IntervalCount; i > 0; i--)
                            {
                                if (string.Compare(order.LevTidOR, saldo.TabGetDate(i)) > 0)
                                {
                                    tmpDisp = Convert.ToDouble(saldo.TabGetAvailable(i).Replace(".", ","));
                                }
                                else
                                {
                                    tmpDisp = Convert.ToDouble(saldo.TabGetAvailable(i).Replace(".", ",")) + order.RadAntal;
                                }
                                if (tmpDisp > 0)
                                {
                                    if (tmpDisp < order.DispDelleverans)
                                    {
                                        order.DispDelleverans = tmpDisp;
                                    }
                                    else
                                    {
                                        order.DispDelleverans = order.Disponibelt + order.RadAntal;
                                    }
                                    order.DatumDelleverans = saldo.TabGetDate(i);
                                }
                                else
                                {
                                    i = 0;
                                }
                            }
                            // Test om nytt datum är tidigare än orderradens
                            if (string.Compare(order.DatumDelleverans, order.LevTidOR) < 0)
                            {
                                order.DatumDelleverans = order.LevTidOR;
                                DatumDelleveransNu = true;
                            }
                            order.NyOrderrad = false;

                            //Disponibelt inom ledtid räcker inte för aktuell orderrad, ny leveranstid = ledtiden för artikeln
                            if (order.Disponibelt < 0)
                            {
                                order.NyttLevDatum = order.DateLedtidArtikel;
                                order.TextPurcase = "  (Preliminärt, nytt inköp krävs!)";
                            }

                            //Beräkna datum när leverans kan ske inom ledtiden
                            else
                            {
                                bool NegativeValueInLead = false;

                                order.Available = saldo.TabGetAvailable(1).Replace(".", ",");
                                double SenasteDisponibelt = Convert.ToDouble(order.Available);
                                string NewList = string.Empty;

                                //Loppa alla händelser om saldo är minst 0 kan leverans ske nu
                                for (int DispTest1 = 1; DispTest1 < saldo.IntervalCount; DispTest1++)
                                {
                                    order.Available = saldo.TabGetAvailable(DispTest1).Replace(".", ",");
                                    order.Disponibelt = Convert.ToDouble(order.Available);
                                    if (order.Disponibelt < 0)
                                    {
                                        NegativeValueInLead = true;
                                    }
                                }

                                //Om saldot är under 0 någon gång under ledtiden, testa när leverans kan ske (Påfyllnad efter sista negativa saldot)
                                if (NegativeValueInLead == true)
                                {
                                    for (int DispTest2 = saldo.IntervalCount; DispTest2 >= 1; DispTest2--) // Börja på sista händelsen inom ledtid och stega bakåt
                                    {
                                        order.Available = saldo.TabGetAvailable(DispTest2).Replace(".", ",");
                                        order.Disponibelt = Convert.ToDouble(order.Available);
                                        if (order.Disponibelt < 0)
                                        {
                                            order.TextPurcase = "  (Inköpsorder finns)";
                                            break; // Senaste datum funnet när saldot är under 0
                                        }
                                        else
                                        // Flytta datumet till aktuell händelse
                                        {
                                            order.NyttLevDatum = saldo.TabGetDate(DispTest2);
                                        }
                                    }

                                }
                            }
                        }
                        saldo = null;
                    }
                    else
                    {
                        order.UtanforLedtid = true;
                    }
                }

                // Om förslag på nytt datum ligger längre fram än aktuellt datum
                if (string.Compare(order.NyttLevDatum, order.LevTidOR) > 0)
                {
                    //Sätt ny leveranstid + 1 dag
                    order.NyttLevDatum = GarpOrder.DateCalculator.AddDays(order.NyttLevDatum, 1, -1);

                    // Sätt datum för delleverans + 1 dag om det är i framtiden
                    if (!DatumDelleveransNu)
                    {
                        order.DatumDelleverans = DeliveryDate();
                    }

                    order.TextNewDelTime += "Skickas: " + order.NyttLevDatum + order.TextPurcase;
                    if (order.DispDelleverans > 0)
                    {
                        if (order.DispDelleverans > order.RadAntal)
                        {
                            order.DispDelleverans = order.RadAntal;
                        }
                        order.TextNewDelTime += Environment.NewLine + Environment.NewLine + order.DatumDelleverans + " finns " + order.DispDelleverans + " st för delleverans.";
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Testa en orderrad " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return order.NyttLevDatum;
        }

        public void LevEnlPlock()
        {
            OrderLevEnlPlocklista lep = new OrderLevEnlPlocklista();
            List<OrderRows> paketorlst = new List<OrderRows>();
            string markSettings = FindAppSettings("orderlep");
            //string paketkod = FindAppSettings("PackagedCode");
            int PackageCount = 0;
            bool PackageLoop = false;
            bool IgnoreInvoiceFlag = false;

            //tblOGR.IndexNo = 1;
            tblOGR.Find(order.OrderNr);

            //if (!tblOGR.Find(order.Order_Rad))
            //{
            tblOGR.Next();
            //}

            try
            {
                while (!tblOGR.Eof && fld_OGR_OrderNr.Value == order.OrderNr)
                {
                    OrderRows or = new OrderRows();
                    or.MarkForDeliver = false;
                    or.HasValueNX1 = false;

                    or.RowNo = fld_OGR_RadNr.Value;
                    or.RowNoCheck = string.Compare(fld_OGR_RadNr.Value, "250");
                    or.ProductNo = fld_OGR_ArtNr.Value;
                    or.Description = tblOGR.Fields.Item("BEN").Value;
                    or.Amount = fld_OGR_UrsprungligtAntal.Value;
                    or.OrderNo = order.OrderNr;
                    or.DeliverDate = tblOGR.Fields.Item("LDT").Value;
                    or.DeliverDateCheck = string.Compare(or.DeliverDate, DateTime.Now.ToString("yyMMdd"));
                    or.PLF = tblOGR.Fields.Item("PLF").Value;
                    or.DeliverState = tblOGR.Fields.Item("LVF").Value;


                    if (markSettings == "old")                                                                                          //Skall vi kolla enl gamla regler eller nya?
                    {
                        or.AmountToDeliver = tblOGR.Fields.Item("NX1").Value;
                        if (!string.IsNullOrEmpty(or.AmountToDeliver))
                        {
                            double.TryParse(or.AmountToDeliver, out double ATD);                                                               // Då vissa NX1-värden kom ut lite konstigt från Garp, gör vi en "säker" koll här
                            if (ATD != 0)
                            {
                                or.HasValueNX1 = true;
                            }
                        }
                    }
                    if (markSettings == "new")                                                                                          //Skall vi kolla enl gamla regler eller nya?
                    {
                        or.AmountToDeliver = or.Amount;
                        if (or.DeliverState == "4")
                        {
                            or.AmountToDeliver = (Convert.ToInt16(or.Amount) - Convert.ToInt16(fld_OGR_LevereratAntal.Value)).ToString();
                        }
                    }

                    tblAGA.Find(or.ProductNo);
                    or.SUS = fld_AGA_Saldohanteras.Value;

                    if (fld_AGA_TillvKopKod.Value == order.Paketkod || PackageLoop)                                                     //Kollar om det är en paketartikel eller om vi är inne i en paketloop, sätter ett "ID" på den och lägger den i listan "paketorlst"
                                                                                                                                        //if (fld_AGA_TillvKopKod.Value == paketkod || PackageLoop)
                    {
                        if (!PackageLoop) //Första gången vi går in i en ny paketloop
                        {
                            IgnoreInvoiceFlag = true;
                        }

                        PackageLoop = true;
                        if (PackageCount == 0)
                        {
                            PackageCount++;
                        }

                        or.PackageID = PackageCount;
                        if (fld_OGR_Fakturaflagga.Value == "i" || IgnoreInvoiceFlag)
                        {
                            paketorlst.Add(or);
                        }

                        //if (fld_OGR_ArtNr.Value == "sp")

                        //Ingående artiklar i paketet har "i", övriga "p" eller "1"
                        if (fld_OGR_Fakturaflagga.Value != "i" && !IgnoreInvoiceFlag)
                        {
                            PackageCount++;
                            PackageLoop = false;                                                                                        //Paketet är slut, vi avslutar paketloopen
                            continue;
                        }

                        IgnoreInvoiceFlag = false;

                        tblOGR.Next();
                        continue;
                    }

                    if (or.DeliverState == "5")                                                                                         //Om raden är slutlevererad, tar vi inte med den i listan (men vi vill ändå hålla koll på den i paketet ovan)
                    {
                        tblOGR.Next();
                        continue;
                    }

                    if (markSettings == "old")                                                                                          //Skall vi kolla enl gamla regler eller nya?
                    {
                        if ((or.DeliverDateCheck <= 0) && (or.DeliverState != "5") && ((or.PLF == "3") || (or.PLF == "5") || (or.SUS == "9") || (or.HasValueNX1)))      //Om det inte är en paketartikel, kollar vi om den skall markeras för leverans
                        {
                            or.MarkForDeliver = true;
                        }
                    }

                    if (markSettings == "new")                                                                                          //Skall vi kolla enl gamla regler eller nya?
                    {
                        if ((or.DeliverState != "5") && ((or.PLF == "J") || (or.RowNoCheck == 1)))
                        {
                            or.MarkForDeliver = true;
                        }
                    }

                    lep.OrderRowList.Add(or);
                    tblOGR.Next();
                }

                if (paketorlst.Count > 0)                                                                                               //Finns det några paketartiklar i listan?
                {
                    int CheckEachPackage = 1;
                    while (CheckEachPackage < PackageCount)
                    {
                        List<OrderRows> tmporlst = new List<OrderRows>();
                        tmporlst = paketorlst.FindAll(o => o.PackageID == CheckEachPackage);                                             //Lägger alla artiklar som hör till detta paket i en temp-lista

                        if (markSettings == "old")
                        {
                            if ((tmporlst.All(o => o.DeliverDateCheck <= 0)) && (tmporlst.All(o => o.PLF == "3") || tmporlst.All(o => o.PLF == "5") || tmporlst.All(o => o.SUS == "9") || tmporlst.All(o => o.HasValueNX1)))
                            {
                                foreach (OrderRows or in tmporlst)
                                {
                                    if (or.DeliverState == "5")
                                    {
                                        or.MarkForDeliver = false;
                                        continue;                                                                                        //Vi tar bort redan slutlevererade orderrader från listan
                                    }
                                    else
                                    {
                                        or.MarkForDeliver = true;
                                    }

                                    lep.OrderRowList.Add(or);
                                }
                            }
                            else
                            {
                                foreach (OrderRows or in tmporlst)
                                {
                                    lep.OrderRowList.Add(or);
                                }
                            }
                            CheckEachPackage++;
                        }

                        if (markSettings == "new")
                        {
                            if ((tmporlst.All(o => o.PLF == "J")) || (tmporlst.All(o => o.SUS == "9")))
                            {
                                foreach (OrderRows or in tmporlst)
                                {
                                    if (or.DeliverState == "5")
                                    {
                                        or.MarkForDeliver = false;
                                        continue;                                                                                        //Vi tar bort redan slutlevererade orderrader från listan
                                    }
                                    else
                                    {
                                        or.MarkForDeliver = true;
                                    }

                                    lep.OrderRowList.Add(or);
                                }
                            }
                            else
                            {
                                foreach (OrderRows or in tmporlst)
                                {
                                    lep.OrderRowList.Add(or);
                                }
                            }
                            CheckEachPackage++;
                        }
                    }
                }

                lep.OrderRowList = lep.OrderRowList.OrderBy(o => o.RowNo).ToList();
                if (dsOGA.Fields.Item("PLF").Value.ToString() != "0")
                {
                    lep.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Ordern måste markeras som 'KLAR' innan utleverans kan ske!", "Meddelande:");
                }



                IOrderRowDeliver garpord = GarpOrder.OrderRowDeliver;
                foreach (OrderRows or in lep.OrderRowList)
                {
                    if (or.MarkForDeliverByUser)
                    {
                        garpord.Ordernr = or.OrderNo;
                        garpord.Radnr = or.RowNo;
                        garpord.Antal = or.AmountToDeliver;
                        garpord.Deliver();
                    }
                }
                SendKeys.SendWait("{F5}");
            }
            catch (Exception e)
            {
                MessageBox.Show("Leverans enl plocklista " + e.Message, "FEL!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #region AppSettings
        string GetAppSetting(Configuration config, string key)
        {
            KeyValueConfigurationElement element = config.AppSettings.Settings[key];
            if (element != null)
            {
                string value = element.Value;
                if (!string.IsNullOrEmpty(value))
                    return value;
            }
            return string.Empty;
        }

        string FindAppSettings(string key)
        {
            Configuration config = null;
            string exeConfigPath = this.GetType().Assembly.Location;
            string myValue = "new";
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(exeConfigPath);
            }
            catch
            { }

            if (config != null)
            {
                myValue = GetAppSetting(config, key);
                if (string.IsNullOrEmpty(myValue))
                {
                    myValue = "new";
                }
            }
            return myValue;
        }
        #endregion

        // Dialogruta
        private static DialogResult ShowInputDialog(ref string input)
        {
            try
            {
                int boxWidth = 300;
                int boxHight = 400;
                int textWidth = 200;
                int textLeft = 40;
                int textTop = 13;
                int inputWidth = 80;

                System.Drawing.Size size = new System.Drawing.Size(boxWidth, boxHight); // 230,85
                Form inputBox = new Form
                {
                    FormBorderStyle = FormBorderStyle.FixedDialog,
                    ClientSize = size,
                    Text = "Förskott",
                    StartPosition = FormStartPosition.CenterScreen
                };

                Label TextBelopp = new Label
                {
                    AutoSize = true,
                    Size = new System.Drawing.Size(textWidth, 13),
                    Location = new System.Drawing.Point(textLeft, textTop),
                    Text = "Belopp"
                };
                inputBox.Controls.Add(TextBelopp);

                TextBox belopp = new TextBox
                {
                    Size = new System.Drawing.Size(inputWidth, 130),
                    Location = new System.Drawing.Point(195, textTop - 3),
                    Text = input
                };
                inputBox.Controls.Add(belopp);




                Button okButton = new Button
                {
                    DialogResult = DialogResult.OK,
                    Name = "okButton",
                    Size = new System.Drawing.Size(75, 23),
                    Text = "&OK",
                    Location = new System.Drawing.Point(size.Width - 80 - 80, size.Height - 45)
                };
                inputBox.Controls.Add(okButton);

                Button cancelButton = new Button
                {
                    DialogResult = DialogResult.Cancel,
                    Name = "cancelButton",
                    Size = new System.Drawing.Size(75, 23),
                    Text = "&Cancel",
                    Location = new System.Drawing.Point(size.Width - 80, size.Height - 45)
                };
                inputBox.Controls.Add(cancelButton);

                inputBox.AcceptButton = okButton;
                inputBox.CancelButton = cancelButton;

                DialogResult result = inputBox.ShowDialog();
                if (inputBox.DialogResult == DialogResult.OK)
                {
                    input = belopp.Text;
                }
                else
                {
                    input = "cancel";
                }
                return result;
            }

            catch (Exception e)
            {
                MessageBox.Show("Information om delleverans " + e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return DialogResult.Cancel;
            }
        }

        // Skapa Orderradstext
        private void InsertRowText(string OrderNr, string RadNr, string Text, string PLfl = "0", string FAfl = "0", string FSfl = "0", string OBfl = "0")
        {
            tblOGK.Insert();
            fld_OrderRadText_OrderNr.Value = OrderNr;
            fld_OrderRadText_RadNr.Value = RadNr;
            fld_OrderRadText_Sekvens.Value = "255";
            fld_OrderRadText_FAfl.Value = FAfl;
            fld_OrderRadText_FSfl.Value = FSfl;
            fld_OrderRadText_OBfl.Value = OBfl;
            fld_OrderRadText_PLfl.Value = PLfl;
            fld_OrderRadText_Text.Value = Text;
            tblOGK.Post();
        }

        private class CampaignBracketPricing
        {
            public int Quantity { get; set; }
            public decimal Price { get; set; }
        }

        private List<CampaignBracketPricing> GetCampaignBracketPricing(string artikelnr, string kampanjprislisteid, int modellangd = 13)
        {
            try
            {
                int nyckel = 13;
                List<CampaignBracketPricing> result = new List<CampaignBracketPricing>();
                tblKP.IndexNo = 3;
                tblAPD.Find("K" + kampanjprislisteid);
                if (tblAPD.Fields.Item("KLG").Value != "")
                {
                    nyckel = Int32.Parse(tblAPD.Fields.Item("NL1").Value, CultureInfo.InvariantCulture);
                }
                if (artikelnr.Length < nyckel)
                {
                    nyckel = artikelnr.Length;
                }

                // Läs hela artikelnumret från listan
                artikelnr = Verktyg.fillBlankRight(Verktyg.Left(artikelnr, nyckel), 13);
                kampanjprislisteid = Verktyg.fillBlankRight(kampanjprislisteid, 10);

                for (int i = 65; i <= 90; i++)
                {

                    string kpIndex = "12" + artikelnr + " A" + kampanjprislisteid + Convert.ToChar(i).ToString();
                    if (tblKP.FindReal(kpIndex))
                    {
                        if (!string.IsNullOrEmpty(fld_KP_Anmarkning.Value))
                        {
                            if (int.TryParse(fld_KP_Anmarkning.Value, out int quantity) && decimal.TryParse(fld_KP_Pris.Value.Replace('.', ','), out decimal priceListPrice))
                            {
                                result.Add(new CampaignBracketPricing()
                                {
                                    Quantity = quantity,
                                    Price = priceListPrice
                                });
                            }
                        }
                    }
                }

                // Läs modeller från listan
                artikelnr = Verktyg.fillBlankRight(Verktyg.Left(artikelnr, modellangd), 13);

                for (int i = 65; i <= 90; i++)
                {

                    string kpIndex = "12" + artikelnr + " A" + kampanjprislisteid + Convert.ToChar(i).ToString();
                    if (tblKP.FindReal(kpIndex))
                    {
                        if (!string.IsNullOrEmpty(fld_KP_Anmarkning.Value))
                        {
                            if (int.TryParse(fld_KP_Anmarkning.Value, out int quantity) && decimal.TryParse(fld_KP_Pris.Value.Replace('.', ','), out decimal priceListPrice))
                            {
                                result.Add(new CampaignBracketPricing()
                                {
                                    Quantity = quantity,
                                    Price = priceListPrice
                                });
                            }
                        }
                    }
                }


                return result;
            }
            catch (Exception e)
            {
                _ = MessageBox.Show(e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }

        private string GetBestPrice(List<CampaignBracketPricing> bracketPrices, double quantityOnOrder, decimal priceOnOrder)
        {
            try
            {
                if (bracketPrices == null || bracketPrices.Count == 0)
                {
                    return priceOnOrder.ToString().Replace(",", ".");
                }
                string result = "";
                decimal priceFromBracket = priceOnOrder;
                foreach (CampaignBracketPricing bracketPrice in bracketPrices)
                {
                    if (quantityOnOrder >= bracketPrice.Quantity)
                    {
                        priceFromBracket = bracketPrice.Price;
                    }
                    else
                    {
                        break;
                    }
                }
                if (priceFromBracket < priceOnOrder)
                {
                    result = priceFromBracket.ToString();
                }
                else
                {
                    result = priceOnOrder.ToString();
                }
                return result.Replace(',', '.');
            }
            catch (Exception e)
            {
                _ = MessageBox.Show(e.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
            }
        }

        private string GetAdjustedRequestedOrderDate(string requestedDeliveryDate)//, string rowResCode, string rowD)
        {
            string result = "";
            try
            {
                // Leveransvilkor som alltid är idag, tex upphämtning.
                if (fld_OGA_LevVilk.Value == ConfigurationManager.AppSettings["DeliveryTermsToday"])
                {
                    return DateTime.Now.ToString("yyMMdd");
                }

                if (string.IsNullOrEmpty(requestedDeliveryDate))
                {
                    requestedDeliveryDate = DateTime.Now.ToString("yyMMdd"); //GarpOrder.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), 1, -1);
                }

                if (DateTime.TryParseExact(requestedDeliveryDate, "yyMMdd", null, DateTimeStyles.None, out DateTime onskadLevTid))
                {
                    if (DateTime.Compare(onskadLevTid, DateTime.Now) < 0)
                    {
                        onskadLevTid = DateTime.Now;
                    }
                }
                else
                {
                    // Datum kan inte konverteras.
                    onskadLevTid = DateTime.Now;
                }

                if (string.Compare(onskadLevTid.ToString("yyMMdd"), DateTime.Now.ToString("yyMMdd")) > 0)
                {
                    return requestedDeliveryDate;
                }

                // The date is today. Should it still be today or adjusted to tomorrow?
                tblKA.Find(fld_OGA_KundNr.Value);
                if (string.IsNullOrEmpty(fld_KA_Kod5.Value))
                {
                    // Ta nästa dag
                    return GarpOrder.DateCalculator.AddDays(onskadLevTid.ToString("yyMMdd"), 1, -1);
                }
                else
                {
                    // Kontrollera tiden om den är innan tiden i konfig, om inte ta nästa dag
                    if (DateTime.TryParseExact(DateTime.Now.ToString("yyMMdd") + " " + ConfigurationManager.AppSettings["TimeOrderStopToday"], "yyMMdd HH:mm:ss", null, DateTimeStyles.None, out DateTime deadline))
                    {
                        if (DateTime.Compare(DateTime.Now, deadline) < 0)
                        {
                            // Before deadline
                            return DateTime.Now.ToString("yyMMdd");
                        }
                        else
                        {
                            // Add one day
                            return GarpOrder.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), 1, -1);
                        }
                    }
                    else
                    {
                        // Faulty config? Take tomorrow
                        return GarpOrder.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), 1, -1);
                    }
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in SetAdjustRequestedOrderDate: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                result = GarpOrder.DateCalculator.AddDays(requestedDeliveryDate, 1, -1);
            }
            return result;
        }

        /// <summary>
        /// Resets the price bracket controlls IF the new item no is different than the one the controlls belong to
        /// </summary>
        /// <returns>True of the controlls have been reset and new ones can be displayed or False if no new controlls are needed</returns>
        private bool ResetPriceBracketControlls(string itemNo)
        {
            try
            {
                if (BracketQuantities != null && BracketsPrices != null)
                {
                    if (BracketQuantities.Count > 0 && BracketsPrices.Count > 0)
                    {
                        Dictionary<string, List<IComponent>>.KeyCollection quantities = BracketQuantities.Keys;
                        Dictionary<string, List<IComponent>>.KeyCollection prices = BracketsPrices.Keys;
                        string oldQItemNo = quantities.First();
                        string oldPItemNo = prices.First();
                        if (oldPItemNo == itemNo && oldQItemNo == itemNo)
                        {
                            return false;
                        }
                    }
                }

                if (BracketQuantities == null)
                {
                    BracketQuantities = new Dictionary<string, List<IComponent>>();
                }
                else if (BracketQuantities.Count > 0)
                {
                    foreach (KeyValuePair<string, List<IComponent>> keys in BracketQuantities)
                    {
                        foreach (IComponent cmp in keys.Value)
                        {
                            ComponentsOrder.Delete(cmp.Name);
                        }
                    }
                    BracketQuantities = new Dictionary<string, List<IComponent>>();
                }

                if (BracketsPrices == null)
                {
                    BracketsPrices = new Dictionary<string, List<IComponent>>();
                }
                else if (BracketsPrices.Count > 0)
                {
                    foreach (KeyValuePair<string, List<IComponent>> keys in BracketsPrices)
                    {
                        foreach (IComponent cmp in keys.Value)
                        {
                            ComponentsOrder.Delete(cmp.Name);
                        }
                    }
                    BracketsPrices = new Dictionary<string, List<IComponent>>();
                }

                return true;
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in DeletePriceBracketControlls: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
        }

        private void DisplayPriceBrackets(string itemNo, List<CampaignBracketPricing> campaignBracketPricings)
        {
            try
            {
                if (!ResetPriceBracketControlls(itemNo))
                {
                    return;
                }

                int color = ComponentsOrder.Item("artikelinfoEdit").Color;

                int i = 1;
                List<IComponent> quantities = new List<IComponent>();
                List<IComponent> prices = new List<IComponent>();
                foreach (CampaignBracketPricing bracketPricing in campaignBracketPricings)
                {
                    IComponent cmpAntal = ComponentsOrder.AddEdit(StrAntal + i);
                    cmpAntal.Top = Top + (RowSpacing * i);
                    cmpAntal.Left = Left;
                    cmpAntal.Width = Width;
                    cmpAntal.ReadOnly = true;
                    cmpAntal.Color = color;
                    cmpAntal.Text = bracketPricing.Quantity.ToString();

                    IComponent cmpPris = ComponentsOrder.AddEdit(StrPris + i);
                    cmpPris.Top = Top + (RowSpacing * i);
                    cmpPris.Left = Left + Width + ColumnSpacing;
                    cmpPris.Width = Width * 2;
                    cmpPris.ReadOnly = true;
                    cmpPris.Color = color;
                    cmpPris.Text = bracketPricing.Price.ToString("0.00").Replace(",", ".");

                    quantities.Add(cmpAntal);
                    prices.Add(cmpPris);

                    i++;
                }

                if (quantities.Count > 0 && prices.Count > 0)
                {
                    LblAntal.Visible = true;
                    LblPris.Visible = true;
                    BracketQuantities.Add(itemNo, quantities);
                    BracketsPrices.Add(itemNo, prices);
                }
                else
                {
                    LblAntal.Visible = false;
                    LblPris.Visible = false;
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in DisplayPriceBrackets: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetWarehouseNoFromRow(string orderNo, string rowNo)
        {
            try
            {
                if (string.IsNullOrEmpty(orderNo) || string.IsNullOrEmpty(rowNo))
                {
                    return null;
                }

                if (tblOGR.Find(orderNo.PadRight(6) + rowNo.PadLeft(3)))
                {
                    return fld_OGR_LagerNr.Value;
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in GetWarehouseNoFromRow: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        private void SetWarehouseNoOnRow(string orderNo, string rowNo, string warehouseNo)
        {
            try
            {
                if (string.IsNullOrEmpty(orderNo) || string.IsNullOrEmpty(rowNo) || string.IsNullOrEmpty(warehouseNo))
                {
                    return;
                }

                if (tblOGR.Find(orderNo.PadRight(6) + rowNo.PadLeft(3)))
                {
                    fld_OGR_LagerNr.Value = warehouseNo;
                    tblOGR.Post();
                }
            }
            catch (Exception e)
            {
                _ = MessageBox.Show("Error in GetWarehouseNoFromRow: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //private void CheckRowsLikePrioOrder()
        //{
        //    try
        //    {
        //        string onr = dsOGA.Fields.Item("ONR").Value;

        //        if (string.IsNullOrEmpty(onr))
        //        {
        //            return;
        //        }

        //        if (!OrderHead.Table.Find(onr))
        //        {
        //            _ = MessageBox.Show("Customer Order No " + onr + " not found in order table (OGA)");
        //            return;
        //        }

        //        if (!OrderHeadOKForPrioOrderCheck())
        //        {
        //            return;
        //        }

        //        ResetOrderHeadAccordingToPrioOrder();
                
        //        string config = ConfigurationManager.AppSettings["WarehouseNos"] ?? string.Empty;
        //        List<string> warehouseNos = config.Split('|').ToList();

        //        _ = OrderRow.Table.Find(onr);
        //        OrderRow.Table.Next();

        //        CustomerOrderRow undeliveredOrderRow = new CustomerOrderRow(GarpOrder);
        //        undeliveredOrderRow.Table.IndexNo = 2;
        //        CustomerOrder undeliveredOrderHead = new CustomerOrder(GarpOrder);
        //        List<CustomerOrderRow> orderRows = new List<CustomerOrderRow>();
                
        //        while (!OrderRow.Table.Eof && OrderRow.OrderNo?.PadRight(6) == onr?.PadRight(6))
        //        {
        //            if (Item.Table.Find(OrderRow.ItemNo))
        //            {
        //                if (OrderRowOKForPrioOrderCheck(warehouseNos))
        //                {
        //                    CheckOrderRowLikePrioOrder(warehouseNos);
        //                }
        //            }

        //            undeliveredOrderRow.Table.Find(OrderRow.ItemNo);
        //            undeliveredOrderRow.Table.Next();
        //            while (!undeliveredOrderRow.Table.Eof && undeliveredOrderRow.ItemNo == OrderRow.ItemNo)
        //            {
        //                if (!Item.Table.Find(OrderRow.ItemNo))
        //                {
        //                    _ = MessageBox.Show($"Item no '{OrderRow.ItemNo}' not found.");
        //                    undeliveredOrderRow.Table.Next();
        //                    continue;
        //                }

        //                if (undeliveredOrderHead.Table.Find(undeliveredOrderRow.OrderNo))
        //                {
        //                    _ = MessageBox.Show($"Order no '{undeliveredOrderRow.OrderNo}' not found.");
        //                    undeliveredOrderRow.Table.Next();
        //                    continue;
        //                }

        //                if (
        //                    (undeliveredOrderRow.WarehouseNo == OrderRow.WarehouseNo
        //                    && undeliveredOrderRow.RowType != "9"
        //                    && undeliveredOrderRow.AmountRowState != "*"
        //                    && undeliveredOrderRow.RowNo < 251
        //                    && undeliveredOrderHead.WaitingAt != "000"
        //                    && undeliveredOrderHead.FreezeCode != "z"
        //                    && undeliveredOrderHead.OrderType != "0"
        //                    && undeliveredOrderHead.X1State != "1")
        //                    ||
        //                    (undeliveredOrderHead.OrderType == "9"
        //                    && undeliveredOrderRow.OriginalQuantity > 0
        //                    && undeliveredOrderRow.WarehouseNo == OrderRow.WarehouseNo
        //                    && undeliveredOrderRow.RowType != "9"
        //                    && undeliveredOrderRow.AmountRowState != "*")
        //                    )
        //                {
        //                    decimal remainingQuantity = OrderRow.OriginalQuantity - OrderRow.DeliveredQuantity;

        //                    if (undeliveredOrderHead.OrderType != "9"
        //                        || (undeliveredOrderHead.OrderType == "9" && remainingQuantity < 0))
        //                    {
        //                        if (!OrderFreight.Table.Find(undeliveredOrderHead.OrderNo))
        //                        {
        //                            _ = MessageBox.Show($"Order Freight '{undeliveredOrderHead.OrderNo}' not found.");
        //                        }

        //                        string requestedDate = OrderFreight.Freight06;

        //                        if (!Customer.Table.Find(undeliveredOrderHead.CustomerNo))
        //                        {
        //                            _ = MessageBox.Show($"Customer no '{undeliveredOrderHead.CustomerNo}' not found.");
        //                        }

        //                        if (string.Compare(requestedDate, DateTime.Now.ToString("yyMMdd")) < 0)
        //                        {
        //                            if (CompareTimeToNow(1)
        //                                || (CompareTimeToNow(2) && !string.IsNullOrEmpty(Customer.Code5))
        //                                || (CompareTimeToNow(3) && undeliveredOrderHead.ModeOfDelivery == ConfigurationManager.AppSettings["ModeOfDeliveryForPickUp"]))
        //                            {
        //                                requestedDate = DateTime.Now.ToString("yyMMdd");
        //                            }
        //                            else
        //                            {
        //                                requestedDate = GarpOrder.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), 1, -1);
        //                            }
        //                        }

        //                        if (undeliveredOrderHead.OrderType == "9")
        //                        {
        //                            remainingQuantity *= -1;
        //                        }

        //                        PurchaseOrderRow.Table.IndexNo = 2;
        //                        PurchaseOrderRow.Table.Find(undeliveredOrderRow.ItemNo);
        //                        PurchaseOrderRow.Table.Next();

        //                        while (!PurchaseOrderRow.Table.Eof && PurchaseOrderRow.ItemNo == undeliveredOrderRow.ItemNo)
        //                        {
        //                            if (PurchaseOrderRow.WarehouseNo == OrderRow.WarehouseNo)
        //                            {
        //                                if (!PurchaseOrderHead.Table.Find(PurchaseOrderRow.PurchaseOrderNo))
        //                                {
        //                                    _ = MessageBox.Show("Couldn't find purchase order no " + PurchaseOrderRow.PurchaseOrderNo, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //                                }

        //                                if (PurchaseOrderHead.OrderType == "0")
        //                                {
        //                                    PurchaseOrderRow.Table.Next();
        //                                    continue;
        //                                }

        //                                decimal remainingPurchaseQuantity = PurchaseOrderRow.InitialQuantity;
        //                                if (PurchaseOrderRow.DeliveryState == "4")
        //                                {
        //                                    remainingPurchaseQuantity -= PurchaseOrderRow.DeliveredQuantity;
        //                                }

        //                            }
        //                        }

        //                        if (undeliveredOrderRow.BState == "5")
        //                        {
        //                            if (true)
        //                            {

        //                            }
        //                        }
        //                    }
        //                }
        //                undeliveredOrderRow.Table.Next();
        //            }

        //            OrderRow.Table.Next();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        _ = MessageBox.Show("Error in CheckRowsLikePrioOrder: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        //private void CompareCurrentOrderLikePrioOrder()
        //{
        //    try
        //    {
        //        string onr = dsOGA.Fields.Item("ONR").Value;
        //        if (string.IsNullOrEmpty(onr))
        //        {
        //            return;
        //        }

        //        if (!OrderHead.Table.Find(onr))
        //        {
        //            return;
        //        }

        //        if (!OrderFreight.Table.Find(onr))
        //        {
                    
        //        }

        //        if (OrderHead.DeliveryState == "4" && string.IsNullOrEmpty(OrderHead.ReservationCode))
        //        {
        //            OrderHead.ReservationCode = "1";
        //            OrderHead.Table.Post();
        //        }

        //        bool allRowsOK = true;
        //        bool anyRowOK = false;
        //        decimal remainingQuantity = 0;
        //        DateTime firstDeliveryDate = new DateTime();
        //        DateTime lastDeliveryDate = DateTime.Now.AddYears(100);
        //        DateTime res2FirstDeliveryDate = new DateTime();
        //        DateTime res2LastDeliveryDate = new DateTime();
        //        DateTime requestedDate = DateTime.Now;

        //        OrderRow.Table.IndexNo = 1;
        //        _ = OrderRow.Table.Find(onr);
        //        OrderRow.Table.Next();

        //        while (!OrderRow.Table.Eof
        //            && OrderRow.OrderNo.PadRight(6) == onr.PadRight(6)
        //            && OrderRow.RowNo < 251)
        //        {
        //            if (!int.TryParse(OrderRow.DeliveryState, out int deliveryState))
        //            {
        //                _ = MessageBox.Show("Delivery state (OGR:LVF) couldn't be parsed to an integer: " + OrderRow.DeliveryState);
        //            }
        //            if (deliveryState < 5)
        //            {
        //                if (OrderRow.PickState == "N")
        //                {
        //                    allRowsOK = false;
        //                }
        //                else if (OrderRow.PickState == "Y")
        //                {
        //                    anyRowOK = true;
        //                }

        //                if (!string.IsNullOrEmpty(OrderRow.ItemNo)
        //                    && OrderRow.RowType != "9"
        //                    && OrderRow.AmountRowState != "*"
        //                    && Item.Table.Find(OrderRow.ItemNo))
        //                {
        //                    if (int.TryParse(Item.WarehousingImpact, out int warehouseingImpact))
        //                    {
        //                        _ = MessageBox.Show($"Warehousing impact (AGA:SUS) for item no {Item.ItemNo} couldn't be parsed to an integer: " + Item.WarehousingImpact);
        //                    }

        //                    if (Item.ItemType != "z" && warehouseingImpact < 5)
        //                    {
        //                        // Save the last delivery date
        //                        if (DateTime.Compare(OrderRow.DeliveryDate, lastDeliveryDate) > 0)
        //                        {
        //                            lastDeliveryDate = OrderRow.DeliveryDate;
        //                        }

        //                        // Save the earliest delivery date
        //                        if (DateTime.Compare(OrderRow.DeliveryDate, firstDeliveryDate) < 0)
        //                        {
        //                            firstDeliveryDate = OrderRow.DeliveryDate;
        //                        }

        //                        if (OrderRow.ReservationCode == "2")
        //                        {
        //                            if (res2FirstDeliveryDate == new DateTime() || DateTime.Compare(OrderRow.DeliveryDate, res2FirstDeliveryDate) < 0)
        //                            {
        //                                res2FirstDeliveryDate = OrderRow.DeliveryDate;
        //                            }

        //                            if (res2LastDeliveryDate == new DateTime() || DateTime.Compare(OrderRow.DeliveryDate, res2LastDeliveryDate) > 0)
        //                            {
        //                                res2FirstDeliveryDate = OrderRow.DeliveryDate;
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //            OrderRow.Table.Next();
        //        }

        //        // Consolidation of delivery dates
        //        // All order rows gets the last delivery date on orders with reservation code 1
        //        if (OrderHead.ReservationCode == "1")
        //        {
        //            firstDeliveryDate = lastDeliveryDate;
        //        }

        //        // Move old dates before today to today
        //        if (DateTime.Compare(firstDeliveryDate, DateTime.Now) < 0)
        //        {
        //            firstDeliveryDate = DateTime.Now;
        //        }

        //        if (DateTime.Compare(lastDeliveryDate, DateTime.Now) < 0)
        //        {
        //            lastDeliveryDate = DateTime.Now;
        //        }

        //        // Move dates before the requested date of the customers
        //        if (DateTime.Compare(firstDeliveryDate, requestedDate) < 0)
        //        {
        //            firstDeliveryDate = requestedDate;
        //        }

        //        if (res2FirstDeliveryDate != new DateTime())
        //        {
        //            if (DateTime.Compare(firstDeliveryDate, res2FirstDeliveryDate) > 0)
        //            {
        //                firstDeliveryDate = res2FirstDeliveryDate;
        //            }
        //        }

        //        if (DateTime.Compare(lastDeliveryDate, requestedDate) < 0)
        //        {
        //            lastDeliveryDate = requestedDate;
        //        }

        //        _ = OrderRow.Table.Find(onr);
        //        OrderRow.Table.Next();

        //        while (!OrderRow.Table.Eof && OrderRow.OrderNo.PadRight(6) == onr.PadRight(6) && OrderRow.RowNo < 251)
        //        {
        //            if (OrderRow.ReservationCode == "3") // Reservation code for blocked rows
        //            {

        //            }
        //            else if (OrderRow.ReservationCode == "2") // Reservation code for rows which will get aggregated dates
        //            {
        //                if (OrderRow.DeliveryDate != res2LastDeliveryDate)
        //                {
        //                    OrderRow.DeliveryDate = res2LastDeliveryDate;
        //                    OrderRow.Table.Post();
        //                }
        //            }
        //            else
        //            {
        //                if (DateTime.Compare(OrderRow.DeliveryDate, firstDeliveryDate) < 0)
        //                {
        //                    OrderRow.DeliveryDate = firstDeliveryDate;
        //                    OrderRow.Table.Post();
        //                }

        //                if (OrderHead.ReservationCode == "1" && OrderRow.DeliveryDate != lastDeliveryDate)
        //                {
        //                    OrderRow.DeliveryDate = lastDeliveryDate;
        //                    OrderRow.Table.Post();
        //                }
        //            }
        //            OrderRow.Table.Next();
        //        }

        //        // Update the X1F-flag
        //        if (firstDeliveryDate.ToString("yyMMdd") == DateTime.Now.ToString("yyMMdd")
        //            && res2FirstDeliveryDate.ToString("yyMMdd") == DateTime.Now.ToString("yyMMdd")
        //            && allRowsOK)
        //        {

        //        }
        //        else if (
        //            (firstDeliveryDate.ToString("yyMMdd") == DateTime.Now.ToString("yyMMdd")
        //            && res2FirstDeliveryDate.ToString("yyMMdd") == DateTime.Now.ToString("yyMMdd"))
        //            )
        //        {

        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        _ = MessageBox.Show("Error in CompareCurrentOrderLikePrioOrder: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        //private void CheckOrderRowLikePrioOrder(List<string> warehouseNos)
        //{
        //    try
        //    {
        //        ResetOrderRowAccordingToPrioOrder(warehouseNos);

        //        CustomerOrder orderHead = new CustomerOrder(GarpOrder);
        //        CustomerOrderRow orderRow = new CustomerOrderRow(GarpOrder);

        //        bool pickToday = CompareTimeToNow(1);
        //        bool pickTodayVIP = CompareTimeToNow(2);
        //        bool pickUpToday = CompareTimeToNow(3);
        //        string modeOfDeliveryForPickUp = ConfigurationManager.AppSettings["ModeOfDeliveryForPickUp"] ?? string.Empty;

        //        orderRow.Table.IndexNo = 2;
        //        _ = orderRow.Table.Find(OrderRow.ItemNo);
        //        orderRow.Table.Next();
        //        while (!orderRow.Table.Eof && orderRow.ItemNo == orderRow.ItemNo)
        //        {
        //            try
        //            {
        //                if (orderHead.Table.Find(orderRow.OrderNo))
        //                {
        //                    if (orderRow.WarehouseNo == OrderRow.WarehouseNo
        //                        && orderRow.RowType != "9"
        //                        && orderRow.AmountRowState != "*"
        //                        && orderRow.RowNo < 251
        //                        && orderHead.WaitingAt != "000"
        //                        && orderHead.FreezeCode != "z"
        //                        && orderHead.OrderType != "0"
        //                        && orderHead.X1State != "1")
        //                    {
        //                        if (Item.Table.Find(orderRow.ItemNo))
        //                        {
        //                            decimal remainingQuantity = orderRow.OriginalQuantity;
        //                            if (orderRow.DeliveryState == "4")
        //                            {
        //                                remainingQuantity -= orderRow.DeliveredQuantity;
        //                            }

        //                            if (orderHead.OrderType != "9" || (orderHead.OrderType == "9" && remainingQuantity < 0))
        //                            {
        //                                if (OrderFreight.Table.Find(orderHead.OrderNo) && Customer.Table.Find(orderHead.CustomerNo))
        //                                {
        //                                    string requestedDate = OrderFreight.Freight06;
        //                                    if (string.Compare(requestedDate, DateTime.Now.ToString("yyMMdd")) < 0)
        //                                    {
        //                                        if (pickToday
        //                                            || (pickTodayVIP && !string.IsNullOrEmpty(Customer.Code5))
        //                                            || (pickUpToday && orderHead.ModeOfDelivery == modeOfDeliveryForPickUp))
        //                                        {
        //                                            requestedDate = DateTime.Now.ToString("yyMMdd");
        //                                        }
        //                                        else
        //                                        {
        //                                            requestedDate = GarpOrder.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), 1, -1);
        //                                        }
        //                                    }
        //                                }

        //                                if (orderHead.OrderType == "9")
        //                                {
        //                                    remainingQuantity *= -1;
        //                                }

        //                                PurchaseOrderRow.Table.IndexNo = 2;
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception)
        //            {

        //            }
        //            orderRow.Table.Next();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        _ = MessageBox.Show("Error in CheckOrderRowLikePrioOrder: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}
        /// <summary>
        /// Return true if pick today is possible timewise: 1 = Normal pick, 2 = VIP pick, 3 = Pick up in store
        /// </summary>
        /// <param name="time">1 = Normal pick, 2 = VIP pick, 3 = Pick up in store</param>
        /// <returns></returns>
        //private bool CompareTimeToNow(int choice)
        //{
        //    try
        //    {
        //        string time = string.Empty;
        //        switch (choice)
        //        {
        //            case 1: time = ConfigurationManager.AppSettings["PickTodayTime"] ?? string.Empty; break;
        //            case 2: time = ConfigurationManager.AppSettings["PickTodayTimeVIP"] ?? string.Empty; break;
        //            case 3: time = ConfigurationManager.AppSettings["PickUpTodayTime"] ?? string.Empty; break;
        //            default: return false;
        //        }
        //        return string.Compare(DateTime.Now.ToString("yyMMdd"), time) < 0;
        //    }
        //    catch (Exception e)
        //    {
        //        _ = MessageBox.Show("Error in CompareTimeToNow: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    return false;
        //}

        //private void ResetOrderRowAccordingToPrioOrder(List<string> warehouseNos)
        //{
        //    try
        //    {
        //        string onr = dsOGA.Fields.Item("ONR").Value;
        //        if (onr != OrderRow.OrderNo)
        //        {

        //        }

        //        if (string.IsNullOrEmpty(OrderRow.WarehouseNo))
        //        {
        //            OrderRow.WarehouseNo = "1";
        //            OrderRow.Table.Post();
        //        }

        //        if (warehouseNos == null || warehouseNos.Count == 0)
        //        {
        //            return;
        //        }

        //        if (warehouseNos.Exists(x => x == OrderRow.WarehouseNo))
        //        {
        //            if (OrderRow.DeliveryDate == new DateTime())
        //            {
        //                OrderRow.DeliveryDate = DateTime.Now;
        //                OrderRow.Table.Post();
        //            }

        //            if (OrderRow.PickState != "Y")
        //            {
        //                _ = int.TryParse(OrderRow.BState, out int blf);
        //                if (OrderRow.RowNo > 251 || blf > 0)
        //                {
        //                    OrderRow.PickState = "Y";
        //                }
        //                else
        //                {
        //                    OrderRow.PickState = "1";
        //                }
        //                OrderRow.Table.Post();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        _ = MessageBox.Show("Error in ResetOrderRowAccordingToPrioOrder: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        //private void ResetOrderHeadAccordingToPrioOrder()
        //{
        //    try
        //    {
        //        string onr = dsOGA.Fields.Item("ONR").Value;
        //        if (onr != OrderHead.OrderNo)
        //        {
        //            if (!OrderHead.Table.Find(onr))
        //            {
        //                return;
        //            }
        //        }

        //        if (!OrderFreight.Table.Find(OrderHead.OrderNo))
        //        {
        //            OrderFreight.Table.Insert();
        //            OrderFreight.OrderNo = OrderHead.OrderNo;
        //            OrderFreight.Freight06 = OrderHead.OrderDateTime.ToString("yyMMdd");
        //            OrderFreight.Table.Post();
        //        }
        //        else if (string.IsNullOrEmpty(OrderFreight.Freight06))
        //        {
        //            OrderFreight.Freight06 = OrderHead.OrderDateTime.ToString("yyMMdd");
        //            OrderFreight.Table.Post();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        _ = MessageBox.Show("Error in ResetOrderHeadAccordingToPrioOrder: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        //private bool OrderRowOKForPrioOrderCheck(List<string> warehouseNos)
        //{
        //    try
        //    {
        //        if (OrderRow.PickState != "1" || OrderRow.DeliveryState == "5")
        //        {
        //            return false;
        //        }

        //        if (!warehouseNos.Exists(x => x == OrderRow.WarehouseNo))
        //        {
        //            return false;
        //        }

        //        if (!(OrderRow.RowType == "9" || OrderRow.AmountRowState == "*"))
        //        {
        //            return false;
        //        }

        //        if (OrderRow.RowNo > 250)
        //        {
        //            return false;
        //        }

        //        if (OrderHead.OrderType == "9" && OrderRow.OriginalQuantity >= 0)
        //        {
        //            return false;
        //        }

        //        if (OrderRow.ItemNo != Item.ItemNo)
        //        {
        //            if (!Item.Table.Find(OrderRow.ItemNo))
        //            {
        //                return false;
        //            }
        //        }

        //        _ = int.TryParse(Item.WarehousingImpact, out int warehousingImpact);
        //        if (warehousingImpact > 4)
        //        {
        //            return false;
        //        }

        //        if (Item.ItemType == "z")
        //        {
        //            return false;
        //        }

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        _ = MessageBox.Show("Error in OrderRowOKForPrioOrderCheck: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    return false;
        //}

        //private bool OrderHeadOKForPrioOrderCheck()
        //{
        //    try
        //    {
        //        if (!(
        //            OrderHead.PickState == "1" ||
        //            OrderHead.WaitingAt == "000" ||
        //            OrderHead.FreezeCode == "z" ||
        //            OrderHead.OrderConfirmationState == "0" ||
        //            OrderHead.X1State == "1"
        //            ))
        //        {
        //            return false;
        //        }

        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        _ = MessageBox.Show("Error in OrderHeadOKForPrioOrderCheck: " + e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    return false;
        //}

        // Stäng Garp-kopplingen
        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName + "Forms order", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Dispose(bool Disposing)
        {
            try
            {
                if (!m_disposed)
                {
                    if (Disposing)
                    {
                        m_disposed = true;
                        if (GarpOrder != null)
                        {
                            GarpOrder.FieldExit -= FieldExit;
                            GarpOrder.FieldEnter -= FieldEnter;
                            GarpOrder.ButtonClick -= ButtonClik;
                            //dsOGA.BeforeDelete -= BeforeDeleteOGR;
                            //dsOGA.BeforeDelete -= BeforeDeleteOGA;
                            dsOGR.BeforePost -= BeforePostOR;
                            dsOGA.BeforePost -= BeforePostOH;
                            dsOGR.AfterScroll -= AfterScrollOGR;
                            dsOGA.AfterScroll -= AfterScrollOGA;

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblOGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblOGF);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblOGC);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblOGL);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblOGR);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblOGK);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAGT);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAGL);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAGS);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblKA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblKB);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblTA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAPD);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblKP);

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGR);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGL);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGF);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(GarpOrder);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName + "Forms order", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        ~Order()
        {
            Dispose(false);
        }
    }
}