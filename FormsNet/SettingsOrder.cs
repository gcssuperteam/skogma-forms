﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace FormsSkogma
{
    public class SettingsOrder
    {
        public string ArtKatFrakter = ConfigurationManager.AppSettings["KategoriFrakter"];

        // inställningar för leveransdatum
        /*
        public string OrderStoppLevIdag = "12:00:00"; // Stopptid för leverans samma dag
        public string KundNrLevIdag = "N"; // Stopptid gäller endast kundnr som börjar på .. // Ej aktuell 2019-04-17 / Per N
        public string KundFaltVIP = "KD5"; // VIP-kunder för leverans idag
        public string KundNrNordforest = "N"; // Kunder som börjar på .. är nordforestkund
        public string LevSattIdag = "05"; // Order med detta leveranssät får leveransdatum idag
        */

        public string OrderStoppLevIdag = ConfigurationManager.AppSettings["TimeOrderStopToday"]; // Stopptid för leverans samma dag
        public string KundFaltVIP = ConfigurationManager.AppSettings["FieldVIPCustomer"]; // VIP-kunder för leverans idag
        public string KundNrNordforest = ConfigurationManager.AppSettings["PrefixCustomerNordforest"]; // Kunder som börjar på .. är nordforestkund
        public string LevSattIdag = ConfigurationManager.AppSettings["DeliveryTermsToday"]; // Order med detta leveranssät får leveransdatum idag

        // Paketartikel
        //public string PaketSlut = "PS";
        public string Paketkod = ConfigurationManager.AppSettings["PackagedCode"];
        public string tmpPaketArtikel { get; set; }
        public string tmpPaketLevTid { get; set; }
        public string tmpPaketOrderRad { get; set; }
        public string tmpPaketStartLevTid { get; set; }
        public string tmpPaketSlutOrderRad { get; set; }
        public int IndexNrPaket { get; set; }
        public bool Paketstart { get; set; }
        public bool NyttPaket { get; set; }

        // fält för version
        public string LblVersion = "Ver";

        // variabler
        public string Bolag { get; set; }
        public string OrderNr { get; set; }
        public string RadNr { get; set; }
        public string Order_Rad { get; set; }
        public string ArtNr { get; set; }
        public string NewOrderNr { get; set; }

        public double OrderVikt { get; set; }
        public bool InternOrder = false;
        public bool BtnNewOrder = false;
        public bool TestNewOrder { get; set; }
        public bool TestNewOrderBtn { get; set; }
        public bool Levererad { get; set; }


        // fält för önskad levveranstid
        public string LblDesiredDeliveryText = "DesiredDeliveryText";
        public string LblDesiredDelivery = "DesiredDelivery";
        public string LblDateCreditControlTxt { get; set; }
        public string LblDateCreditControl { get; set; }
        public string OnskadLeveranstid { get; set; }
        public string DesiredDeliveryTmp { get; set; }

        // knapp för justering leveranstider alla orderrader
        public string ButtonSaldoTest = "BalanceTest";
        public string ButtonORSaldoTest = "RowBalanceTest";

        // förvald reservationskod
        public string ForvadReservationskodOH = "";

        // saldotest
        public string OrderEjKlarFlagga = ConfigurationManager.AppSettings["OrderNotReadyFlag"];
        public string LedtidDatum { get; set; } // Datum som ersätter ledtiden
        public string LevTidOR { get; set; }
        public string TestArtNr { get; set; }
        public string NyttLevDatum { get; set; }
        public string TextNewDelTime { get; set; }
        public string TextPurcase { get; set; }
        public string Available { get; set; }
        public string DateLedtidArtikel { get; set; }
        public string Saldo { get; set; }
        public double RadAntal { get; set; }
        public double Disponibelt { get; set; }
        public double SaldoNu { get; set; }
        public bool UtanforLedtid { get; set; }
        public bool NyOrderrad { get; set; }
        public double DispDelleverans { get; set; }
        public string DatumDelleverans { get; set; }
        public bool SplittaOrderrad { get; set; }
        public double NyRadAntal { get; set; }
        public string LagerNr { get; set; }

        //public string DatePurcase { get; set; }
        //public bool FindInkOrder { get; set; }

        // bevakning av fält
        public string tmpOrderHuvudLeveranstid { get; set; }
        public string tmpOrderHuvudReservationskod { get; set; }
        public string tmpRadLeveranstid { get; set; }
        public string tmpOrderTyp { get; set; }
        public string tmpRadAntal { get; set; }
        public string tmpORLevtid { get; set; }
        public bool tmpResKodSatt { get; set; }

        // Leveranssätt
        public string tmpLevSatt { get; set; }
        public string tmpLevSattBtn { get; set; }
        public bool BtnLevSattKlick { get; set; }

        public bool RadAntalEllerDatumAndrad { get; set; }

        // Betalningsvillkor för förbetald order
        public List<string> PrePaymentConditions = ConfigurationManager.AppSettings["PrePaymentConditions"]?.Split('|')?.ToList();
        public string PrePaymentConditionsName = ConfigurationManager.AppSettings["PrePaymentConditionsName"];

        // Förskott
        public string AdvancePayItem = ConfigurationManager.AppSettings["AdvancePayItem"];
        public string AdvancePaymentTermAdvance = ConfigurationManager.AppSettings["AdvancePayTermsAdvance"];
        public string AdvancePaymentTermGoodsOrder = ConfigurationManager.AppSettings["AdvancePayTerms"];
        public string AdvPayActive = ConfigurationManager.AppSettings["AdvancePayActive"];
    }

    //Order - Leverans enligt plocklista
    public class OrderRows
    {
        public string ProductNo { get; set; }
        public string RowNo { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string AmountToDeliver { get; set; }
        public string OrderNo { get; set; }
        public bool MarkForDeliver { get; set; }
        public bool MarkForDeliverByUser { get; set; }
        public int PackageID { get; set; }
        public string PLF { get; set; }
        public string DeliverState { get; set; }
        public string DeliverDate { get; set; }
        public int DeliverDateCheck { get; set; }
        public string SUS { get; set; }
        public bool HasValueNX1 { get; set; }
        public int RowNoCheck { get; set; }
    }
}
