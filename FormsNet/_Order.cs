﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FormsSkogma
{
    public class Order : IDisposable
    {
        Garp.Application GarpOrder = null;
        Garp.Dataset dsOGA, dsOGR;
<<<<<<< .mine
||||||| .r10674
        Garp.ITable tblOGA, tblOGR, tblAGA, tblIGR, tblOGC;
=======
        
        Garp.ITable tblOGA // Orderhuvud
            , tblOGR // Orderrad
            , tblAGA // Artikel
            //, tblIGR // Inköpsorderrad
            , tblOGC // OrderHuvudFrakt
            ;
        
        Garp.ITabField fld_OGA_LevStatus
            , fld_OGA_LevTid
            , fld_OGR_OrderNr
            , fld_OGR_RadNr
            , fld_OGR_Reservationskod
            , fld_OGC_OrderNr
            , fld_OGC_CH6
            , fld_AGA_TillvKopKod
            , fld_AGA_Saldohanteras
            , fld_AGA_Ledtid
            , fld_AGA_Saldo
            ;
        
        Garp.IComponent LabelVersion
            , LabelDesiredDeliveryText
            , LabelDesiredDelivery
            , btnNyOrder
            , fldOrderTyp
            , fldOrderNr
            , fldOHLevTid
            , fldRadNr
            , fldRadLevTid
            , fldRadAntal
            , fldArtNr
            , fldKundNr
            ;
>>>>>>> .r10677

<<<<<<< .mine
        Garp.ITable tblOGA // Orderhuvud
            , tblOGR // Orderrad
            , tblAGA // Artikel
            //, tblIGR // Inköpsorderrad
            , tblOGC // OrderHuvudFrakt
            ;

        Garp.ITabField fld_OGA_LevStatus
            , fld_OGA_LevTid
            , fld_OGR_OrderNr
            , fld_OGR_RadNr
            , fld_OGR_Reservationskod
            , fld_OGC_OrderNr
            , fld_OGC_CH6
            , fld_AGA_TillvKopKod
            , fld_AGA_Saldohanteras
            , fld_AGA_Ledtid
            , fld_AGA_Saldo
            ;

        Garp.IComponent LabelVersion
            , LabelDesiredDeliveryText
            , LabelDesiredDelivery
            , btnNyOrder
            , fldOrderTyp
            , fldOrderNr
            , fldOHLevTid
            , fldRadNr
            , fldRadLevTid
            , fldRadAntal
            , fldArtNr
            , fldKundNr
            ;


||||||| .r10674
        Garp.IComponent LabelVersion, LabelDesiredDeliveryText, LabelDesiredDelivery, btnNyOrder, fldOrderTyp, fldOrderNr, fldOHLevTid, fldRadNr, fldRadLevTid, fldRadAntal, fldArtNr, fldKundNr;
=======

>>>>>>> .r10677
        string LblVersion = "Ver";
        string LblDesiredDeliveryText = "DesiredDeliveryText";
        string LblDesiredDelivery = "DesiredDelivery";

        string Bolag = string.Empty;
        string OrderNr = string.Empty;
        string RadNr = string.Empty;
        string ArtNr = string.Empty;
        string LevTidOR = string.Empty;
        string NewOrderNr = string.Empty;
        bool InternOrder = false;
        bool BtnNewOrder = false;
        bool NewOrder = true;
        bool m_disposed = false;
        SettingsOrder setting = new SettingsOrder();

        public Order()
        {
            GarpOrder = new Garp.Application();
            Bolag = GarpOrder.Bolag;

            // Hämta datakällan för OrderHuvud och OrderRad
            dsOGA = GarpOrder.Datasets.Item("ogaMcDataSet");
            dsOGR = GarpOrder.Datasets.Item("ogrMcDataSet");

            // Skapa bevakning på händelser
            dsOGA.AfterScroll += AfterScrollOGA;
            dsOGR.AfterScroll += AfterScrollOGR;
            dsOGA.BeforePost += BeforePost;
            //dsOGA.BeforeDelete += BeforeDeleteOGA;
            //dsOGR.BeforeDelete += BeforeDeleteOGR;

            GarpOrder.ButtonClick += ButtonClik;
            GarpOrder.FieldEnter += FieldEnter;
            GarpOrder.FieldExit += FieldExit;

            // Hämta tabeller
            tblOGA = GarpOrder.Tables.Item("OGA");
            tblOGR = GarpOrder.Tables.Item("OGR");
            tblAGA = GarpOrder.Tables.Item("AGA");
            tblOGC = GarpOrder.Tables.Item("OGC");

            // Tabellfält
            fld_OGA_LevStatus = tblOGA.Fields.Item("LEF");
            fld_OGA_LevTid = tblOGA.Fields.Item("BLT");
            fld_OGR_OrderNr = tblOGR.Fields.Item("ONR");
            fld_OGR_RadNr = tblOGR.Fields.Item("RDC");
            fld_OGR_Reservationskod = tblOGR.Fields.Item("RES");
            fld_OGC_OrderNr = tblOGC.Fields.Item("ONR");
            fld_OGC_CH6 = tblOGC.Fields.Item("CH6");
            fld_AGA_TillvKopKod = tblAGA.Fields.Item("TVK");
            fld_AGA_Saldohanteras = tblAGA.Fields.Item("SUS");
            fld_AGA_Ledtid = tblAGA.Fields.Item("NLT");
            fld_AGA_Saldo = tblAGA.Fields.Item("LGA");

            // Bevakning av knappar och fält i formuläret
            btnNyOrder = GarpOrder.Components.Item("plusBitBtn");
            fldOrderTyp = GarpOrder.Components.Item("ogaOtyMcEdit");
            fldOrderNr = GarpOrder.Components.Item("onrEdit");
            fldOHLevTid = GarpOrder.Components.Item("ogaBltMcEdit");
            fldRadNr = GarpOrder.Components.Item("oradEdit");
            fldRadLevTid = GarpOrder.Components.Item("ogrLdtMcEdit");
            fldRadAntal = GarpOrder.Components.Item("ogrOraMcEdit");
            fldArtNr = GarpOrder.Components.Item("ogrAnrMcEdit");
            fldKundNr = GarpOrder.Components.Item("knrEdit");

            //TODO: Om gamla händelser finns som ger minussaldo repeteras varningen om nytt leveransdatum!
        }

        public void AfterScrollOGA()
        {
            try
            {
                if (fldOrderNr.Text != null)
                {
                    //Test om ny order är vald
                    if (fldOrderNr.Text != OrderNr) { NewOrder = true; }
                    else { NewOrder = false; }

                    OrderNr = fldOrderNr.Text;

                    // Test om internorder
                    if (fldOrderTyp.Text == "9") { InternOrder = true; }
                    else { InternOrder = false; }

                    // Välj flik för fält och knappar
                    GarpOrder.Components.BaseComponent = "TabSheet1";

                    // Ej internorder, ej levererad
                    if (InternOrder == false && tblOGA.Find(Verktyg.fillBlankRight(OrderNr, 6)) && string.Compare(fld_OGA_LevStatus.Value, "4") < 0)
                    {
                        // Lägg till fälttext för önskad leveranstid
                        if (GarpOrder.Components.Item(LblDesiredDeliveryText) == null)
                        {
                            LabelDesiredDeliveryText = GarpOrder.Components.AddLabel(LblDesiredDeliveryText);
                            LabelDesiredDeliveryText.Top = 314;
                            LabelDesiredDeliveryText.Left = 13;
                            LabelDesiredDeliveryText.Width = 40;
                            LabelDesiredDeliveryText.Text = "Önskad leveranstid:";
                        }

                        // Lägg till fält för önskad leveranstid. Tabell OrderHuvudFrakt, fältet Frakt06
                        if (GarpOrder.Components.Item(LblDesiredDelivery) == null)
                        {
                            LabelDesiredDelivery = GarpOrder.Components.AddEdit(LblDesiredDelivery);
                            LabelDesiredDelivery.Top = 311;
                            LabelDesiredDelivery.Left = 120;
                            LabelDesiredDelivery.Width = 60;
                            LabelDesiredDelivery.Height = 21;
                        }

                        // Skapa tabellpost för OGC
                        if (!tblOGC.Find(OrderNr))
                        {
                            tblOGR.Find(OrderNr + "000");
                            tblOGR.Next();

                            // Ordern saknar orderrader, CH6 = imorgon
                            if (fld_OGR_OrderNr.Value != OrderNr)
                            {
                                string CurrentTime = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                                string KundNr = fldKundNr.Text;

                                // Test om klockan är före klockslag enligt inställning eller om kundnummer inte börjar på nummer enligt inställning
                                if (string.Compare(CurrentTime, setting.OrderStoppLevIdag) > 0 || Verktyg.Left(KundNr, 1) != setting.KundNrLevIdag)
                                {
                                    // Önskad leveranstid = imorgon
                                    setting.DesiredDelivery = GarpOrder.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), 1, -1);
                                }
                                else
                                {
                                    // Önskad leveranstid = idag
                                    setting.DesiredDelivery = DateTime.Now.ToString("yyMMdd");
                                }
                            }

                            // Order med befintliga rader, CH6 = OH, leveranstid
                            else
                            {
                                setting.DesiredDelivery = fldOHLevTid.Text;
                            }

                            tblOGC.Insert();
                            fld_OGC_OrderNr.Value = OrderNr;
                            fld_OGC_CH6.Value = setting.DesiredDelivery;
                            tblOGC.Post();

                        }

                        // Uppdatera fältet önskad leveranstid med värdet från tabellen
                        else
                        {
                            tblOGC.Find(OrderNr);
                            setting.DesiredDelivery = fld_OGC_CH6.Value;
                        }

                        // Visa värdet i fältet från tabellen, varna med rött om önksat datum skiljer mot OrderHuvud Levtid
                        LabelDesiredDelivery.Text = setting.DesiredDelivery;
                        if (LabelDesiredDelivery.Text != fldOHLevTid.Text)
                        {
                            LabelDesiredDelivery.Color = 10066431;
                        }
                        else
                        {
                            LabelDesiredDelivery.Color = 16777215;
                        }
                    }

                    // Släck fälten på internorder och leverad order
                    else
                    {
                        if (GarpOrder.Components.Item(LblDesiredDelivery) != null)
                        {
                            GarpOrder.Components.Delete(LblDesiredDelivery);
                        }
                        if (GarpOrder.Components.Item(LblDesiredDeliveryText) != null)
                        {
                            GarpOrder.Components.Delete(LblDesiredDeliveryText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("After scroll OGA " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void AfterScrollOGR()
        {
            try
            {
                //MessageBox.Show(GarpOrder.ObmDel);
                if (fldRadNr.Text != null)
                {
                    ArtNr = fldArtNr.Text;
                    RadNr = fldRadNr.Text;

                    // Välj flik för fält och knappar
                    GarpOrder.Components.BaseComponent = "TabSheet3";

                    // Visa versionsnummer
                    if (GarpOrder.Components.Item(LblVersion) == null)
                    {
                        LabelVersion = GarpOrder.Components.AddLabel(LblVersion);
                        LabelVersion.Top = 300;
                        LabelVersion.Left = 8;
                        LabelVersion.Width = 40;
                        LabelVersion.Text = "Skogma forms version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("After scroll OGR " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void ButtonClik()
        {
            try
            {
                // Knapp för ny order
                if (GarpOrder.Components.CurrentField == btnNyOrder.Name)
                {
                    BtnNewOrder = true;
                }
                else
                {
                    BtnNewOrder = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Button klick " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        public void FieldEnter()
        {
            try
            {
                //MessageBox.Show("Enter " + GarpOrder.Components.Item("onrEdit").Text);

                //Aktuellt fält = 
                if (GarpOrder.Components.CurrentField == "")
                {
                    // TODO: Testa om offert byts till order, läsa igenom raderna för att ge leveranstid?
                    // TODO: Ändring leveranstid på orderhuvudet i efterhand,  läsa igenom raderna för att testa leveranstid?
                }

                //Aktuellt fält = Radnr och ny order skapad via plus-knappen, sätt leveranstiden
                if (GarpOrder.Components.CurrentField == fldRadNr.Name)
                {
                    if (BtnNewOrder == true)
                    {
                        DeliveryDate();
                        BtnNewOrder = false;
                    }

                    // HÄR LANDAR MAN EFTER PAKETARTIKEL HAR LAGT TILL ORDERRADER
                    if (ArtNr == setting.PaketSlut && setting.Paketstart == true)
                    {
                        // TODO: Saldokontroll på alla paketraderna
                        tblOGR.Find(Verktyg.fillBlankRight(OrderNr, 6) + Verktyg.fillBlankLeft(RadNr, 3));
                        while (tblOGR.Bof != true && fld_OGR_OrderNr.Value == OrderNr && fld_OGR_RadNr.Value != setting.tmpOrderRad)
                        {
                            // Här loppas alla ingående i paket, gör saldotest på de som saldohanteras
                            tblOGR.Prior();
                        }
                        //MessageBox.Show(setting.tmpPaketArtikel);
                        setting.Paketstart = false;
                        // TODO: Sätt fokus på radnr
                    }
                }

                //Aktuellt fält = Önskad leveranstid, spara värdet
                if (GarpOrder.Components.Item(LblDesiredDelivery) != null && GarpOrder.Components.CurrentField == GarpOrder.Components.Item(LblDesiredDelivery).Name)
                {
                    setting.DesiredDeliveryTmp = GarpOrder.Components.Item(LblDesiredDelivery).Text;
                }

                //Aktuellt fält = Orderrad leveranstid, spara värdet
                if (GarpOrder.Components.CurrentField == fldRadLevTid.Name)
                {
                    setting.tmpORLevtid = fldRadLevTid.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldEnter " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void FieldExit()
        {
            try
            {
                //Aktuellt fält = Ordernr
                if (GarpOrder.Components.CurrentField == fldOrderNr.Name && fldOrderNr.Text != null)
                {
                    //Ny order, ge förslag på leveranstid
                    DeliveryDate();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldExit, spara levtid orderhuvud " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            try
            {
                //Aktuellt fält = Önskad leveranstid och nytt värde har angetts
                if (
                    GarpOrder.Components.Item(LblDesiredDelivery) != null
                    && GarpOrder.Components.CurrentField == GarpOrder.Components.Item(LblDesiredDelivery).Name
                    && GarpOrder.Components.Item(LblDesiredDelivery).Text != setting.DesiredDeliveryTmp // Ändring har gjorts på önskad leveranstid
                    )
                {
                    // Spara nya värdet till OrderHuvudFrakt, Frakt06
                    if (tblOGC.Find(OrderNr))
                    {
                        fld_OGC_CH6.Value = LabelDesiredDelivery.Text;
                        tblOGC.Post();

                        // TODO: Här skall saldokontroll göras på alla orderrader
                        tblOGR.Find(OrderNr + "000");
                        tblOGR.Next();

                        // Ordern saknar orderrader
                        if (fld_OGR_OrderNr.Value != OrderNr)
                        {

                        }

                        // OH Levtid = önskad leveranstid
<<<<<<< .mine
                        //fldOHLevTid.Text = LabelDesiredDelivery.Text;
                        //GarpOrder.Components.Item("ogaBltMcEdit").Text = LabelDesiredDelivery.Text;
                        tblOGA.Find(OrderNr);
                        fld_OGA_LevTid.Value = LabelDesiredDelivery.Text;
                        tblOGA.Post();

                        //MessageBox.Show("Ändrar OH Levtid " + fldOHLevTid.Text + ", Tabellvärde: " + fld_OGA_LevTid.Value);

||||||| .r10674
                        fldOHLevTid.Text = LabelDesiredDelivery.Text;



=======
                        //fldOHLevTid.Text = LabelDesiredDelivery.Text;
                        GarpOrder.Components.Item("ogaBltMcEdit").Text = LabelDesiredDelivery.Text;
                        //tblOGA.Find(OrderNr);
                        //MessageBox.Show("Ändrar OH Levtid " + fldOHLevTid.Text + ", Tabellvärde: " + fld_OGA_LevTid.Value);
                         
>>>>>>> .r10677
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldExit, ny önskad leveranstid " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            try
            {
                // TODO: Hantera internorder??? Nu görs ingen test på ordertyp 9

                //Aktuellt fält = Antal eller Radleveranstid och antal > 0, ej internorder
                if (InternOrder == false
                    && fldRadNr.Text != null
                    && GarpOrder.Components.CurrentField == fldRadAntal.Name
                    || GarpOrder.Components.CurrentField == fldRadLevTid.Name)
                {
                    //SendKeys.SendWait("{F5}");

                    LevTidOR = fldRadLevTid.Text;
                    setting.NyLevDatum = LevTidOR;
                    setting.TextNewDelTime = string.Empty;
                    setting.TextPurcase = string.Empty;

                    setting.NyLevDatum = TestOneLine(); // Testa om leveranstiden håller på raden

                    if (setting.NyLevDatum != LevTidOR) // Om ny leveranstid räknats ut
                    {
                        // Sätt ny leveranstid till idag om det uträknade datumet är dåtid
                        if (string.Compare(setting.NyLevDatum, DateTime.Now.ToString("yyMMdd")) < 0)
                        {
                            setting.NyLevDatum = DateTime.Now.ToString("yyMMdd");
                        }
                        //Sätt ny leveranstid + 1 dag
                        setting.NyLevDatum = GarpOrder.DateCalculator.AddDays(setting.NyLevDatum, 1, -1);
                        setting.TextNewDelTime = "Ny leveranstid: " + setting.NyLevDatum + setting.TextPurcase;

                        /*
                        // Om inköp finns inom ledtiden
                        if (setting.FindInkOrder == true)
                        {
                            // Sök inköp per datum
                            tblIGR = GarpOrder.Tables.Item("IGR");
                            Garp.ITabFilter findInkRad = tblIGR.Filter;
                            findInkRad.AddField("LDT");
                            findInkRad.AddConst(setting.DatePurcase);
                            findInkRad.AddField("ANR");
                            findInkRad.AddConst(ArtNr);
                            findInkRad.Expression = "1=2&3=4";
                            findInkRad.Active = true;

                            tblIGR.First();

                            // Hämta antalet och resrevationskod för inköpet
                            while (tblIGR.Eof != true)
                            {
                                setting.TextNewDelTime = setting.TextNewDelTime + tblIGR.Fields.Item("ORA").Value + " st";
                                if (tblIGR.Fields.Item("RES").Value != "5")
                                {
                                    setting.TextNewDelTime = setting.TextNewDelTime + " Ej bekräftad av leverantör!" + Environment.NewLine;
                                }
                                else
                                {
                                    setting.TextNewDelTime = setting.TextNewDelTime + " Bekräftad!" + Environment.NewLine;
                                }
                                tblIGR.Next();
                            }
                        }
                         */

                        MessageBox.Show(setting.TextNewDelTime, "Varning, saldobrist", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        fldRadLevTid.Text = setting.NyLevDatum;
                        //SendKeys.SendWait("{F5}");
                    }

                    // Sök artikeln på orderraden, kolla om artikeln är en paketartikel, spara värden
                    tblAGA.Find(ArtNr);
                    if (fld_AGA_TillvKopKod.Value == setting.Paketkod)
                    {
                        setting.Paketstart = true;
                        setting.tmpPaketArtikel = ArtNr;
                        setting.tmpOrderRad = RadNr;
                    }
                    else
                    {
                        setting.Paketstart = false;
                    }

                    // Om leveranstid på orderrad har ändrats på en paketartikel, ändra datum på ingående
                    if (
                        GarpOrder.Components.CurrentField == fldRadLevTid.Name
                        && setting.Paketstart == true
                        && fldRadLevTid.Text != setting.tmpORLevtid
                        )
                    {
                        MessageBox.Show("Paketarttikel med nytt datum!");
                        // TODO: uppdatera datum på ingående detaljer i paket
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("FieldExit, test saldo " + ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void BeforePost()
        {
            try
            {
                // Aktuellt fält = OH Leveranstid och ändring gjorts till tidigare datum än önskat datum
                if (GarpOrder.Components.CurrentField == fldOHLevTid.Name && string.Compare(fldOHLevTid.Text, setting.DesiredDelivery) < 0)
                {
                    dsOGA.Abort();
                    tblOGA.Find(OrderNr);
                    MessageBox.Show("Leveranstiden kan ej sättas före önskad leveranstid: " + setting.DesiredDelivery + Environment.NewLine, "Varning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
<<<<<<< .mine
                    fldOHLevTid.Text = fld_OGA_LevTid.Value;

||||||| .r10674
                    fldOHLevTid.Text = tblOGA.Fields.Item("BLT").Value;
=======
                    fldOHLevTid.Text = fld_OGA_LevTid.Value;
                    
>>>>>>> .r10677
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("BeforePost " + e.Message);
            }
        }

        public void DeliveryDate()
        {
            try
            {

                string CurrentTime = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                string KundNr = fldKundNr.Text;

                // Om klockan är efter 12 eller EJ Nxxx -kund, sätt leveranstid imorgon
                if (string.Compare(CurrentTime, setting.OrderStoppLevIdag) > 0 || Verktyg.Left(KundNr, 1) != setting.KundNrLevIdag)
                {
                    // Test om ny order (rader saknas)
                    tblOGR.Find(OrderNr + "000");
                    tblOGR.Next();
                    if (fld_OGR_OrderNr.Value != OrderNr)
                    {
                        // OH Levtid = imorgon
                        fldOHLevTid.Text = GarpOrder.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), 1, -1);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Sätt leveranstid till imorgon " + e.Message);
            }
        }

        public void TestAllLines()
        {
            try
            {

            }
            catch (Exception e)
            {
                MessageBox.Show("Testa alla orderrader " + e.Message);
            }
        }

        public string TestOneLine()
        {
            try
            {
                //Hämta ledtid och saldo från artikeln
                tblAGA.Find(ArtNr);
                if (string.Compare(fld_AGA_Saldohanteras.Value, "5") < 0)
                {
                    // Spara leveranstid orderrad, konvertera radantalet till double

                    string antal = fldRadAntal.Text.Replace(".", ",");
                    double RadAntal = Convert.ToDouble(antal);
                    int Ledtid = Int32.Parse(fld_AGA_Ledtid.Value);
                    setting.DateLedtidArtikel = DateTime.Now.AddDays(Ledtid).ToString("yyMMdd");
                    //string DateTo = DateTime.Now.AddDays(+100).ToString("yyMMdd");

                    //Testa disponibelt om leveranstiden är inom ledtiden och antal > 0
                    if ((string.Compare(LevTidOR, setting.DateLedtidArtikel) < 0 || LevTidOR == setting.DateLedtidArtikel) && RadAntal > 0)
                    {

                        setting.DatePurcase = string.Empty;
                        setting.Saldo = fld_AGA_Saldo.Value.Replace(".", ",");
                        setting.SaldoNu = Convert.ToDouble(setting.Saldo);
                        //setting.FindInkOrder = false;

                        //Kalkylera åtgångar inom ledtiden
                        Garp.ICalcAvailable saldo = GarpOrder.CalcAvailable;
                        saldo.Artnr = ArtNr;
                        saldo.DateFrom = "010101";
                        saldo.DateTo = setting.DateLedtidArtikel;
                        saldo.Func = "R"; // Endast åtgångar
                        saldo.Calculate();

                        //Alla åtgångar summerade inom ledtid + saldo nu
                        setting.Available = saldo.TabGetAvailable(saldo.IntervalCount).Replace(".", ",");
                        setting.Disponibelt = Convert.ToDouble(setting.Available);
                        setting.Disponibelt = setting.Disponibelt + setting.SaldoNu;

                        //Om Saldo nu minus kundorder inom ledtid är minst 0 håller leveranstiden. Annars beräkna när leverans kan ske:
                        if (setting.Disponibelt < 0)
                        {
                            // ************* Beräkna om saldo finns under hela ledtiden *************
                            saldo.Func = ""; // Åtgångar och tillgångar
                            saldo.Calculate();

                            setting.Available = saldo.TabGetAvailable(saldo.IntervalCount).Replace(".", ",");
                            setting.Disponibelt = Convert.ToDouble(setting.Available);

                            //Disponibelt inom ledtid räcker inte för aktuell orderrad, ny leveranstid = ledtiden för artikeln
                            if (setting.Disponibelt < 0)
                            {
                                setting.NyLevDatum = setting.DateLedtidArtikel;
                                setting.TextPurcase = Environment.NewLine + Environment.NewLine + "Preliminärt, nytt inköp krävs!";
                            }

                            //Beräkna datum när leverans kan ske inom ledtiden
                            else
                            {
                                bool NegativeValueInLead = false;

                                setting.DispList = string.Empty;
                                setting.Available = saldo.TabGetAvailable(1).Replace(".", ",");
                                double SenasteDisponibelt = Convert.ToDouble(setting.Available);
                                string NewList = string.Empty;

                                //Loppa alla händelser om saldo är minst 0 kan leverans ske nu
                                for (int DispTest1 = 1; DispTest1 < saldo.IntervalCount; DispTest1++)
                                {
                                    setting.Available = saldo.TabGetAvailable(DispTest1).Replace(".", ",");
                                    setting.Disponibelt = Convert.ToDouble(setting.Available);
                                    if (setting.Disponibelt < 0)
                                    {
                                        NegativeValueInLead = true;
                                    }
                                }

                                //Om saldot är under 0 någon gång under ledtiden, testa när leverans kan ske (Påfyllnad efter sista negativa saldot)
                                if (NegativeValueInLead == true)
                                {
                                    for (int DispTest2 = saldo.IntervalCount; DispTest2 >= 1; DispTest2--) // Börja på sista händelsen inom ledtid och stega bakåt
                                    {
                                        setting.DispList = setting.DispList + saldo.TabGetDate(DispTest2) + " " + saldo.TabGetAvailable(DispTest2) + " " + Environment.NewLine;

                                        setting.Available = saldo.TabGetAvailable(DispTest2).Replace(".", ",");
                                        setting.Disponibelt = Convert.ToDouble(setting.Available);
                                        if (setting.Disponibelt < 0)
                                        {
                                            //setting.FindInkOrder = true;
                                            setting.TextPurcase = Environment.NewLine + Environment.NewLine + "Inköpsorder finns." + Environment.NewLine;
                                            break; // Senaste datum funnet när saldot är under 0
                                        }
                                        else
                                        // Flytta datumet till aktuell händelse
                                        {
                                            setting.NyLevDatum = saldo.TabGetDate(DispTest2);
                                            setting.DatePurcase = saldo.TabGetDate(DispTest2);
                                        }

                                        //MessageBox.Show("DispList:" + Environment.NewLine + setting.DispList + Environment.NewLine
                                        //+ " Nytt leveransdatum " + setting.NyLevDatum + Environment.NewLine
                                        //);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Testa en orderrad " + e.Message);
            }
            return setting.NyLevDatum;
        }

        // Stäng Garp-kopplingen
        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        protected virtual void Dispose(bool Disposing)
        {
            try
            {
                if (!m_disposed)
                {
                    if (Disposing)
                    {
                        if (GarpOrder != null)
                        {
                            GarpOrder.Components.Restore();
                            GarpOrder.FieldExit -= FieldExit;
                            GarpOrder.FieldEnter -= FieldEnter;
                            GarpOrder.ButtonClick -= ButtonClik;
                            //dsOGA.BeforeDelete -= BeforeDeleteOGR;
                            //dsOGA.BeforeDelete -= BeforeDeleteOGA;
                            dsOGR.BeforePost -= BeforePost;
                            dsOGR.AfterScroll -= AfterScrollOGR;
                            dsOGA.AfterScroll -= AfterScrollOGA;

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGR);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(GarpOrder);
                        }
                    }
                }
                m_disposed = true;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        ~Order()
        {
            Dispose(false);
        }
    }
}
