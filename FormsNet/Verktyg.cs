﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormsSkogma
{
    public class Verktyg
    {
        private static string mDecimalSeparator = "";

        /// <summary>
        /// Antal tecken från vänster
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="length">Längd från vänster</param>
        /// <returns></returns>
        public static string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        /// Antal tecken från höger
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="length">Längd från höger</param>
        /// <returns></returns>
        public static string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// Tecken i mitten
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="startIndex">Startposition</param>
        /// <param name="length">Antal tecken</param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        /// Från mitten till höger
        /// </summary>
        /// <param name="param">Text</param>
        /// <param name="startIndex">Startposition</param>
        /// <returns></returns>
        public static string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        /// <summary>
        /// Fyll ut blanktecken till höger
        /// </summary>
        /// <param name="param">text</param>
        /// <param name="length">antal tecken</param>
        /// <returns></returns>
        public static string fillBlankRight(string param, int length)
        {
            string result = param.PadRight(length, ' ');
            return result;
        }

        /// <summary>
        /// Fyll ut blanktecken till vänster
        /// </summary>
        /// <param name="param">text</param>
        /// <param name="length">antal tecken</param>
        /// <returns></returns>
        public static string fillBlankLeft(string param, int length)
        {
            string result = param.PadLeft(length, ' ');
            return result;
        }

        public static string getCurrentDecimalSeparator()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mDecimalSeparator))
                {
                    System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    mDecimalSeparator = ci.NumberFormat.CurrencyDecimalSeparator;
                }

                return mDecimalSeparator;
            }
            catch (Exception)
            {
                return mDecimalSeparator;
            }
        }

        public static int getIntFromStr(string value)
        {
            int result = 0;

            try
            {
                //if (!int.TryParse(value, out result))
                //{
                //    result = 0;
                //}
                if (getCurrentDecimalSeparator().Equals(","))
                    return Convert.ToInt32(value.Replace(".", ","));
                else
                    return Convert.ToInt32(value.Replace(",", "."));
            }
            catch (Exception)
            {
                return 0;
            }

            return result;
        }

        public static double getDoubleFromStr(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return 0;
                }

                if (getCurrentDecimalSeparator().Equals(","))
                    return Convert.ToDouble(value.Replace(".", ","));
                else
                    return Convert.ToDouble(value.Replace(",", "."));
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public static decimal GetDecimalFromStr(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return 0;
                }

                if (getCurrentDecimalSeparator().Equals(","))
                    return Convert.ToDecimal(value.Replace(".", ","));
                else
                    return Convert.ToDecimal(value.Replace(",", "."));
            }
            catch (Exception)
            {
                return 0;
            }

        }
    }
}
