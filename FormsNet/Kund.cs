﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace FormsSkogma
{
    public class Kund : IDisposable
    {

        private SettingsKund setting = null;
        Garp.Application GarpKund = null;
        Garp.IComponents GarpCompKund = null; // Skapa en pekare för componets för att undvika krock med annat formulärs forms

        Garp.Dataset dsKA;
        Garp.IComponent
            LabelVer
            , KundNr
            , KundNamn
            , PrivatKund
            , Form_OrderHuvud_AdressExtraLabel
            , Form_OrderHuvud_AdressExtraEdit
            , Form_Kund_TextMall
            , Form_Kund_Mall
            , Label_FirstName
            , Edit_FirstName
            ;
        
        Garp.ITable tblKA, tblKB;
        Garp.ITabField fld_AdressExtra, fld_KB_Kundnr, fld_KA_KundNamn, fld_KA_PrivatKund;

        Garp.IComponent iShopbutton;

        List<Customer> Customers = new List<Customer>();

        public bool isPrivatKundSet = false;
        bool blocked = false;
        bool m_disposed = false;
        string tmpEditFirstName = string.Empty;

        public Kund()
        {
            try
            {
                GarpKund = new Garp.Application();
                setting = new SettingsKund(GarpKund, Customers);

                GarpCompKund = GarpKund.Components;
                KundNr = GarpCompKund.Item("KnrEdit");
                KundNamn = GarpCompKund.Item("McEdit2");
                PrivatKund = GarpCompKund.Item("McEdit51");
                Form_OrderHuvud_AdressExtraLabel = GarpCompKund.Item("Ledtext10086");
                Form_OrderHuvud_AdressExtraEdit = GarpCompKund.Item("mceKbAD3");
                Form_Kund_TextMall = GarpCompKund.Item("mallkundLabel");
                Form_Kund_Mall = GarpCompKund.Item("mallKundEdit");

                // Visa versionsnummer
                GarpCompKund.BaseComponent = "Panel1";
                LabelVer = GarpCompKund.AddLabel("Version");
                LabelVer.Top = 10;
                LabelVer.Left = 470;
                LabelVer.Width = 100;
                LabelVer.Text = "Forms version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build;

                #region dölj och flytta fält

                Form_OrderHuvud_AdressExtraLabel.Visible = false;
                Form_OrderHuvud_AdressExtraEdit.Visible = false;

                GarpCompKund.BaseComponent = "Panel2";
                Label_FirstName = GarpCompKund.AddLabel("FirstName");
                Label_FirstName.Text = "Förnamn:";
                Label_FirstName.Top = 8; // 10
                Label_FirstName.Left = 460; // 530

                Edit_FirstName = GarpCompKund.AddEdit("EditFirstName");
                Edit_FirstName.Top = 5; // 7
                Edit_FirstName.Left = 511; // 610
                Edit_FirstName.Width = 200;
                Edit_FirstName.Height = 21;
                Edit_FirstName.MaxLength = 20;

                Form_Kund_TextMall.Left = 720;
                Form_Kund_Mall.Left = 750;

                #endregion dölj och flytta fält

                // Hämta datakällan för Kundtabell och skapa händelse för inläsning ny post
                dsKA = GarpKund.Datasets.Item("McDataSet1");
                dsKA.AfterScroll += AfterScroll;
                dsKA.BeforePost += BeforePost;
                dsKA.BeforeDelete += BeforeDelete;
                GarpKund.FieldExit += FieldExit;
                GarpKund.FieldEnter += FieldEnter;
                GarpKund.ButtonClick += ButtonClick;


                //Hämta tabeller
                tblKA = GarpKund.Tables.Item("KA");
                tblKB = GarpKund.Tables.Item("KB");

                fld_AdressExtra = tblKB.Fields.Item("AD3");
                fld_KB_Kundnr = tblKB.Fields.Item("KNR");
                fld_KA_KundNamn = tblKA.Fields.Item("NAM");
                fld_KA_PrivatKund = tblKA.Fields.Item("TX6");

                CreateLabelsAndButtons();
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning kund." + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #region Init

        private void CreateLabelsAndButtons()
        {
            try
            {
                GarpCompKund.BaseComponent = "TabSheet1";

                if (iShopbutton == null)
                {
                    iShopbutton = GarpCompKund.AddButton("iShopbutton");
                    iShopbutton.Top = GarpCompKund.Item("EORMcEdit").Top + 52;
                    iShopbutton.Left = GarpCompKund.Item("EORMcEdit").Left + 290;
                    iShopbutton.Width = GarpCompKund.Item("EORMcEdit").Width;
                    //iPIMbutton.Top = 205;
                    //iPIMbutton.Left = 355;
                    //iPIMbutton.Width = 70;
                    iShopbutton.Height = 20;
                    iShopbutton.Text = "Synka till iShop";
                    iShopbutton.TabStop = false;
                    iShopbutton.Visible = true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning kund - skapa knappar." + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        #endregion

        #region Events

        public void AfterScroll()
        {
            try
            {
                isPrivatKundSet = false;
           
                if (!string.IsNullOrEmpty(KundNr.Text) && tblKB.Find(dsKA.Fields.Item("KNR").Value)) // TODO: Hämta från datasetet för att få aktuellt värde
                {
                    Edit_FirstName.Text = fld_AdressExtra.Value;
                }

                blocked = false;

                //Test om kund får ändras
                foreach (Customer C in Customers)
                {
                    if (KundNr.Text == C.CustNr)
                    {
                        blocked = true;
                    }
                }

                //Dölj flikar för vald kund
                if (blocked == true && GarpKund.User != setting.UserCanUppdate)
                {
                    GarpCompKund.Item("TabSheet7").TabVisible = false; //Kontaktpersoner
                    GarpCompKund.Item("TabSheet8").TabVisible = false; //Ärenden
                    GarpCompKund.Item("TabSheet9").TabVisible = false; //Dokument/E-post
                    GarpCompKund.Item("TabSheet11").TabVisible = false; //EDI
                    GarpCompKund.Item("TabSheet12").TabVisible = false; //Begreppsök
                }
                //Visa flikarna för övriga kunder
                else
                {
                    GarpCompKund.Item("TabSheet7").TabVisible = true;
                    GarpCompKund.Item("TabSheet8").TabVisible = true;
                    GarpCompKund.Item("TabSheet9").TabVisible = true;
                    GarpCompKund.Item("TabSheet11").TabVisible = true;
                    GarpCompKund.Item("TabSheet12").TabVisible = true;
                }
            }

            catch (Exception e)
            {
                MessageBox.Show("Fel vid inläsning kund." + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void BeforePost()
        {
            blocked = false;
            isPrivatKundSet = false;
            //Test om kund får ändras
            foreach (Customer C in Customers)
            {
                if (KundNr.Text == C.CustNr)
                {
                    blocked = true;
                }
            }

           

            //Vald kund får ej redigeras
            if (blocked == true && GarpKund.User != setting.UserCanUppdate)
            {
                dsKA.Abort();
                MessageBox.Show("Denna kund får ej ändras", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void BeforeDelete()
        {
            blocked = false;

            //Test om kund får raderas
            foreach (Customer C in Customers)
            {
                if (KundNr.Text == C.CustNr)
                {
                    blocked = true;
                }
            }

            //Vald kund får ej raderas
            if (blocked == true && GarpKund.User != setting.UserCanUppdate)
            {
                dsKA.Abort();
                MessageBox.Show("Denna kund får ej raderas", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void FieldExit()
        {
            if (GarpCompKund.CurrentField == Edit_FirstName.Name && Edit_FirstName.Text != tmpEditFirstName)
            {
                if (!tblKB.Find(KundNr.Text))
                {
                    tblKB.Insert();
                    fld_KB_Kundnr.Value = KundNr.Text;
                }
                fld_AdressExtra.Value = Edit_FirstName.Text;
                tblKB.Post();
            }

            if (GarpCompKund.CurrentField == "knrEdit")
            {
                SetPrivateCustomer();
            }
        }

        public void FieldEnter()
        {
            if (GarpCompKund.CurrentField == Edit_FirstName.Name)
            {
                tmpEditFirstName = Edit_FirstName.Text;
            }

            if (GarpCompKund.CurrentField == "knrEdit")
            {

              //  Delayed(1000, () => SetPrivateCustomer());

            }
        }

        public void ButtonClick()
        {
            if(GarpCompKund.CurrentField == "actInsert")
            {
                isPrivatKundSet = false;

                Delayed(500, ()=>SetPrivateCustomer());
            }

            if (GarpCompKund.CurrentField == "iShopbutton")
            {
                SendCustomerToiShop();
            }
        }

        #endregion

        #region Functions

        public void SendCustomerToiShop()
        {
            try
            {
                string response = CalliShop();

                if (response.StartsWith("\""))
                {
                    response = response.Trim('"');
                }

                int resCode = getIntFromStr(response);

                if (resCode >= 200 && resCode <= 299)
                {
                    //MessageBox.Show("Synkning lyckad!", "iShop", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Synkning misslyckad!", "iShop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning kund - SendCustomerToiShop" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public string CalliShop()
        {
            string result = "ERROR";

            try
            {
                //string rest = "http://localhost:55565/iShopSvc/REST/SendSingleCustomer/0311";
                string baseAddress = ConfigurationManager.AppSettings["iShopBaseAddress"] ?? "";  //http://192.168.1.111:55565/iShopSvc
                string customerNo = KundNr.Text;
                string rest = baseAddress + "/REST/SendSingleCustomer/" + customerNo;

                WebRequest request = WebRequest.Create(rest);

                WebResponse ws = request.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                StreamReader responseStream = new StreamReader(ws.GetResponseStream());
                result = responseStream.ReadToEnd();
                responseStream.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning kund - CalliShop" + Environment.NewLine + Environment.NewLine
                + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
                , Application.ProductName + ", anpassning Forms "
                , MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return result;
        }

        public void SetPrivateCustomer()
        {
            try
            {
                //SendKeys.SendWait("{F5}");
                bool copiedCustomer = false;
                tblKA.Find(Form_Kund_Mall.Text);
                string mallName = Form_Kund_Mall.Text;
                mallName = mallName?.PadRight(10).Substring(0, 4).ToLower();
                if (mallName != "mall" && mallName?.Length > 1)
                {
                    copiedCustomer = true;
                    isPrivatKundSet = true;
                }
                int? nextCustomer = Int32.Parse(GarpKund.Counters.GetCustomerNo(false)) - 1;
                string nextCustomerString = nextCustomer.ToString();
                string trimmedKundNamn = "";
                trimmedKundNamn = KundNamn.Text + "      ";
                trimmedKundNamn = trimmedKundNamn?.Substring(0, 4).ToLower();
                string test = fld_KA_KundNamn.Value;
                if ((string.IsNullOrEmpty(GarpCompKund.Item("McEdit2").Text) || trimmedKundNamn == "mall") && !isPrivatKundSet)
                {
                    PrivatKund.Text = "X";

                    isPrivatKundSet = true;

                }
                else if (KundNr.Text == nextCustomerString && !isPrivatKundSet)
                {
                    PrivatKund.Text = "";
                    isPrivatKundSet = true;
                }
                else if (copiedCustomer)
                {
                    PrivatKund.Text = "";
                }

            }
            catch
            {

            }
        }

        public void Delayed(int delay, Action action)
        {
            Timer timer = new Timer();
            timer.Interval = delay;
            timer.Tick += (s, e) =>
            {
                action();
                timer.Stop();
            };
            timer.Start();
        }

        public int getIntFromStr(string value)
        {
            int result = 0;

            try
            {
                if (!int.TryParse(value, out result))
                {
                    result = 0;
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        #endregion


        // Stäng Garp-kopplingen
        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName + "Forms kund", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void Dispose(bool Disposing)
        {
            try
            {
                if (!m_disposed)
                {
                    if (Disposing)
                    {
                        m_disposed = true;
                        if (GarpKund != null)
                        {
                            GarpKund.ButtonClick -= ButtonClick;
                            GarpKund.FieldEnter -= FieldEnter;
                            GarpKund.FieldExit -= FieldExit;
                            dsKA.BeforeDelete -= BeforeDelete;
                            dsKA.BeforePost -= BeforePost;
                            dsKA.AfterScroll -= AfterScroll;

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblKA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblKB);

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsKA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(GarpKund);
                        }
                    }
                }
                
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName + "Forms kund", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        ~Kund()
        {
            Dispose(false);
        }
    }
}
