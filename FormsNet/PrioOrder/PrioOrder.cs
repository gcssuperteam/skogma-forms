﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;
using System.Configuration;
using System.Windows.Forms;

namespace PrioOrder
{
    public class PrioOrder
    {
        static Garp.Application GarpApp;
        static Garp.ITable tblOGR, tblOGA, tblOGC, tblIGR, tblIGA, tblAGA, tblAGL, tblKA;
        static LoginInfo settings;
        static List<string> stockNumbers;
        static List<string> uniqueRowStockNumbers;
        static List<string> articlesLists;
        static List<string> stockNoArticlesList;
        static List<string> articlesListPerStockNo;
        static List<OrderList> customerOrderList;
        static List<OrderList> purchaseList;
        static List<OrderList> allOrderList;
        static List<string> Orders;
        static List<OrderRow> orderRows;
        static List<OrderListPerItem> allOrderPerItem;
        static List<OrderListPerItem> TestDateCustomerOrderPerItem;
        static bool allOrder = false;

        //
        // TODO: Hantera paketartiklar
        //
        // Sätt orderhuvudet till datum för nästa leverans??
        //

        public PrioOrder(string[] args, Garp.Application garp)
        {

            try
            {
                settings = new LoginInfo();
                //WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Startar, version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor + Environment.NewLine);

                string itemFrom = string.Empty, itemTo = string.Empty;
                if (args == null || args.Length == 0)
                {
                    //WriteToLog("Endast nya order testas" + Environment.NewLine);
                }
                else if (args[0] == "allaOrder")
                {
                    allOrder = true;
                }
                else if (args.Length == 2)
                {
                    itemFrom = args[0];
                    itemTo = args[1];
                }

                GarpLogin(garp);
                bool find = FindArticlesAndOrders(itemFrom, itemTo);
                if (find)
                {
                    ResetOrders();
                    PriorityOrder();
                    CompareOrders();
                    if (allOrder)
                    {
                        OrderListPerItemListAfterPrio();
                    }
                }
                EndGarp();
                //RemoveOldLogFiles();
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel " + e.Message);
                //WriteToLog("Fel " + e.Message + Environment.NewLine);
                //WriteToLogError("Fel " + e.Message + Environment.NewLine);
            }
        }

        public static bool FindArticlesAndOrders(string itemFrom = "", string itemTo = "")
        {
            try
            {
                stockNumbers = settings.StockNo.Split(',').ToList();
                uniqueRowStockNumbers = new List<string>();
                articlesLists = new List<string>();
                stockNoArticlesList = new List<string>();
                Orders = new List<string>();
                List<OrderList> tmpOrders = new List<OrderList>();

                //WriteToLog("Hämtar artiklar och order med lagernummer " + settings.StockNo + " " + Environment.NewLine);

                // Endast olevererade orderrader
                tblOGR.IndexNo = 2;

                // TESTLÄGE
                //if (settings.ItemFrom != "" || settings.ItemTo.PadLeft(1) != "ö")
                //{
                //    tblOGR.Filter.AddField("ANR");
                //    tblOGR.Filter.AddConst(string.IsNullOrEmpty(itemFrom) ? settings.ItemFrom : itemFrom);
                //    tblOGR.Filter.AddConst(string.IsNullOrEmpty(itemTo) ? settings.ItemTo : itemTo);
                //    tblOGR.Filter.Expression = "1n2&1m3";
                //    tblOGR.Filter.Active = true;
                //}

                tblOGR.First();
                while (tblOGR.Eof != true)
                {
                    try
                    {
                        if (
                            (allOrder || (!allOrder && tblOGR.Fields.Item("PLF").Value == "1") || ((tblOGR.Fields.Item("PLF").Value == "Y" || tblOGR.Fields.Item("PLF").Value == "N") && tblOGR.Fields.Item("RES").Value == "2"))
                            && (stockNumbers.Contains(tblOGR.Fields.Item("LAG").Value) || string.IsNullOrEmpty(tblOGR.Fields.Item("LAG").Value))
                            && tblOGR.Fields.Item("RAT").Value != "9"
                            && tblOGR.Fields.Item("BRA").Value != "*"
                            && int.Parse(tblOGR.Fields.Item("RDC").Value) < 251
                            )
                        {
                            OrderList order = new OrderList()
                            {
                                OrderNr = tblOGR.Fields.Item("ONR").Value,
                                OrderRow = tblOGR.Fields.Item("RDC").Value
                            };
                            tmpOrders.Add(order);
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Fel vid läsning artikel " + tblOGR.Fields.Item("ANR").Value + Environment.NewLine + e.Message);
                        //WriteToLogError(Environment.NewLine + "Fel vid läsning artikel " + tblOGR.Fields.Item("ANR").Value + Environment.NewLine + e + Environment.NewLine + "Fortsätter... ");
                        //WriteToLog(Environment.NewLine + "Fel vid läsning artikel " + tblOGR.Fields.Item("ANR").Value + Environment.NewLine + e + Environment.NewLine + "Fortsätter... ");
                    }
                    tblOGR.Next();
                }

                //WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Olevererade rader " + tmpOrders.Count + Environment.NewLine);

                tmpOrders = tmpOrders.OrderBy(o => o.OrderNr).ToList();
                string tmpStockItem;

                foreach (var order in tmpOrders)
                {
                    tblOGR.IndexNo = 1;
                    try
                    {
                        tblOGA.Find(order.OrderNr);

                        if (tblOGA.Fields.Item("OTY").Value != "0"                  // Ej offert
                            && tblOGA.Fields.Item("WHO").Value != "000"             // EJ order som väntar hos användare 000 (förskott som blivit spärrat pga obetalt)
                            && tblOGA.Fields.Item("FRY").Value != "z"               // EJ fryst order
                            && tblOGA.Fields.Item("OBF").Value != "0"               // Order skall vara klar
                            && tblOGA.Fields.Item("X1F").Value != "1")              // Spärr om X1-flaggan är 1
                        {
                            tblOGR.Find(order.OrderNr + order.OrderRow);

                            if (tblOGA.Fields.Item("OTY").Value != "9" || (tblOGA.Fields.Item("OTY").Value == "9" && double.Parse(tblOGR.Fields.Item("ORA").Value.Replace(".", ",")) < 0))
                            {
                                if (tblAGA.Find(tblOGR.Fields.Item("ANR").Value) && int.Parse(tblAGA.Fields.Item("SUS").Value) < 5 && tblAGA.Fields.Item("TYP").Value != "z")
                                {
                                    // Spara unika lagernummer
                                    if (!uniqueRowStockNumbers.Contains(tblOGR.Fields.Item("LAG").Value))
                                    {
                                        uniqueRowStockNumbers.Add(tblOGR.Fields.Item("LAG").Value);
                                    }

                                    // Spara unika artikelnummer
                                    if (!articlesLists.Contains(tblOGR.Fields.Item("ANR").Value))
                                    {
                                        articlesLists.Add(tblOGR.Fields.Item("ANR").Value);
                                    }

                                    // Lägg till unika lagernr + artikelnr
                                    tmpStockItem = tblOGR.Fields.Item("LAG").Value + ";" + tblOGR.Fields.Item("ANR").Value;
                                    if (!stockNoArticlesList.Contains(tmpStockItem))
                                    {
                                        stockNoArticlesList.Add(tmpStockItem);
                                    }

                                    // Spara unika ordernummer
                                    if (!Orders.Contains(order.OrderNr) && tblAGA.Fields.Item("TYP").Value != "z")
                                    {
                                        Orders.Add(order.OrderNr);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Fel vid läsning order " + order.OrderNr + "-" + order.OrderRow + Environment.NewLine + e + Environment.NewLine + e.Message);
                        //WriteToLogError(Environment.NewLine + "Fel vid läsning order " + order.OrderNr + "-" + order.OrderRow + Environment.NewLine + e + Environment.NewLine + "Fortsätter... ");
                        //WriteToLog(Environment.NewLine + "Fel vid läsning order " + order.OrderNr + "-" + order.OrderRow + Environment.NewLine + e + Environment.NewLine + "Fortsätter... ");
                    }
                }

                Orders = Orders.OrderBy(o => o).ToList();
                articlesLists = articlesLists.OrderBy(o => o).ToList();
                tblOGR.Filter.Active = false;

                if (articlesLists.Count == 0 || Orders.Count == 0)
                {
                    //WriteToLogInfo("Lager " + settings.StockNo + ". Hittar inga artiklar eller order" + Environment.NewLine);
                    return false;
                }
                else
                {
                    //WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Läst in " + Orders.Count + " order " + Orders[0] + " - " + Orders[Orders.Count - 1] + Environment.NewLine
                    //+ articlesLists.Count + " artiklar " + articlesLists[0] + " - " + articlesLists[articlesLists.Count - 1] + Environment.NewLine
                    //);
                    return true;
                }
            }
            catch (Exception e)
            {
                //WriteToLogError("Fel vid läsning artiklar" + Environment.NewLine + e + Environment.NewLine);
                //WriteToLog("Fel vid läsning artiklar" + Environment.NewLine + e + Environment.NewLine);
                MessageBox.Show("Fel vid läsning artiklar" + Environment.NewLine + e.Message);
                return false;
            }
        }

        public static void ResetOrders()
        {
            try
            {
                bool update = false;
                string Today = DateTime.Now.ToString("yyMMdd");

                //WriteToLog("Nollställer order" + Environment.NewLine);

                foreach (var order in Orders)
                {
                    tblOGA.Find(order);
                    update = false;

                    try
                    {
                        // Test att kundens önskad leveranstid är ifylld
                        if (!tblOGC.Find(order))
                        {
                            tblOGC.Insert();
                            tblOGC.Fields.Item("ONR").Value = order;
                            tblOGC.Fields.Item("CH6").Value = tblOGA.Fields.Item("ODT").Value;
                            update = true;
                        }
                        else if (string.IsNullOrEmpty(tblOGC.Fields.Item("CH6").Value))
                        {
                            tblOGC.Fields.Item("CH6").Value = tblOGA.Fields.Item("ODT").Value;
                            update = true;
                        }

                        if (update)
                        {
                            tblOGC.Post();
                            //WriteToLogInfo("Order " + order + " är uppdaterad, saknade önskad leveranstid" + Environment.NewLine);
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("Fel vid test orderhuvud " + order + Environment.NewLine + e.Message);
                        //WriteToLogError("Fel vid test orderhuvud " + order + Environment.NewLine + e + " fortsätter..." + Environment.NewLine);
                        //WriteToLog("Fel vid test orderhuvud " + order + Environment.NewLine + e + " fortsätter..." + Environment.NewLine);
                    }

                    tblOGR.IndexNo = 1;
                    tblOGR.Find(order);
                    tblOGR.Next();
                    while (tblOGR.Eof != true && tblOGR.Fields.Item("ONR").Value == order)
                    {
                        try
                        {
                            if (int.Parse(tblOGR.Fields.Item("LVF").Value) < 5)
                            {

                                // Lägg till lagernummer 1 om det saknas
                                if (string.IsNullOrEmpty(tblOGR.Fields.Item("LAG").Value))
                                {
                                    tblOGR.Fields.Item("LAG").Value = "1";
                                    if (int.Parse(tblOGR.Fields.Item("RDC").Value) < 251)
                                    {
                                        //WriteToLogInfo("Orderrad " + tblOGR.Fields.Item("ONR").Value + "-" + tblOGR.Fields.Item("RDC").Value + " saknar lagernummer, lägger till 1!" + Environment.NewLine);
                                    }
                                    tblOGR.Post();
                                }

                                // Om rätt lagernummer
                                if (stockNumbers.Contains(tblOGR.Fields.Item("LAG").Value))
                                {
                                    // Test om leveransdatum saknas
                                    if (string.IsNullOrEmpty(tblOGR.Fields.Item("LDT").Value) && tblOGR.Fields.Item("RES").Value != settings.ResCodeLockedDeliveryTime)
                                    {
                                        tblOGR.Fields.Item("LDT").Value = Today;
                                        tblOGR.Post();
                                    }

                                    if (tblOGR.Fields.Item("PLF").Value != "Y")
                                    {
                                        if (
                                            !articlesLists.Contains(tblOGR.Fields.Item("ANR").Value)
                                            || int.Parse(tblOGR.Fields.Item("RDC").Value) > 250 // Kostnadsrader är alltid OK
                                            || int.Parse(tblOGR.Fields.Item("BLF").Value) > 0 // Produktionsplanerade eller delrapporterade är alltid OK
                                            )
                                        {
                                            tblOGR.Fields.Item("PLF").Value = "Y";
                                            tblOGR.Post();
                                            tblOGR.Next();
                                            continue;
                                        }
                                        else
                                        {
                                            // Börja om med PL-flaggan på kundorder och internorder för uttag
                                            // Om PL-flaggan redan är Y får den ligga kvar
                                            tblOGR.Fields.Item("PLF").Value = "1";
                                            tblOGR.Post();
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Fel vid nollställning orderrader " + order + " för artikel " + tblOGR.Fields.Item("ANR").Value + Environment.NewLine + e.Message);
                            //WriteToLogError("Fel vid nollställning orderrader " + order + " för artikel " + tblOGR.Fields.Item("ANR").Value + Environment.NewLine + e + " fortsätter..." + Environment.NewLine);
                        }

                        tblOGR.Next();
                    }
                }

                //WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Klar med nollställning order" + Environment.NewLine);
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel vid nollställning order " + Environment.NewLine + e.Message);
                //WriteToLogError("Fel vid nollställning order " + Environment.NewLine + e + Environment.NewLine);
                //WriteToLog("Fel vid nollställning order " + Environment.NewLine + e + Environment.NewLine);
            }
        }

        public static void PriorityOrder()
        {
            try
            {
                string Today = DateTime.Now.ToString("yyMMdd");

                allOrderPerItem = new List<OrderListPerItem>();
                TestDateCustomerOrderPerItem = new List<OrderListPerItem>();

                //WriteToLog("Skapar orderlistor i prioordning och testar mot saldon" + Environment.NewLine);



                // Loopa orderlista per artikel och lagernummer
                foreach (var uniqueStockNumber in uniqueRowStockNumbers)
                {
                    articlesListPerStockNo = new List<string>();
                    foreach (var item in stockNoArticlesList)
                    {
                        if (item.Split(';')[0] == uniqueStockNumber)
                        {
                            articlesListPerStockNo.Add(item.Split(';')[1]);
                        }
                    }

                    // Loopa listan med artiklar och hämta order
                    foreach (var item in articlesListPerStockNo)
                    {

                        //WriteToLogItem(item, "Hämtar orderlista för artikel " + item + " Lagernr: " + uniqueStockNumber + Environment.NewLine + Environment.NewLine);

                        bool PurchaseNeed = false;
                        string OrderType, OrderNr, DesiredDate;
                        double RestQty = 0;

                        customerOrderList = new List<OrderList>();
                        purchaseList = new List<OrderList>();
                        allOrderList = new List<OrderList>();

                        // Hämta alla olevererade orderrader för artikeln
                        tblOGR.IndexNo = 2;
                        tblOGR.Find(item);
                        tblOGR.Next();
                        while (tblOGR.Eof != true && tblOGR.Fields.Item("ANR").Value == item)
                        {
                            try
                            {
                                RestQty = double.Parse(tblOGR.Fields.Item("ORA").Value.Replace(".", ","));
                                OrderNr = tblOGR.Fields.Item("ONR").Value;
                                tblOGA.Find(OrderNr);
                                OrderType = tblOGA.Fields.Item("OTY").Value;

                                bool OrderToBring = false;

                                if (
                                    !allOrder
                                    && tblOGR.Fields.Item("LAG").Value == uniqueStockNumber
                                    && tblOGR.Fields.Item("RAT").Value != "9"
                                    && tblOGR.Fields.Item("BRA").Value != "*"
                                    && int.Parse(tblOGR.Fields.Item("RDC").Value) < 251
                                    && tblOGA.Fields.Item("WHO").Value != "000"             // EJ order som väntar hos användare 000 (förskott som blivit spärrat pga obetalt)
                                    && tblOGA.Fields.Item("FRY").Value != "z"               // EJ fryst order
                                    && OrderType != "0"
                                    && tblOGA.Fields.Item("X1F").Value != "1"               // Order skall vara klar
                                )
                                {
                                    OrderToBring = true;
                                }
                                else if (
                                    (Orders.Contains(OrderNr) || (OrderType == "9" && RestQty > 0))
                                    && tblOGR.Fields.Item("LAG").Value == uniqueStockNumber
                                    && tblOGR.Fields.Item("RAT").Value != "9"
                                    && tblOGR.Fields.Item("BRA").Value != "*"
                                    )
                                {
                                    OrderToBring = true;
                                }

                                if (OrderToBring)
                                {
                                    tblAGA.Find(tblOGR.Fields.Item("ANR").Value);

                                    string RowLevDate = tblOGR.Fields.Item("LDT").Value;

                                    // Hämta rest antal

                                    if (int.Parse(tblOGR.Fields.Item("LVF").Value) == 4) // Dellevererad
                                    {
                                        RestQty -= double.Parse(tblOGR.Fields.Item("TLA").Value.Replace(".", ","));
                                    }

                                    // Hämta info från aktuell orderrad och spara i kundorderlsita och lista med alla order

                                    // Kundorder plus Internorder för lageruttag
                                    if (OrderType != "9"
                                        || (OrderType == "9" && RestQty < 0))
                                    {
                                        // Hämta kundens önskade leveransdatum
                                        tblOGC.Find(OrderNr);
                                        DesiredDate = tblOGC.Fields.Item("CH6").Value;

                                        tblKA.Find(tblOGA.Fields.Item("KNR").Value);

                                        // Om önskad leveranstid är tidigare än idag
                                        if (string.Compare(DesiredDate, Today) < 0)
                                        {
                                            // Test om leverans/ hämtning idag eller VIP-kund för leverans idag
                                            if (
                                                settings.PickToday
                                                || (settings.PickVIPToday && !string.IsNullOrEmpty(tblKA.Fields.Item("KD5").Value))
                                                || (settings.PickUpToday && tblOGA.Fields.Item("LSE").Value == settings.DeliveryPickUp)
                                                )
                                            {
                                                DesiredDate = Today;
                                            }
                                            else
                                            {
                                                DesiredDate = GarpApp.DateCalculator.AddDays(Today, 1, -1);
                                            }
                                        }

                                        // Vänd tecken på internorder för uttag lagret
                                        if (OrderType == "9")
                                        {
                                            RestQty = 0 - RestQty;
                                        }

                                        // Fyll på listan med orderrader för aktuell artikel
                                        OrderList order = new OrderList
                                        {
                                            OrderNr = OrderNr,
                                            OrderRow = tblOGR.Fields.Item("RDC").Value,
                                            OrderAndRow = OrderNr + "-" + tblOGR.Fields.Item("RDC").Value,
                                            LevDate = RowLevDate,
                                            Qty = RestQty,
                                            OrderType = OrderType,
                                            RecordType = tblOGA.Fields.Item("OSE").Value, // Kundorder eller inköpsorder (K för kundorder)
                                            OrderDate = tblOGA.Fields.Item("ODT").Value,
                                            OrderTime = tblOGA.Fields.Item("OKL").Value,
                                            OrderSort = tblOGA.Fields.Item("ODT").Value + tblOGA.Fields.Item("OKL").Value + OrderNr + tblOGR.Fields.Item("RDC").Value.Replace(' ', '0'),
                                            tmpDesiredDate = DesiredDate,
                                            orgDesiredDate = tblOGC.Fields.Item("CH6").Value,
                                            RowOK = "N",
                                            NewLevDate = RowLevDate,
                                            X1flag = tblOGA.Fields.Item("X1F").Value,
                                            RowStockNo = tblOGR.Fields.Item("LAG").Value,
                                            BelFlag = tblOGR.Fields.Item("BLF").Value
                                        };
                                        customerOrderList.Add(order);
                                        if (allOrder)
                                        {
                                            allOrderList.Add(order);
                                        }
                                    }
                                    else
                                    // Internorder för inleverans läggs i listan av inköpsorderrader och lista med alla order
                                    {
                                        // Internordern är försenad, flytta till "imorgon"
                                        if (string.Compare(RowLevDate, Today) < 0)
                                        {
                                            RowLevDate = GarpApp.DateCalculator.AddDays(Today, 1, -1);
                                        }

                                        OrderList purchase = new OrderList
                                        {
                                            OrderNr = OrderNr,
                                            OrderRow = tblOGR.Fields.Item("RDC").Value,
                                            OrderAndRow = OrderNr + "-" + tblOGR.Fields.Item("RDC").Value,
                                            LevDate = RowLevDate,
                                            Qty = RestQty,
                                            OrderType = OrderType,
                                            RecordType = "I",                               // Kundorder eller inköpsorder (I för inköpsorder)
                                            OrderSort = RowLevDate + "0000" + OrderNr + tblOGR.Fields.Item("RDC").Value.Replace(' ', '0'),
                                            RowOK = "Y",
                                            orgDesiredDate = "",
                                            NewLevDate = RowLevDate,
                                            RowStockNo = tblOGR.Fields.Item("LAG").Value
                                        };
                                        purchaseList.Add(purchase);
                                        if (allOrder)
                                        {
                                            allOrderList.Add(purchase);
                                        }
                                    }
                                }
                                tblOGR.Next();
                            }
                            catch (Exception)
                            {
                                tblOGR.Next();
                            }
                        }

                        if (customerOrderList.Count > 0)
                        {
                            // Sortera om orderlistan enligt sortetingsfältet (orderdatumordning, tid, radnr mm)
                            customerOrderList = customerOrderList.OrderBy(o => o.OrderSort).ToList();

                            // Hämta inköpsorder för artikeln och fyll på inköpslistan och lista med alla order
                            tblIGR.IndexNo = 2;
                            tblIGR.Find(item);
                            tblIGR.Next();

                            while (tblIGR.Eof != true && tblIGR.Fields.Item("ANR").Value == item)
                            {
                                try
                                {
                                    if (tblIGR.Fields.Item("LAG").Value == uniqueStockNumber)
                                    {
                                        OrderNr = tblIGR.Fields.Item("ONR").Value;
                                        tblIGA.Find(OrderNr);

                                        OrderType = tblIGA.Fields.Item("OTY").Value;

                                        // EJ förfrågningar
                                        if (OrderType != "0")
                                        {
                                            string tmpLevDate = tblIGR.Fields.Item("LDT").Value;

                                            // Inköpsordern är försenad, flytta till "imorgon"
                                            if (string.Compare(tmpLevDate, Today) < 0)
                                            {
                                                tmpLevDate = GarpApp.DateCalculator.AddDays(Today, 1, -1);
                                            }
                                            string RowLevDate = tmpLevDate;

                                            // Hämta rest antal
                                            RestQty = double.Parse(tblIGR.Fields.Item("ORA").Value.Replace(".", ","));
                                            if (int.Parse(tblIGR.Fields.Item("LVF").Value) == 4) // Dellevererad
                                            {
                                                RestQty -= double.Parse(tblIGR.Fields.Item("TLA").Value.Replace(".", ","));
                                            }

                                            OrderList purchase = new OrderList
                                            {
                                                OrderNr = OrderNr,
                                                OrderRow = tblIGR.Fields.Item("RDC").Value,
                                                OrderAndRow = OrderNr + "-" + tblIGR.Fields.Item("RDC").Value,
                                                LevDate = tmpLevDate,
                                                Qty = RestQty,
                                                OrderType = OrderType,
                                                RecordType = tblIGA.Fields.Item("OSE").Value, // Kundorder eller inköpsorder (I för inköpsorder)
                                                OrderSort = tmpLevDate + "0000" + OrderNr + tblOGR.Fields.Item("RDC").Value.Replace(' ', '0'),
                                                RowOK = "Y",
                                                orgDesiredDate = "",
                                                NewLevDate = RowLevDate, // Alltid leveranstiden på inköpsorder
                                                RowStockNo = tblIGR.Fields.Item("LAG").Value
                                            };
                                            purchaseList.Add(purchase);
                                            if (allOrder)
                                            {
                                                allOrderList.Add(purchase);
                                            }
                                        }
                                    }
                                }
                                catch (Exception)
                                { }
                                tblIGR.Next();
                            }

                            // Sortera om inköpslistan i leveransdatumordning
                            purchaseList = purchaseList.OrderBy(o => o.LevDate).ToList();


                            #region Presentation av orderlistor om alla order körs
                            if (allOrder)
                            {
                                // Inköpsorder plus internorder för inleverans
                                if (purchaseList.Count == 0)
                                {
                                    //WriteToLogItem(item, "Inga aktuella inköpsorder" + Environment.NewLine);
                                }
                                else
                                {
                                    //WriteToLogItem(item, "Inköpsorder i levdatumordning" + Environment.NewLine + "Order-rad    Antal Levdat" + Environment.NewLine);
                                    foreach (var purchase in purchaseList)
                                    {
                                        try
                                        {
                                            string tmpText = "";

                                            // Test om inköpet är försenat, "nya" datumet (imorgon) visas
                                            if (string.Compare(purchase.LevDate, purchase.NewLevDate) > 0)
                                            {
                                                tmpText = " <-- försenad inköpsorder, levdat: " + purchase.NewLevDate;
                                            }
                                            //WriteToLogItem(item,
                                            //    purchase.OrderAndRow.PadRight(11, ' ')
                                            //    + purchase.Qty.ToString().PadLeft(7, ' ') + " "
                                            //    + purchase.LevDate.PadRight(7, ' ')
                                            //    + tmpText
                                            //    + Environment.NewLine);

                                            if (tmpText != "")
                                            {
                                                //WriteToLogInfo("Inköpsorder " + purchase.OrderAndRow + ", artikel: " + item + ", är försenad, planerad levtid:  " + purchase.NewLevDate + Environment.NewLine);
                                            }
                                        }
                                        catch (Exception)
                                        { }
                                    }
                                }

                                // Kundorder plus internorder för utleverans
                                //WriteToLogItem(item, Environment.NewLine + "Orderstock i orderdatum" + Environment.NewLine + "Order-rad  Ant     Levdat  Orderdatum    önsk datum  X1-fl" + Environment.NewLine);
                                foreach (var orderSort in customerOrderList)
                                {
                                    try
                                    {
                                        //WriteToLogItem(item,
                                        //    orderSort.OrderAndRow.PadRight(12, ' ')
                                        //    + orderSort.Qty.ToString().PadRight(7, ' ')
                                        //    + orderSort.LevDate + "  "
                                        //    + orderSort.OrderDate + " "
                                        //    + orderSort.OrderTime + "   "
                                        //    + orderSort.orgDesiredDate + "      "
                                        //    + orderSort.X1flag
                                        //    + Environment.NewLine);

                                    }
                                    catch (Exception)
                                    { }
                                }
                            }
                            #endregion Presentation order

                            #region Saldotester

                            // Nuvarande lagersaldo och ledtid för artikel.
                            tblAGL.Find(item.PadRight(13, ' ') + uniqueStockNumber);
                            double StockBalance = double.Parse(tblAGL.Fields.Item("LGA").Value.Replace(".", ","));
                            double PlannedArticleBalance = StockBalance;
                            //double leadTime = Math.Round(Convert.ToDouble(tblAGA.Fields.Item("NLT").Value.Replace(".", ",")), 0);
                            _ = int.TryParse(tblAGA.Fields.Item("NLT").Value.Replace(".", ","), out int leadTime);
                            string leadDate = GarpApp.DateCalculator.AddDays(Today, leadTime, -1);
                            bool ProductionOrder = false;

                            //
                            // Test vilka order som kan skickas nu (Saldo finns)
                            //

                            // Testa om orderrader är produktionsplanerade
                            foreach (var order in customerOrderList)
                            {
                                try
                                {
                                    // Orderraden är produktionsplanerad men ej fullrapporterad
                                    if (int.Parse(order.BelFlag) > 0)
                                    {
                                        if (!ProductionOrder)
                                        {
                                            //WriteToLogItem(item, Environment.NewLine + "Produktionsplanerade kundorder: " + Environment.NewLine);
                                        }
                                        order.RowOK = "Y";
                                        ProductionOrder = true;
                                        //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " Y - Tillv. -" + Environment.NewLine);

                                        // Orderraden är producerad
                                        if (order.BelFlag == "5")
                                        {
                                            // Levtid är större än önskad levtid
                                            if (string.Compare(order.LevDate, order.tmpDesiredDate) > 0)
                                            {
                                                // Planerat datum flyttas till önskat datum (idag eller imorgon)
                                                order.NewLevDate = order.tmpDesiredDate;
                                            }

                                            tblOGA.Find(order.OrderNr);
                                            tblKA.Find(tblOGA.Fields.Item("KNR").Value);

                                            // EJ leverans idag
                                            if (
                                                !settings.PickToday
                                                && (!settings.PickVIPToday || string.IsNullOrEmpty(tblKA.Fields.Item("KD5").Value))
                                                && (!settings.PickUpToday || tblOGA.Fields.Item("LSE").Value != settings.DeliveryPickUp)
                                                )
                                            {
                                                order.NewLevDate = GarpApp.DateCalculator.AddDays(Today, 1, -1);

                                                // Aldrig leverans före önskat leveransdatum
                                                if (string.Compare(order.NewLevDate, order.tmpDesiredDate) < 0)
                                                {
                                                    order.NewLevDate = order.tmpDesiredDate;
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception)
                                { }
                            }

                            if (PlannedArticleBalance > 0)
                            {
                                //WriteToLogItem(item, Environment.NewLine + "Saldo: " + PlannedArticleBalance + Environment.NewLine + "Testar nuvarande saldo: " + PlannedArticleBalance + Environment.NewLine
                                //    + "Order-rad Ant ber saldo" + Environment.NewLine);

                                foreach (var order in customerOrderList)
                                {
                                    try
                                    {
                                        // Om raden är produktionsplanerad
                                        if (order.BelFlag != "0")
                                        {
                                            continue;
                                        }
                                        tblOGA.Find(order.OrderNr);

                                        // Testa om önskat leveransdatum ligger längre fram än
                                        // ledtiden. Gör den det ska det fiktiva lagersaldot INTE räknas ner
                                        // Om ovan villkor är sant behöver aldrig saldo för artikeln testas
                                        if (string.Compare(order.tmpDesiredDate, leadDate) >= 0)
                                        {
                                            order.RowOK = "Y";

                                            if (order.X1flag != "0")
                                            {
                                                //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " Y - Utanför ledtid -" + Environment.NewLine);
                                            }
                                            else
                                            {
                                                //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " N - Ny order och utanför ledtid -" + Environment.NewLine);
                                            }
                                        }
                                        //
                                        // Planerat saldo finns till aktuell order
                                        //
                                        else if (PlannedArticleBalance > 0)
                                        {
                                            if (order.Qty <= PlannedArticleBalance)
                                            {
                                                order.RowOK = "Y";

                                                PlannedArticleBalance -= order.Qty;

                                                // Levtid är större än önskad levtid
                                                if (string.Compare(order.LevDate, order.tmpDesiredDate) > 0)
                                                {
                                                    // Planerat datum flyttas till önskat datum (idag eller imorgon)
                                                    order.NewLevDate = order.tmpDesiredDate;
                                                }

                                                tblOGA.Find(order.OrderNr);
                                                tblKA.Find(tblOGA.Fields.Item("KNR").Value);

                                                // EJ leverans idag
                                                if (
                                                    !settings.PickToday
                                                    && (!settings.PickVIPToday || string.IsNullOrEmpty(tblKA.Fields.Item("KD5").Value))
                                                    && (!settings.PickUpToday || tblOGA.Fields.Item("LSE").Value != settings.DeliveryPickUp)
                                                    )
                                                {
                                                    order.NewLevDate = GarpApp.DateCalculator.AddDays(Today, 1, -1);

                                                    // Aldrig leverans före önskat leveransdatum
                                                    if (string.Compare(order.NewLevDate, order.tmpDesiredDate) < 0)
                                                    {
                                                        order.NewLevDate = order.tmpDesiredDate;
                                                    }
                                                }
                                                if (order.X1flag != "0")
                                                {
                                                    //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " Y" + " nytt saldo: " + PlannedArticleBalance + Environment.NewLine);
                                                }
                                                else
                                                {
                                                    //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " Y" + " nytt saldo: " + PlannedArticleBalance + "  - Ny order -" + Environment.NewLine);
                                                }

                                            }
                                            else
                                            {
                                                order.RowOK = "N";
                                                if (order.X1flag != "0")
                                                {
                                                    //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " N" + Environment.NewLine);
                                                }
                                                else
                                                {
                                                    //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " N" + "  - Ny order -" + Environment.NewLine);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    { }
                                }
                            }

                            //
                            // Loppa orderlistan lika många gånger som antalet inköpsorder
                            // Test vilka order som kan plockas efter respektiver inköpsorder
                            //
                            if (purchaseList.Count > 0)
                            {
                                for (int i = 0; i < purchaseList.Count; i++)
                                {
                                    PlannedArticleBalance += purchaseList[i].Qty;
                                    //WriteToLogItem(item, Environment.NewLine + "Testar saldo efter inköp " + purchaseList[i].OrderAndRow + " Saldo: " + PlannedArticleBalance + Environment.NewLine);

                                    string purchaseOrder = purchaseList[i].OrderAndRow;

                                    // Loopa alla order som inte fått ok (Y) för plockning
                                    foreach (var order in customerOrderList)
                                    {
                                        try
                                        {
                                            // Orderraden är produktionsplanerad
                                            if (order.BelFlag != "0")
                                            {
                                                continue;
                                            }

                                            // Testa om önskat leveransdatum ligger längre fram än
                                            // ledtiden. Gör den det ska det fiktiva lagersaldot INTE räknas ner
                                            // Om ovan villkor är sant behöver aldrig saldo för artikeln testas

                                            if (string.Compare(order.tmpDesiredDate, leadDate) >= 0)
                                            {
                                                order.RowOK = "Y";

                                                if (order.X1flag != "0")
                                                {
                                                    //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " Y - Utanför ledtid -" + Environment.NewLine);
                                                }
                                                else
                                                {
                                                    //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " Y - Ny order och utanför ledtid -" + Environment.NewLine);
                                                }
                                            }
                                            else if (order.RowOK != "Y" && PlannedArticleBalance > 0)
                                            {
                                                if (order.Qty <= PlannedArticleBalance)
                                                {
                                                    // Planerad levtid dagen efter aktuellt inköp
                                                    order.NewLevDate = purchaseList[i].LevDate;
                                                    order.NewLevDate = GarpApp.DateCalculator.AddDays(order.NewLevDate, 1, -1);

                                                    // Aldrig leverans före önskat leveransdatum
                                                    if (string.Compare(order.NewLevDate, order.tmpDesiredDate) < 0)
                                                    {
                                                        order.NewLevDate = order.tmpDesiredDate;
                                                    }

                                                    order.RowOK = "Y";
                                                    PlannedArticleBalance -= order.Qty;
                                                    if (order.X1flag != "0")
                                                    {
                                                        //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " Y" + " nytt saldo: " + PlannedArticleBalance + Environment.NewLine);
                                                    }
                                                    else
                                                    {
                                                        //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " Y" + " nytt saldo: " + PlannedArticleBalance + "  - Ny order -" + Environment.NewLine);
                                                    }

                                                }
                                                else
                                                {
                                                    order.RowOK = "N";
                                                    if (order.X1flag != "0")
                                                    {
                                                        //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " N" + Environment.NewLine);
                                                    }
                                                    else
                                                    {
                                                        //WriteToLogItem(item, order.OrderAndRow + "  -" + order.Qty + " N" + "  - Ny order -" + Environment.NewLine);
                                                    }
                                                    // break; // Loopar vidare för att se om någon annan order kan tömma lagret /Per N 211006
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        { }
                                    }
                                }
                            }
                            if (StockBalance <= 0 && purchaseList.Count == 0 && int.Parse(tblAGA.Fields.Item("SUS").Value) < 5 && !ProductionOrder)
                            {
                                //WriteToLogItem(item, "Saldo = 0, inga inköp" + Environment.NewLine);
                                //if (allOrder)
                                //{
                                //    WriteToLogInfo("Kundorder finns för artikel " + item + ", men inget saldo eller inköp" + Environment.NewLine);
                                //}
                            }
                            #endregion Tester saldon


                            #region Justera leveranstid på nekade orderrader till ledtiden

                            // Flytta levtid till ledtid + 1 dag
                            foreach (var order in customerOrderList)
                            {
                                try
                                {
                                    if (order.RowOK == "N")
                                    {
                                        order.NewLevDate = DateTime.Now.AddDays(leadTime).ToString("yyMMdd");
                                        order.NewLevDate = GarpApp.DateCalculator.AddDays(order.NewLevDate, 1, -1);

                                        // Aldrig leverans före önskat leveransdatum
                                        if (string.Compare(order.NewLevDate, order.tmpDesiredDate) < 0)
                                        {
                                            order.NewLevDate = order.tmpDesiredDate;
                                        }
                                    }
                                }
                                catch (Exception)
                                { }
                            }
                            #endregion Justera leveranstid


                            #region Presentation av resultatet

                            if (allOrder)
                            {

                                // Sortera om orderlistan i planerade leveransdatumordningen
                                allOrderList = allOrderList.OrderBy(o => o.NewLevDate).ToList();

                                double tmpQty = StockBalance;
                                string tmpRowOK = "inköp";
                                string tmpLevdat = "";

                                //WriteToLogItem(item, Environment.NewLine + "Omprioriterad framtid, startsaldo: " + tmpQty + Environment.NewLine + "Test om orderrader skall flyttas" + Environment.NewLine);
                                //WriteToLogItem(item, "Order      Typ    Antal  LevDat Önskat OK? Nytt datum  nytt saldo" + Environment.NewLine);

                                foreach (var order in allOrderList)
                                {
                                    try
                                    {
                                        tmpLevdat = "";

                                        // Kundorder, visa om rad är ok
                                        if (order.RecordType == "K")
                                        {
                                            tmpRowOK = "-" + order.RowOK + "-";

                                            // Visa förslag på nytt leveransdatum
                                            if (order.LevDate != order.NewLevDate)
                                            {
                                                tmpLevdat = order.NewLevDate;
                                            }

                                            // Minska saldot
                                            if (order.RowOK == "Y") // &&  Komplettera if-satsen så vi inte hamnar in i den om önskat leveransdatum ligger inom ledtiden
                                            {
                                                // Om leddtiden är kortare än önskat leveransdatum ska saldot inte dras av
                                                if (string.Compare(order.tmpDesiredDate, leadDate) <= 0) // Om önskat leveransdatum ligger inom ledtiden
                                                {
                                                    if (int.Parse(order.BelFlag) > 0)
                                                    {
                                                        tmpRowOK = "Tillv.";
                                                    }
                                                    else
                                                    {
                                                        tmpQty -= order.Qty;
                                                    }

                                                }
                                                else
                                                {
                                                    tmpRowOK = "Framtid";
                                                }
                                            }
                                            // Kan ej skickas inom ledtiden
                                            else
                                            {
                                                tmpRowOK = "-Leditd-";
                                                PurchaseNeed = true;
                                            }
                                        }
                                        // Inköpsorder
                                        else
                                        {
                                            tmpQty += order.Qty;
                                            order.tmpDesiredDate = order.LevDate;
                                            tmpRowOK = "inköp";

                                            // Fyll på totala orderlistan med inköpen per artikel
                                            OrderListPerItem OrderPerItem = new OrderListPerItem
                                            {
                                                OrderNr = order.OrderNr,
                                                OrderRow = order.OrderRow,
                                                ItemNo = item,
                                                ItemLeadTime = leadTime,
                                                OrderAndRow = order.OrderAndRow,
                                                LevDate = order.LevDate,
                                                Qty = order.Qty,
                                                OrderType = order.OrderType,
                                                RecordType = order.RecordType,
                                                tmpDesiredDate = order.LevDate,
                                                orgDesiredDate = "",
                                                RowOK = "Y",
                                                NewLevDate = order.LevDate,
                                                StockBalance = StockBalance,
                                                StockNo = order.RowStockNo
                                            };
                                            allOrderPerItem.Add(OrderPerItem);
                                        }

                                        //WriteToLogItem(item,
                                        //    order.OrderAndRow + " "
                                        //    + order.RecordType + "-"
                                        //    + order.OrderType + " "
                                        //    + order.Qty.ToString().PadLeft(8, ' ') + "  "
                                        //    + order.LevDate.PadRight(7, ' ')
                                        //    + order.orgDesiredDate.PadRight(7, ' ')
                                        //    + tmpRowOK.PadRight(8, ' ')
                                        //    + tmpLevdat.PadRight(10, ' ')
                                        //    + tmpQty.ToString().PadLeft(8, ' ')
                                        //    + Environment.NewLine);

                                    }
                                    catch (Exception)
                                    { }
                                }
                            }
                            tblOGR.IndexNo = 1;
                            foreach (var order in customerOrderList)
                            {
                                try
                                {
                                    // EJ fullevererade order
                                    if (tblOGR.Find(order.OrderNr + order.OrderRow) && int.Parse(tblOGR.Fields.Item("LVF").Value) < 5)
                                    {
                                        if (allOrder)
                                        {
                                            // Fyll på orderlista med kundorderrader per artikel för senare test om leveranstider blivit nya
                                            OrderListPerItem TestOrderPerItem = new OrderListPerItem
                                            {
                                                OrderNr = order.OrderNr,
                                                OrderRow = order.OrderRow,
                                                ItemNo = item,
                                                OrderAndRow = order.OrderAndRow,
                                                LevDate = order.LevDate,
                                                Qty = order.Qty,
                                                tmpDesiredDate = order.tmpDesiredDate,
                                                NewLevDate = order.NewLevDate
                                            };
                                            TestDateCustomerOrderPerItem.Add(TestOrderPerItem);

                                            //
                                            // Uppdatera PL-flaggan på raderna i Garp
                                            //
                                            if (tblOGR.Fields.Item("PLF").Value == "Y" && order.RowOK != "Y")
                                            {
                                                // Raden har varit OK sedan tidigare. Meddela att orderraden plötsligt inte är OK
                                                //WriteToLogItem(item, Environment.NewLine + "Order " + order.OrderAndRow + " -- Raden har varit OK tidigare! -- " + Environment.NewLine);
                                                //WriteToLogInfo("Artikel " + item + ", på order " + order.OrderAndRow + ", har varit OK tidigare. Kan ej levereras!" + Environment.NewLine);
                                            }
                                        }

                                        if (order.LevDate != order.NewLevDate)
                                        {
                                            order.LevDate = order.NewLevDate;

                                            // Uppdatera leveranstiden på orderrader i Garp
                                            if (tblOGR.Fields.Item("RES").Value != settings.ResCodeLockedDeliveryTime)
                                            {
                                                tblOGR.Fields.Item("LDT").Value = order.NewLevDate;
                                            }
                                        }

                                        // Leveranstiden håller, testa om ordern är försenad. Flytta fram den till idag eller imorgon
                                        else if (string.Compare(order.LevDate, Today) < 0)
                                        {
                                            if (tblOGR.Fields.Item("RES").Value != settings.ResCodeLockedDeliveryTime)
                                            {
                                                tblOGA.Find(order.OrderNr);
                                                tblKA.Find(tblOGA.Fields.Item("KNR").Value);

                                                if (
                                                    settings.PickToday
                                                    || (settings.PickVIPToday && !string.IsNullOrEmpty(tblKA.Fields.Item("KD5").Value))
                                                    || (settings.PickUpToday && tblOGA.Fields.Item("LSE").Value == settings.DeliveryPickUp)
                                                    )
                                                {
                                                    tblOGR.Fields.Item("LDT").Value = Today;
                                                }
                                                else
                                                {
                                                    tblOGR.Fields.Item("LDT").Value = GarpApp.DateCalculator.AddDays(Today, 1, -1);
                                                }
                                            }
                                        }

                                        try
                                        {
                                            // Uppdarera radens PL-flagga (Y/N) (och leveranstiden enling nya planeringen)
                                            tblOGR.Fields.Item("PLF").Value = order.RowOK;
                                            tblOGR.Post();
                                        }
                                        catch (Exception e)
                                        {
                                            //WriteToLogError("Fel vid uppdatering PL-flagga " + order.OrderAndRow + ", fortsätter......" + Environment.NewLine + e + Environment.NewLine);
                                        }

                                        if (allOrder)
                                        {
                                            // Fyll på totala orderlistan med kundorderrader per artikel
                                            OrderListPerItem OrderPerItem = new OrderListPerItem
                                            {
                                                OrderNr = order.OrderNr,
                                                OrderRow = order.OrderRow,
                                                ItemNo = item,
                                                ItemLeadTime = leadTime,
                                                OrderAndRow = order.OrderAndRow,
                                                LevDate = tblOGR.Fields.Item("LDT").Value,
                                                Qty = order.Qty,
                                                OrderType = order.OrderType,
                                                RecordType = order.RecordType,
                                                tmpDesiredDate = order.tmpDesiredDate,
                                                orgDesiredDate = order.orgDesiredDate,
                                                RowOK = tblOGR.Fields.Item("PLF").Value,
                                                NewLevDate = order.NewLevDate,
                                                StockNo = order.RowStockNo,
                                                StockBalance = StockBalance,
                                                BelFlag = tblOGR.Fields.Item("BLF").Value
                                            };
                                            allOrderPerItem.Add(OrderPerItem);
                                        }
                                    }
                                }
                                catch (Exception)
                                { }
                            }
                            #endregion Presentation av resultatet

                            if (PurchaseNeed && allOrder)
                            {
                                //WriteToLogInfo("Inköp saknas för artikel " + item + Environment.NewLine);
                            }
                            //WriteToLogItem(item, "---" + Environment.NewLine + Environment.NewLine);
                        }
                    }
                }
                //WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Klar med prioritering order per artikel" + Environment.NewLine);
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel vid prioritering order per artikel " + Environment.NewLine + e.Message);
                //WriteToLogError("Fel vid prioritering order per artikel " + Environment.NewLine + e + Environment.NewLine);
                //WriteToLog("Fel vid prioritering order per artikel " + Environment.NewLine + e + Environment.NewLine);
            }
        }

        public static void CompareOrders()
        {
            try
            {
                if (Orders.Count > 0)
                {
                    string Today = DateTime.Now.ToString("yyMMdd");

                    //WriteToLog("Hämtar order " + Orders[0] + " - " + Orders[Orders.Count - 1] + " för test av orderns leveranstid" + Environment.NewLine);

                    foreach (var order in Orders)
                    {
                        try
                        {
                            //WriteToLogOrder(order, DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + Environment.NewLine);

                            orderRows = new List<OrderRow>();
                            bool allRowOK = true;
                            bool anyRowOK = false;
                            string lastLevDate = "";
                            string firstLevDate = "999999";
                            //string RowLevTime = null;
                            double RestQty = 0;
                            string DesiredDate = Today;
                            bool updateLevTime = false;

                            // Hämta orderhuvudet för aktuell order
                            tblOGA.Find(order);

                            // Hämta kundens önskad leveranstid
                            tblOGC.Find(order);
                            DesiredDate = tblOGC.Fields.Item("CH6").Value;

                            // Uppdatera orderns reservationskod om den är dellevererad
                            if (int.Parse(tblOGA.Fields.Item("LEF").Value) == 4 && string.IsNullOrEmpty(tblOGA.Fields.Item("RES").Value))
                            {
                                tblOGA.Fields.Item("RES").Value = "1";
                                tblOGA.Post();
                            }


                            // Hämta raderna för aktuell order
                            tblOGR.IndexNo = 1;
                            tblOGR.Find(order);
                            tblOGR.Next();

                            //
                            // Nedan ändrat 220124 / Per N
                            // Lagt om looparna för test av leveransdatumen och om någon/ alla rader kan skickas
                            //

                            // Test om någon rad kan plockas
                            while (tblOGR.Eof != true && tblOGR.Fields.Item("ONR").Value == order && int.Parse(tblOGR.Fields.Item("RDC").Value) < 251)
                            {
                                try
                                {
                                    if (
                                        int.Parse(tblOGR.Fields.Item("LVF").Value) < 5
                                        && stockNumbers.Contains(tblOGR.Fields.Item("LAG").Value)  // Rader med valt lagernummer
                                        && articlesLists.Contains(tblOGR.Fields.Item("ANR").Value) // Rader med artiklar enligt selektering
                                        )
                                    {
                                        // Om någon rad EJ kan plcokas
                                        if (tblOGR.Fields.Item("PLF").Value == "N")
                                        {
                                            allRowOK = false;
                                        }

                                        // Rad kan plockas
                                        if (tblOGR.Fields.Item("PLF").Value == "Y")
                                        {
                                            anyRowOK = true;
                                        }
                                    }
                                }
                                catch (Exception)
                                { }
                                tblOGR.Next();
                            }


                            tblOGR.Find(order);
                            tblOGR.Next();

                            // Hämta första och sista leveransdatum
                            string res2firstLevDate = "", res2lastLevdate = "";
                            while (tblOGR.Eof != true && tblOGR.Fields.Item("ONR").Value == order && int.Parse(tblOGR.Fields.Item("RDC").Value) < 251)
                            {
                                try
                                {
                                    if (int.Parse(tblOGR.Fields.Item("LVF").Value) < 5
                                        && !string.IsNullOrEmpty(tblOGR.Fields.Item("ANR").Value)
                                        && tblOGR.Fields.Item("RAT").Value != "9"
                                        && tblOGR.Fields.Item("BRA").Value != "*"
                                        && tblAGA.Find(tblOGR.Fields.Item("ANR").Value)
                                        && int.Parse(tblAGA.Fields.Item("SUS").Value) < 5
                                        && tblAGA.Fields.Item("TYP").Value != "z"
                                        //&& tblOGR.Fields.Item("RES").Value != settings.ResCodeLockedDeliveryTime
                                        )
                                    {
                                        // Spara sämsta leveransdatumet
                                        if (string.Compare(tblOGR.Fields.Item("LDT").Value, lastLevDate) > 0)
                                        {
                                            lastLevDate = tblOGR.Fields.Item("LDT").Value;
                                        }

                                        // Spara första leveranstid
                                        if (string.Compare(tblOGR.Fields.Item("LDT").Value, firstLevDate) < 0)
                                        {
                                            firstLevDate = tblOGR.Fields.Item("LDT").Value;
                                        }

                                        if (tblOGR.Fields.Item("RES").Value == "2")
                                        {
                                            if (string.IsNullOrEmpty(res2firstLevDate) || string.Compare(tblOGR.Fields.Item("LDT").Value, res2firstLevDate) < 0)
                                            {
                                                res2firstLevDate = tblOGR.Fields.Item("LDT").Value;
                                            }

                                            if (string.IsNullOrEmpty(res2lastLevdate) || string.Compare(tblOGR.Fields.Item("LDT").Value, res2lastLevdate) > 0)
                                            {
                                                res2lastLevdate = tblOGR.Fields.Item("LDT").Value;
                                            }
                                        }
                                    }
                                }
                                catch (Exception)
                                { }
                                tblOGR.Next();
                            }



                            // Order med resevationskod 1 får alla rader sämsta leveranstid
                            //string res2firstLevDate = firstLevDate;
                            if (tblOGA.Fields.Item("RES").Value == "1")
                            {
                                firstLevDate = lastLevDate;
                            }

                            // Flytta ev. gamla datum till idag
                            if (string.Compare(firstLevDate, Today) < 0)
                            {
                                firstLevDate = Today;
                            }

                            if (string.Compare(lastLevDate, Today) < 0)
                            {
                                lastLevDate = Today;
                            }

                            // Flytta ev. gamla datum till kundens önskade datum
                            if (string.Compare(firstLevDate, DesiredDate) < 0)
                            {
                                firstLevDate = DesiredDate;
                            }

                            if (!string.IsNullOrEmpty(res2firstLevDate))
                            {
                                if (string.Compare(firstLevDate, res2firstLevDate) > 0)
                                {
                                    firstLevDate = res2firstLevDate;
                                }
                            }

                            if (string.Compare(lastLevDate, DesiredDate) < 0)
                            {
                                lastLevDate = DesiredDate;
                            }

                            //WriteToLogOrder(order,
                            //    "Order " + order
                            //    + ", reskod:" + tblOGA.Fields.Item("RES").Value
                            //    + ", första levdatum " + firstLevDate + " , sista levdatum " + lastLevDate + Environment.NewLine
                            //    + "Lista rader och eventuella justeringar" + Environment.NewLine
                            //    + "Order      Levdat     Antal LgrNr Res PL Artikel" + Environment.NewLine);

                            tblOGR.Find(order);
                            tblOGR.Next();

                            // Uppdatera orderradernas leveranstid
                            while (tblOGR.Eof != true && tblOGR.Fields.Item("ONR").Value == order && int.Parse(tblOGR.Fields.Item("RDC").Value) < 251)
                            {
                                try
                                {
                                    if (
                                        int.Parse(tblOGR.Fields.Item("LVF").Value) < 5
                                        && !string.IsNullOrEmpty(tblOGR.Fields.Item("ANR").Value)
                                        && tblOGR.Fields.Item("RAT").Value != "9"
                                        && tblOGR.Fields.Item("BRA").Value != "*"
                                        && tblAGA.Find(tblOGR.Fields.Item("ANR").Value)
                                        && int.Parse(tblAGA.Fields.Item("SUS").Value) < 5
                                        && tblAGA.Fields.Item("TYP").Value != "z"
                                        )
                                    {
                                        // Hämta rest antal
                                        RestQty = double.Parse(tblOGR.Fields.Item("ORA").Value.Replace(".", ","));
                                        if (int.Parse(tblOGR.Fields.Item("LVF").Value) == 4) // Dellevererad
                                        {
                                            RestQty -= double.Parse(tblOGR.Fields.Item("TLA").Value.Replace(".", ","));
                                        }

                                        //WriteToLogOrder(order,
                                        //    order + "-"
                                        //   + tblOGR.Fields.Item("RDC").Value + " "
                                        //   + tblOGR.Fields.Item("LDT").Value + " "
                                        //   + RestQty.ToString().PadLeft(9, ' ') + " "
                                        //   + tblOGR.Fields.Item("LAG").Value.PadLeft(4, ' ') + "   "
                                        //   + tblOGR.Fields.Item("RES").Value + "  "
                                        //   + tblOGR.Fields.Item("PLF").Value + "  "
                                        //   + tblOGR.Fields.Item("ANR").Value.PadRight(13, ' ')
                                        //);

                                        //if (tblOGR.Fields.Item("RES").Value != settings.ResCodeLockedDeliveryTime)
                                        //{
                                        if (tblOGR.Fields.Item("RES").Value == settings.ResCodeLockedDeliveryTime)
                                        {
                                            //WriteToLogOrder(order, " - Blockerad rad");
                                        }
                                        else if (tblOGR.Fields.Item("RES").Value == "2")
                                        {
                                            // Orderrader med Reskod 2

                                            if (tblOGR.Fields.Item("LDT").Value != res2lastLevdate)
                                            {
                                                //WriteToLogOrder(order, " --> " + tblOGR.Fields.Item("LDT").Value);
                                                tblOGR.Fields.Item("LDT").Value = res2lastLevdate;
                                                updateLevTime = true;
                                            }
                                            else
                                            {
                                                //WriteToLogOrder(order, tblOGR.Fields.Item("LDT").Value);
                                            }

                                            //if (tblOGR.Fields.Item("LDT").Value != res2firstLevDate && tblOGR.Fields.Item("LDT").Value != lastLevDate)
                                            //{
                                            //    WriteToLogOrder(order, " - Egen leverans " + tblOGR.Fields.Item("LDT").Value);
                                            //}
                                        }
                                        else // Alla andra RES-koder
                                        {
                                            // Om orderradens leveranstid ligger före första leveranstid flyttas den fram till första leveranstid
                                            if (string.Compare(tblOGR.Fields.Item("LDT").Value, firstLevDate) < 0)
                                            {
                                                //WriteToLogOrder(order, " --> " + firstLevDate);
                                                tblOGR.Fields.Item("LDT").Value = firstLevDate;
                                                updateLevTime = true;
                                            }

                                            //
                                            // Om orderradens leveranstid ligger efter första leveranstid flyttas den fram till sista leveranstid
                                            //
                                            if (tblOGA.Fields.Item("RES").Value == "1" &&
                                                //string.Compare(tblOGR.Fields.Item("LDT").Value, firstLevDate) > 0 && 
                                                tblOGR.Fields.Item("LDT").Value != lastLevDate)
                                            {
                                                //WriteToLogOrder(order, " --> " + lastLevDate);
                                                tblOGR.Fields.Item("LDT").Value = lastLevDate;
                                                updateLevTime = true;
                                            }

                                            if (updateLevTime)
                                            {
                                                tblOGR.Post();
                                            }
                                        }
                                        //}
                                        //WriteToLogOrder(order, Environment.NewLine);
                                    }
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show("Fel vid uppdatering leveranstid på, orderad " + order + "-" + tblOGR.Fields.Item("RDC").Value + Environment.NewLine + e.Message);
                                    //WriteToLogError("Fel vid uppdatering leveranstid på, orderad " + order + "-" + tblOGR.Fields.Item("RDC").Value + Environment.NewLine + e + Environment.NewLine);
                                    //WriteToLogOrder(order, "Fel vid uppdatering leveranstid på, orderad " + order + "-" + tblOGR.Fields.Item("RDC").Value + Environment.NewLine + e + Environment.NewLine);
                                }
                                tblOGR.Next();
                            }

                            //
                            // Ovan ändrat 220124 / Per N
                            // Lagt om looparna för test av leveransdatumen och om någon/ alla rader kan skickas
                            //

                            #region Nedan borttaget 220124 Per N
                            /*
                            while (tblOGR.Eof != true && tblOGR.Fields.Item("ONR").Value == order && int.Parse(tblOGR.Fields.Item("RDC").Value) < 251)
                            {
                                try
                                {
                                    // EJ fullevererade rader och rader som har rätt lagernummer
                                    if (
                                        int.Parse(tblOGR.Fields.Item("LVF").Value) < 5
                                        // && stockNumbers.Contains(tblOGR.Fields.Item("LAG").Value)  // Alla rader skall testas tillsammans för gemensamma leveranstider / Per N 220121
                                        // && articlesLists.Contains(tblOGR.Fields.Item("ANR").Value) // Alla rader skall testas tillsammans för gemensamma leveranstider / Per N 220121
                                        )
                                    {
                                        RowLevTime = tblOGR.Fields.Item("LDT").Value;

                                        // Hämta rest antal
                                        RestQty = double.Parse(tblOGR.Fields.Item("ORA").Value.Replace(".", ","));
                                        if (int.Parse(tblOGR.Fields.Item("LVF").Value) == 4) // Dellevererad
                                        {
                                            RestQty -= double.Parse(tblOGR.Fields.Item("TLA").Value.Replace(".", ","));
                                        }

                                        OrderRow orderrows = new OrderRow
                                        {
                                            OrderNr = tblOGR.Fields.Item("ONR").Value,
                                            OrderRowNr = tblOGR.Fields.Item("RDC").Value,
                                            LevDate = RowLevTime,
                                            Qty = RestQty,
                                            ResCode = tblOGR.Fields.Item("RES").Value,
                                            PLfl = tblOGR.Fields.Item("PLF").Value,
                                            ItemNo = tblOGR.Fields.Item("ANR").Value,
                                            StockNo = tblOGR.Fields.Item("LAG").Value
                                        };
                                        orderRows.Add(orderrows);

                                        // Om någon rad EJ kan plcokas
                                        if (tblOGR.Fields.Item("PLF").Value == "N")
                                        {
                                            allRowOK = false;
                                        }

                                        // Rad kan plockas
                                        if (tblOGR.Fields.Item("PLF").Value == "Y")
                                        {
                                            anyRowOK = true;
                                        }

                                        // Spara sämsta leveransdatumet
                                        if (string.Compare(RowLevTime, lastLevDate) > 0)
                                        {
                                            lastLevDate = RowLevTime;
                                        }

                                        // Spara första leveranstid
                                        if (string.Compare(RowLevTime, firstLevDate) < 0)
                                        {
                                            firstLevDate = RowLevTime;
                                        }
                                    }
                                }
                                catch (Exception)
                                { }
                                tblOGR.Next();
                            }

                            // Order med resevationskod 1 får alla rader sämsta leveranstid
                            string res2firstLevDate = firstLevDate;
                            if (tblOGA.Fields.Item("RES").Value == "1")
                            {
                                firstLevDate = lastLevDate;
                            }

                            // Flytta ev. gamla datum till idag
                            if (string.Compare(firstLevDate, Today) < 0)
                            {
                                firstLevDate = Today;
                            }

                            if (string.Compare(lastLevDate, Today) < 0)
                            {
                                lastLevDate = Today;
                            }

                            // Flytta ev. gamla datum till kundens önskade datum
                            if (string.Compare(firstLevDate, DesiredDate) < 0)
                            {
                                firstLevDate = DesiredDate;
                            }

                            if (string.Compare(lastLevDate, DesiredDate) < 0)
                            {
                                lastLevDate = DesiredDate;
                            }

                            WriteToLogOrder(order,
                                "Order " + order
                                + ", reskod:" + tblOGA.Fields.Item("RES").Value
                                + ", första levdatum " + firstLevDate + " , sista levdatum " + lastLevDate + Environment.NewLine
                                + "Lista rader och eventuella justeringar" + Environment.NewLine
                                + "Order      Levdat     Antal LgrNr Res PL Artikel" + Environment.NewLine);

                            foreach (var row in orderRows)
                            {
                                try
                                {
                                    WriteToLogOrder(row.OrderNr,
                                     row.OrderNr + "-"
                                    + row.OrderRowNr + " "
                                    + row.LevDate + " "
                                    + row.Qty.ToString().PadLeft(9, ' ') + " "
                                    + row.StockNo.PadLeft(4, ' ') + "   "
                                    + row.ResCode + "  "
                                    + row.PLfl + "  "
                                    + row.ItemNo.PadRight(13, ' ')
                                     );

                                    if (row.ResCode != settings.ResCodeLockedDeliveryTime)
                                    {
                                        if (row.ResCode != "2")
                                        {
                                            //
                                            // Om orderradens leveranstid ligger före första leveranstid flyttas den fram till första leveranstid
                                            //
                                            if (string.Compare(row.LevDate, firstLevDate) < 0)
                                            {
                                                tblOGR.Find(row.OrderNr + row.OrderRowNr);
                                                WriteToLogOrder(row.OrderNr, " --> " + firstLevDate);
                                                tblOGR.Fields.Item("LDT").Value = firstLevDate;
                                                updateLevTime = true;
                                            }

                                            //
                                            // Om orderradens leveranstid ligger efter första leveranstid flyttas den fram till sista leveranstid
                                            //
                                            if (tblOGA.Fields.Item("RES").Value != "1" && string.Compare(row.LevDate, firstLevDate) > 0 && row.LevDate != lastLevDate)
                                            {
                                                tblOGR.Find(row.OrderNr + row.OrderRowNr);
                                                WriteToLogOrder(row.OrderNr, " --> " + lastLevDate);
                                                tblOGR.Fields.Item("LDT").Value = lastLevDate;
                                                updateLevTime = true;
                                            }

                                            if (updateLevTime)
                                            {
                                                tblOGR.Post();
                                            }
                                        }
                                        else
                                        // Orderrader med Reskod 2
                                        {

                                            //if (RowLevTime != firstLevDate && RowLevTime != lastLevDate)
                                            //{
                                            //    WriteToLogOrder(order, " - Egen leverans " + RowLevTime);
                                            //}
                                            if (RowLevTime != res2firstLevDate && RowLevTime != lastLevDate)
                                            {
                                                WriteToLogOrder(order, " - Egen leverans " + RowLevTime);
                                            }
                                        }


                                    }
                                    WriteToLogOrder(row.OrderNr, Environment.NewLine);
                                }
                                catch (Exception e)
                                {
                                    WriteToLogError("Fel vid uppdatering leveranstid på, orderad " + order + "-" + row.OrderRowNr + Environment.NewLine + e + Environment.NewLine);
                                    WriteToLogOrder(order, "Fel vid uppdatering leveranstid på, orderad " + order + "-" + row.OrderRowNr + Environment.NewLine + e + Environment.NewLine);
                                }
                            }
                            */
                            #endregion Ova borttaget 220124 Per N

                            //WriteToLogOrder(order, Environment.NewLine + "X1-flaggan uppdateras till: ");

                            //
                            // X1-flaggan uppdateras med nytt värde
                            //
                            if (firstLevDate == Today && firstLevDate == lastLevDate && allRowOK)
                            {
                                // Hela ordern kan levereras idag
                                //WriteToLogOrder(order, settings.X1completeOrder + " - hela ordern kan levereras idag" + Environment.NewLine);
                                tblOGA.Fields.Item("X1F").Value = settings.X1completeOrder;
                            }
                            else if (
                                (firstLevDate == Today && firstLevDate != lastLevDate) ||
                                (firstLevDate == Today && (anyRowOK || !allRowOK)) ||
                                (res2lastLevdate == Today && (anyRowOK || !allRowOK))
                                )
                            {
                                // Delar av ordern kan levereras idag
                                //WriteToLogOrder(order, settings.X1partOrder + " - delar av ordern kan levereras idag" + Environment.NewLine);
                                tblOGA.Fields.Item("X1F").Value = settings.X1partOrder;
                            }
                            else if (!anyRowOK && !(firstLevDate == Today || res2firstLevDate == Today))
                            {
                                // Ingen leverans idag
                                //WriteToLogOrder(order, settings.X1noPick + " - ingen leverans idag" + Environment.NewLine);
                                tblOGA.Fields.Item("X1F").Value = settings.X1noPick;
                            }
                            else
                            {
                                // Ingen leverans idag
                                //WriteToLogOrder(order, settings.X1noPick + " - ingen leverans idag" + Environment.NewLine);
                                tblOGA.Fields.Item("X1F").Value = settings.X1noPick;
                            }
                            tblOGA.Post();

                            //WriteToLogOrder(order, Environment.NewLine + "----------------------------" + Environment.NewLine);


                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("Fel vid uppdatering order " + order + Environment.NewLine + e.Message);
                            //WriteToLogError("Fel vid uppdatering order " + order + Environment.NewLine + e + Environment.NewLine + "Fortsätter......" + Environment.NewLine);
                        }
                    }

                    //WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Klar med leveranstider per order" + Environment.NewLine);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel vid jämförelse order " + Environment.NewLine + e.Message);
                //WriteToLogError("Fel vid jämförelse order " + Environment.NewLine + e + Environment.NewLine);
                //WriteToLog("Fel vid jämförelse order " + Environment.NewLine + e + Environment.NewLine);
            }
        }

        public static void OrderListPerItemListAfterPrio()
        {
            try
            {
                tblOGR.IndexNo = 1;
                // Sortera om orderlistan i föreslagen leveransdatumordningen
                allOrderPerItem = allOrderPerItem.OrderBy(o => o.ItemNo).ThenBy(o => o.NewLevDate).ToList();

                //WriteToLog("Visar lista efter omprioriteringar per artikel" + Environment.NewLine);

                foreach (var uniqueStockNumber in uniqueRowStockNumbers)
                {
                    string tmpItem = "";
                    double tmpQty = 0;

                    // Loopa listan med artiklar och hämta order
                    foreach (OrderListPerItem orderPerItem in allOrderPerItem)
                    {
                        if (orderPerItem.StockNo == uniqueStockNumber)
                        {
                            if (orderPerItem.ItemNo != tmpItem)
                            {
                                // Skapa rubrik och utgångsläge för planerat framtida saldo
                                //WriteToLogItem(orderPerItem.ItemNo, Environment.NewLine + "Orderlista efter omprioritering, lagernummer: " + uniqueStockNumber + Environment.NewLine
                                //+ "Order      Typ    Antal  Förslag Önskat OK?     Levdat   nytt saldo LgrNr" + Environment.NewLine);

                                tmpQty = orderPerItem.StockBalance;
                                tmpItem = orderPerItem.ItemNo;
                            }

                            string tmpRowOK = "inköp";
                            string tmpLevdat = "";

                            // Kundorder, visa om rad är ok
                            if (orderPerItem.RecordType == "K")
                            {
                                tblOGR.Find(orderPerItem.OrderNr + orderPerItem.OrderRow);
                                tmpLevdat = tblOGR.Fields.Item("LDT").Value;

                                tmpRowOK = "-" + orderPerItem.RowOK + "-";

                                // Minska saldot
                                if (orderPerItem.RowOK == "Y") // && Komplettera if-satsen så vi inte hamnar in i den om önskat leveransdatum ligger utanför ledtiden
                                {
                                    string leadDate = GarpApp.DateCalculator.AddDays(DateTime.Now.ToString("yyMMdd"), orderPerItem.ItemLeadTime, -1);

                                    if (string.Compare(orderPerItem.orgDesiredDate, leadDate) <= 0) // Om önskat leveransdatum ligger inom ledtiden
                                    {
                                        if (int.Parse(orderPerItem.BelFlag) > 0)
                                        {
                                            tmpRowOK = "Tillv.";
                                        }
                                        else
                                        {
                                            tmpQty -= orderPerItem.Qty;
                                        }

                                    }
                                    else
                                    {
                                        tmpRowOK = "Framtid";
                                    }
                                }
                                else
                                {
                                    tmpRowOK = "Ledtid";
                                }
                            }
                            // Inköpsorder
                            else
                            {
                                tmpQty += orderPerItem.Qty;
                                tmpRowOK = "inköp";
                            }

                            try
                            {
                                //WriteToLogItem(orderPerItem.ItemNo,
                                //    orderPerItem.OrderAndRow + " "
                                //    + orderPerItem.RecordType + "-"
                                //    + orderPerItem.OrderType + " "
                                //    + orderPerItem.Qty.ToString().PadLeft(8, ' ') + "  "
                                //    + orderPerItem.LevDate.PadRight(8, ' ')
                                //    + orderPerItem.orgDesiredDate.PadRight(7, ' ')
                                //    + tmpRowOK.PadRight(8, ' ')
                                //    + tmpLevdat.PadRight(10, ' ')
                                //    + tmpQty.ToString().PadLeft(8, ' ') + " "
                                //    + orderPerItem.StockNo.PadLeft(6, ' ')
                                //    + Environment.NewLine);
                            }
                            catch (Exception)
                            { }
                        }
                    }

                    foreach (var order in TestDateCustomerOrderPerItem)
                    {
                        tblOGR.Find(order.OrderNr + order.OrderRow);

                        // Visa förslag på nytt leveransdatum efter ordertester
                        if (order.LevDate != tblOGR.Fields.Item("LDT").Value)
                        {
                            tblOGA.Find(order.OrderNr);
                            if ((!int.TryParse(tblOGA.Fields.Item("OBF").Value, out int OBflvalue) || OBflvalue > 3))
                            {
                                //WriteToLogInfo("Order " + order.OrderAndRow + " , artikel " + order.ItemNo + " orderbekräftad order har ny leveranstid efter omprioriteringar, " + order.LevDate + " ---> " + tblOGR.Fields.Item("LDT").Value + Environment.NewLine);
                            }
                        }
                    }
                }

                //WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Klar med lista efter omprioriteringar" + Environment.NewLine);
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel vid orderlista per artikel efter prioritering " + Environment.NewLine + e.Message);
                //WriteToLogError("Fel vid orderlista per artikel efter prioritering " + Environment.NewLine + e + Environment.NewLine);
                //WriteToLog("Fel vid orderlista per artikel efter prioritering " + Environment.NewLine + e + Environment.NewLine);
            }
        }

        private static void GarpLogin(Garp.Application garp = null)
        {
            try
            {
                GarpApp = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                if (garp == null)
                {
                    GarpApp = new Garp.Application();
                }
                else
                {
                    GarpApp = garp;
                }

                //Loggar in i Garp och sätter bolag, användare och lösen
                //WriteToLog("Loggar in i Garp. ");
                //if (!string.IsNullOrEmpty(settings.GarpConfig))
                //{
                //    GarpApp.ChangeConfig(settings.GarpConfig);
                //}

                //if (settings.TraningMode == true)
                //{
                //    GarpApp.SwitchToTrainingMode();

                //    if (GarpApp.IsTrainingMode != true)
                //    {
                //        GarpApp = null;
                //        GC.Collect();
                //        GC.WaitForPendingFinalizers();
                //        WriteToLog("Kan ej logga in i Övningsbolaget!" + Environment.NewLine);

                //        Environment.Exit(0);
                //    }
                //    else
                //    {
                //        WriteToLog("ÖVNINGSBOLAGET! ");
                //    }
                //}

                //GarpApp.Login(settings.Usr, settings.Password);
                //GarpApp.SetBolag(settings.Bolag);

                //if (GarpApp.User == null)
                //{
                //    GarpApp = null;
                //    GC.Collect();
                //    GC.WaitForPendingFinalizers();
                //    WriteToLog("Kan ej logga in i Garp!" + Environment.NewLine);

                //    Environment.Exit(0);
                //}

                tblOGR = GarpApp.Tables.Item("OGR");
                tblOGA = GarpApp.Tables.Item("OGA");
                tblOGC = GarpApp.Tables.Item("OGC");
                tblIGR = GarpApp.Tables.Item("IGR");
                tblIGA = GarpApp.Tables.Item("IGA");
                tblAGA = GarpApp.Tables.Item("AGA");
                tblAGL = GarpApp.Tables.Item("AGL");
                tblKA = GarpApp.Tables.Item("KA");

                //WriteToLog("Inloggad som " + GarpApp.User + ", terminal " + GarpApp.Terminal + " och bolag " + GarpApp.Bolag + Environment.NewLine);
            }
            catch (Exception e)
            {
                //GarpApp = null;
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
                MessageBox.Show("Kan ej logga in i Garp!" + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
                //WriteToLog("Kan ej logga in i Garp!" + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
                Environment.Exit(0);
            }
        }

        private static void EndGarp()
        {
            //Stäng Garp
            tblOGR = null;
            tblOGA = null;
            tblOGC = null;
            tblIGR = null;
            tblIGA = null;
            tblAGA = null;
            tblAGL = null;
            tblKA = null;
            //GarpApp = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            //WriteToLog("Stänger Garp" + Environment.NewLine);
        }

        //private static void WriteToLog(string text)
        //{
        //    try
        //    {
        //        //if (settings.SkrivLoggFil)
        //        //{
        //        //    File.AppendAllText(settings.LogFileName, text);
        //        //}
        //        //else
        //        //{
        //        //    MessageBox.Show(text);
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        WriteToLogError("Fel uppstod vid skrivning logfil " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //    }
        //}

        //private static void WriteToLogError(string text)
        //{
        //    try
        //    {
        //        //MessageBox.Show(settings.SkrivLoggFil.ToString());
        //        //if (settings.SkrivLoggFil)
        //        //{
        //        //    if (!string.IsNullOrEmpty(settings.LogFileError))
        //        //    {
        //        //        File.AppendAllText(settings.LogFileError, text);
        //        //    }
        //        //}
        //        //else
        //        //{
        //            _ = MessageBox.Show(text ?? string.Empty, "Forms: ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show("Fel uppstod vid skrivning logfil för felmeddelanden " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //    }
        //}

        //private static void WriteToLogInfo(string text)
        //{
        //    try
        //    {
        //        //if (settings.SkrivLoggFil)
        //        //{
        //        //    if (!string.IsNullOrEmpty(settings.LogFileInfo))
        //        //    {
        //        //        File.AppendAllText(settings.LogFileInfo, text);
        //        //    }
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show("Fel uppstod vid skrivning logfil för felmeddelanden " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //    }
        //}

        //private static void WriteToLogItem(string itemNo, string text)
        //{
        //    try
        //    {
        //        //if (settings.SkrivLoggFil)
        //        //{
        //        //    string FileName = settings.LogFileItemPath + @"\" + itemNo + @"\" + settings.LogTime + ".txt";
        //        //    string Path = settings.LogFileItemPath + @"\" + itemNo;
        //        //    if (!Directory.Exists(Path))
        //        //    {
        //        //        Directory.CreateDirectory(Path);
        //        //    }

        //        //    File.AppendAllText(FileName, text);
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        if (settings.SkrivLoggFil)
        //        {
        //            WriteToLogError("Fel uppstod vid skrivning logfil för artikel " + itemNo + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //            WriteToLog("Fel uppstod vid skrivning logfil för artikel " + itemNo + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //        }
        //    }
        //}

        //private static void WriteToLogOrder(string orderNo, string text)
        //{
        //    try
        //    {
        //        //if (settings.SkrivLoggFil)
        //        //{
        //        //    string FileName = settings.LogFileOrderPath + @"\" + orderNo + ".txt";
        //        //    string Path = settings.LogFileOrderPath;

        //        //    if (!Directory.Exists(Path))
        //        //    {
        //        //        Directory.CreateDirectory(Path);
        //        //    }

        //        //    File.AppendAllText(FileName, text);
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        if (settings.SkrivLoggFil)
        //        {
        //            WriteToLogError("Fel uppstod vid skrivning logfil för order " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //            WriteToLog("Fel uppstod vid skrivning logfil för order " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //        }
        //    }
        //}

        //public static void RemoveOldLogFiles()
        //{
        //    try
        //    {
        //        if (settings.SkrivLoggFil)
        //        {

        //            //// Logfiler
        //            //WriteToLog("Raderar logfiler äldre än " + settings.MonthSaveLog + " månader" + Environment.NewLine);
        //            //string[] OldFilses = Directory.GetFiles(Path.GetDirectoryName(settings.LogFileName));
        //            //foreach (string file in OldFilses)
        //            //{
        //            //    FileInfo fileToDelete = new FileInfo(file);
        //            //    if (fileToDelete.CreationTime < DateTime.Now.AddMonths(0 - settings.MonthSaveLog))
        //            //        fileToDelete.Delete();
        //            //}

        //            //OldFilses = Directory.GetFiles(Path.GetDirectoryName(settings.LogFileError));
        //            //foreach (string file in OldFilses)
        //            //{
        //            //    FileInfo fileToDelete = new FileInfo(file);
        //            //    if (fileToDelete.CreationTime < DateTime.Now.AddMonths(0 - settings.MonthSaveLog))
        //            //        fileToDelete.Delete();
        //            //}

        //            //OldFilses = Directory.GetFiles(Path.GetDirectoryName(settings.LogFileInfo));
        //            //foreach (string file in OldFilses)
        //            //{
        //            //    FileInfo fileToDelete = new FileInfo(file);
        //            //    if (fileToDelete.CreationTime < DateTime.Now.AddMonths(0 - settings.MonthSaveLog))
        //            //        fileToDelete.Delete();
        //            //}

        //            //// Logg artiklar
        //            //DirectoryInfo di = new DirectoryInfo(settings.LogFileItemPath);
        //            //foreach (FileInfo file in di.EnumerateFiles())
        //            //{
        //            //    if (file.CreationTime < DateTime.Now.AddMonths(0 - settings.MonthSaveLog))
        //            //        file.Delete();
        //            //}
        //            //foreach (DirectoryInfo dir in di.EnumerateDirectories())
        //            //{
        //            //    if (dir.CreationTime < DateTime.Now.AddMonths(0 - settings.MonthSaveLog))
        //            //        dir.Delete(true);
        //            //}

        //            //// Logg order
        //            //OldFilses = Directory.GetFiles(settings.LogFileOrderPath);
        //            //foreach (string file in OldFilses)
        //            //{
        //            //    FileInfo fileToDelete = new FileInfo(file);
        //            //    if (fileToDelete.LastWriteTime < DateTime.Now.AddMonths(0 - settings.MonthSaveLog))
        //            //        fileToDelete.Delete();
        //            //}
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        WriteToLogError("Fel uppstod vid radering gamla logfiler " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //        WriteToLog("Fel uppstod vid radering gamla logfiler " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
        //    }
        //}
    }
}
