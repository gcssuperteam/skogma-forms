﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace PrioOrder
{
    public class LoginInfo
    {
        public string Bolag { get; set; }
        public string Usr { get; set; }
        public string Password { get; set; }
        public string GarpConfig { get; set; }
        public bool TraningMode { get; set; }
        public bool SkrivLoggFil { get; set; }
        public string LogFileName { get; set; }
        public string LogFileError { get; set; }
        public string LogFileInfo { get; set; }
        public string LogFileItemPath { get; set; }
        public string LogFileOrderPath { get; set; }
        public int MonthSaveLog { get; set; }
        public string ItemFrom { get; set; }
        public string ItemTo { get; set; }
        public string LogTime { get; set; }
        public string X1noCalc { get; set; }
        public string X1completeOrder { get; set; }
        public string X1partOrder { get; set; }
        public string X1noPick { get; set; }
        public bool PickToday { get; set; }
        public bool PickVIPToday { get; set; }
        public bool PickUpToday { get; set; }
        public string DeliveryPickUp { get; set; }
        public string StockNo { get; set; }
        public string ResCodeLockedDeliveryTime { get; set; }

        public LoginInfo()
        {
            Bolag = ConfigurationManager.AppSettings["Bolag"];
            Usr = ConfigurationManager.AppSettings["Användare"];
            Password = ConfigurationManager.AppSettings["Lösen"];
            GarpConfig = ConfigurationManager.AppSettings["Konfiguration"];
            if (ConfigurationManager.AppSettings["Övningsbolag"].ToLower() == "true")
            {
                TraningMode = true;
            }

            #region Logfiler

            if (bool.TryParse(ConfigurationManager.AppSettings["SkrivLog"], out bool skrivLoggFil))
            {
                SkrivLoggFil = skrivLoggFil;
            }

            //LogTime = DateTime.Now.ToString("yyyy-MM-dd HHmm");

            //// Logfil
            //LogFileName = string.Format(@"{0}"
            //, Path.GetDirectoryName(ConfigurationManager.AppSettings["FilFörLog"])
            //+ @"\"
            //+ Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["FilFörLog"])
            //+ " "
            //+ LogTime)
            //+ Path.GetExtension(ConfigurationManager.AppSettings["FilFörLog"]);
            //if (!Directory.Exists(Path.GetDirectoryName(ConfigurationManager.AppSettings["FilFörLog"])))
            //{
            //    Directory.CreateDirectory(Path.GetDirectoryName(ConfigurationManager.AppSettings["FilFörLog"]));
            //}

            //// Logfil med fel
            //LogFileError = string.Format(@"{0}"
            //, Path.GetDirectoryName(ConfigurationManager.AppSettings["LogError"])
            //+ @"\"
            //+ Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["LogError"])
            //+ " "
            //+ LogTime)
            //+ Path.GetExtension(ConfigurationManager.AppSettings["LogError"]);
            //if (!Directory.Exists(Path.GetDirectoryName(ConfigurationManager.AppSettings["LogError"])))
            //{
            //    Directory.CreateDirectory(Path.GetDirectoryName(ConfigurationManager.AppSettings["LogError"]));
            //}

            //// Logfil med extra info
            //LogFileInfo = string.Format(@"{0}"
            //, Path.GetDirectoryName(ConfigurationManager.AppSettings["LogExtraInfo"])
            //+ @"\"
            //+ Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["LogExtraInfo"])
            //+ " "
            //+ LogTime)
            //+ Path.GetExtension(ConfigurationManager.AppSettings["LogExtraInfo"]);
            //if (!Directory.Exists(Path.GetDirectoryName(ConfigurationManager.AppSettings["LogExtraInfo"])))
            //{
            //    Directory.CreateDirectory(Path.GetDirectoryName(ConfigurationManager.AppSettings["LogExtraInfo"]));
            //}

            //// Logfil per artikel
            //LogFileItemPath = string.Format(@"{0}", ConfigurationManager.AppSettings["SökvägLogArtikel"]);
            //if (!Directory.Exists(ConfigurationManager.AppSettings["SökvägLogArtikel"]))
            //{
            //    Directory.CreateDirectory(ConfigurationManager.AppSettings["SökvägLogArtikel"]);
            //}

            //// Logfil per order
            //LogFileOrderPath = string.Format(@"{0}", ConfigurationManager.AppSettings["SökvägLogOrder"]);
            //if (!Directory.Exists(ConfigurationManager.AppSettings["SökvägLogOrder"]))
            //{
            //    Directory.CreateDirectory(ConfigurationManager.AppSettings["SökvägLogOrder"]);
            //}

            //MonthSaveLog = int.Parse(ConfigurationManager.AppSettings["AntalMånaderSparaLog"]);

            #endregion Logfiler

            ItemFrom = ConfigurationManager.AppSettings["ArtikelFrom"];
            ItemTo = ConfigurationManager.AppSettings["ArtikelTom"];

            X1noCalc = ConfigurationManager.AppSettings["X1ejKlar"];
            X1completeOrder = ConfigurationManager.AppSettings["X1helPlockOrder"];
            X1partOrder = ConfigurationManager.AppSettings["X1delPlockOrder"];
            X1noPick = ConfigurationManager.AppSettings["X1ingetPlockOrder"];

            // Deadline passerat för leveranser idag
            if (string.Compare(DateTime.Now.ToString("HH:mm"), ConfigurationManager.AppSettings["TidPlockIdag"]) < 0)
            {
                PickToday = true;
            }

            // Deadline passerat leveranser idag för VIP-kunder
            if (string.Compare(DateTime.Now.ToString("HH:mm"), ConfigurationManager.AppSettings["TidPlockVIPIdag"]) < 0)
            {
                PickVIPToday = true;
            }

            // Deadline passerat för hämtning order idag
            if (string.Compare(DateTime.Now.ToString("HH:mm"), ConfigurationManager.AppSettings["TidHämtasIdag"]) < 0)
            {
                PickUpToday = true;
            }
            DeliveryPickUp = ConfigurationManager.AppSettings["LeveranssättHämtas"];

            StockNo = ConfigurationManager.AppSettings["LagerNr"];
            ResCodeLockedDeliveryTime = ConfigurationManager.AppSettings["ReskodLåstRad"];
        }
    }
}
