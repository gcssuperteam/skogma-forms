﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrioOrder
{
    public class OrderListPerItem
    {
        public string OrderNr { get; set; }
        public string OrderRow { get; set; }
        public string OrderAndRow { get; set; }
        public string ItemNo { get; set; }
        public int ItemLeadTime { get; set; }
        public string LevDate { get; set; }
        public double Qty { get; set; }
        public string tmpDesiredDate { get; set; }
        public string orgDesiredDate { get; set; }
        public string OrderType { get; set; }
        public string RecordType { get; set; }
        public string RowOK { get; set; }
        public string NewLevDate { get; set; }
        public double StockBalance  { get; set; }
        public string StockNo { get; set; }
        public string BelFlag { get; set; }
    }
}
