﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrioOrder
{
    public class OrderRow
    {
        public string OrderNr { get; set; }
        public string OrderRowNr { get; set; }
        public string LevDate { get; set; }
        public double Qty { get; set; }
        public string ResCode { get; set; }
        public string PLfl { get; set; }
        public string ItemNo { get; set; }
        public string StockNo { get; set; }
    }
}
