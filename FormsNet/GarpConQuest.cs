﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsSkogma
{
    public static class GarpConQuest
    {
        private static string mDecimalSeparator = "";
        public enum LogType
        {
            Error = 10,
            Debug = 5,
            Information = 0
        }

        public static void Logger(string text, LogType logType = LogType.Debug, bool consoleLog = false, string logFilePath = "c:\\temp\\ConQuestLogger.txt")
        {
            try
            {
                string logLevel = "";
                if (logType == LogType.Error)
                {
                    logLevel = "ERR";
                }
                else if (logType == LogType.Information)
                {
                    logLevel = "NFO";
                }
                string logText = $"{ DateTime.Now }[{ logLevel }]: { text }{ Environment.NewLine }";
                File.AppendAllText(logFilePath, logText);
                if (consoleLog)
                {
                    Console.WriteLine(logText);
                }
            }
            catch
            {
                _ = MessageBox.Show("Couldn't log: " + text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static string GetStrFromDecimal(Nullable<decimal> value)
        {
            try
            {
                if (value.HasValue)
                    return Convert.ToString(value.Value).Replace(",", ".");
                else
                    return "0";
            }
            catch (Exception e)
            {
                Logger("Error in GarpConQuest's GetStrFromDecimal function: " + e.Message, LogType.Error);
                return "0";
            }
        }

        public static decimal GetDecimalFromStr(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return 0;
                }

                if (GetCurrentDecimalSeparator().Equals(","))
                    return Convert.ToDecimal(value.Replace(".", ","));
                else
                    return Convert.ToDecimal(value.Replace(",", "."));
            }
            catch (Exception e)
            {
                Logger("Error in GarpConQuest's GetDecimalFromStr function: " + e.Message, LogType.Error);
                return 0;
            }
        }

        public static string GetCurrentDecimalSeparator()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mDecimalSeparator))
                {
                    System.Globalization.CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
                    mDecimalSeparator = ci.NumberFormat.CurrencyDecimalSeparator;
                }

                return mDecimalSeparator;
            }
            catch (Exception e)
            {
                Logger("Error in GarpConQuest's GetCurrentDecimalSeparator function: " + e.Message, LogType.Error);
                return mDecimalSeparator;
            }
        }

        public static bool IsNumeric(string value)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return false;
                }

                if (GetCurrentDecimalSeparator().Equals(","))
                {
                    return decimal.TryParse(value.Replace(".", ","), out decimal tmp);
                }
                else
                {
                    return decimal.TryParse(value.Replace(",", "."), out decimal tmp);
                }
            }
            catch (Exception e)
            {
                Logger("Error in GarpConQuest's IsNumeric function:" + e.Message, LogType.Error);
                return false;
            }
        }

        /// <summary>
        /// A function which returns a substring. Handles substrings longer than the string.
        /// </summary>
        /// <param name="str">String to be divided</param>
        /// <param name="start">Start position. First position is 0</param>
        /// <param name="length">The length of the substring</param>
        /// <returns>Empty string if the start position is beyond the string and if the requested length is longer than the string then it returns from the indicated start position to the actual endf of the string, regardless of the requested length</returns>
        public static string Substring(string str, int start, int length)
        {
            try
            {
                string result = "";

                for (int i = start; i < str.Length && i < start + length; i++)
                {
                    result += str[i];
                }

                return result;
            }
            catch (Exception e)
            {
                Logger("Error in Substring: " + e.Message, LogType.Error);
                return null;
            }
        }
    }
}
