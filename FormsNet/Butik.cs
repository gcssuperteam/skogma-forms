﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsSkogma
{
    public class Butik : IDisposable
    {

        Garp.Application GarpButik = null;
        Garp.IComponents ComponentsButik = null;

        Garp.Dataset dsOGR;
        Garp.ITable tblOGR, tblAGA, tblAGT;

        Garp.ITabField
            fld_OGR_OrderNr
            , fld_OGR_RadNr
            , fld_OGR_ArtNr
            , fld_OGR_Antal
            , fld_OGR_LevTid
            , fld_OGR_Benamning

            , fld_AGA_Saldohanteras
            , fld_AGA_Ledtid
            , fld_AGA_Saldo
            ;

        Garp.IComponent Form_Butik_OrderNr;
        
        SettingsOrder order = new SettingsOrder();

        bool m_disposed = false;

        public Butik()
        {
            try
            {
                GarpButik = new Garp.Application();
                ComponentsButik = GarpButik.Components;

                // Hämta datakällan för OrderHuvud OrderRad
                dsOGR = GarpButik.Datasets.Item("ogrMcTable");

                // Skapa händelser
                dsOGR.AfterScroll += AfterScrollOGR;
                dsOGR.BeforePost += BeforePost;

                // Hämta tabeller
                tblOGR = GarpButik.Tables.Item("OGR");
                tblAGA = GarpButik.Tables.Item("AGA");
                tblAGT = GarpButik.Tables.Item("AGT");

                // Tabellfält
                fld_OGR_OrderNr = tblOGR.Fields.Item("ONR");
                fld_OGR_RadNr = tblOGR.Fields.Item("RDC");
                fld_OGR_ArtNr = tblOGR.Fields.Item("ANR");
                fld_OGR_Antal = tblOGR.Fields.Item("ORA");
                fld_OGR_LevTid = tblOGR.Fields.Item("LDT");
                fld_OGR_Benamning = tblOGR.Fields.Item("BEN");

                fld_AGA_Saldohanteras = tblAGA.Fields.Item("SUS");
                fld_AGA_Ledtid = tblAGA.Fields.Item("NLT");
                fld_AGA_Saldo = tblAGA.Fields.Item("SUS");

                // Bevakning av knappar och fält i formuläret
                Form_Butik_OrderNr = ComponentsButik.Item("DBEdit2");
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning för butiksmodulen initiering, " + e);
            }
        }

        public void AfterScrollOGR()
        {
            try
            {

            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning för butiksmodulen ny orderrad, " + e);
            }
        }

        public void BeforePost()
        {
            try
            {
                // Sök upp aktuell order och loopa all rader för saldotest
                tblOGR.Find(Form_Butik_OrderNr.Text);
                tblOGR.Next();
                while (tblOGR.Eof != true && fld_OGR_OrderNr.Value == Form_Butik_OrderNr.Text)
                {
                    // Lägg in idag som förvalt datum
                    if (fld_OGR_LevTid.Value == null)
                    {
                        fld_OGR_LevTid.Value = DateTime.Now.ToString("yyMMdd");
                        tblOGR.Post();
                    }
                    order.TestArtNr = fld_OGR_ArtNr.Value;
                    order.LevTidOR = fld_OGR_LevTid.Value;
                    order.NyttLevDatum = order.LevTidOR;
                    order.TextNewDelTime = string.Empty;
                    order.TextPurcase = string.Empty;
                    
                    Saldotest();

                    // Om förslag på nytt datum ligger längre fram än aktuellt datum
                    if (string.Compare(order.NyttLevDatum, order.LevTidOR) > 0)
                    {
                        // Uppdatera orderradens leveranstid
                        MessageBox.Show(
                            "Saldobrist rad " + fld_OGR_RadNr.Value + ", " + fld_OGR_ArtNr.Value + ", " + fld_OGR_Benamning.Value 
                            + Environment.NewLine + order.TextNewDelTime
                            , System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information
                            );

                        fld_OGR_LevTid.Value = order.NyttLevDatum;
                        tblOGR.Post();
                    }
                    tblOGR.Next();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Fel i anpassning för butiksmodulen spara orderrad, " + e);
            }

        }

        public string Saldotest()
        {
            try
            {
                //Hämta ledtid och saldo från artikeln
                if (tblAGA.Find(order.TestArtNr))
                {
                    if (string.Compare(fld_AGA_Saldohanteras.Value, "5") < 0)
                    {
                        // Spara leveranstid orderrad, konvertera radantalet till double
                        string antal = fld_OGR_Antal.Value.Replace(".", ",");
                        double RadAntal = Convert.ToDouble(antal);

                        int Ledtid = Int32.Parse(fld_AGA_Ledtid.Value);
                        order.DateLedtidArtikel = DateTime.Now.AddDays(Ledtid).ToString("yyMMdd");

                        //Testa disponibelt om leveranstiden är inom ledtiden och antal > 0
                        if ((string.Compare(order.LevTidOR, order.DateLedtidArtikel) < 0 || order.LevTidOR == order.DateLedtidArtikel) && RadAntal > 0)
                        {
                            //order.DatePurcase = string.Empty;
                            order.Saldo = fld_AGA_Saldo.Value.Replace(".", ",");
                            order.SaldoNu = Convert.ToDouble(order.Saldo);
                            //setting.FindInkOrder = false;

                            //Kalkylera åtgångar inom ledtiden
                            Garp.ICalcAvailable saldo = GarpButik.CalcAvailable;
                            saldo.Artnr = order.TestArtNr;
                            saldo.DateFrom = "010101";
                            saldo.DateTo = order.DateLedtidArtikel;
                            saldo.Func = "R"; // Endast åtgångar
                            saldo.Calculate();

                            //Alla åtgångar summerade inom ledtid + saldo nu
                            order.Available = saldo.TabGetAvailable(saldo.IntervalCount).Replace(".", ",");
                            order.Disponibelt = Convert.ToDouble(order.Available);
                            order.Disponibelt = order.Disponibelt + order.SaldoNu;

                            //Om Saldo nu minus kundorder inom ledtid är minst 0 håller leveranstiden. Annars beräkna när leverans kan ske:
                            if (order.Disponibelt < 0)
                            {
                                // ************* Beräkna om saldo finns under hela ledtiden *************
                                saldo.Func = ""; // Åtgångar och tillgångar
                                saldo.Calculate();

                                order.Available = saldo.TabGetAvailable(saldo.IntervalCount).Replace(".", ",");
                                order.Disponibelt = Convert.ToDouble(order.Available);

                                //Disponibelt inom ledtid räcker inte för aktuell orderrad, ny leveranstid = ledtiden för artikeln
                                if (order.Disponibelt < 0)
                                {
                                    order.NyttLevDatum = order.DateLedtidArtikel;
                                    order.TextPurcase = Environment.NewLine + Environment.NewLine + "Preliminärt, nytt inköp krävs!";
                                }

                                //Beräkna datum när leverans kan ske inom ledtiden
                                else
                                {
                                    bool NegativeValueInLead = false;

                                    order.Available = saldo.TabGetAvailable(1).Replace(".", ",");
                                    double SenasteDisponibelt = Convert.ToDouble(order.Available);
                                    string NewList = string.Empty;

                                    //Loppa alla händelser om saldo är minst 0 kan leverans ske nu
                                    for (int DispTest1 = 1; DispTest1 < saldo.IntervalCount; DispTest1++)
                                    {
                                        order.Available = saldo.TabGetAvailable(DispTest1).Replace(".", ",");
                                        order.Disponibelt = Convert.ToDouble(order.Available);
                                        if (order.Disponibelt < 0)
                                        {
                                            NegativeValueInLead = true;
                                        }
                                    }

                                    //Om saldot är under 0 någon gång under ledtiden, testa när leverans kan ske (Påfyllnad efter sista negativa saldot)
                                    if (NegativeValueInLead == true)
                                    {
                                        for (int DispTest2 = saldo.IntervalCount; DispTest2 >= 1; DispTest2--) // Börja på sista händelsen inom ledtid och stega bakåt
                                        {
                                            order.Available = saldo.TabGetAvailable(DispTest2).Replace(".", ",");
                                            order.Disponibelt = Convert.ToDouble(order.Available);
                                            if (order.Disponibelt < 0)
                                            {
                                                //setting.FindInkOrder = true;
                                                order.TextPurcase = Environment.NewLine + Environment.NewLine + "Inköpsorder finns." + Environment.NewLine;
                                                break; // Senaste datum funnet när saldot är under 0
                                            }
                                            else
                                            // Flytta datumet till aktuell händelse
                                            {
                                                order.NyttLevDatum = saldo.TabGetDate(DispTest2);
                                                //order.DatePurcase = saldo.TabGetDate(DispTest2);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            order.UtanforLedtid = true;
                        }
                    }

                    // Om förslag på nytt datum ligger längre fram än aktuellt datum
                    if (string.Compare(order.NyttLevDatum, order.LevTidOR) > 0)
                    {
                        //Sätt ny leveranstid + 1 dag
                        order.NyttLevDatum = GarpButik.DateCalculator.AddDays(order.NyttLevDatum, 1, -1);
                        order.TextNewDelTime = order.TextNewDelTime + "Tillgängligt: " + order.NyttLevDatum + order.TextPurcase;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Testa en orderrad " + e, System.Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return order.NyttLevDatum;
        }

        // Stäng Garp-kopplingen
        public void Dispose()
        {
            try
            {
                Dispose(true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName + "Forms butik", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void Dispose(bool Disposing)
        {
            try
            {
                if (!m_disposed)
                {
                    if (Disposing)
                    {
                        m_disposed = true;
                        if (GarpButik != null)
                        {
                            dsOGR.BeforePost -= BeforePost;
                            dsOGR.AfterScroll -= AfterScrollOGR;

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblOGR);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAGA);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(tblAGT);

                            System.Runtime.InteropServices.Marshal.ReleaseComObject(dsOGR);
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(GarpButik);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName + "Forms butik", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        ~Butik()
        {
            Dispose(false);
        }
    }
}
