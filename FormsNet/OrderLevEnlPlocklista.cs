﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormsSkogma
{
    public partial class OrderLevEnlPlocklista : Form
    {

        public List<OrderRows> OrderRowList { get; set; }

        public OrderLevEnlPlocklista()
        {
            InitializeComponent();
            OrderRowList = new List<OrderRows>();
        }

        private void OrderLevEnlPlocklista_Load(object sender, EventArgs e)
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.DataSource = OrderRowList;
            dataGridView1.MaximumSize = new Size(this.dataGridView1.Width, 0);
            dataGridView1.AutoSize = true;
            dataGridView1.CellValueChanged += DataGridView1_CellValueChanged;
            dataGridView1.CellMouseUp += DataGridView1_CellMouseUp;
            //dataGridView1.DataBindingComplete += DataGridView1_DataBindingComplete;
            //dataGridView1.CellClick += DataGridView1_CellClick;

            DataGridViewCheckBoxColumn column1 = new DataGridViewCheckBoxColumn();
            column1.Name = "MarkForDeliver";
            column1.HeaderText = "Levereras";
            column1.DataPropertyName = "MarkForDeliver";
            column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns.Add(column1);

            DataGridViewTextBoxColumn column2 = new DataGridViewTextBoxColumn();
            column2.Name = "RowNo";
            column2.HeaderText = "Rad";
            column2.DataPropertyName = "RowNo";
            column2.ReadOnly = true;
            column2.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns.Add(column2);

            DataGridViewTextBoxColumn column3 = new DataGridViewTextBoxColumn();
            column3.Name = "ProductNo";
            column3.HeaderText = "Artikelnr";
            column3.DataPropertyName = "ProductNo";
            column3.ReadOnly = true;
            column3.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns.Add(column3);

            DataGridViewTextBoxColumn column4 = new DataGridViewTextBoxColumn();
            column4.Name = "Description";
            column4.HeaderText = "Benämning";
            column4.DataPropertyName = "Description";
            column4.ReadOnly = true;
            column4.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.Columns.Add(column4);

            DataGridViewTextBoxColumn column5 = new DataGridViewTextBoxColumn();
            column5.Name = "Amount";
            column5.HeaderText = "Antal";
            column5.DataPropertyName = "Amount";
            column5.ReadOnly = true;
            column5.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns.Add(column5);

            DataGridViewTextBoxColumn column6 = new DataGridViewTextBoxColumn();
            column6.Name = "AmountToDeliver";
            column6.HeaderText = "Lev. Antal";
            column6.DataPropertyName = "AmountToDeliver";
            column6.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns.Add(column6);

            ChangeColor();
        }

        //private void DataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        //{
        //    for (int i = 0; i < dataGridView1.Rows.Count; i++)
        //    {
        //        if (OrderRowList[i].DeliverState == "4")
        //        {
        //            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;
        //        }
        //    }
        //     //   throw new NotImplementedException();
        //}

        private void ChangeColor()
        {
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if (OrderRowList[i].DeliverState == "4")
                {
                    dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(185, 243, 133);
                }
            }
        }

        private void DataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == 0 && OrderRowList[e.RowIndex].PackageID != 0)
                {
                    dataGridView1.EndEdit();
                }
            }
            
        }

        private void DataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (OrderRowList[e.RowIndex].AmountToDeliver != "0" && e.ColumnIndex == 5) //(OrderRowList[e.RowIndex].Amount == OrderRowList[e.RowIndex].AmountToDeliver && OrderRowList[e.RowIndex].Amount != "0" && e.ColumnIndex == 5)
            {
                OrderRowList[e.RowIndex].MarkForDeliver = true;

                if (OrderRowList[e.RowIndex].PackageID != 0)
                {
                    List<OrderRows> pidList = OrderRowList.FindAll(p => p.PackageID == OrderRowList[e.RowIndex].PackageID);

                    foreach (OrderRows or in pidList)
                    {
                        or.MarkForDeliver = true;
                    }
                }
            }

            if (OrderRowList[e.RowIndex].AmountToDeliver == "0" && e.ColumnIndex == 5)
            {
                OrderRowList[e.RowIndex].MarkForDeliver = false;

                if (OrderRowList[e.RowIndex].PackageID != 0)
                {
                    List<OrderRows> pidList = OrderRowList.FindAll(p => p.PackageID == OrderRowList[e.RowIndex].PackageID);

                    foreach (OrderRows or in pidList)
                    {
                        or.MarkForDeliver = false;
                        or.AmountToDeliver = "0";
                    }
                }
            }

            if (e.ColumnIndex == 0 && OrderRowList[e.RowIndex].PackageID != 0)
            {
                List<OrderRows> pidList = OrderRowList.FindAll(p => p.PackageID == OrderRowList[e.RowIndex].PackageID);

                foreach (OrderRows or in pidList)
                {
                    if (OrderRowList[e.RowIndex].MarkForDeliver)
                    {
                        or.MarkForDeliver = true;
                        or.AmountToDeliver = or.Amount;
                    }
                    else
                    {
                        or.MarkForDeliver = false;
                    }
                }
            }

            dataGridView1.Refresh();
        }

        //private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
          
        //}

        private void button1_Click(object sender, EventArgs e) //btn1_deliver
        {
            foreach (OrderRows or in OrderRowList)
            {
                if (or.MarkForDeliver)
                {
                    or.MarkForDeliverByUser = true;
                }
            }
            Close();
        }

        private void btn2_close_Click(object sender, EventArgs e)
        {
            Close();
        }

        //private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}

    }
}
