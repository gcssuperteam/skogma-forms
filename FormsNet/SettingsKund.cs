﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace FormsSkogma
{
    public class SettingsKund
    {

        public string UserCanUppdate { get; set; }

        //Hämta kundlista från tabellregistret (kunder som ej får redigeras)
        public SettingsKund(Garp.Application GarpApp, List<Customer> Customers)
        {
            try
            {
                Garp.ITable Tabell;
                Garp.ITabField Key, CustomerNr;
                string Index = string.Empty;

                Tabell = GarpApp.Tables.Item("TA");
                Key = Tabell.Fields.Item("KEY");
                CustomerNr = Tabell.Fields.Item("TX1"); //KundNr sparas i Text 1
                Index = "9SPKUND"; //Tabell med kunder som ej får raderas 001 - 999

                Tabell.Find(Index);
                Tabell.Next();

                while (Tabell.Eof != true && Key.Value.Length >= 7 && Verktyg.Left(Key.Value, 7) == Index)
                {
                    Customers.Add(new Customer { CustNr = CustomerNr.Value });
                    Tabell.Next();
                }

                //Användare som får radera kund
                UserCanUppdate = ConfigurationManager.AppSettings["UserCanUpdateCustomer"];
            }
            catch (Exception e)
            {
                MessageBox.Show("Kan ej hämta kundlista från tabellregistret" + Environment.NewLine + Environment.NewLine
               + "Kontakta den som gjort anpassningen." + Environment.NewLine + Environment.NewLine + e
               , Application.ProductName + ", anpassning Forms "
               ,MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

    }
}
