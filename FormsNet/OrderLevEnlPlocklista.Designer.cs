﻿namespace FormsSkogma
{
    partial class OrderLevEnlPlocklista
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn1_deliver = new System.Windows.Forms.Button();
            this.btn2_close = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.orderRowsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderRowsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btn1_deliver
            // 
            this.btn1_deliver.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn1_deliver.Location = new System.Drawing.Point(181, 561);
            this.btn1_deliver.Name = "btn1_deliver";
            this.btn1_deliver.Size = new System.Drawing.Size(75, 23);
            this.btn1_deliver.TabIndex = 1;
            this.btn1_deliver.Text = "Leverera";
            this.btn1_deliver.UseVisualStyleBackColor = true;
            this.btn1_deliver.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn2_close
            // 
            this.btn2_close.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btn2_close.Location = new System.Drawing.Point(414, 561);
            this.btn2_close.Name = "btn2_close";
            this.btn2_close.Size = new System.Drawing.Size(75, 23);
            this.btn2_close.TabIndex = 2;
            this.btn2_close.Text = "Stäng";
            this.btn2_close.UseVisualStyleBackColor = true;
            this.btn2_close.Click += new System.EventHandler(this.btn2_close_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(653, 68);
            this.dataGridView1.TabIndex = 3;
            // 
            // orderRowsBindingSource
            // 
            this.orderRowsBindingSource.DataSource = typeof(FormsSkogma.OrderRows);
            // 
            // OrderLevEnlPlocklista
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(676, 596);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn2_close);
            this.Controls.Add(this.btn1_deliver);
            this.Name = "OrderLevEnlPlocklista";
            this.Text = "Leverera enl. plocklista";
            this.Load += new System.EventHandler(this.OrderLevEnlPlocklista_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderRowsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn1_deliver;
        private System.Windows.Forms.Button btn2_close;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource orderRowsBindingSource;
    }
}