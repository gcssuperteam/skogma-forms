﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormsSkogma
{
    public class OrderLikvArtikel
    {
        public string StrukturArtikel { get; set; }
        public bool LikvArtikelHasBeenRun { get; set; }
    }
}
